<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 31.03.15
 * Time: 14:43
 */

// Shopfunktionalität wird eingebunden:
require_once dirname(__FILE__) . "../../bootstrap.php";

// in der oxconfig in der Spalte oxvarname soll oxvarvalue geändert werden (Blob)
$sVarName = 'aDisabledModules';

$config = oxConfig::getInstance()->getConfigParam( $sVarName );

echo 'debug: '.$sVarName;
echo '<pre>';
print_r($config);
echo '</pre>';
