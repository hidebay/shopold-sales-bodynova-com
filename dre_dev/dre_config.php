<?php
/**
 *
 * Die Einstellungen in der Datenbank Tabelle:Oxconfig wird angepasst. Es sollen die vom Kunden
 * auszufüllenden Pflichtfelder geändert werden.
 *
 * Created by PhpStorm.
 * User: andre
 * Date: 26.03.15
 * Time: 11:26
 *
 */

// Utf-8 Darstellung in der Ausgabe
header("Content-Type: text/html; charset=utf-8");

// Shopfunktionalität wird eingebunden:
require_once dirname(__DIR__)."/" . "bootstrap.php";


// in der oxconfig in der Spalte oxvarname soll oxvarvalue geändert werden (Blob)
$sVarName = array(
	'aCacheViews',
	'aNrofCatArticles',
	'aNrofCatArticlesInGrid',
	'aInterfaceProfiles',
	'aLegacyModules',
	'aSearchCols',
	'aSEOReservedWords',
	'aSerials',
	'aSkipTags',
	'aSortCols',
	'blDisableDublArtOnCopy',
	'blDisableNavBars',
	'blDontShowEmptyCategories',
	'blEnableIntangibleProdAgreement',
	'blEnableSeoCache',
	'blExclNonMaterialFromDelivery',
	'blFacebookConfirmEnabled',
	'blFbCommentsEnabled',
	'blFbFacepileEnabled',
	'blFbInviteEnabled',
	'blFbLikeEnabled',
	'blFbShareEnabled',
	'blGBModerate',
	'blInlineImgEmail',
	'blInvitationsEnabled',
	'blLoadDynContents',
	'blLoadVariants',
	'blLogChangesInAdmin',
	'blLogging',
	'blNewArtByInsert',
	'blOrderDisWithoutReg',
	'blOrderOptInEmail',
	'blOtherCountryOrder',
	'blOverrideZeroABCPrices',
	'blPaymentVatOnTop',
	'blPayPalLoggerEnabled',
	'blPerfNoBasketSaving',
	'blPsBasketReservationEnabled',
	'blPsLoginEnabled',
	'blRDFaEmbedding',
	'blSearchUseAND',
	'blShippingCountryVat',
	'blShopStopped',
	'blShowBirthdayFields',
	'blShowCookiesNotification',
	'blShowListDisplayType',
	'blShowNetPrice',
	'blShowOrderButtonOnTop',
	'blShowRDFaProductStock',
	'blShowRememberMe',
	'blShowSorting',
	'blShowTags',
	'blShowTSCODMessage',
	'blShowTSInternationalFeesMessage',
	'blShowVariantReviews',
	'blShowVATForDelivery',
	'blShowVATForPayCharge',
	'blShowVATForWrapping',
	'blSkipDebitOldBankInfo',
	'blStockOffDefaultMessage',
	'blStockOnDefaultMessage',
	'blStoreCreditCardInfo',
	'blStoreIPs',
	'blUseMultidimensionVariants',
	'blUseStock',
	'blUseTimeCheck',
	'blVariantInheritAmountPrice',
	'blVariantParentBuyable',
	'blVariantsSelection',
	'blWarnOnSameArtNums',
	'blWnArShowArticleOwner',
	'blWrappingVatOnTop',
	'bl_perfCalcVatOnlyForBasketOrder',
	'bl_perfLoadAccessoires',
	'bl_perfLoadAktion',
	'bl_perfLoadAttributes',
	'bl_perfLoadCatTree',
	'bl_perfLoadCrossselling',
	'bl_perfLoadCurrency',
	'bl_perfLoadCustomerWhoBoughtThis',
	'bl_perfLoadDelivery',
	'bl_perfLoadDiscounts',
	'bl_perfLoadLanguages',
	'bl_perfLoadManufacturerTree',
	'bl_perfLoadNews',
	'bl_perfLoadNewsOnlyStart',
	'bl_perfLoadPrice',
	'bl_perfLoadPriceForAddList',
	'bl_perfLoadReviews',
	'bl_perfLoadSelectLists',
	'bl_perfLoadSelectListsInAList',
	'bl_perfLoadSimilar',
	'bl_perfLoadTreeForSearch',
	'bl_perfParseLongDescinSmarty',
	'bl_perfShowActionCatArticleCnt',
	'bl_perfUseSelectlistPrice',
	'bl_rssBargain',
	'bl_rssCategories',
	'bl_rssNewest',
	'bl_rssRecommListArts',
	'bl_rssRecommLists',
	'bl_rssSearch',
	'bl_rssTopShop',
	'bl_showCompareList',
	'bl_showFbConnect',
	'bl_showGiftWrapping',
	'bl_showListmania',
	'bl_showVouchers',
	'bl_showWishlist',
	'bOxProbsHeader',
	'bOxProbsProductActiveOnly',
	'bOxProbsProductPreview',
	'bOxProbsProductTimeActive',
	'bOxProbsQuote',
	'd3RemoteServerCache',
	'dDefaultVAT',
	'dPointsForInvitation',
	'dPointsForRegistration',
	'iAttributesPercent',
	'iCntofMails',
	'iD3ModCfgUpdateSkipEndTime',
	'iDefSeoLang',
	'iDownloadExpirationTime',
	'iExportNrofLines',
	'iExportTickerRefresh',
	'iImportNrofLines',
	'iImportTickerRefresh',
	'iLinkExpirationTime',
	'IMA',
	'iMallMode',
	'iMaxDownloadsCount',
	'iMaxDownloadsCountUnregistered',
	'iMaxGBEntriesPerDay',
	'IMD',
	'iMinOrderPrice',
	'IMS',
	'iNewBasketItemMessage',
	'iNewestArticlesMode',
	'iNrofCrossellArticles',
	'iNrofCustomerWhoArticles',
	'iNrofNewcomerArticles',
	'iNrofSimilarArticles',
	'iOlcSuccess',
	'iPsBasketReservationTimeout',
	'iRatingLogsTimeout',
	'iRDFaCondition',
	'iRDFaMaxRating',
	'iRDFaMinRating',
	'iRDFaOfferingValidity',
	'iRDFaPriceValidity',
	'iRDFaVAT',
	'iRssItemsCount',
	'iServerTimeShift',
	'iSessionTimeout',
	'iSmartyPhpHandling',
	'iTimeToUpdatePrices',
	'iTop5Mode',
	'iTopNaviCatCount',
	'iUseGDVersion',
	'sAdditionalServVATCalcMethod',
	'sCatIconsize',
	'sCatPromotionsize',
	'sCatThumbnailsize',
	'sCntOfNewsLoaded',
	'sCSVSign',
	'sCustomTheme',
	'sDecimalSeparator',
	'sDefaultImageQuality',
	'sDefaultLang',
	'sDefaultListDisplayType',
	'sDownloadsDir',
	'sFbAppId',
	'sFbSecretKey',
	'sGiCsvFieldEncloser',
	'sGZSLogFile',
	'sHost',
	'sIconsize',
	'sLargeCustPrice',
	'sLocalDateFormat',
	'sLocalTimeFormat',
	'sManufacturerIconsize',
	'sMerchantID',
	'sMidlleCustPrice',
	'sOEPayPalBorderColor',
	'sOEPayPalBrandName',
	'sOEPayPalCustomShopLogoImage',
	'sOEPayPalEmptyStockLevel',
	'sOEPayPalLogoImageOption',
	'sOEPayPalPassword',
	'sOEPayPalSandboxPassword',
	'sOEPayPalSandboxSignature',
	'sOEPayPalSandboxUserEmail',
	'sOEPayPalSandboxUsername',
	'sOEPayPalSignature',
	'sOEPayPalTransactionMode',
	'sOEPayPalUserEmail',
	'sOEPayPalUsername',
	'sOEThemeSwitcherMobileTheme',
	'sOxProbsBPriceMin',
	'sOxProbsEANField',
	'sOxProbsMaxActionTime',
	'sOxProbsMinDescLen',
	'sOxProbsOrderIgnoredRemark',
	'sOxProbsOrderPaidbyCIA',
	'sOxProbsOrderPaidbyInvoice',
	'sOxProbsOrderPaidLater',
	'sOxProbsPictureDirs',
	'sOxProbsSeparator',
	'sPaymentPwd',
	'sPaymentUser',
	'sRDFaBusinessEntityLoc',
	'sRDFaBusinessFnc',
	'sRDFaDeliveryChargeSpecLoc',
	'sRDFaDUNS',
	'sRDFaGLN',
	'sRDFaISIC',
	'sRDFaLatitude',
	'sRDFaLogoUrl',
	'sRDFaLongitude',
	'sRDFaNAICS',
	'sRDFaPaymentChargeSpecLoc',
	'sSEOSeparator',
	'sSEOuprefix',
	'sShopCountry',
	'sShopVar',
	'sStartPageListDisplayType',
	'sStockWarningLimit',
	'sTagList',
	'sTagSeparator',
	'sTheme',
	'sThumbnailsize',
	'sUtilModule',
	'sZoomImageSize',


);

$config = array();
foreach($sVarName as $varname){
	$config[$varname] = oxConfig::getInstance()->getConfigParam( $varname );
}

#$varname = 'aNrofCatArticles';
#$sValue = '250'; //$config['aNrofCatArticles'][0];

$sVarType = 'arr';
$sVarName = 'aNrofCatArticles';
$config['aNrofCatArticles'][0] = 1500;

$sVarVal = $config['aNrofCatArticles'];



$neueConfig = oxConfig::getInstance()->saveShopConfVar(  $sVarType, $sVarName, $sVarVal, $sShopId = 'oxbaseshop', $sModule = 'theme:bb2b' );



#$anders = oxConfig::getInstance()->getShopConfVar($sVarName, $sShopId = 'oxbaseshop', $sModule = 'theme:bb2b' );



echo 'debug: $config';
echo '<pre>';
print_r($anders);
print_r($config);
echo '</pre>';

/*
$confvar = oxConfig::getInstance()->getShopConfVar( $sVarName, $sShopId = null ); // gibt das gleiche Array aus wie $config.
echo 'debug: $confvar';
echo '<pre>';
print_r($confvar);
echo '</pre>';
*/

