<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 15.06.15
 * Time: 13:39
 */
/*
$fp = curl_init('http://haendlershop.local.dev/index.php?cl=account&sourcecl=start&redirected=1');

curl_setopt($fp,CURLOPT_TIMEOUT,10);

curl_setopt($fp,CURLOPT_FAILONERROR,1);

print_r(curl_setopt($fp,CURLOPT_FAILONERROR,1));

curl_setopt($fp,CURLOPT_RETURNTRANSFER,1);

print_r(curl_setopt($fp,CURLOPT_RETURNTRANSFER,1));

curl_exec($fp);

print_r(curl_exec($fp));

print_r($fp);
*/
/**
 * @param $str
 * @param $needle
 * @return bool
 */
function contains($str, $needle) {
    return (strpos($str, $needle) !== false);
}

/**
 * @param $url
 * @return bool
 */
function validateRemoteUrl($url) {
    $headers = get_headers($url);
    return (isset($headers) && count($headers) > 0 && contains($headers[0], "200"));
}

//Sales.bodynova.com
#$url= "https://sales.bodynova.com/index.php?cl=account&sourcecl=start&redirected=1";
#$url = "https://sales.bodynova.com";


// haendlershop.local.dev
#$url= "http://haendlershop.local.dev/index.php?cl=account&sourcecl=start&redirected=1";
$url = "http://haendlershop.local.dev";


echo '<strong>'.$url.'</strong><br>';

$test = validateRemoteUrl($url);

echo '<pre>';
print_r(get_headers($url));
echo '</pre>';


/*
 * Broken ???:
 *  Array
(
    [0] => HTTP/1.1 302 Found
    [1] => Date: Mon, 15 Jun 2015 12:22:11 GMT
    [2] => Server: Apache/2.2.22 (Ubuntu)
    [3] => X-Powered-By: PHP/5.4.37-1+deb.sury.org~precise+1
    [4] => Location: offline.html
    [5] => Connection: close
    [6] => Vary: Accept-Encoding
    [7] => Content-Length: 0
    [8] => Content-Type: text/html
    [9] => HTTP/1.1 200 OK
    [10] => Date: Mon, 15 Jun 2015 12:22:11 GMT
    [11] => Server: Apache/2.2.22 (Ubuntu)
    [12] => Last-Modified: Tue, 28 Oct 2014 12:41:17 GMT
    [13] => ETag: "323-3b-5067af69fd540"
    [14] => Accept-Ranges: bytes
    [15] => Content-Length: 59
    [16] => Vary: Accept-Encoding
    [17] => Connection: close
    [18] => Content-Type: text/html
)

Working: ???
Array
(
    [0] => HTTP/1.0 500 Internal Server Error
    [1] => Date: Mon, 15 Jun 2015 12:23:41 GMT
    [2] => Server: Apache/2.2.22 (Ubuntu)
    [3] => X-Powered-By: PHP/5.4.37-1+deb.sury.org~precise+1
    [4] => Vary: Accept-Encoding
    [5] => Content-Length: 0
    [6] => Connection: close
    [7] => Content-Type: text/html
)

Sales working :

Array
(
    [0] => HTTP/1.1 301 Moved Permanently
    [1] => Date: Mon, 15 Jun 2015 12:28:21 GMT
    [2] => Server: Apache/2.2.22 (Debian)
    [3] => Location: https://sales.bodynova.com/
    [4] => Vary: Accept-Encoding
    [5] => Content-Length: 319
    [6] => Connection: close
    [7] => Content-Type: text/html; charset=iso-8859-1
    [8] => HTTP/1.1 302 Found
    [9] => Date: Mon, 15 Jun 2015 12:28:21 GMT
    [10] => Server: Apache/2.2.22 (Debian)
    [11] => X-Powered-By: PHP/5.5.25-1~dotdeb+7.1
    [12] => Set-Cookie: language=0; path=/; httponly
    [13] => Expires: Thu, 19 Nov 1981 08:52:00 GMT
    [14] => Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0
    [15] => Pragma: no-cache
    [16] => Set-Cookie: sid=ug7ti1k7heb0ata7a9jrvbm1m0; path=/; httponly
    [17] => Set-Cookie: sid_key=oxid; path=/; httponly
    [18] => Location: https://sales.bodynova.com/index.php?force_sid=ug7ti1k7heb0ata7a9jrvbm1m0&cl=account&sourcecl=start&redirected=1
    [19] => Connection: close
    [20] => Vary: Accept-Encoding
    [21] => Content-Length: 0
    [22] => Content-Type: text/html
    [23] => HTTP/1.1 200 OK
    [24] => Date: Mon, 15 Jun 2015 12:28:21 GMT
    [25] => Server: Apache/2.2.22 (Debian)
    [26] => X-Powered-By: PHP/5.5.25-1~dotdeb+7.1
    [27] => Set-Cookie: language=0; path=/; httponly
    [28] => Set-Cookie: sid=ug7ti1k7heb0ata7a9jrvbm1m0; path=/; httponly
    [29] => Expires: Thu, 19 Nov 1981 08:52:00 GMT
    [30] => Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0
    [31] => Pragma: no-cache
    [32] => Set-Cookie: sid_key=oxid; path=/; httponly
    [33] => Vary: Accept-Encoding
    [34] => Connection: close
    [35] => Content-Type: text/html; charset=UTF- 8
)

 */
