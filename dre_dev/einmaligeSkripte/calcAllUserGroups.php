<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 28.01.15
 * Time: 14:07
 */

/**
 * Durch die bootstrap.php Datei werden alle Shopfunktionen geladen und können verwendet werden.
 */
require_once dirname(__FILE__) . "../../../bootstrap.php";

/**
 * Ein neues leeres User Objekt wird erzeugt.
 */
$oUser = new oxUser();

/**
 *  In das leere Objekt wird der User 'andre@bnder-andre.de' geladen über seine OXID aus OXUSER.
 */
$oUser->load('66c0096b4375ed7738a22c6bfb6704af');

/**
 *  Zur kontrolle wird der Username ausgegeben:
 */
echo 'Geladener User: '.$oUser->oxuser__oxusername.'<br>';

/**
 * Oxgroupid bestimmen:
 */
/*
$i = $oUser->oxuser__preisgruppe;
$groupid = 0;
switch ($i){
    case 1:
        $groupid = 'oxidpricea';
        break;
    case 2:
        $groupid = 'oxidpriceb';
        break;
    case 3:
        $groupid = 'oxidpricec';
        break;
    case 4:
        $groupid = 'oxidpriced';
        break;
    case 5:
        $groupid = 'oxidpricee';
        break;
}
*/
/**
 * Wenn der User bereits einer Händler Benutzergruppe zugeordnet ist ausgabe dieser.
 * Ansonsten fügt er den User dieser hinzu.
 * Zum Test nur mit User: test-a
 */
/*
if (!$oUser->inGroup($groupid)){
    echo 'User nicht in Gruppe: '. $groupid.'<br>';

    $oUser->addToGroup($groupid);
    if($oUser->inGroup($groupid)){
        echo 'hinzufuegen hat funktioniert!'.'<br>';
    }else{
        echo 'hinzufuegen hat nicht geklappt!'.'<br>';
    }

}else{
    echo 'User ist in Gruppe: '.$groupid.'<br>';

    if($oUser->oxuser__oxusername == 'test-a@bodynova.de'){
        echo 'User: '.$oUser->oxuser__oxusername.' wird aus der Gruppe '.$groupid.' geloescht. <br>';
        $oUser->removeFromGroup($groupid);
        if($oUser->inGroup($groupid)){
            echo 'loeschen hat nicht funktioniert!'.'<br>';
        }else{
            echo 'loeschen hat geklappt!'.'<br>';
        }
    }
}
*/
/**
 * Neues Datenbankobjekt erstellen
 */
$oDb = oxDb::getDb();
/**
 * Alle aktive User in als Array in $arUserlist übergeben
 */
$sql = "select * from oxuser where OXACTIVE = 1";
$arUserlist = $oDb->getAll($sql);
echo 'es gibt: '.$oDb->Affected_Rows().' aktive User <br>';


$oUserGroups = $oUser->getUserGroups("66c0096b4375ed7738a22c6bfb6704af");

$oUser->addToGroup("oxidpricea");
$oUser->save();

$i=0;
$oUserbearbeitung = null;
$ineu = 0;
foreach ($arUserlist as $id){

    echo '<hr>';
    echo 'Arraynr: '.$i.'<br>';
    echo 'Userid: '.$id[0].'<br>';
    echo 'Username: '.$id[4].'<br>';
    //echo 'Groupid: '.$groupid.'<br>';
    echo 'Preisgruppe: '.$id[39].'<br>';


    // Wenn der User eine ausgefüllte Preisgruppe hat
    if ($id[39]){
        $ineu = $id[39];
        echo "ineu: ".$ineu.'<br>';
        echo "BLA:";
        $echo = getPreisgruppe($ineu);
        echo $echo.'<br>';
        #$usergruppen = getUsergroups($oUser);
        #echo "Usergruppen: ".$usergruppen;
        #$oUserbearbeitungpr = $oUser->load($id[0]);
        if($oUser->inGroup($echo)){
            #print_r($echo);
            #echo "<hr><p>User ist in Gruppe: $echo</p><hr>";

        }else{
            $oUser->addToGroup($echo);
            $oUser->save();
            echo "<span>User erfolgreich hinzugefügt zu Gruppe: ".$echo.'</span>';
        }



    }





    // Wenn der User keine ausgefüllte Preisgruppe hat
    if (!$id[39]){
        echo 'keine Preisgruppe<br>';
        $oUserbearbeitung = $oUser->load($id[0]);
        $oUserGroups = $oUser->getUserGroups($id[0]);

        $ineu = $id[39];
        switch ($ineu){
            case 1:
                $groupid = 'oxidpricea';
                break;
            case 2:
                $groupid = 'oxidpriceb';
                break;
            case 3:
                $groupid = 'oxidpricec';
                break;
            case 4:
                $groupid = 'oxidpriced';
                break;
            case 5:
                $groupid = 'oxidpricee';
                break;
        }
        /*echo "A:<pre>";
        print_r($ineu);
        echo "</pre><hr>";*/
        if($id[2] == "malladmin" ){
            echo 'Malladmin!<br>';
            echo $id[2].'<br>';
            $oUser->addToGroup('oxidpricea');
            $oUser->save();
        }
    }


/*
    echo "Preisgruppe: ";
    echo '<pre>';
    print_r($oUser->getUserGroups($id[0]));
    echo '</pre>';
*/
    $i2 = 0;
    $i++;
}

/*
echo '<h3>Debugging:$oUserGroups</h3>';
echo '<pre>';
print_r($oUserGroups);
echo '</pre>';


echo '<h3>Debugging:$oUser</h3>';
echo '<pre>';
print_r($oUser);
echo '</pre>';

*/
#echo $arUserlist[0][0];

//$oUsertemp = new oxUser();
/*
$i=0;
foreach ($arUserlist as $id){
    echo '<hr>';
    echo 'Arraynr: '.$i.'<br>';
    echo $id[0].'<br> $i: '.$i.'<br>';
    $i2 = 0;
    if($id[39]){
        echo 'Haendlergruppe: '.$id[39].'<br>';
        $oUsertemp->load($id[0]);
        $ineu = $id[39];
        $groupid = 0;
        switch ($ineu){
            case 1:
                $groupid = 'oxidpricea';
                break;
            case 2:
                $groupid = 'oxidpriceb';
                break;
            case 3:
                $groupid = 'oxidpricec';
                break;
            case 4:
                $groupid = 'oxidpriced';
                break;
            case 5:
                $groupid = 'oxidpricee';
                break;
        }
        echo 'Groupid: '.$groupid.'<br>';
        if (!$oUsertemp->inGroup($groupid)){
            echo 'User nicht in Gruppe: '. $groupid.'<br>';

            $oUsertemp->addToGroup($groupid);
            if($oUsertemp->inGroup($groupid)){
                echo 'hinzufuegen hat funktioniert!'.'<br>';
            }else{
                echo 'hinzufuegen hat nicht geklappt!'.'<br>';
            }

        }else{

            echo 'User ist in Gruppe: '.$groupid.'<br>';
            echo 'test: '. $oUsertemp->inGroup($groupid);

            if($oUsertemp->oxuser__oxusername == 'test-a@bodynova.de'){
                echo 'User: '.$oUsertemp->oxuser__oxusername.' wird aus der Gruppe '.$groupid.' geloescht. <br>';
                $oUsertemp->removeFromGroup($groupid);
                if($oUsertemp->inGroup($groupid)){
                    echo 'loeschen hat nicht funktioniert!'.'<br>';
                }else{
                    echo 'loeschen hat geklappt!'.'<br>';
                }
            }
        }
    }
    $i++;
}




echo '<h3>Debugging:</h3>';
echo '<pre>';
print_r($arUserlist);
echo '</pre>';
*/

function getPreisgruppe($ipreisgruppe){
    $groupid = 0;
    switch ($ipreisgruppe){
        case 1:
            $groupid = 'oxidpricea';
            break;
        case 2:
            $groupid = 'oxidpriceb';
            break;
        case 3:
            $groupid = 'oxidpricec';
            break;
        case 4:
            $groupid = 'oxidpriced';
            break;
        case 5:
            $groupid = 'oxidpricee';
            break;
    }
    return $groupid;
}

function getUsergroups($oUser){

    if($oUser->inGroup('oxidpricea')){
        echo "User ist in Gruppe oxidpricea";
    }elseif($oUser->inGroup('oxidpricea')){
        echo "User ist in Gruppe oxidpriceb";
    }elseif($oUser->inGroup('oxidpricec')){
        echo "User ist in Gruppe oxidpricec";
    }elseif($oUser->inGroup('oxidpriced')){
        echo "User ist in Gruppe oxidpriced";
    }elseif($oUser->inGroup('oxidpricee')){
        echo "User ist in Gruppe oxidpricee";
    }


}
