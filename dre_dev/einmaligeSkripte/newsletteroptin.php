<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 03.06.15
 * Time: 11:25
 */
require_once dirname(__FILE__) . "../../../bootstrap.php";
/**
 * Neues Datenbankobjekt erstellen
 */
$oDb = oxDb::getDb();
/**
 * Alle aktive User in als Array in $arUserlist übergeben
 */
$sql = "select * from oxuser where OXACTIVE = 1";
$arUserlist = $oDb->getAll($sql);
echo 'es gibt: '.$oDb->Affected_Rows().' aktive User <br>';

$oUser = new oxUser();
$oUser->load($arUserlist[0][0]);
#$oUser->setNewsSubscription(true, true);

$oUser->getNewsSubscription();

/*Schleife über alle User! */


foreach ($arUserlist as $id) {
    $oUser = new oxUser();
    $oUser->load($id[0]);
    $oUser->setNewsSubscription(true, true);
}




$oNews = new oxNewsSubscribed();
$oNews->loadFromUserId($oUser->oxuser__oxid->value);

#$oNews->setOptInStatus(1);



$onewsDb = oxDb::getDb();
$newsSql = "select * from oxnewssubscribed";
$arnews = $onewsDb->getAll($newsSql);

echo 'es sind : '.$onewsDb->Affected_Rows().' User zugeordnet <br>';



/* TEST */
echo '<hr>';
echo 'OXID: '.$oUser->oxuser__oxid->value;
echo '<hr>';



/* DEBUG: */
echo '$oNews: <br>';
echo '<pre>';
print_r($oNews);
echo '</pre>';


echo '$oUser: <br>';
echo '<pre>';
print_r($oUser);
echo '</pre>';

echo '$arUserlist: <br>';
echo '<pre>';
print_r($arUserlist);
echo '</pre>';
