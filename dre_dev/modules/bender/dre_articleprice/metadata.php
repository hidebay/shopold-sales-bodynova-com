<?php
/**
 */

/**
 * Metadata version
 */
$sMetadataVersion = '0.1';

/**
 * Module information
 *	Dieses Modul erweitert den Head im Admin um den Cache zu leeren.
 */
$aModule = array(
    'id'           => 'dre_articleprice',
    'title'        => '<img src="../modules/bodynova/img/favicon.ico" title="Bender Clear Temp Modul">ody Article Price',
    'description'  => array(
        'de' => 'Modul erweitert die Klasse Oxarticle.',
        'en' => 'Modul see german.',
    ),
    'thumbnail'    => 'picture.png',
    'version'      => '0.1',
    'author'       => 'Andre Bender',
    'url'          => 'www.bodynova.de',
    'email'        => 'a.bender@bodynova.de',
    'extend'	   => array(
	    //'oxarticle'	    => 'bender/dre_articleprice/models/dre_oxarticle',
        //'oxbasket'	    => 'bender/dre_articleprice/models/dre_oxbasket',
        //'oxbasketitem'	=> 'bender/dre_articleprice/models/dre_oxbasketitem',
	),
);
