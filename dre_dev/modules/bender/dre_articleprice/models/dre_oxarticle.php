<?php

class dre_oxarticle extends dre_oxarticle_parent{


    protected function _getVarMinPrice()
    {
        // lade den suffix für den aktuellen user
        $sPriceSufix = $this->_getUserPriceSufix();
        // wenn kein _dVarMinPrice geladen wurde und der user keiner gruppe (abc preis) angehört laden wir
        // den niedrigsten preis der ersten aktiven variante
        if ( $this->_dVarMinPrice === null && $sPriceSufix === '' )
        {
            //
            $sSql = 'SELECT OXPRICE ';
            $sSql .=  ' FROM ' . $this->getViewName(true) . '
                        WHERE ' .$this->getSqlActiveSnippet(true) . '
                            AND ( `bodyshowprice` = ' . '1' . ')
                            AND ( `oxparentid` = ' . oxDb::getDb()->quote( $this->getId() ) . ' )';
            //
            $this->_dVarMinPrice = oxDb::getDb()->getOne( $sSql );
        } else {
            parent::_getVarMinPrice();
        }
        return $this->_dVarMinPrice;
    }



    // $this->_modifySelectListPrice( $dBasePrice, $aSelList );

    protected $blContainsVariantPrice = false;


    public function getPrice( $dAmount = 1 ) {

        $ret =  parent::getPrice( $dAmount );



        if(!$this->blContainsVariantPrice && isset($_POST['sel'])) {
            $ret->setPrice($this->_modifySelectListPrice( $ret->getPrice(), $_POST['sel']));
            $this->blContainsVariantPrice = true;
        }


        return $ret;


    }



}