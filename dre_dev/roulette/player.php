<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 25.01.15
 * Time: 22:00
 */

class player {

    protected $_name         = null;
    protected $_haben        = null;
    protected $_spiele       = null;
    protected $_strategie    = null;
    protected $_farbe        = null;
    protected $_zahl         = null;
    protected $_einsatz      = null;
    protected $_defeinsatz   = null;
    protected $_alt          = null;
    protected $_neu          = null;
    protected $_max          = null;
    protected $_min          = null;
    protected $_maxrnd       = null;
    protected $_minrnd       = null;
    protected $_split        = null;
    protected $_splitOn      = false;
    protected $_splitvalue   = null;
    protected $_splitcnt     = null;
    protected $_spliteinsatz = null;
    protected $_kredit       = null;

    function __construct($param){
        if (is_array($param)){
            if (isset($param['name'])){
                $this->_name = $param['name'];
            }
            if (isset($param['haben'])){
                $this->_haben = $param['haben'];
                $this->_max = $param['haben'];
                $this->_min = $param['haben'];

            }
            if (isset($param['spiele'])){
                $this->_spiele = $param['spiele'];
            }
            if (isset($param['strategie'])){
                $this->_strategie = $param['strategie'];
            }
            if (isset($param['einsatz'])){
                $this->_defeinsatz = $param['einsatz'];
            }
            if (isset($param['farbe'])){
                $this->_farbe = $param['farbe'];
            }
        }
    }

    public function returnarr(){
        $return = array(
            'haben'         => $this->_haben,
            'einsatz'       => $this->_einsatz,
            'defeinsatz'    => $this->_defeinsatz,
            'strategie'     => $this->_strategie,
            'splitOn'       => $this->_splitOn,
            'splitValue'    => $this->_splitvalue,
            'splitcnt'      => $this->_splitcnt,
            'spliteinsatz'  => $this->_spliteinsatz,

        );
        return $return;
    }
    public function spielt($param){
        if (is_array($param)){
            if (isset($param['farbe'])){
                $this->_farbe = $param['farbe'];
            }
            if (isset($param['zahl'])){
                $this->_zahl = $param['zahl'];
            }
            if (isset($param['einsatz'])){
                $this->_einsatz = $param['einsatz'];
                #$this->_haben = $this->_haben - $this->_einsatz;
            }
        }
    }

    public function gewinnt(){

        if($this->_haben > $this->_max){
            $this->_max = $this->_haben;
        }

        // Kredit
        if($this->_kredit > $this->_einsatz){
            if($this->_haben > 100){
                $kredrueck = $this->_haben - 100;

                if($kredrueck > $this->_kredit){
                    $addhaben = $kredrueck - $this->_kredit;
                    $this->_kredit = null;
                    $this->_haben = $this->_haben + $addhaben;
                }else{

                }
            }
            if($this->_haben > ($this->_kredit - $this->_einsatz)){
                $this->_kredit = $this->_kredit - $this->_einsatz;
            }

        } else {
            if($this->_kredit < 0){
                $add = $this->_kredit * -1;
                $this->_haben = $this->_haben + $add;
            }
            $this->_haben = $this->_haben + $this->_einsatz;
        }



        $this->_einsatz = $this->_defeinsatz;

        /*
        $return = array(
            'haben'         => $this->_haben,
            'einsatz'       => $this->_einsatz,
            'defeinsatz'    => $this->_defeinsatz,
            'strategie'     => $this->_strategie,
            'splitOn'       => $this->_splitOn,
            'splitValue'    => $this->_splitvalue,
            'splitcnt'      => $this->_splitcnt,
            'spliteinsatz'  => $this->_spliteinsatz,

        );*/

        return $this->returnarr();

    }

    public function verliert(){
        $einsatzalt = $this->_einsatz;

        if(isset($this->_strategie)){
            if($this->_strategie == 'martingale'){
                $this->_einsatz = $this->_einsatz * 2;

                if($this->_einsatz >= 32){
                    $this->_splitOn = true;
                }
            }
        }
        $this->_haben = $this->_haben - $einsatzalt;

        if($this->_haben < $this->_min){
            $this->_min = $this->_haben;
        }

        // Kredit:
        if($this->_einsatz > $this->_haben){
            $this->_kredit = $this->_kredit + ($this->_einsatz - $this->_haben);
        }
        /*
        if ($this->_haben <= 0){
            echo 'PLEITE!!!<br>';
            break;
        }
        */
        /*
        $return = array(
            'haben'         => $this->_haben,
            'einsatz'       => $this->_einsatz,
            'defeinsatz'    => $this->_defeinsatz,
            'strategie'     => $this->_strategie,
            'splitOn'       => $this->_splitOn,
            'splitValue'    => $this->_splitvalue,
            'splitcnt'      => $this->_splitcnt,
            'spliteinsatz'  => $this->_spliteinsatz,
        );
        */

        return $this->returnarr();
    }

    public function getPlayerName(){
        return $this->_name;
    }
    public function getPlayerHaben(){
        return $this->_haben;
    }



    public function getPlayerEinsatz(){
        return $this->_einsatz;
    }
    public function setPlayerEinsatz($param){
        if(is_int($param)){
            $this->_einsatz = $param;
            return true;
        }else{
            echo 'Es gelten nur Ganzzahlige Einsaetze!';
            return false;
        }
    }
    public function getPlayerFarbe(){
        return $this->_farbe;
    }
    public function getPlayerZahl(){
        return $this->_zahl;
    }

    public function getAlt(){
        return $this->_alt;
    }
    public function getNeu(){
        return $this->_neu;
    }
    public function getMax(){
        return $this->_max;
    }
    public function getMin(){
        return $this->_min;
    }
    public function getMaxRnd(){
        return $this->_maxrnd;
    }
    public function getMinRnd(){
        return $this->_minrnd;
    }
    public function getSplit(){
        return $this->_split;
    }
    public function getKredit(){
        return $this->_kredit;
    }
}