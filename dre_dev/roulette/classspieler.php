<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 25.01.15
 * Time: 19:36
 */

class spieler {
    /**
     * @var null
     * Spielername
     */
    protected  $_name        = null;
    protected  $_geldboerse  = null;
    protected  $_einsatz     = null;
    protected  $_gewinn      = null;
    protected  $_verlust     = null;
    protected  $_setztauf    = null;
    protected  $_maximal     = null;
    protected  $_maxrnd      = null;
    protected  $_minimal     = null;
    protected  $_minrnd      = null;

    /**
     * @return $_name
     */
    public function getname(){
        return $this->_name;
    }

    /**
     * @param $name
     * Set Spielername
     */
    public function setname($name){
        $this->_name = $name;
    }

    public function getgeldboerse(){
        return $this->_geldboerse;
    }
    public function setgeldboerse($wert){
        $this->_geldboerse = $wert;
    }
    public function geteinsatz(){
        return $this->_einsatz;
    }
    public function seteinsatz($betrag){
        $this->_einsatz = $betrag;
        $this->_geldboerse = $this->_geldboerse - $betrag;
    }
    public function getsetztauf(){
        return $this->_setztauf;
    }
    public function setsetztauf($farbe){
        $this->_setztauf = $farbe;
    }
    public function gewinnt(){
        $this->_gewinn = $this->_einsatz*2;
        $this->_geldboerse = $this->_geldboerse + $this->_gewinn;
        $this->_gewinn = null;
    }
    public function verliert(){
        $this->_verlust = $this->_einsatz;
        $this->_geldboerse = $this->_geldboerse - $this->_verlust;
        $this->_verlust = null;
    }
    public function getmaximal(){
        return $this->_maximal;
    }
    public function setmaximal($wert, $rnd){
        $this->_maximal = $wert;
        $this->_maxrnd  = $rnd;
    }
    public function getmaxrnd(){
        return $this->_maxrnd;
    }
    public function getminimal(){
        return $this->_minimal;
    }
    public function setminimal($wert, $rnd){
        $this->_minimal = $wert;
        $this->_minrnd  = $rnd;
    }
    public function getminrnd(){
        return $this->_minrnd;
    }
}