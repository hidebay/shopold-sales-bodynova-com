<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 25.01.15
 * Time: 19:35
 */

class roulette {

    protected $_maxrot  = null;
    protected $_maxschw = null;
    protected $_maxeinsatz = null;
    protected $_nummer = null;

    public function getRandNumber(){

        $number = rand (0, 30);
        $farbe = $this->getFarbe($number);
        if($farbe == 'rot'){
            $this->_maxrot = $this->_maxrot + 1;
        } else {
            $this->_maxschw = $this->_maxschw + 1;
        }
        return $number;
    }

    public function getFarbe($zahl){

        if ($zahl % 2 != 0) {
            $farbe = 'schwarz';
        } else {
            $farbe = 'rot';
        }

        return $farbe;
    }

    public function zeichneTabelle(array $daten) {
        echo '<table border="1">';

        foreach($daten as $key=>$val) {

            echo '<tr><td>'.$key.'</td><td>'.$val.'</td></tr>';
        }

        echo '</table>';

    }
    public function getmaxrot(){
        return $this->_maxrot;
    }
    public function getmaxschw(){
        return $this->_maxschw;
    }
    public function getmaxeinsatz(){
        return $this->_maxeinsatz;
    }
    public function setmaxeinsatz($wert){
        $this->_maxeinsatz = $wert;
    }

}