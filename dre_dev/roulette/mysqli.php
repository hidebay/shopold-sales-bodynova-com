<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 20.01.15
 * Time: 23:33
 */

/**
 * Reihenfolgen des Construktors:
 */

/* $dbhost
 * $dbuser
 * $dbpass
 * $db
 * $dbport
 * $dbsocket
 */

$dbhost     = '192.168.40.44';
$dbuser     = 'root';
$dbpass     = 'Qq7Urdhi';
$db         = 'roulette';
$dbport     = '3306';
$dbsocket   = null;

/**
 * Verbindung zur DB herstellen
 */

/*
 * mittels Construktor:
 */

$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $db);

/*
 *  mittels einer Methode:
 *
 * Ein leeres Objekt erstellen und mittels der Methode connect die Verbindung herstellen:
 *  $mysqli = new mysqli();
 *  $mysqli->connect($dbhost, $dbuser, $dbpass, $db);
 */

/**
 * Verbindungsfehler abfangen:
 */

if(!$mysqli)
{
    die('Es war keine Verbindung zur Datenbank möglich.');
}

/**
 * User wechseln:
 */

/*
$dbuserneu  = 'neuerUser';
$dbpassneu  = 'neuespasswort';
$dbneu      = 'neueDatenbank'; // oder 0 dann wird nur der User gewechselt.

$userchange = $mysqli->change_user($dbuserneu, $dbpassneu, $dbneu);
if ($userchange == 1) {

    echo 'User Wechsel hat geklappt';

} elseif ($userchange == 0) {

    echo 'Leider gab es einen Fehler!';
}
*/

/**
 *  Db wechseln:
 */

/*
$mysqli->select_db($dbneu);
*/

/**
 * Informationen über die Verbindung erhalten:
 */

print '<hr>';
print '<h3>Verbindungsinformationen</h3>';
print '<hr>';

print 'Zeichensatz: ';
print $mysqli->character_set_name().'<br />';   // Zeichensatz

print 'Adresse und Protokoll: ';
print $mysqli->host_info.'<br />'; // Adresse und Protokoll

print 'Client-Version: ';
print $mysqli->client_info.'<br />'; // Versionsnummer

print 'Server-Version: ';
print $mysqli->server_info.'<br />'; // MySQL-Server-Version

print '<hr>';

/**
 * Abfrage auf Serverversion:
 */

if($mysqli->server_info{0} >= 5) {
    //MySQL 5: setze Neuerungen ein, also Trigger etc.
    print 'Serverversion >= 5';
}
else {
    //MySQL 4 oder niedriger: nutze den Workaround
    print 'Serverversion < 4';
}

/**
 * Query:
 */

/*
 * aufbauen und der Variablen $rawsql zuweisen.
 */

$rawsql = 'SHOW TABLES';

/*
 * Sollten sich Sonderzeichen in der Query befinden müssen diese erst noch escaped werden.
 */

$sql = $mysqli->real_escape_string($rawsql);

/*
 * Nun wird das escaped query an die Datenbank gesendet und in der Variablen $result gespeichert.
 *
 * Der optionale Parameter $ergebnismodus bestimmt ob das query benutzt wird oder dessen Ergebnis gespeichert wird.
 * Die Konstanten:
 * MYSQLI_USE_RESULT   = 1
 * MYSQLI_STORE_RESULT = 0
 * default ist 0
 *
 * Während use result das Ergebnis sukzessive abholt speichet store dieses im ganzen und ermöglicht data_seek()
 */

$ergebnismodus = 0;
$result = $mysqli->query($sql, $ergebnismodus);

/*
 * Verarbeitung der Ergebnismenge:
 */


/*
 * Eingerückte Ausgabe einer Variablen oder Anweisung.
 */

echo '<hr>';
echo '<pre>';
print_r($result);
echo '</pre>';

/*
 * Verbindung zur DB wieder schließen:
 */

$mysqli->close();