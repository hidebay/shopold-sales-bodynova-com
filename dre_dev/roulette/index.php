<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 25.01.15
 * Time: 21:59
 */

include('config.inc.php');
include('player.php');
include('tisch.php');
include('debug.php');

/**
 * Initial config:
 */
/*
// Player Details:
$aruser = array(
    'name'      =>  'Andre',
    'haben'     =>  100,
    'spiele'    =>  20,
    'strategie' =>  'martingale',
    'farbe'     =>  'rot',
);
// Player first play:
$arspielt = array(
    'farbe'     => 'schwarz',
    'zahl'      => '2',
    'einsatz'   => '2',
);
*/


/**
 * initialisierung der Objekte
 */
$player = new player($aruser);
$tisch  = new tisch();
$debug  = new debug();

/**
 * Ausgaben Head:
 */
// Player:

echo '<strong> Player: </strong><br>';
echo 'Name: '       .$player->getPlayerName()       .'<br>';
echo 'Guthaben: '   .$player->getPlayerHaben()      .'<br>';
echo 'Einsatz: '    .$player->getPlayerEinsatz()    .'<br>';

$arspielt['farbe']      = 'schwarz';
$arspielt['zahl']       = 2;
$arspielt['einsatz']    = 2;
$player->spielt($arspielt);


/*
echo '<hr>';

echo '<strong>Spieler setzt: </strong><br>';
echo 'Farbe: '      .$player->getPlayerFarbe()      .'<br>';
echo 'Einsatz: '    .$player->getPlayerEinsatz()    .'<br>';
echo 'Zahl: '       .$player->getPlayerZahl()       .'<br>';


echo '<hr>';

// Tisch:
$tisch->play();
echo '<strong> Tisch: </strong><br>';
echo 'Zahl gekommen: ' .$tisch->getNummer()    .'<br>';
echo 'Farbe gekommen: '.$tisch->getFarbe()     .'<br>';
echo '<br>';

echo '<hr>';
*/

// Ergebnisse:
#$ergebnis = $tisch->playerEinsatz($arspielt);
#$player->gespielt($ergebnis);

#echo '<strong>Spieler: </strong><br>';
#echo 'Einsatz: '    .$player->getPlayerEinsatz()    .'<br>';
#echo 'Guthaben: '   .$player->getPlayerHaben()      .'<br>';
#echo 'Einsatz: '    .$player->getPlayerEinsatz()    .'<br>';

#echo '<hr>';
$ergebnis = $tisch->playerEinsatz($arspielt);
$anzahl = 1000;

$gewonnen = null;
$verloren = null;
for($i=0; $i<=$anzahl; $i++) {

    $tisch->play();
    echo '<strong> Tisch: '.$i.'</strong><br>';
    if($tisch->getNummer() == $player->getPlayerZahl() ){
        echo "<span style='background-color: #008000;'> Zahl gekommen: ".$tisch->getNummer().' Player Zahl: '.$player->getPlayerZahl().'</span><br>';
    }else{
        echo 'Zahl gekommen: ' .$tisch->getNummer()    .' Player Zahl: '.$player->getPlayerZahl()   .'<br>';
    }
    if($tisch->getFarbe() == $player->getPlayerFarbe()){
        echo "<span style='background-color: #008000;'>Farbe gekommen: ".$tisch->getFarbe().' Player Farbe: '.$player->getPlayerFarbe() .'</span><br>';
    }else{
        echo 'Farbe gekommen: '.$tisch->getFarbe()     .' Player Farbe: '.$player->getPlayerFarbe() .'<br>';
    }

    echo '<br>';
    $ergebnis = $tisch->playerEinsatz($arspielt);
    echo '<hr>';

    echo '<strong>Spieler vorher: </strong><br>';

    echo 'Guthaben: ' . $player->getPlayerHaben() . '<br>';
    echo 'Einsatz: ' . $player->getPlayerEinsatz() . '<br>';
    if($player->getKredit() > 0){
        echo '<hr><hr>';
        echo 'Kredit: '.$player->getKredit().'<br>';
    }


    #echo '<strong>'.$ergebnis['status'].'</strong><br>';

    if ($ergebnis['status'] == 'gewinnt') {
        $player->gewinnt();
        $gewonnen++;
        echo "<span style='background-color: #008000;'>gewonnen!!!</span><br>";
    } elseif ($ergebnis['status'] == 'verloren') {
        $player->verliert();
        $verloren++;
        echo "<span style='background-color: red;'>verloren!!! </span><br>";
    }
    echo '<strong>Spieler nachher: </strong><br>';
    echo 'Neues Guthaben: ' . $player->getPlayerHaben() . '<br>';
    echo 'Neuer Einsatz: ' . $player->getPlayerEinsatz() . '<br>';
    if($player->getKredit() > 0){
        echo '<hr><hr>';
        echo 'Neuer Kredit: '.$player->getKredit().'<br>';
    }
    echo '<hr><hr>';
}

echo 'Gewonnen: '.$gewonnen.' Maximal: '.$player->getMax().'<br>';
echo 'Verloren: '.$verloren.' Minimal: '.$player->getMin().'<br>';

$debug->dies($player);
/*
echo '<strong>Schleife: </strong><br>';
$anzahl = 1000;
for($i=0; $i<=$anzahl; $i++){
    echo $i.'<br>';
    echo '<strong>Spieler: </strong><br>';
    echo 'Guthaben: '   .$player->getPlayerHaben()      .'<br>';
    echo 'Einsatz: '    .$player->getPlayerEinsatz()    .'<br>';
    echo 'spielt...';
    $tisch->play();
    $ergebnis = $tisch->playerEinsatz($arspielt);
    $ergebnis['runde'] = $i;
    $player->gespielt($ergebnis);
    echo '  Status: '     .$ergebnis['status']          .'<br>';
    echo 'Alt: '.$player->getAlt().' <br>';
    echo 'Neu: '.$player->getNeu().' <br>';

    echo '<strong>Ergebnis: </strong><br>';
    echo 'Guthaben: '   .$player->getPlayerHaben()      .'<br>';
    echo 'Einsatz: '    .$player->getPlayerEinsatz()    .'<br>';
    #echo '<hr>';
    #$debug->dies($tisch);
    #$debug->dies($ergebnis);
    #$debug->dies($player);
    #echo '<hr>';
    if ($i == $anzahl){
        $rnd = $i;
    }
    if($player->getPlayerHaben() < 2){
        echo 'Pleite!!! in Runde '.$i.'<br>';
        break;
    }
    echo '<hr>';
    echo '<hr>';
}

*/
echo '<strong>Statistics:</strong><br>';
echo 'Runden gespielt: '.$rnd.'<br>';
echo 'Guthaben: '   .$player->getPlayerHaben()      .'<br>';
echo 'Max. Guthaben: '   .$player->getMax().' in Runde: '.$player->getMaxRnd().'<br>';
echo 'Min. Guthaben: '   .$player->getMin().' in Runde: '.$player->getMinRnd().'<br>';


/**
 * Funktionsaufrufe
 */







echo '<hr>';
echo '<strong>DEBUG: </strong>';
/**
 * Debugging:
 */
// Debugging:
// array von Objekten:
$ardebug = array(
    $player,
    $tisch,
    $ergebnis,
);

/**
 * ein einzelnes Objekt ausgeben:
 */
#$debug->dies($ergebnis);
/**
 * alle Objekte ausgeben:
 */
#$debug->all($ardebug);