<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 13.10.16
 * Time: 22:59
 */

/*
require_once dirname(__DIR__).'/bootstrap.php';

$jahresUmsatzNachMonaten = "SELECT 
round(sum(OXTOTALORDERSUM),2) as Summe, 
MONTH(OXORDERDATE) as Monat,
count(OXORDERNR) as Anzahl
FROM oxorder
where 
OXORDERDATE <= 'Current_Date()' AND
OXORDERDATE >= MAKEDATE(YEAR(curdate()),01)
GROUP BY MONTH(OXORDERDATE)";

$arUmsatzJahr = oxDb::getDb()->getAll($jahresUmsatzNachMonaten);
$arbearbeitet = array();
$id = 0;
foreach ($arUmsatzJahr as $Jahr){
	$arbearbeitet[$id]['Umsatz']   = $Jahr[0];
	$arbearbeitet[$id]['Monat']    = $Jahr[1];
	$arbearbeitet[$id]['Anzahl']   = $Jahr[2];
	$id++;
}

echo '<table><th><tr><td>Umsatz</td><td>Monat</td><td>Anzahl</td><td>von</td></tr></th>';
foreach ($arUmsatzJahr as $Jahr){
	echo '<tr>';
	echo '<td>'.$Jahr[0].'</td>';
	echo '<td>'.$Jahr[1].'</td>';
	echo '<td>'.$Jahr[2].'</td>';
	echo '<td>'.$Jahr[3].'</td>';
	echo'</tr>';
}
echo '</table>';

echo '<pre>';
print_r($arbearbeitet);
*/

// connect to bodynova.de
/*
if($_SERVER['HTTP_HOST'] == 'bodynova.local.dev'){
	$host = 'localhost';
	$user = 'root';         // root;
	$pass = 'peanuts30';     // '';
	$db   = 'bodynova';   // bodynova;
}else{
	$host = 'bodynova.de';
	$user = 'www02651';         // root;
	$pass = 'hEgVO75tANZ7';     // '';
	$db   = 'usrdb_www02651';   // bodynova;
	$port = 3307;
}
*/
$host = 'bodynova.de';
$user = 'www02651';         // root;
$pass = 'hEgVO75tANZ7';     // '';
$db   = 'usrdb_www02651';   // bodynova;
$port = 3307;

$mysqli = new mysqli($host, $user, $pass, $db,$port);
if ($mysqli->connect_errno) {
	printf("Connect failed: %s\n", $mysqli->connect_error);
	exit();
}
// Umsätze des aktuellen Jahres holen und in Array speichern
if ($result = $mysqli->query("SELECT 
	round(sum(OXTOTALORDERSUM),2) as Summe, 
	MONTH(OXORDERDATE) as Monat,
	count(OXORDERNR) as Anzahl
	FROM oxorder
	where 
	OXORDERDATE <= 'Current_Date()' AND
	OXORDERDATE >= MAKEDATE(YEAR(curdate()),01)
	GROUP BY MONTH(OXORDERDATE)")) {

	#printf("Select returned %d rows.\n", $result->num_rows);
	
	while ($row = $result->fetch_assoc()){
	$perform['bodynova']['aktuell'][] = $row;
	}
}
// Umsätze des vorjahres holen und in Array speichern
if ($result = $mysqli->query("SELECT 
	round(sum(OXTOTALORDERSUM),2) as Summe, 
	MONTH(OXORDERDATE) as Monat,
	count(OXORDERNR) as Anzahl
	FROM oxorder
	where 
	OXORDERDATE <= DATE_SUB(curdate(), INTERVAL 1 YEAR) AND
	OXORDERDATE >= MAKEDATE(YEAR(DATE_SUB(curdate(), INTERVAL 1 YEAR)),01)
	GROUP BY MONTH(OXORDERDATE)")) {
	
	#printf("Select returned %d rows.\n", $result->num_rows);
	
	while ($row = $result->fetch_assoc()){
		$perform['bodynova']['vorjahr'][] = $row;
	}
}

// connect to sales.bodynova.com
/*
if($_SERVER['HTTP_HOST'] == 'bodynova.local.dev'){
	$host = 'localhost';
	$user = 'root';         // root;
	$pass = 'peanuts30';     // '';
	$db   = 'bodynova';   // bodynova;
}else{
	$host = 'sales.bodynova.com';
	$user = 'filemaker';         // root;
	$pass = 'EF7-r8e-JHu-TCX';     // '';
	$db   = 'livedb';   // bodynova;
	$port = 3306;
}*/

$host = 'sales.bodynova.com';
$user = 'filemaker';         // root;
$pass = 'EF7-r8e-JHu-TCX';     // '';
$db   = 'livedb';   // bodynova;
$port = 3306;

$mysqli = new mysqli($host, $user, $pass, $db,$port);
if ($mysqli->connect_errno) {
	printf("Connect failed: %s\n", $mysqli->connect_error);
	exit();
}
if ($result = $mysqli->query("SELECT 
	round(sum(OXTOTALORDERSUM),2) as Summe, 
	MONTH(OXORDERDATE) as Monat,
	count(OXORDERNR) as Anzahl
	FROM oxorder
	where 
	OXORDERDATE <= 'Current_Date()' AND
	OXORDERDATE >= MAKEDATE(YEAR(curdate()),01)
	GROUP BY MONTH(OXORDERDATE)")) {
	
	#printf("Select returned %d rows.\n", $result->num_rows);
	
	while ($row = $result->fetch_assoc()){
		$perform['sales']['aktuell'][] = $row;
	}
}
if ($result = $mysqli->query("SELECT 
	round(sum(OXTOTALORDERSUM),2) as Summe, 
	MONTH(OXORDERDATE) as Monat,
	count(OXORDERNR) as Anzahl
	FROM oxorder
	where 
	OXORDERDATE <= DATE_SUB(curdate(), INTERVAL 1 YEAR) AND
	OXORDERDATE >= MAKEDATE(YEAR(DATE_SUB(curdate(), INTERVAL 1 YEAR)),01)
	GROUP BY MONTH(OXORDERDATE)")) {
	
	#printf("Select returned %d rows.\n", $result->num_rows);
	
	while ($row = $result->fetch_assoc()){
		$perform['sales']['vorjahr'][] = $row;
	}
}
#echo '<pre>';
#print_r($perform);
header('Content-Type: application/json;charset=utf-8');
echo json_encode($perform);

