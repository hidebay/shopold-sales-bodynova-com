<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 29.10.15
 * Time: 12:28
 */
// Utf-8 Darstellung in der Ausgabe
header('Content-Type: text/html; charset=utf-8');
error_reporting(E_ALL);
ini_set('display_errors' , false);
ini_set('html_errors'    , true);
require '../../bootstrap.php';
require 'array2xml.php';

echo '<h1>Alter Weg! Nicht mehr in Funktion!</h1>';
die();

if($_SERVER['HTTP_HOST'] === 'haendlershop.local.dev')
{
    $host = 'localhost';
    $user = 'root';         // root;
    $pass = 'peanuts30';     // '';
    $db   = 'bodynova';   // bodynova;
}else{
    $host = 'localhost';
    $user = 'liveuser';         // root;
    $pass = 'Uguka8Obir75';     // '';
    $db   = 'livedb';   // bodynova;
}
$_sPaymentKey = 'fq45QS09_fqyx09239QQ';
$sPaymentKey = oxRegistry::getUtils()->strRot13( $_sPaymentKey );

if(isset($_GET['order']) || isset($_GET['nurorder'])){
	if(isset($_GET['order'])){
		$OrderNr = $_GET['order'];
		$arOrderSelect  = "select * from oxorder where oxordernr > '".$OrderNr."' order by oxordernr desc";
	}
	if(isset($_GET['nurorder'])){
		$OrderNr = $_GET['nurorder'];
		$arOrderSelect  = "select * from oxorder where oxordernr = '".$OrderNr."' order by oxordernr desc";
	}
}else{
	echo'<h1>Bitte die letzte importierte Ordernummer angeben!!! ODER die genaue Ordernr</h1>';
	echo'<h3>Adresszeile: getorder.php?order=81480 ODER: getorder.php?nurorder=81480</h3>';
	die;
}
echo '<pre>';

// Alle Orders abrufen, größer als $OrderNr:
#$arOrderSelect  = "select * from oxorder where oxordernr > '".$OrderNr."'";
$arOrderIds     = oxDb::getDb()->getArray($arOrderSelect);
$arAlleOrders = array();

// Abbruch, wenn keine neuen Orders verfügbar sind.
if(count($arOrderIds) == 0){
	echo '<h1>Keine neuen Orders da.<br>Bitte später erneut versuchen.</h1>';
	die;
}

echo 'Es sind noch: '.count($arOrderIds).' Orders zu importieren.<br>';
$debug = 0;
$arOrderArray = oxnew('oxorder');
$iar=0;
$anzahlOrders = count($arOrderIds);
#print_r($anzahlOrders);
for($i=0; $anzahlOrders > $i; $i++){
    $iar++;
	if($debug == 1){
		echo'<br>';
		echo $iar.' OXID: '.$arOrderIds[$i][0].'<br>';
	}
    $arOrderArray->load($arOrderIds[$i][0]);

    $arAlleOrders[] = array('ORDER' => array(
    'OXID'                  =>$arOrderArray->oxorder__oxid->value,
    'OXSHOPID'              =>'oxbaseshop',
    'OXUSERID'              =>$arOrderArray->oxorder__oxuserid->value,
    'OXCUSTNR'              =>'',
    'OXORDERDATE'           =>date_format(date_create($arOrderArray->oxorder__oxorderdate->value), 'Y-m-d H:i:s'),
    'OXORDERNR'             =>$arOrderArray->oxorder__oxordernr->value,
    'OXBILLCOMPANY'         =>$arOrderArray->oxorder__oxbillcompany->value,
    'OXBILLEMAIL'           =>$arOrderArray->oxorder__oxbillemail->value,
    'OXBILLFNAME'           =>$arOrderArray->oxorder__oxbillfname->value,
    'OXBILLLNAME'           =>$arOrderArray->oxorder__oxbilllname->value,
    'OXBILLSTREET'          =>$arOrderArray->oxorder__oxbillstreet->value,
    'OXBILLSTREETNR'        =>$arOrderArray->oxorder__oxbillstreetnr->value,
    'OXBILLADDINFO'         =>$arOrderArray->oxorder__oxbilladdinfo->value,
    'OXBILLUSTID'           =>$arOrderArray->oxorder__oxbillustid->value,
    'OXBILLCITY'            =>$arOrderArray->oxorder__oxbillcity->value,
    'OXBILLCOUNTRYID'       =>$arOrderArray->oxorder__oxbillcountryid->value,
    'OXBILLCOUNTRYTITLE'    =>'',
    'OXBILLISOALPHA2'       =>'',
    'OXBILLISOALPHA3'       =>'',
    'OXBILLSTATEID'         =>$arOrderArray->oxorder__oxbillstateid->value,
    'OXBILLZIP'             =>$arOrderArray->oxorder__oxbillzip->value,
    'OXBILLFON'             =>$arOrderArray->oxorder__oxbillfon->value,
    'OXBILLFAX'             =>$arOrderArray->oxorder__oxbillfax->value,
    'OXBILLSAL'             =>$arOrderArray->oxorder__oxbillsal->value,
    'OXDELCOMPANY'          =>$arOrderArray->oxorder__oxdelcompany->value,
    'OXDELFNAME'            =>$arOrderArray->oxorder__oxdelfname->value,
    'OXDELLNAME'            =>$arOrderArray->oxorder__oxdellname->value,
    'OXDELSTREET'           =>$arOrderArray->oxorder__oxdelstreet->value,
    'OXDELSTREETNR'         =>$arOrderArray->oxorder__oxdelstreetnr->value,
    'OXDELADDINFO'          =>$arOrderArray->oxorder__oxdeladdinfo->value,
    'OXDELCITY'             =>$arOrderArray->oxorder__oxdelcity->value,
    'OXDELCOUNTRYID'        =>$arOrderArray->oxorder__oxdelcountryid->value,
    'OXDELCOUNTRYTITLE'     =>'',
    'OXDELISOALPHA2'        =>'',
    'OXDELISOALPHA3'        =>'',
    'OXDELSTATEID'          =>$arOrderArray->oxorder__oxdelstateid->value,
    'OXDELZIP'              =>$arOrderArray->oxorder__oxdelzip->value,
    'OXDELFON'              =>$arOrderArray->oxorder__oxdelfon->value,
    'OXDELFAX'              =>$arOrderArray->oxorder__oxdelfax->value,
    'OXDELSAL'              =>$arOrderArray->oxorder__oxdelsal->value,
    'OXPAYMENTID'           =>$arOrderArray->oxorder__oxpaymentid->value,
    'OXPAYMENTTYPE'         =>$arOrderArray->oxorder__oxpaymenttype->value,
    'OXTOTALNETSUM'         =>str_replace('.',',',round($arOrderArray->oxorder__oxtotalnetsum->value, 2)),
    'OXTOTALBRUTSUM'        =>str_replace('.',',',round($arOrderArray->oxorder__oxtotalbrutsum->value, 2)),
    'OXTOTALORDERSUM'       =>str_replace('.',',',round($arOrderArray->oxorder__oxtotalordersum->value, 2)),
    'OXARTVAT1'             =>str_replace('.',',',round($arOrderArray->oxorder__oxartvat1->value, 2)),
    'OXARTVATPRICE1'        =>str_replace('.',',',round($arOrderArray->oxorder__oxartvatprice1->value, 2)),
    'OXARTVAT2'             =>str_replace('.',',',round($arOrderArray->oxorder__oxartvat2->value, 2)),
    'OXARTVATPRICE2'        =>str_replace('.',',',round($arOrderArray->oxorder__oxartvatprice2->value, 2)),
    'OXDELCOST'             =>str_replace('.',',',round($arOrderArray->oxorder__oxdelcost->value, 2)),
    'OXDELVAT'              =>str_replace('.',',',round($arOrderArray->oxorder__oxdelvat->value, 2)),
    'OXPAYCOST'             =>str_replace('.',',',round($arOrderArray->oxorder__oxpaycost->value, 2)),
    'OXPAYVAT'              =>str_replace('.',',',round($arOrderArray->oxorder__oxpayvat->value, 2)),
    /*
    'OXWRAPCOST'            =>$arOrderArray->oxorder__oxwrapcost->value,
    'OXWRAPVAT'             =>$arOrderArray->oxorder__oxwrapvat->value,
    'OXGIFTCARDCOST'        =>$arOrderArray->oxorder__oxgiftcardcost->value,
    'OXGIFTCARDVAT'         =>$arOrderArray->oxorder__oxgiftcardvat->value,
    'OXCARDID'              =>$arOrderArray->oxorder__oxcardid->value,
    'OXCARDTEXT'            =>$arOrderArray->oxorder__oxcardtext->value,
    */
    'OXDISCOUNT'            =>$arOrderArray->oxorder__oxdiscount->value,
    /*
    'OXEXPORT'              =>$arOrderArray->oxorder__oxexport->value,
    'OXBILLNR'              =>$arOrderArray->oxorder__oxbillnr->value,
    'OXBILLDATE'            =>$arOrderArray->oxorder__oxbilldate->value, //date_format(date_create($arOrderArray->oxorder__oxbilldate->value), 'Y-m-d H:i:s'), //$arOrderArray->oxorder__oxbilldate->value,
    */
    'OXTRACKCODE'           =>$arOrderArray->oxorder__oxtrackcode->value,
    'OXSENDDATE'            =>$arOrderArray->oxorder__oxsenddate->value, //date_format(date_create($arOrderArray->oxorder__oxsenddate->value), 'Y-m-d H:i:s'), //$arOrderArray->oxorder__oxsenddate->value,
    'OXREMARK'              =>$arOrderArray->oxorder__oxremark->value,
    'OXVOUCHERDISCOUNT'     =>str_replace('.',',',round($arOrderArray->oxorder__oxvoucherdiscount->value, 2)),
    /*
    'OXCURRENCY'            =>$arOrderArray->oxorder__oxcurrency->value,
    'OXCURRATE'             =>$arOrderArray->oxorder__oxcurrate->value,
    */
    'OXFOLDER'              =>$arOrderArray->oxorder__oxfolder->value,
    'OXTRANSID'             =>$arOrderArray->oxorder__oxtransid->value,
    'OXPAYID'               =>$arOrderArray->oxorder__oxpayid->value,
    'OXXID'                 =>$arOrderArray->oxorder__oxxid->value,
    'OXPAID'                =>$arOrderArray->oxorder__oxpaid->value,
    'OXSTORNO'              =>$arOrderArray->oxorder__oxstorno->value,
    //'OXIP'                  =>$arOrderArray->oxorder__oxip->value,
    'OXTRANSSTATUS'         =>$arOrderArray->oxorder__oxtransstatus->value,
    'OXLANG'                =>$arOrderArray->oxorder__oxlang->value,
    /*
    'OXINVOICENR'           =>$arOrderArray->oxorder__oxinvoicenr->value,
    'OXDELTYPE'             =>$arOrderArray->oxorder__oxdeltype->value,
    'OXTSPROTECTID'         =>$arOrderArray->oxorder__oxtsprotectid->value,
    'OXTSPROTECTCOSTS'      =>$arOrderArray->oxorder__oxtsprotectcosts->value,
    */
    'OXTIMESTAMP'           =>date_format(date_create($arOrderArray->oxorder__oxtimestamp->value), 'Y-m-d H:i:s'), //$arOrderArray->oxorder__oxtimestamp->value,
    'OXISNETTOMODE'         =>$arOrderArray->oxorder__oxisnettomode->value,
    /*'oxefiorderchecksum'    =>$arOrderArray->oxorder__oxefiorderchecksum->value,
    'oxefiaddchecksum'      =>$arOrderArray->oxorder__oxefiaddchecksum->value,
    'oxefiorderartschecksum'=>$arOrderArray->oxorder__oxefiorderartschecksum->value,*/
    'flagversand'           =>$arOrderArray->oxorder__flagversand->value,
    'Konstante_Lieferschein'=>'Angebot',
    'bankdatenexport'       =>$arOrderArray->oxorder__bankdatenexport->value,
	'AuthCode'              =>'',
	'lsbankname'            =>'',
	'lsblz'                 =>'',
	'lsktonr'               =>'',
	'lsktoinhaber'          =>'',
	'oxdateused'            =>'',
	'oxreserved'            =>'',
	'oxvouchernr'           =>'',
	'oxvoucherserieid'      =>'',
	'oxvouchervoucherdiscount'            =>'',
	'oxvoucherid'                  =>'',
	'oxvouchertimestamp'           =>'',
	'oxvoucherserie'        =>'',
	'oxvoucherseriedescription'=>'',
	'oxvoucherseriediscount'=>'',
	'oxvoucherseriediscounttyp'=>'',
	'oxshop'                =>'haendler',
	'iswhitelabel'          =>$arOrderArray->oxorder__iswhitelabel->value,
    ));
}
$AnzahlOrders = count($arAlleOrders);
if ($debug == 1){
	echo '<h1>Order Array</h1>';
	echo 'Anzahl gefundener Orders: '.$AnzahlOrders.'<pre>';
}

$arArticles = array();
$Voucher = array();
for($i=0; $AnzahlOrders > $i; $i++){
	if($debug == 1){
		echo '<hr>';
		echo 'Nummer: '.$i . '  '.$arAlleOrders[$i]['ORDER']['OXID'].'<br>';
	}


	if(isset($arAlleOrders[$i]['ORDER']['OXUSERID'])){
		$UserSelect = "select oxcustnr from oxuser where OXID = '".$arAlleOrders[$i]['ORDER']['OXUSERID']."'";
		$Usernr = oxDb::getDb()->getOne($UserSelect);
		#echo '<h1>USERNR = '.$Usernr.'</h1>';
		$arAlleOrders[$i]['ORDER']['OXCUSTNR'] = $Usernr;
	}

    if(isset($arAlleOrders[$i]['ORDER']['OXBILLCOUNTRYTITLE'])){
        $Billcountryselect = "select oxid from oxcountry where OXID = '".$arAlleOrders[$i]['ORDER']['OXBILLCOUNTRYID']."'";
        $Billcountryid     = oxDb::getDb()->getOne($Billcountryselect);
        $oBillCountry      = oxNew("oxcountry");
        $oBillCountry ->load($Billcountryid);

        $arAlleOrders[$i]['ORDER']['OXBILLCOUNTRYTITLE']     = $oBillCountry->oxcountry__oxtitle->value;
        $arAlleOrders[$i]['ORDER']['OXBILLISOALPHA2']        = $oBillCountry->oxcountry__oxisoalpha2->value;
        $arAlleOrders[$i]['ORDER']['OXBILLISOALPHA3']        = $oBillCountry->oxcountry__oxisoalpha3->value;
    }


    if(isset($arAlleOrders[$i]['ORDER']['OXDELCOUNTRYTITLE'])){
        $Delcountryselect = "select oxid from oxcountry where OXID = '".$arAlleOrders[$i]['ORDER']['OXDELCOUNTRYID']."'";
        $Delcountryid     = oxDb::getDb()->getOne($Delcountryselect);
        $oDelCountry      = oxNew("oxcountry");
        $oDelCountry->load($Delcountryid);

        $arAlleOrders[$i]['ORDER']['OXDELCOUNTRYTITLE']  = $oDelCountry->oxcountry__oxtitle->value;
        $arAlleOrders[$i]['ORDER']['OXDELISOALPHA2']     = $oDelCountry->oxcountry__oxisoalpha2->value;
        $arAlleOrders[$i]['ORDER']['OXDELISOALPHA3']     = $oDelCountry->oxcountry__oxisoalpha3->value;
    }

	/*
    // OXUSERPAYMENTS
    // SEPA:
    if($arAlleOrders[$i]['ORDER']['OXPAYMENTTYPE'] === 'oxiddebitnote'){
        $oPayment = oxNew("oxuserpayment");
        $oPayment->load($arAlleOrders[$i]['ORDER']['OXPAYMENTID']);
        $dyn = $oPayment->getDynValues();
    }


    // az_ipayment nicht im Händlershop
    $ipaymentselect = "select AuthCode from az_ipayment_logs where OXORDERID = '".$arAlleOrders[$i]['ORDER']['OXID']."'";
    $ipaymentAuthCode = oxDb::getDb()->getOne($ipaymentselect);

    // Kreditkarte
    if($arAlleOrders[$i]['ORDER']['OXPAYMENTTYPE'] === 'oxidcreditcard'){
            $arAlleOrders[$i]['ORDER']['AuthCode'] = $ipaymentAuthCode;
    }else{
            $arAlleOrders[$i]['ORDER']['AuthCode'] = '';
    }

    // Lastschrift
    if($arAlleOrders[$i]['ORDER']['OXPAYMENTTYPE'] === 'oxiddebitnote'){

        foreach($dyn as $key){
            $arAlleOrders[$i]['ORDER'][$key->name] = $key->value;
        }
    }else{
        $arAlleOrders[$i]['ORDER']['lsbankname']   = '';
        $arAlleOrders[$i]['ORDER']['lsblz']        = '';
        $arAlleOrders[$i]['ORDER']['lsktonr']      = '';
        $arAlleOrders[$i]['ORDER']['lsktoinhaber'] = '';
    }


	#echo '<h1>Voucher:</h1>';
	$Voucherselect      = "select * from oxvouchers where OXORDERID = '".$arAlleOrders[$i]['ORDER']['OXID']."'";
	$Voucher          = oxDb::getDb()->getArray($Voucherselect);
	if(isset($Voucher[0])){
		$arAlleOrders[$i]['ORDER']['oxdateused']        = $Voucher[0][0];
		$arAlleOrders[$i]['ORDER']['oxreserved']        = $Voucher[0][3];
		$arAlleOrders[$i]['ORDER']['oxvouchernr']       = $Voucher[0][4];
		$arAlleOrders[$i]['ORDER']['oxvoucherserieid']  = $Voucher[0][5];
		$arAlleOrders[$i]['ORDER']['oxdiscount']        = str_replace('.',',',round($Voucher[0][6], 2));
		$arAlleOrders[$i]['ORDER']['oxid']              = $Voucher[0][7];
		$arAlleOrders[$i]['ORDER']['oxtimestamp']       = $Voucher[0][8];
		$Voucherserieselect = "select * from oxvoucherseries where OXID ='".$Voucher[0][5]."'";
		$Voucherserie = oxDb::getDb()->getArray($Voucherserieselect);

		$arAlleOrders[$i]['ORDER']['oxvoucherserie']                  = $Voucherserie[0][2];
		$arAlleOrders[$i]['ORDER']['oxvoucherseriedescription']       = $Voucherserie[0][3];
		$arAlleOrders[$i]['ORDER']['oxvoucherseriediscount']          = $Voucherserie[0][4];
		$arAlleOrders[$i]['ORDER']['oxvoucherseriediscounttyp']       = $Voucherserie[0][5];
	}else{
		$arAlleOrders[$i]['ORDER']['oxdateused']        = '';
		$arAlleOrders[$i]['ORDER']['oxreserved']        = '';
		$arAlleOrders[$i]['ORDER']['oxvouchernr']       = '';
		$arAlleOrders[$i]['ORDER']['oxvoucherserieid']  = '';
		$arAlleOrders[$i]['ORDER']['oxdiscount']        = '';
		$arAlleOrders[$i]['ORDER']['oxid']              = '';
		$arAlleOrders[$i]['ORDER']['oxtimestamp']       = '';
		$arAlleOrders[$i]['ORDER']['oxvoucherserie']                  = '';
		$arAlleOrders[$i]['ORDER']['oxvoucherseriedescription']       = '';
		$arAlleOrders[$i]['ORDER']['oxvoucherseriediscount']          = '';
		$arAlleOrders[$i]['ORDER']['oxvoucherseriediscounttyp']       = '';
	}

	#echo $arAlleOrders[$i]['ORDER']['oxdiscount'].'<br>';
	#echo $arAlleOrders[$i]['ORDER']['oxvoucherserie'].'<br>';
	#echo $arAlleOrders[$i]['ORDER']['oxvoucherseriedescription'].'<br>';
	#echo $arAlleOrders[$i]['ORDER']['oxvoucherseriediscount'].'<br>';
	#echo $arAlleOrders[$i]['ORDER']['oxvoucherseriediscounttyp'].'<br>';
	*/
}
if($debug == 1){
	print_r($arAlleOrders).'<br>';
}

$anz = 0;
$id1 = 0;
$zähler = 0;
$orderarticlesSelect = array();
$arOrderIds = array();
if($debug == 1){
	echo '<h2>OXORDER Articels</h2>';
}

// OXORDERARTICLES
$oOrderArticle = null;
$oOrderArticle =  oxNew('oxorderarticle');
foreach($arAlleOrders as $kei){
    $zähler++;
    $orderarticlesSelect = "select * from oxorderarticles where OXORDERID = '".$kei['ORDER']['OXID']."'";
    $arOrderIds[] =  oxDb::getDb()->getArray($orderarticlesSelect);
}

#$mal = oxDb::getDb()->getArray($orderarticlesSelect);
#print_r($mal);

#echo '$arOrderIds: <br>';

#print_r(array_keys($arOrderIds));
#print_r(array_values($arOrderIds));

#echo 'MAL:'.print_r($mal).'<br>';
#echo '<br>';
#print_r($arOrderIds[0][0]);
#echo 'ZAHL: '.$zahl = count($arOrderIds);
$alleOrderarticles= array();
$n = 0;
$test=array();

#print_r($arAlleOrders[0]['ORDER']['OXID']);
$ornr = 0;
foreach ($arOrderIds as  $artikel['ORDER'] => $value){

    #print_r($value[0]);
	if(is_array($value)) {
		#print_r($value);
		// unterste Ebene :
		#print_r($arOrderIds[$n][0]);

		if (isset($value[0][0])) {
			#$ornr++;
			if (count($value) > 1) {
				if($debug == 1){
					echo '<hr> Bei DS ' . $n . '  mehrfaches Value: ' . count($value) . '<br><br>';
				}
				#echo 'ORDERNR: '.$test[$n] = $value[0][0];
				for ($g = 0; count($value) > $g; $g++) {
					if($debug == 1){
						echo " Artikelnummer: $n";
						echo ' OXORDER: ' . $value[ $g ][1] . ' OXARTICLES: ' . $value[ $g ][0]. '<br>';
					}
					$test[ $n ] = $value[ $g ];
					$n++;
				}

			} else {
				if($debug == 1){
					echo "<hr> Artikelnummer: $n ";
					echo 'OXORDER: ' . $value[0][1] . ' OXARTICLES: ' . $test[ $n ] = $value[0][0] . '<br>';
				}

				$test[ $n ] = $value[0];
				$n++;
				#$ornr++;
			}
		}
        #echo 'VALUE: '.$bla;
        #$test[$n] = $value[0][0];
        #print_r($value);
	}
    #$alleOrderarticles[$n] = $artikel['ORDER'];
}
#echo '$test: '.count($test).'<br>';
#print_r($test);
for($i=0; count($test) > $i; $i++){
	#echo 'Artikelnr: '. $i.'<br>';
	$alleOrderarticles[$i] = $arOrderIds[$i]['ORDER'][0];
}
#print_r($alleOrderarticles);

$orderids = $oOrderArticle->load($arOrderIds);
#print_r($arOrderIds);
$orderzöhler = 0;
foreach($test as $key){
    $arArticles[$orderzöhler]['ORDER'] = array(
        'OXID'              => $key[0],
        'OXORDERID'         => $key[1],
        'OXAMOUNT'          => $key[2],
        'OXARTID'           => $key[3],
        'OXARTNUM'          => $key[4],
        'OXTITLE'           => $key[5],
        'OXSHORTDESC'       => $key[6],
        'OXSELVARIANT'      => $key[7],
        'OXNETPRICE'        => str_replace('.',',',round($key[8], 2)),
        'OXBRUTPRICE'       => str_replace('.',',',round($key[9], 2)),
        'OXVATPRICE'        => str_replace('.',',',round($key[10], 2)),
        'OXVAT'             => str_replace('.',',',round($key[11],2)),
        /*'OXPERSPARAM'       => $key[12],*/
        'OXPRICE'           => str_replace('.',',',round($key[13], 2)),
        'OXBPRICE'          => str_replace('.',',',round($key[14], 2)),
        'OXNPRICE'          => str_replace('.',',',round($key[15], 2)),
        'OXWRAPID'          => $key[16],
        'OXEXTURL'          => $key[17],
        'OXURLDESC'         => $key[18],
        'OXURLIMG'          => $key[19],
        'OXTHUMB'           => $key[20],
        'OXPIC1'            => $key[21],
        'OXPIC2'            => $key[22],
        'OXPIC3'            => $key[23],
        'OXPIC4'            => $key[24],
        'OXPIC5'            => $key[25],
        'OXWEIGHT'          => round($key[26], 2),
        'OXSTOCK'           => $key[27],
        'OXDELIVERY'        => $key[28],
        'OXINSERT'          => $key[29],
        'OXTIMESTAMP'       => $key[30],
        'OXLENGTH'          => $key[31],
        'OXWIDTH'           => round($key[32], 2),
        'OXHEIGHT'          => round($key[33], 2),
        'OXFILE'            => $key[34],
        'OXSEARCHKEYS'      => $key[35],
        'OXTEMPLATE'        => $key[36],
        'OXQUESTIONEMAIL'   => $key[37],
        'OXISSEARCH'        => $key[38],
        'OXFOLDER'          => $key[39],
        'OXSUBCLASS'        => $key[40],
        'OXSTORNO'          => $key[41],
        'OXORDERSHOPID'     => $key[42],
        'OXISBUNDLE'        => $key[43],
        'D3_GALOCATOR'      => $key[44],
	    'OXSHOP'            => 'haendler',
    );
    $orderzöhler++;
}
echo 'Anzahl Orderarticles: '.count($arArticles).'<br>';
	#print_r($arArticles);
	// OXORDERARTICALS

try {
	$xml = new array2xml('ROW');
	$anzahlAlleOrders = count($arArticles);
	#echo $anzahlAlleOrders;
	$null = null;
	$id1  = 0;
	//$null = null;


	foreach ($arArticles as $keyarray) {
		$xml->createOrderartNode($keyarray, $null, $anzahlAlleOrders, $id1);
		$id1++;
	}

	#echo $xml;
} catch (Exception $e) {
	echo $e->getMessage();
}

$fp = fopen( 'Orderdaten/'.$OrderNr.'_orderarticles.xml', 'w');
fwrite($fp, $xml);

fclose($fp);
chmod('Orderdaten/'.$OrderNr.'_orderarticles.xml', 0777);
// Orderxml

try
{

    $null = null;
    $id2 = 0;
    $xml = new array2xml('ROW');
    $anzahlAlleOrders = count($arAlleOrders);
    foreach($arAlleOrders as $keyarray){

        $xml->createNode( $keyarray, $null, $anzahlAlleOrders ,$id2);
        $id2++;
    }
}catch (Exception $e)
{
    echo $e->getMessage();
}

$fp = fopen( 'Orderdaten/'.$OrderNr.'.xml', 'w');
fwrite($fp, $xml);

fclose($fp);
chmod('Orderdaten/'.$OrderNr.'.xml', 0777);
