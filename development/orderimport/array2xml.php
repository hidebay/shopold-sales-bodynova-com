<?php
/**
 *
 * Array 2 XML class
 * Convert an array or multi-dimentional array to XML
 *
 * @author Kevin Waterson
 * @copyright 2009 PHPRO.ORG
 *
 */
class array2xml extends DomDocument
{

    public $nodeName;

    private $xpath;

    private $root;

    private $node_name;


    /**
     * Constructor, duh
     *
     * Set up the DOM environment
     *
     * @param    string    $root        The name of the root node
     * @param    string    $nod_name    The name numeric keys are called
     *
     */
    public function __construct($root='ROW', $node_name = 'COL')
    {
        parent::__construct();

        /*** set the encoding ***/
        $this->encoding = "utf-8"; //"ISO-8859-1";

        /*** format the output ***/
        $this->formatOutput = true;

        /*** set the node names ***/
        $this->node_name = $node_name;

        /*** create the root element ***/
        $this->root = $this->appendChild($this->createElement( $root ));

        $this->xpath = new DomXPath($this);
    }

    /*
    * creates the XML representation of the array
    *
    * @access    public
    * @param     array     $arr     The array to convert
    * @aparam    string    $node    The name given to child nodes when recursing
    *
    */
    public function createNode( $arr, $node = null, $attribute = null, $id)
    {
        if (is_null($node))
        {
            $node = $this->root;
        }

        foreach($arr as $element => $value)
        {

            $element = is_numeric( $element ) ? $this->node_name : $element;

            $child = $this->createElement($element, (is_array($value) ? null : $value));

            if($child->tagName == 'OXID'){

                $id++;
                #echo $attribute;
                $at = $this->createAttribute('ID');
                $at->value = $id;
                $node->appendChild($at);
                #print_r($node);

            }
            $node->appendChild($child);
            if (is_array($value))
            {
                if(is_array($value)){
                    #$attribute--;
                    $attribute--;
                    #echo '$attribute: '.$attribute.'<br>';
                    #print_r($value);
	                self::createNode($value, $child , $attribute, $id);
                    $id++;
                }

            }
        }
    }

    public function createOrderartNode( $arr, $node = null, $attribute = null , $id)
    {
        $thisid = $id;
        if (is_null($node))
        {
            $node = $this->root;
        }

        foreach($arr as $element => $value)
        {

            $element = is_numeric( $element ) ? $this->node_name : $element;


            $child = $this->createElement($element, (is_array($value) ? null : $value));

            #print_r($child); //Order $this->node_name = COL

            if($child->tagName == 'OXID'){
                $thisid++;
                #echo $attribute;
                $at = $this->createAttribute('ID');
                $at->value = $thisid;
                #echo 'ID = '.$thisid.'<br>';
                $node->appendChild($at);
                #print_r($node);

            }
            $node->appendChild($child);
            if (is_array($value))
            {

                #echo '<pre>$Value: <br>';
                #print_r($value);

                if(is_array($value)){
                    #echo 'ORDER!!!<br>';

                    $attribute--;
                    #echo '$attribute: '.$attribute.'<br>';
                    #print_r($value);

	                #echo 'OrderArticle geschrieben';
                    self::createOrderartNode($value, $child , $attribute, $thisid);
                    $thisid++;
                }
            }
        }
    }
    /*
    * Return the generated XML as a string
    *
    * @access    public
    * @return    string
    *
    */
    public function __toString()
    {
        return $this->saveXML();
    }

    /*
    * array2xml::query() - perform an XPath query on the XML representation of the array
    * @param str $query - query to perform
    * @return mixed
    */
    public function query($query)
    {
        return $this->xpath->evaluate($query);
    }

} // end of class
