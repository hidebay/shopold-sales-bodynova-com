<?xml version="1.0" encoding="utf-8" ?>

<!-- The one-fits-all XSLT for FileMaker.

     Transforms arbitrary XML grammars and namespaces
     into FileMaker records. Every node or attribute one
     record.

     Usage: Import your XML data with grammar FMPXMLRESULT using this file as
     XSLT stylesheet into a new FM table.

     Jens Teich v-0.4 12.08.2013
-->


<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.filemaker.com/fmpxmlresult">

    <xsl:template match="/">
        <FMPXMLRESULT>

            <ERRORCODE>0</ERRORCODE>
            <PRODUCT BUILD="" NAME="" VERSION=""/>
            <DATABASE DATEFORMAT="M/d/yyyy" LAYOUT="" NAME="" RECORDS="1" TIMEFORMAT="h:mm:ss a"/>

            <METADATA>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXID" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXSHOPID" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXUSERID" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXORDERDATE" TYPE="TIMESTAMP"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXORDERNR" TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXBILLCOMPANY" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXBILLEMAIL" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXBILLFNAME" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXBILLLNAME" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXBILLSTREET" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXBILLSTREETNR" TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXBILLADDINFO" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXBILLUSTID" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXBILLCITY" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXBILLCOUNTRYID" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXBILLCOUNTRYTITLE" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXBILLISOALPHA2" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXBILLISOALPHA3" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXBILLSTATEID" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXBILLZIP" TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXBILLFON" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXBILLFAX" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXBILLSAL" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXDELCOMPANY" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXDELFNAME" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXDELLNAME" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXDELSTREET" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXDELSTREETNR" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXDELADDINFO" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXDELCITY" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXDELCOUNTRYID" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXDELCOUNTRYTITEL" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXDELISOALPHA2" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXDELISOALPHA3" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXDELSTATEID" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXDELZIP" TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXDELFON" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXDELFAX" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXDELSAL" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXPAYMENTID" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXPAYMENTTYPE" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXTOTALNETSUM" TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXTOTALBRUTSUM" TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXTOTALORDERSUM" TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXARTVAT1" TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXARTVATPRICE1" TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXARTVAT2" TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXARTVATPRICE2" TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXDELCOST" TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXDELVAT" TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXPAYCOST" TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXPAYVAT" TYPE="NUMBER"/>
                <!--
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXWRAPCOST" TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXWRAPVAT" TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXGIFTCARDCOST" TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXGIFTCARDVAT" TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXCARDID" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXCARDTEXT" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXDISCOUNT" TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXEXPORT" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXBILLNR" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXBILLDATE" TYPE="TEXT"/>
                -->
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXTRACKCODE" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXSENDDATE" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXREMARK" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXVOUCHERDISCOUNT" TYPE="NUMBER"/>
                <!--
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXCURRENCY" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXCURRATE" TYPE="NUMBER"/>
                -->
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXFOLDER" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXTRANSID" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXPAYID" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXXID" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXPAID" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXSTORNO" TYPE="TEXT"/>
                <!--
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXIP" TYPE="TEXT"/>
                -->
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXTRANSSTATUS" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXLANG" TYPE="NUMBER"/>
                <!--
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXINVOICENR" TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXDELTYPE" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXTSPROTECTID" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXTSPROTECTCOSTS" TYPE="NUMBER"/>
                -->
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXTIMESTAMP" TYPE="TEXT"/>
                <!--
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXISNETTOMODE" TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="oxefiorderchecksum" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="oxefiaddchecksum" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="oxefiorderartschecksum" TYPE="TEXT"/>
                -->
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="flagversand" TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="Konstante_Lieferschein" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="bankdatenexport" TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="oxshop" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="iswhitelabel" TYPE="TEXT"/>

                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="AuthCode" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="lsbankname" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="lsblz" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="lsktonr" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="lsktoinhaber" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="oxdateused" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="oxreserved" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="oxvouchernr" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="oxvoucherserieid" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="oxvouchervoucherdiscount" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="oxvoucherid" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="oxvouchertimestamp" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="oxvoucherserie" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="oxvoucherseriedescription" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="oxvoucherseriediscount" TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="oxvoucherseriediscounttyp" TYPE="TEXT"/>

            </METADATA>

            <RESULTSET FOUND="">
                <xsl:for-each select="/ROW/ORDER">
                <ROW MODID="0" xmlns="http://www.filemaker.com/fmpxmlresult">
                    <COL><DATA><xsl:value-of select="OXID"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXSHOPID"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXUSERID"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXORDERDATE"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXORDERNR"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXBILLCOMPANY"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXBILLEMAIL"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXBILLFNAME"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXBILLLNAME"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXBILLSTREET"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXBILLSTREETNR"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXBILLADDINFO"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXBILLUSTID"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXBILLCITY"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXBILLCOUNTRYID"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXBILLCOUNTRYTITLE"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXBILLISOALPHA2"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXBILLISOALPHA3"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXBILLSTATEID"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXBILLZIP"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXBILLFON"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXBILLFAX"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXBILLSAL"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXDELCOMPANY"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXDELFNAME"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXDELLNAME"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXDELSTREET"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXDELSTREETNR"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXDELADDINFO"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXDELCITY"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXDELCOUNTRYID"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXDELCOUNTRYTITLE"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXDELISOALPHA2"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXDELISOALPHA3"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXDELSTATEID"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXDELZIP"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXDELFON"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXDELFAX"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXDELSAL"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXPAYMENTID"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXPAYMENTTYPE"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXTOTALNETSUM"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXTOTALBRUTSUM"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXTOTALORDERSUM"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXARTVAT1"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXARTVATPRICE1"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXARTVAT2"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXARTVATPRICE2"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXDELCOST"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXDELVAT"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXPAYCOST"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXPAYVAT"/></DATA></COL>
                    <!--<COL><DATA><xsl:value-of select="OXWRAPCOST"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXWRAPVAT"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXGIFTCARDCOST"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXGIFTCARDVAT"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXCARDID"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXCARDTEXT"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXDISCOUNT"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXEXPORT"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXBILLNR"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXBILLDATE"/></DATA></COL>-->
                    <COL><DATA><xsl:value-of select="OXTRACKCODE"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXSENDDATE"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXREMARK"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXVOUCHERDISCOUNT"/></DATA></COL>
                    <!--<COL><DATA><xsl:value-of select="OXCURRENCY"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXCURRATE"/></DATA></COL>-->
                    <COL><DATA><xsl:value-of select="OXFOLDER"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXTRANSID"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXPAYID"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXXID"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXPAID"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXSTORNO"/></DATA></COL>
                    <!--<COL><DATA><xsl:value-of select="OXIP"/></DATA></COL>-->
                    <COL><DATA><xsl:value-of select="OXTRANSSTATUS"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXLANG"/></DATA></COL>
                    <!--<COL><DATA><xsl:value-of select="OXINVOICENR"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXDELTYPE"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXTSPROTECTID"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXTSPROTECTCOSTS"/></DATA></COL>-->
                    <COL><DATA><xsl:value-of select="OXTIMESTAMP"/></DATA></COL>
                    <!--<COL><DATA><xsl:value-of select="OXISNETTOMODE"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXefiorderchecksum"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXefiaddchecksum"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="OXefiorderartschecksum"/></DATA></COL>-->
                    <COL><DATA><xsl:value-of select="flagversand"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="Konstante_Lieferschein"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="bankdatenexport"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="oxshop"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="iswhitelabel"/></DATA></COL>

                    <COL><DATA><xsl:value-of select="AuthCode"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="lsbankname"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="lsblz"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="lsktonr"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="lsktoinhaber"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="oxdateused"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="oxreserved"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="oxvouchernr"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="oxvoucherserieid"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="oxdiscount"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="oxid"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="oxtimestamp"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="oxvoucherserie"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="oxvoucherseriedescription"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="oxvoucherseriediscount"/></DATA></COL>
                    <COL><DATA><xsl:value-of select="oxvoucherseriediscounttyp"/></DATA></COL>
                </ROW>
                </xsl:for-each>
            </RESULTSET>
        </FMPXMLRESULT>
    </xsl:template>
</xsl:stylesheet>
