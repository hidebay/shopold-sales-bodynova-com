<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 29.10.15
 * Time: 12:28
 */
// Utf-8 Darstellung in der Ausgabe
header('Content-Type: text/html; charset=utf-8');
error_reporting(E_ALL);
ini_set('display_errors' , true);
ini_set('html_errors'    , true);
require '../../bootstrap.php';
require 'array2xml.php';
$debug = 0;

if(isset($_GET['ordernr'])){
	$OrderSelect = "Select OXID from oxorder where OXORDERNR = '".$_GET['ordernr']."'";
	$OrderID     = oxDb::getDb()->getOne($OrderSelect);
	$oOrder      = oxNew("oxorder");
	$oOrder->load($OrderID);


	if(isset($_GET['senddate']) || isset($_GET['folder'])){
		if(isset($_GET['senddate'])){
			$oOrder->oxorder__oxsenddate->value = date_format(date_create($_GET['senddate']), 'Y-m-d H:i:s');
		}
		if(isset($_GET['folder'])){
			#echo 'folder';
			//Folder update
			$oDb = oxDb::getDb();
			$ssql = "update oxorder set OXFOLDER = '" . $_GET['folder'] . "' where OXID = '" . $oOrder->oxorder__oxid->value . "'";
			#echo $ssql;
			$oDb->execute($ssql);
			$oOrder->oxorder__oxfolder->value = $_GET['folder'];
			$oOrder->oxorder__oxfolder->rawValue = $_GET['folder'];
		}
	}else{
		echo 'sendate oder folder fehlt<br>';
		echo 'bitte wie folgt: setorder.php?ordernr=99999&senddate=31.12.2015 12:22:24&folder=OXORDERFOLDER_FINISHED<br>';
		die;
	}

	if($oOrder->save()){
		echo 'hat geklappt<br>';
		echo 'ordernr = '.$_GET['ordernr'].'<br>';
		echo 'senddate = '.$_GET['senddate'].'<br>';
		echo 'folder = '.$_GET['folder'].'<br>';
	}
	if($debug == 1){
		echo '<pre>';
		print_r($oOrder);
		echo '</pre>';
	}

} else {
	echo '<h3>Keine Parameter übergeben!</h3>';
	echo 'bitte wie folgt: setorder.php?ordernr=99999&senddate=31.12.2015 12:22:24&folder=OXORDERFOLDER_FINISHED<br>';
}
