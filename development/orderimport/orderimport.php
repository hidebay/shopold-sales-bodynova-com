<?php

/**
 * Created by PhpStorm.
 * User: andre
 * Date: 02.11.15
 * Time: 15:03
 */
class orderimport
{
    public $oOrder;
    public $oCountry;


    public function getOneOrderFromDb($OrderNr){
        // OXORDER
        $sSelect = "select oxid from oxorder where 1 and oxordernr = '".$OrderNr ."'";
        $soxId= oxDb::getDb()->getOne( $sSelect );
        $oOrder = oxNew( "oxorder" );
        $oOrder->load( $soxId);
        #return $oOrder;
    }

    public function setArrOrder($oOrder, $oBillCountry, $oDelCountry){

        $arOrder = array('ORDER' => array(
            'OXID'              =>$oOrder->oxorder__oxid->value,
            'OXSHOPID'          =>'oxbaseshop',
            'OXUSERID'          =>$oOrder->oxorder__oxuserid->value,
            'OXORDERDATE'       =>date_format(date_create($oOrder->oxorder__oxorderdate->value), 'Y-m-d H:i:s'),
            'OXORDERNR'         =>$oOrder->oxorder__oxordernr->value,
            'OXBILLCOMPANY'     =>$oOrder->oxorder__oxbillcompany->value,
            'OXBILLEMAIL'       =>$oOrder->oxorder__oxbillemail->value,
            'OXBILLFNAME'       =>$oOrder->oxorder__oxbillfname->value,
            'OXBILLLNAME'       =>$oOrder->oxorder__oxbilllname->value,
            'OXBILLSTREET'      =>$oOrder->oxorder__oxbillstreet->value,
            'OXBILLSTREETNR'    =>$oOrder->oxorder__oxbillstreetnr->value,
            'OXBILLADDINFO'     =>$oOrder->oxorder__oxbilladdinfo->value,
            'OXBILLUSTID'       =>$oOrder->oxorder__oxbillustid->value,
            'OXBILLCITY'        =>$oOrder->oxorder__oxbillcity->value,
            'OXBILLCOUNTRYID'   =>$oOrder->oxorder__oxbillcountryid->value,
            'OXBILLCOUNTRYTITLE'=>$oBillCountry->oxcountry__oxtitle->value,
            'OXBILLISOALPHA2'   =>$oBillCountry->oxcountry__oxisoalpha2->value,
            'OXBILLISOALPHA3'   =>$oBillCountry->oxcountry__oxisoalpha3->value,
            'OXBILLSTATEID'     =>$oOrder->oxorder__oxbillstateid->value,
            'OXBILLZIP'         =>$oOrder->oxorder__oxbillzip->value,
            'OXBILLFON'         =>$oOrder->oxorder__oxbillfon->value,
            'OXBILLFAX'         =>$oOrder->oxorder__oxbillfax->value,
            'OXBILLSAL'         =>$oOrder->oxorder__oxbillsal->value,
            'OXDELCOMPANY'      =>$oOrder->oxorder__oxdelcompany->value,
            'OXDELFNAME'        =>$oOrder->oxorder__oxdelfname->value,
            'OXDELLNAME'        =>$oOrder->oxorder__oxdellname->value,
            'OXDELSTREET'       =>$oOrder->oxorder__oxdelstreet->value,
            'OXDELSTREETNR'     =>$oOrder->oxorder__oxdelstreetnr->value,
            'OXDELADDINFO'      =>$oOrder->oxorder__oxdeladdinfo->value,
            'OXDELCITY'         =>$oOrder->oxorder__oxdelcity->value,
            'OXDELCOUNTRYID'    =>$oOrder->oxorder__oxdelcountryid->value,
            'OXDELCOUNTRYTITLE' =>$oDelCountry->oxcountry__oxtitle->value,
            'OXDELISOALPHA2'    =>$oDelCountry->oxcountry__oxisoalpha2->value,
            'OXDELISOALPHA3'    =>$oDelCountry->oxcountry__oxisoalpha3->value,
            'OXDELSTATEID'      =>$oOrder->oxorder__oxdelstateid->value,
            'OXDELZIP'          =>$oOrder->oxorder__oxdelzip->value,
            'OXDELFON'          =>$oOrder->oxorder__oxdelfon->value,
            'OXDELFAX'          =>$oOrder->oxorder__oxdelfax->value,
            'OXDELSAL'          =>$oOrder->oxorder__oxdelsal->value,
            'OXPAYMENTID'       =>$oOrder->oxorder__oxpaymentid->value,
            'OXPAYMENTTYPE'     =>$oOrder->oxorder__oxpaymenttype->value,
            'OXTOTALNETSUM'     =>$oOrder->oxorder__oxtotalnetsum->value,
            'OXTOTALBRUTSUM'    =>$oOrder->oxorder__oxtotalbrutsum->value,
            'OXTOTALORDERSUM'   =>$oOrder->oxorder__oxtotalordersum->value,
            'OXARTVAT1'         =>$oOrder->oxorder__oxartvat1->value,
            'OXARTVATPRICE1'    =>$oOrder->oxorder__oxartvatprice1->value,
            'OXARTVAT2'         =>$oOrder->oxorder__oxartvat2->value,
            'OXARTVATPRICE2'    =>$oOrder->oxorder__oxartvatprice2->value,
            'OXDELCOST'         =>$oOrder->oxorder__oxdelcost->value,
            'OXDELVAT'          =>$oOrder->oxorder__oxdelvat->value,
            'OXPAYCOST'         =>$oOrder->oxorder__oxpaycost->value,
            'OXPAYVAT'          =>$oOrder->oxorder__oxpayvat->value,
            'OXWRAPCOST'        =>$oOrder->oxorder__oxwrapcost->value,
            'OXWRAPVAT'         =>$oOrder->oxorder__oxwrapvat->value,
            'OXGIFTCARDCOST'    =>$oOrder->oxorder__oxgiftcardcost->value,
            'OXGIFTCARDVAT'     =>$oOrder->oxorder__oxgiftcardvat->value,
            'OXCARDID'          =>$oOrder->oxorder__oxcardid->value,
            'OXCARDTEXT'        =>$oOrder->oxorder__oxcardtext->value,
            'OXDISCOUNT'        =>$oOrder->oxorder__oxdiscount->value,
            'OXEXPORT'          =>$oOrder->oxorder__oxexport->value,
            'OXBILLNR'          =>$oOrder->oxorder__oxbillnr->value,
            'OXBILLDATE'        =>date_format(date_create($oOrder->oxorder__oxbilldate->value), 'Y-m-d H:i:s'), //$oOrder->oxorder__oxbilldate->value,
            'OXTRACKCODE'       =>$oOrder->oxorder__oxtrackcode->value,
            'OXSENDDATE'        =>date_format(date_create($oOrder->oxorder__oxsenddate->value), 'Y-m-d H:i:s'), //$oOrder->oxorder__oxsenddate->value,
            'OXREMARK'          =>$oOrder->oxorder__oxremark->value,
            'OXVOUCHERDISCOUNT' =>$oOrder->oxorder__oxvoucherdiscount->value,
            'OXCURRENCY'        =>$oOrder->oxorder__oxcurrency->value,
            'OXCURRATE'         =>$oOrder->oxorder__oxcurrate->value,
            'OXFOLDER'          =>$oOrder->oxorder__oxfolder->value,
            'OXTRANSID'         =>$oOrder->oxorder__oxtransid->value,
            'OXPAYID'           =>$oOrder->oxorder__oxpayid->value,
            'OXXID'             =>$oOrder->oxorder__oxxid->value,
            'OXPAID'            =>$oOrder->oxorder__oxpaid->value,
            'OXSTORNO'          =>$oOrder->oxorder__oxstorno->value,
            'OXIP'              =>$oOrder->oxorder__oxip->value,
            'OXTRANSSTATUS'     =>$oOrder->oxorder__oxtransstatus->value,
            'OXLANG'            =>$oOrder->oxorder__oxlang->value,
            'OXINVOICENR'       =>$oOrder->oxorder__oxinvoicenr->value,
            'OXDELTYPE'         =>$oOrder->oxorder__oxdeltype->value,
            'OXTSPROTECTID'     =>$oOrder->oxorder__oxtsprotectid->value,
            'OXTSPROTECTCOSTS'  =>$oOrder->oxorder__oxtsprotectcosts->value,
            'OXTIMESTAMP'       =>date_format(date_create($oOrder->oxorder__oxtimestamp->value), 'Y-m-d H:i:s'), //$oOrder->oxorder__oxtimestamp->value,
            'OXISNETTOMODE'     =>$oOrder->oxorder__oxisnettomode->value,
            'oxefiorderchecksum'=>$oOrder->oxorder__oxefiorderchecksum->value,
            'oxefiaddchecksum'  =>$oOrder->oxorder__oxefiaddchecksum->value,
            'oxefiorderartschecksum'=>$oOrder->oxorder__oxefiorderartschecksum->value,
            'flagversand'       =>$oOrder->oxorder__flagversand->value,
            'Konstante_Lieferschein'=>'Lieferschein',
            'bankdatenexport'   =>$oOrder->oxorder__bankdatenexport->value,
            ));
            //Paymentvariablen anhängen:
            // Kreditkarte
            if($oOrder->oxorder__oxpaymenttype->value == 'oxidcreditcard'){

            $arOrder['ORDER']['AuthCode'] = $ipaymentAuthCode;
            }else{
                $arOrder['ORDER']['AuthCode'] = '';
            }

            // Lastschrift
            if($oOrder->oxorder__oxpaymenttype->value == 'oxiddebitnote'){

                foreach($dyn as $key){

                    $arOrder['ORDER'][$key->name] = $key->value;

                }
            }else{
                $arOrder['ORDER']['lsbankname']  = '';
                $arOrder['ORDER']['lsblz']       = '';
                $arOrder['ORDER']['lsktonr']     = '';
                $arOrder['ORDER']['lsktoinhaber']= '';
            }
        }
}
