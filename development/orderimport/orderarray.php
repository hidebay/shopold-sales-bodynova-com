<?php

$arOrder = array('ORDER' => array(

    'OXID'              =>$key->oxorder__oxid->value,
    'OXSHOPID'          =>'oxbaseshop',
    'OXUSERID'          =>$key->oxorder__oxuserid->value,
    'OXORDERDATE'       =>date_format(date_create($oOrder->oxorder__oxorderdate->value), 'Y-m-d H:i:s'),
    'OXORDERNR'         =>$key->oxorder__oxordernr->value,
    'OXBILLCOMPANY'     =>$key->oxorder__oxbillcompany->value,
    'OXBILLEMAIL'       =>$key->oxorder__oxbillemail->value,
    'OXBILLFNAME'       =>$key->oxorder__oxbillfname->value,
    'OXBILLLNAME'       =>$key->oxorder__oxbilllname->value,
    'OXBILLSTREET'      =>$key->oxorder__oxbillstreet->value,
    'OXBILLSTREETNR'    =>$key->oxorder__oxbillstreetnr->value,
    'OXBILLADDINFO'     =>$key->oxorder__oxbilladdinfo->value,
    'OXBILLUSTID'       =>$key->oxorder__oxbillustid->value,
    'OXBILLCITY'        =>$key->oxorder__oxbillcity->value,
    'OXBILLCOUNTRYID'   =>$key->oxorder__oxbillcountryid->value,
    'OXBILLCOUNTRYTITLE'=>$oBillCountry->oxcountry__oxtitle->value,
    'OXISOALPHA2'       =>$oBillCountry->oxcountry__oxisoalpha2->value,
    'OXISOALPHA3'       =>$oBillCountry->oxcountry__oxisoalpha3->value,
    'OXBILLSTATEID'     =>$key->oxorder__oxbillstateid->value,
    'OXBILLZIP'         =>$key->oxorder__oxbillzip->value,
    'OXBILLFON'         =>$key->oxorder__oxbillfon->value,
    'OXBILLFAX'         =>$key->oxorder__oxbillfax->value,
    'OXBILLSAL'         =>$key->oxorder__oxbillsal->value,
    'OXDELCOMPANY'      =>$key->oxorder__oxdelcompany->value,
    'OXDELFNAME'        =>$key->oxorder__oxdelfname->value,
    'OXDELLNAME'        =>$key->oxorder__oxdellname->value,
    'OXDELSTREET'       =>$key->oxorder__oxdelstreet->value,
    'OXDELSTREETNR'     =>$key->oxorder__oxdelstreetnr->value,
    'OXDELADDINFO'      =>$key->oxorder__oxdeladdinfo->value,
    'OXDELCITY'         =>$key->oxorder__oxdelcity->value,
    'OXDELCOUNTRYID'    =>$key->oxorder__oxdelcountryid->value,
    'OXDELCOUNTRYTITLE' =>$oDelCountry->oxcountry__oxtitle->value,
    'OXDELISOALPHA2'    =>$oDelCountry->oxcountry__oxisoalpha2->value,
    'OXDELISOALPHA3'    =>$oDelCountry->oxcountry__oxisoalpha3->value,
    'OXDELSTATEID'      =>$key->oxorder__oxdelstateid->value,
    'OXDELZIP'          =>$key->oxorder__oxdelzip->value,
    'OXDELFON'          =>$key->oxorder__oxdelfon->value,
    'OXDELFAX'          =>$key->oxorder__oxdelfax->value,
    'OXDELSAL'          =>$key->oxorder__oxdelsal->value,
    'OXPAYMENTID'       =>$key->oxorder__oxpaymentid->value,
    'OXPAYMENTTYPE'     =>$key->oxorder__oxpaymenttype->value,
    'OXTOTALNETSUM'     =>$key->oxorder__oxtotalnetsum->value,
    'OXTOTALBRUTSUM'    =>$key->oxorder__oxtotalbrutsum->value,
    'OXTOTALORDERSUM'   =>$key->oxorder__oxtotalordersum->value,
    'OXARTVAT1'         =>$key->oxorder__oxartvat1->value,
    'OXARTVATPRICE1'    =>$key->oxorder__oxartvatprice1->value,
    'OXARTVAT2'         =>$key->oxorder__oxartvat2->value,
    'OXARTVATPRICE2'    =>$key->oxorder__oxartvatprice2->value,
    'OXDELCOST'         =>$key->oxorder__oxdelcost->value,
    'OXDELVAT'          =>$key->oxorder__oxdelvat->value,
    'OXPAYCOST'         =>$key->oxorder__oxpaycost->value,
    'OXPAYVAT'          =>$key->oxorder__oxpayvat->value,
    'OXWRAPCOST'        =>$key->oxorder__oxwrapcost->value,
    'OXWRAPVAT'         =>$key->oxorder__oxwrapvat->value,
    'OXGIFTCARDCOST'    =>$key->oxorder__oxgiftcardcost->value,
    'OXGIFTCARDVAT'     =>$key->oxorder__oxgiftcardvat->value,
    'OXCARDID'          =>$key->oxorder__oxcardid->value,
    'OXCARDTEXT'        =>$key->oxorder__oxcardtext->value,
    'OXDISCOUNT'        =>$key->oxorder__oxdiscount->value,
    'OXEXPORT'          =>$key->oxorder__oxexport->value,
    'OXBILLNR'          =>$key->oxorder__oxbillnr->value,
    'OXBILLDATE'        =>$key->oxorder__oxbilldate->value,
    'OXTRACKCODE'       =>$key->oxorder__oxtrackcode->value,
    'OXSENDDATE'        =>$key->oxorder__oxsenddate->value,
    'OXREMARK'          =>$key->oxorder__oxremark->value,
    'OXVOUCHERDISCOUNT' =>$key->oxorder__oxvoucherdiscount->value,
    'OXCURRENCY'        =>$key->oxorder__oxcurrency->value,
    'OXCURRATE'         =>$key->oxorder__oxcurrate->value,
    'OXFOLDER'          =>$key->oxorder__oxfolder->value,
    'OXTRANSID'         =>$key->oxorder__oxtransid->value,
    'OXPAYID'           =>$key->oxorder__oxpayid->value,
    'OXXID'             =>$key->oxorder__oxxid->value,
    'OXPAID'            =>$key->oxorder__oxpaid->value,
    'OXSTORNO'          =>$key->oxorder__oxstorno->value,
    'OXIP'              =>$key->oxorder__oxip->value,
    'OXTRANSSTATUS'     =>$key->oxorder__oxtransstatus->value,
    'OXLANG'            =>$key->oxorder__oxlang->value,
    'OXINVOICENR'       =>$key->oxorder__oxinvoicenr->value,
    'OXDELTYPE'         =>$key->oxorder__oxdeltype->value,
    'OXTSPROTECTID'     =>$key->oxorder__oxtsprotectid->value,
    'OXTSPROTECTCOSTS'  =>$key->oxorder__oxtsprotectcosts->value,
    'OXTIMESTAMP'       =>$key->oxorder__oxtimestamp->value,
    'OXISNETTOMODE'     =>$key->oxorder__oxisnettomode->value,
    'oxefiorderchecksum'=>$key->oxorder__oxefiorderchecksum->value,
    'oxefiaddchecksum'  =>$key->oxorder__oxefiaddchecksum->value,
    'oxefiorderartschecksum'=>$key->oxorder__oxefiorderartschecksum->value,
    'flagversand'       =>$key->oxorder__flagversand->value,
    'Konstante_Lieferschein'=>'Lieferschein',
    'bankdatenexport'   =>$key->oxorder__bankdatenexport->value,
));
//Paymentvariablen anhängen:
// Kreditkarte
if($key->oxorder__oxpaymenttype->value == 'oxidcreditcard'){

    $arOrder['ORDER']['AuthCode'] = $ipaymentAuthCode;
}else{
    $arOrder['ORDER']['AuthCode'] = '';
}

// Lastschrift
if($key->oxorder__oxpaymenttype->value == 'oxiddebitnote'){

    foreach($dyn as $key){

        $arOrder['ORDER'][$key->name] = $key->value;

    }
}else{
    $arOrder['ORDER']['lsbankname']  = '';
    $arOrder['ORDER']['lsblz']       = '';
    $arOrder['ORDER']['lsktonr']     = '';
    $arOrder['ORDER']['lsktoinhaber']= '';
}