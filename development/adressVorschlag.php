<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Adress Vorschlag</title>
	<style>
		html, body {
			height: 100%;
			margin: 0;
			padding: 0;
		}
		#map {
			height: 100%;
		}
		#right-panel {
			font-family: 'Roboto','sans-serif';
			line-height: 30px;
			padding-left: 10px;
		}
		
		#right-panel select, #right-panel input {
			font-size: 15px;
		}
		
		#right-panel select {
			width: 100%;
		}
		
		#right-panel i {
			font-size: 12px;
		}
	
	</style>
</head>
<body>
<div id="right-panel">
	<input id="question" value="<?php echo 'Question: '.$_GET['query']?>"></input>
	<ul id="results"></ul>
</div>
<script>
	// This example retrieves autocomplete predictions programmatically from the
	// autocomplete service, and displays them as an HTML list.
	
	function initService() {
		var displaySuggestions = function(predictions, status) {
			if (status != google.maps.places.PlacesServiceStatus.OK) {
				alert(status);
				return;
			}
			
			predictions.forEach(function(prediction) {
				var li = document.createElement('li');
				li.appendChild(document.createTextNode(prediction.description));
				document.getElementById('results').appendChild(li);
			});
			
			autocomplete = new google.maps.places.Autocomplete(
				/** @type {!HTMLInputElement} */
				((document.getElementById('question'))) , {types: ['geocode']}
			);
			
			console.log(predictions);
			
			console.log(document.getElementById('question'));
			
		};
		
		var service = new google.maps.places.AutocompleteService();
		service.getQueryPredictions({ input: '<?php echo $_GET['query']; ?>' }, displaySuggestions);
	}

</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBkT5RQcZoHKRWJf507x8uS1fhRaFTO9y8&signed_in=true&libraries=places&callback=initService"
        async defer></script>
</body>
</html>
