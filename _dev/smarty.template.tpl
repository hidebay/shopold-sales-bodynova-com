[{*}]
    /**
    * Created by PhpStorm.
    * User: andre
    * Date: 04.02.15
    * Time: 16:06
    */
[{*}]

[{* Debug *}]
    <h3>Debug:</h3>
    [{assign var="objekt" value=$product}]
    <pre>
        <b>get_template_vars<b>
        <hr>
            [{php}]
                print_r($this->get_template_vars('objekt'));
                #exit;
            [{/php}]
        <hr>
    </pre>


    <pre>
        <b>get_object_vars<b>
        <hr>
        [{php}]
            print_r($this->get_object_vars('objekt'));
            #exit;
        [{/php}]
        <hr>
    </pre>
[{* /Debug *}]


<span class="debug">!$product || $product->isBuyable()</span>


[{$product |@print_r}]

[{$product |@var_dump()}]

[$smarty.session|@debug_print_var}]

[{$varname|@debug_print_var}]

[{assign var="ausgabe" value="foo"}]
[{$ausgabe}] [{* Im Layout = foo *}]


[{oxmultilang ident="CA"}] = ca

