*Dies ist eine Übersetzung der [originalen Syntax-Dokumentation][osd]
von [John Grubers][jg] [Markdown][md]. Diese Übersetzung bezieht sich
auf den Stand vom 15.12.2013 (Markdown Version 1.0.1). FÃ¼r die
Richtigkeit der Ãœbersetzung Ã¼bernehme ich keine GewÃ¤hr. Bei Fehlern in
der Ãœbersetzung bitte ich um eine kurze Nachricht an
<lasar@liepins.net>. Auch jede andere Art von Feedback ist willkommen.*

   [jg]: http://daringfireball.net/
   [md]: http://daringfireball.net/projects/markdown/
  [osd]: http://daringfireball.net/projects/markdown/syntax

* * *

Markdown: Syntax
================

* [Übersicht](#overview)
    * [Philosophie](#philosophy)
    * [Inline HTML](#html)
    * [Automatische Maskierung besonderer Zeichen](#autoescape)
* [Block-Elemente](#block)
    * [Absätze und Zeilenumbrüche](#p)
    * [Kopfzeilen](#header)
    * [Zitate](#blockquote)
    * [Listen](#list)
    * [Code-Blöcke](#precode)
    * [Horizontale Linien](#hr)
* [Span-Elemente](#span)
    * [Links](#link)
    * [Betonung](#em)
    * [Code](#code)
    * [Grafiken](#img)
* [Verschiedenes](#misc)
    * [Automatische Links](#autolink)
    * [Backslash-Maskierung](#backslash)
* [Interna](#interna)
	* [Versionen](#versionen)
	* [Lizenz](#lizenz)

* * *

<h2 id="overview">Übersicht</h2>

<h3 id="philosophy">Philosopie</h3>

Markdown wurde mit den Grundgedanken konzipiert, so einfach lesbar und
schreibbar wie möglich zu sein.

Lesbarkeit ist hierbei das oberste Ziel. Ein Markdown-formatiertes
Dokument sollte in seiner Grundform verÃ¶ffentlicht werden kÃ¶nnen, ohne
den Anschein zu erwecken, es sei mit Tags oder Formatierungsbefehlen
versehen (wie es bei HTML der Fall ist).

Markdown wurde von verschiedenen Text-zu-HTML-Filtern beeinflusst,
darunter [Setext] [1], [atx] [2], [Textile] [3], [reStructuredText] [4],
[Grutatext] [5], und [EtText] [6]. Die grÃ¶ÃŸte Quelle der Inspiration fÃ¼r
Markdowns Syntax ist aber die Formatierung von Nur-Text E-Mails.

  [1]: http://docutils.sourceforge.net/mirror/setext.html
  [2]: http://www.aaronsw.com/2002/atx/
  [3]: http://textism.com/tools/textile/
  [4]: http://docutils.sourceforge.net/rst.html
  [5]: http://www.triptico.com/software/grutatxt.html
  [6]: http://ettext.taint.org/doc/

Dementsprechend besteht Markdowns Syntax nur aus Zeichen, die mit
Bedacht so gewÃ¤hlt wurden, dass ihr Aussehen ihrer Bedeutung entspricht.
Zum Beispiel sehen Sternchen um ein Wort tatsÃ¤chlich wie eine
\*Betonung\* aus. Listen in Markdown sehen aus wie Listen. Sogar
Zitat-BlÃ¶cke sehen wie zitierte Textpassagen aus, wie man sie aus eMails
kennt.



<h3 id="html">Inline-HTML</h3>

Markdowns Syntax hat eine Bestimmung: Benutzt zu werden um fÃ¼r das Web
zu *schreiben*.

Markdown ist kein Ersatz fÃ¼r HTML, nicht einmal Ansatzweise. Der Umfang
der Syntax ist sehr klein und entspricht nur einem geringen Teil aller
HTML-Tags. Es ist nicht die Absicht von Markdown, das EinfÃ¼gen von
HTML-Tags zu erleichtern. HTML ist bereits einfach genug. Die Idee
hinter Markdown ist es, Text so einfach wie mÃ¶glich zu lesen, zu
schreiben und zu bearbeiten. HTML ist ein *Publikations-Format*;
Markdown ist ein *Schreib-Format*. Daher berÃ¼cksichtigt seine Syntax nur
Inhalte, die mit purem Text zu vermitteln sind.

FÃ¼r jede Formatierung, die nicht mit Markdown machbar ist, kann einfach
HTML verwendet werden. Es ist nicht nÃ¶tig, HTML zu markieren um es vom
Rest abzugrenzen. Es wird einfach in den Text geschrieben.

Die einzige BeschrÃ¤nkung sind Block-Elemente wie z.B. `<div>`,
`<table>`, `<pre>`, `<p>` und so weiter. Sie mÃ¼ssen vom umgebenden
Inhalt durch leere Zeilen getrennt sein, auÃŸerdem sollten die Start- und
End-Tags nicht mit Leerzeichen oder Tabs eingerÃ¼ckt sein. Markdown ist
intelligent genug um keine zusÃ¤tzlichen (ungewollten) `<p>`-Tags um
HTML-BlÃ¶cke zu setzen.

So baut man zum Beispiel eine HTML-Tabelle in einen Markdown-Artikel
ein:

    Dies ist ein normaler Absatz.

    <table>
        <tr>
            <td>Foo</td>
        </tr>
    </table>

    Dies ist noch ein normaler Absatz.

Es ist zu beachten, dass Markdowns Syntax innerhalb von HTML-BlÃ¶cken
nicht interpretiert wird. Es kann zum Beispiel keine *Betonung*
innerhalb von HTML-BlÃ¶cken benutzt werden.

Inline-HTML-Tags wie z.B. `<span>`, `<cite>`, oder `<del>` kÃ¶nnen
Ã¼berall in einen Markdown-Absatz, Listenpunkt oder einer Kopfzeile
verwendet werden. HTML-Tags kÃ¶nnen sogar statt der entsprechenden
Markdown-Formatierung verwendet werden. Es ist kein Problem, statt
Markdows Syntax fÃ¼r Links oder Grafiken einfach `<a>` oder `<img>` zu
verwenden.

Im Gegensatz zu Block-Tags *wird* die Markdown-Syntax innerhalb
von Inline-Tags interpretiert.



<h3 id="autoescape">Automatische Maskierung besonderer Zeichen</h3>

In HTML gibt es zwei Zeichen, die einer besonderen Behandlung bedÃ¼rfen:
`<` und `&`. Die linke spitze Klammer wird verwendet um HTML-Tags zu
Ã¶ffnen, das kaufmÃ¤nnische Und wird verwendet um benannte Zeichen
(Entities) zu beschreiben. Wenn diese Zeichen in HTML-Dokumenten als
"sie selbst" verwendet werden sollen, mÃ¼ssen sie als Entities maskiert
werden, also als `&lt;` und `&amp;`.

Das kaufmÃ¤nnische Und ist besonders unpraktisch fÃ¼r Web-Entwickler. Will
man Ã¼ber "AT&T" schreiben, muss man "`AT&amp;T`" schreiben. Das
kaufmÃ¤nnische Und muss sogar in URLs maskiert werden. In einem Link zur
Seite

    http://images.google.com/images?num=30&q=larry+bird

muss die URL wie folgt kodiert werden:

    http://images.google.com/images?num=30&amp;q=larry+bird

Dies ist einfach zu vergessen und vermutlich der beliebteste Fehler bei
der Validierung von ansonsten wohlgeformten HTML-Dokumenten.

Markdown erlaubt es, diese Zeichen ganz normal zu benutzen. Es regelt
die Kodierung selbst. Wenn ein Kaufmanns-Und in einem Entity verwendet
wird, wird es nicht kodiert, sonst zu `&amp;` umgewandelt.

Wenn man also zum Beispiel ein Copyright-Symbol eingeben will, kann man
einfach

    &copy;

schreiben, und Markdown wird dies nicht modifizieren. Aber aus

    AT&T

wird Markdown

    AT&amp;T

machen. Da Markdown Inline HTML unterstÃ¼tzt, werden spitze Klammern im
entsprechenden Fall ganz normal als HTML behandelt. Nur aus Sachen wie

    4 < 5

wird Markdown

    4 &lt; 5

machen. In Code- oder Span-BlÃ¶cken werden spitze Klammern und das
kaufmÃ¤nnische Und jedoch *immer* kodiert. Dies vereinfacht das Schreiben
Ã¼ber HTML in Markdown (Im Gegensatz zu rohem HTML, wo es meist ein
Alptraum ist, jedes `<` und `&` zu kodieren).



* * *



<h2 id="block">Block-Elemente</h2>


<h3 id="p">AbsÃ¤tze und ZeilenumbrÃ¼che</h3>

Ein Absatz besteht einfach aus einer oder aus mehreren Zeilen von Text,
abgetrennt durch eine oder mehrere leere Zeilen. (Eine leere Zeile ist
jede Zeile die *aussieht* wie eine leere Zeile -- eine Zeile die nichts
anderes enthÃ¤lt als Leerzeichen und Tabs wird als leer behandelt.)
Normale AbsÃ¤tze sollten nicht mit Leerzeichen oder Tabs eingerÃ¼ckt sein.

Die "eine oder mehrere Zeilen"-Regel impliziert eine Sache: Markdown
unterstÃ¼tzt AbsÃ¤tze mit "harten UmbrÃ¼chen". Dies ist ein groÃŸer
Unterschied zu den meisten anderen Text-zu-HTML-Formatierern (inklusive
der "Convert Line Breaks" Option von Movable Type), die jeden
Zeilenumbruch in einem Absatz als `<br />` formatieren.

Wenn man ein `<br />` als Umbruch haben *will*, kann man die Zeile
einfach mit zwei oder mehr Leerzeichen beenden.

Dies ist zwar ein kleiner Mehraufwand um ein `<br />` zu erzeugen, aber
eine einfache "jeder Zeilenumbruch ist ein `<br />`" Regel wÃ¼rde in
Markdown nicht funktionieren.

Markdowns Email-artige [Zitate][bq] und [Listen-EintrÃ¤ge][l] mit
mehreren AbsÃ¤tzen funktionieren am Besten - und sehen besser aus -- wenn
sie mit ZeilenumbrÃ¼chen formatiert werden.

  [bq]: #blockquote
  [l]:  #list



<h3 id="header">Kopfzeilen</h3>

Markdown unterstÃ¼tzt zwei Arten von Kopfzeilen-Formatierung, [Setext]
[1] und [atx] [2].

Setext-artige Kopfzeilen werden mit Gleichheitszeichen (fÃ¼r
Ãœberschriften erster Ebene) und Bindestrichen (fÃ¼r die
zweite Ebene) "unterstrichen". Zum Beispiel:

    Dies ist ein H1
    ===============

    Dies ist ein H2
    ---------------

Eine beliebige Zahl von `=` oder `-` funktioniert.

Atx-artige Kopfzeilen verwenden 1-6 Rauten-Zeichen am Anfang der Zeile,
entsprechend den Ebenen 1-6. Zum Beispiel:

    # Dies ist ein H1

    ## Dies ist ein H2

    ###### Dies ist ein H6

Wenn gewÃ¼nscht kÃ¶nnen atx-artige Kopfzeilen auch "geschlossen" werden.
Dies ist nur Kosmetik -- es kann genutzt werden wenn es besser aussieht.
Die Zahl der schlieÃŸenden Rauten-Zeichen muss nicht einmal der Zahl der
Ã¶ffnenden Zeichen entsprechen.

    # Dies ist ein H1 #

    ## Dies ist ein H2 ##

    ### Dies ist ein H3 ######


<h3 id="blockquote">Zitate</h3>

Markdown verwendet - wie E-Mails - das Zeichen `>` fÃ¼r Zitat-BlÃ¶cke.
Wenn man Erfahrung mit Zitaten in E-Mails hat, weiÃŸ man auch wie man
Zitate in Markdown erstellt. Es sieht am Besten aus wenn man den Text
pro Zeile umbricht und ein `>` vor jede Zeile setzt:

    > Dies ist ein Zitat mit zwei AbsÃ¤tzen. Lorem ipsum dolor sit amet,
    > consectetuer adipiscing elit. Aliquam hendrerit mi posuere
    > lectus. Vestibulum enim wisi, viverra nec, fringilla in, laoreet
    > vitae, risus.
    > 
    > Donec sit amet nisl. Aliquam semper ipsum sit amet velit.
    > Suspendisse id sem consectetuer libero luctus adipiscing.

Markdown erlaubt aber auch faul zu sein und das `>` nur auf der ersten
Zeile eines hart umgebrochenen Absatzes zu verwenden:

    > Dies ist ein Zitat mit zwei AbsÃ¤tzen. Lorem ipsum dolor sit amet,
    consectetuer adipiscing elit. Aliquam hendrerit mi posuere lectus.
    Vestibulum enim wisi, viverra nec, fringilla in, laoreet vitae,
    risus.

    > Donec sit amet nisl. Aliquam semper ipsum sit amet velit.
    Suspendisse id sem consectetuer libero luctus adipiscing.


Zitate kÃ¶nnen verschachtelt werden (also Zitat in einem Zitat) indem man
mehr `>` verwendet:

    > Dies ist die erste Zitat-Ebene.
    >
    > > Dies ist ein verschachteltes Zitat.
    >
    > ZurÃ¼ck auf der ersten Ebene.

Zitate kÃ¶nnen andere Markdown-Elemente enthalten, darunter Kopfzeilen,
Listen und Code-BlÃ¶cke:

	> ## Dies ist eine Ãœberschrift.
	> 
	> 1.   Dies ist der erste Listenpunkt.
	> 2.   Dies ist der zweite Listenpunkt.
	> 
	> Hier ist ein wenig Beispiel-Code:
	> 
	>     return shell_exec("echo $input | $Markdown_script");

Jeder vernÃ¼nftige Text-Editor sollte das Zitieren im Stil von E-Mail
einfach machen. In BBEdit zum Beispiel kann man eine Auswahl machen und
aus dem MenÃ¼ `Text` den Punkt `Increase Quote Level` wÃ¤hlen.



<h3 id="list">Listen</h3>

Markdown unterstÃ¼tzt sortierte (nummerierte) und unsortierte Listen
(AufzÃ¤hlungen).

Unsortierte Listen benutzen Sternchen, Plus und Bindestriche --
austauschbar -- als Listen-Marker:

    *   Rot
    *   GrÃ¼n
    *   Blau

ist gleich:

    +   Rot
    +   GrÃ¼n
    +   Blau

Und:

    -   Rot
    -   GrÃ¼n
    -   Blau

Sortierte Listen verwenden Zahlen mit darauf folgendem Punkt:

    1.  Hund
    2.  Katze
    3.  Maus

Hierbei ist es wichtig zu verstehen, dass die Zahlen selber keine
Auswirkung auf den Output von Markdown hat. Markdown erstellt aus der
letzten Liste folgenden HTML-Code:

    <ol>
    <li>Hund</li>
    <li>Katze</li>
    <li>Maus</li>
    </ol>

Wenn man die Liste statt dessen so schreibt:

    1.  Hund
    1.  Katze
    1.  Maus

Oder sogar:

    3. Hund
    1. Katze
    8. Maus

kommt trotzdem jedes Mal die gleiche Liste heraus. Wenn gewÃ¼nscht, kann
man seine Listen von Hand richtig nummerieren. Aber wer faul sein will,
kann getrost immer die gleiche Ziffer benutzen.

Allerdings sollte man auch dann die Liste mit der Nummer 1 beginnen. In
Zukunft wird Markdown vielleicht eine Startziffer fÃ¼r den ersten
Listeneintrag festlegen wollen.

ListeneintrÃ¤ge beginnen normalerweise am linken Rand des Dokuments, sie
kÃ¶nnen jedoch bis zu drei Leerzeichen nach Rechts eingerÃ¼ckt sein.
Listen-Marker mÃ¼ssen mit ein oder mehr Leerzeichen oder einem Tab vom
folgenden Text getrennt werden.

Um Listen schÃ¶n zu formatieren, kÃ¶nnen die einzelnen EintrÃ¤ge weiter
eingerÃ¼ckt werden, so wie hier:

    *   Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
        Aliquam hendrerit mi posuere lectus. Vestibulum enim wisi,
        viverra nec, fringilla in, laoreet vitae, risus.
    *   Donec sit amet nisl. Aliquam semper ipsum sit amet velit.
        Suspendisse id sem consectetuer libero luctus adipiscing.

Das folgende Beispiel generiert den gleichen Code, ist aber weniger
aufgerÃ¤umt:

    *   Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
    Aliquam hendrerit mi posuere lectus. Vestibulum enim wisi,
    viverra nec, fringilla in, laoreet vitae, risus.
    *   Donec sit amet nisl. Aliquam semper ipsum sit amet velit.
    Suspendisse id sem consectetuer libero luctus adipiscing.

Wenn ListeintrÃ¤ge durch Leerzeilen getrennt sind, wird Markdown die
ListeneintrÃ¤ge mit `<p>` und `</p>` umhÃ¼llen.

Zum Beispiel wird dies:

    *   Warsteiner
    *   KÃ¶nig

zu

    <ul>
    <li>Warsteiner</li>
    <li>KÃ¶nig</li>
    </ul>

Aber dies:

    *   Warsteiner

    *   KÃ¶nig

wird zu

    <ul>
    <li><p>Warsteiner</p></li>
    <li><p>KÃ¶nig</p></li>
    </ul>

Listenpunkte kÃ¶nnen aus mehreren AbsÃ¤tzen bestehen. Jeder folgende
Absatz in einem Listenpunkt muss mit mindestens 4 Leerzeichen oder einem
Tab eingerÃ¼ckt werden:

    1.  Dies ist eine Listenpunkt mit zwei AbsÃ¤tzen. Lorem ipsum dolor
        sit amet, consectetuer adipiscing elit. Aliquam hendrerit
        mi posuere lectus.

        Vestibulum enim wisi, viverra nec, fringilla in, laoreet
        vitae, risus. Donec sit amet nisl. Aliquam semper ipsum
        sit amet velit.

    2.  Suspendisse id sem consectetuer libero luctus adipiscing.

Es sieht gut aus wenn jede Zeile des folgenden Absatzes eingerÃ¼ckt ist,
aber wieder erlaubt Markdown dem Faulen, nur die erste Zeile
einzurÃ¼cken:

    *   Dies ist ein Listenpunkt mit zwei AbsÃ¤tzen

        Dies ist der zweite Absatz in diesem Listenpunkt. Nur die
    erste Zeile muss eingerÃ¼ckt sein. Lorem ipsum dolor sit amet,
    consectetuer adipiscing elit.

    *   Ein weiterer Punkt in der selben Liste.

Um ein Zitat in einem Listenpunkt zu verwenden muss das Zitat eingerÃ¼ckt
werden:

    *   Ein Listenpunkt mit einem Zitat:

        > Dies ist ein Zitat
        > In einer Liste.

Um einen Code-Block in einem Listenpunkt zu verwenden muss er *zwei mal*
eingerÃ¼ckt werden -- 8 Leerzeichen oder zwei Tabs:

    *   Ein Listenpunkt mit Codebeispiel:

            <hier Code einfÃ¼gen>


Es ist mÃ¶glich Listen unabsichtlich zu erstellen, indem man z.B.
folgendes schreibt:

    1986. Was fÃ¼r ein wundervolles Jahr.

Mit anderen Worten: Die Sequenz *Zahl-Punkt-Leerzeichen* am Anfang einer
Zeile. Um dieses Problem zu vermeiden kann der Punkt per Backslash
maskiert werden:

    1986\. Was fÃ¼r ein wundervolles Jahr.



<h3 id="precode">Code-BlÃ¶cke</h3>

Vorformatierte Code-BlÃ¶cke werden verwendet um Ã¼ber Programm- oder
Markup-Quelltext zu schreiben. Statt normale AbsÃ¤tze zu bilden werden
die Zeilen in einem Code-Block wie vorgefunden interpretiert. Markdown
umfasst Code-BlÃ¶cke mit den Tags `<pre>` und `<code>`.

Um einen Code-Block in Markdown zu erstellen, wird einfach jede Zeile
des Blocks mit mindestens 4 Leeerzeichen oder einem Tab eingerÃ¼ckt. Aus
folgender Eingabe...

    Dies ist ein normaler Absatz.

        Dies ist ein Code-Block.

...macht Markdown folgendes:

    <p>Dies ist ein normaler Absatz.</p>

    <pre><code>Dies ist ein Code-Block.
    </code></pre>

Eine Stufe der EinrÃ¼ckung -- 4 Leerzeichen oder 1 Tab -- werden von
jeder Zeile der EinrÃ¼ckung entfernt. Folgendes zum Beispiel...

    Ein Beispiel in AppleScript:

        tell application "Foo"
            beep
        end tell

...wird zu

    <p>Ein Beispiel in AppleScript:</p>

    <pre><code>tell application "Foo"
        beep
    end tell
    </code></pre>

Ein Code-Block endet bei der ersten Zeile, die nicht eingerÃ¼ckt ist
(oder am Ende des Dokuments).

In einem Code-Block werden das kaufmÃ¤nnische Und (`&`) und spitze
Klammern (`<` und `>`) automatisch in HTML-Entities umgewandelt. Dies
erleichtert die Einbindung von HTML-StÃ¼cken ungemein -- einfach HTML in
das Dokument kopieren, einrÃ¼cken und Markdown erledigt die Kodierung des
kaufmÃ¤nnischen Unds und der spitzen Klammern. Zum Beispiel:

    <div class="footer">
        &copy; 2004 Foo Corporation
    </div>

wird zu:

    <pre><code>&lt;div class="footer"&gt;
        &amp;copy; 2004 Foo Corporation
    &lt;/div&gt;
    </code></pre>

Normale Markdown-Syntax wird in Code-BlÃ¶cken nicht verarbeitet. D.h.
Sternchen sind einfach nur Sternchen in einem Code-Block und nicht das
Signal, den Text hervorzuheben. Die Konsequenz ist, dass es einfach ist,
in Markdown *Ã¼ber* Markdown zu sprechen.



<h3 id="hr">Horizontale Linien</h3>

Der Tag fÃ¼r horizontale Linien (`<hr />`) kann generiert werden indem 3
oder mehr Bindestriche oder Sternchen allein auf einer
Zeile geschrieben werden. Leerzeichen zwischen den Zeichen sind auch
erlaubt. Alle folgenden Beispiele wÃ¼rden eine horizontale Linie
generieren:

    * * *

    ***

    *****
	
    - - -

    ---------------------------------------


* * *



<h2 id="span">Span-Elemente</h2>

<h3 id="link">Links</h3>

Markdown unterstÃ¼tzt zwei Arten von Links: *Inline* und *Referenzen*.

In beiden Stilen wird der Link-Text mit [eckigen Klammern] markiert.

Um einen Inline-Link zu erstellen schreibt man direkt hinter der
schlieÃŸenden eckigen Klammer normale Klammern. In diese Klammern wird
die URL geschrieben auf die verlinkt werden soll zusammen mit einem
*optionalen* Titel fÃ¼r den Link in AnfÃ¼hrungsstrichen. Beispiele:

    Dies ist [ein Beispiel](http://example.com/ "Der Linktitel") fÃ¼r
    einen Inline-Link.

    [Dieser Link](http://example.net/) hat kein Titel-Attribut.

Daraus wird:

    <p>Dies ist <a href="http://example.com/" title="Titel">
    ein Beispiel</a> fÃ¼r einen Inline-Link.</p>

    <p><a href="http://example.net/">Dieser Link</a> hat kein
    Titel-Attribut.</p>

Wenn man auf Inhalte auf dem gleichen Server verweisen will, kann man
relative Pfade verwenden:

    Auf der Seite [Ãœber mich](/about/) gibt es weitere Informationen.

Referenz-Links verwenden einen zweiten Satz eckige Klammern in die ein
beliebig gewÃ¤hlter Bezeichner fÃ¼r den Link geschrieben wird:

    Dies ist [ein Beispiel][id] fÃ¼r einen Referenz-Link.

Nach Belieben kann auch ein Leerzeichen zwischen den Klammern eingefÃ¼gt
werden:

    Dies ist [ein Beispiel] [id] fÃ¼r einen Referenz-Link.

Dann, irgendwo im Dokument, wird der Link wie folgt auf einer eigenen
Zeile definiert:

    [id]: http://example.com/  "Optionalen Titel hier eintragen"

Also:

*   Eckige Klammern welche den Link-Bezeichner enthalten (wahlweise mit
    mit bis zu drei Leerzeichen eingerÃ¼ckt);
*   gefolgt von einem Doppelpunkt;
*   gefolgt von einem oder mehreren Leerzeichen (oder Tabs);
*   gefolgt von der URL fÃ¼r den Link;
*   optional gefolgt vom Text fÃ¼r das Titel-Attribut des Links,
    gehÃ¼llt in Klammern, einzelne oder doppelte AnfÃ¼hrungsstriche.

Die folgenden drei Definitionen sind identisch:

	[foo]: http://example.com/  "Optionaler Titel"
	[foo]: http://example.com/  'Optionaler Titel'
	[foo]: http://example.com/  (Optionaler Titel)

**Anmerkung:** Es existiert bekannter Bug in Markdown 1.0.1 der die
Funktion von einzelnen AnfÃ¼hrungszeichen als Trenner fÃ¼r Link-Titel
behindert.


Die Link-URL kann wahlweise in spitzen Klammern verpackt werden:

    [id]: <http://example.com/>  "Optionaler Titel hier"

Das Titel-Attribut kann auch auf die nÃ¤chste Zeile gesetzt und mit mehr
Leerzeichen oder Tabs eingerÃ¼ckt werden. Dies sieht mit langen URLs
besser aus:

    [id]: http://example.com/langer/pfad/zu/seite
        "Optionaler Titel hier"

Link-Definitionen werden nur verwendet um Links zu erstellen wÃ¤hrend
Markdown das Dokument verarbeitet und werden aus dem Dokument entfernt
bevor das HTML ausgegeben wird.

Die Bezeichner fÃ¼r Link-Definitionen dÃ¼rfen aus Buchstaben, Zahlen,
Leerzeichen und Satzzeichen bestehen. Sie sind *unabhÃ¤ngig* von GroÃŸ-
und Kleinschreibung:

	[Link-Text][a]
	[Link-Text][A]

Die beiden Link-Definitionen sind Ã¤quivalent.

Der *implizierte Link-Bezeichner* erlaubt das Auslassen des
Link-Bezeichers. In diesem Fall wird der Link-Text als Bezeichner
verwendet. Es wird einfach ein leerer Satz eckige Klammern an den
Link-Text angefÃ¼gt:

	[Google][]

Dann wird der Link definiert:

	[Google]: http://google.com/

Da Link-Bezeichner Leerzeichen enthalten dÃ¼rfen, funktioniert diese
AbkÃ¼rzung sogar fÃ¼r mehrere WÃ¶rter im Link-Text:

	Besuchen Sie [Daring Fireball][] fÃ¼r weitere Informationen.

Dann wird der Link definiert:
	
	[Daring Fireball]: http://daringfireball.net/

Link-Definitionen kÃ¶nnen an einen beliebige Stelle im Markdown-Dokument
gemacht werden. Generell ist es eine gute Idee, sie nach dem Absatz zu
machen, in dem sie verwendet werden. Sie kÃ¶nnen aber auch wie FuÃŸnoten
alle zusammen am Ende des Dokuments aufgelistet werden.

Ein kleines Beispiel:

    Ich bekomme zehn Mal mehr Traffic von [Google] [1] als von
    [Yahoo] [2] oder [MSN] [3].

      [1]: http://google.com/        "Google"
      [2]: http://search.yahoo.com/  "Yahoo Search"
      [3]: http://search.msn.com/    "MSN Search"

Mit der AbkÃ¼rzung Ã¼ber den implizierten Link-Bezeichner kann man auch
folgendes Schreiben:

Ich bekomme zehn Mal mehr Traffic von [Google][] als von
    [Yahoo][] oder [MSN][].

      [google]: http://google.com/       "Google"
      [yahoo]: http://search.yahoo.com/  "Yahoo Search"
      [msn]: http://search.msn.com/      "MSN Search"

Beide Beispiele wÃ¼rden folgenden HTML-Code ergeben:

    <p>Ich bekomme zehn Mal mehr Traffic von <a href="http://google.com/"
    title="Google">Google</a> als von
    <a href="http://search.yahoo.com/" title="Yahoo Search">Yahoo</a>
    oder <a href="http://search.msn.com/" title="MSN Search">MSN</a>.</p>

Zum Vergleich folgt der gleiche Absatz unter Verwendung von Markdowns
Inline-Links:

    Ich bekomme zehn Mal mehr Traffic von [Google](http://google.com/ "Google")
    als von [Yahoo](http://search.yahoo.com/ "Yahoo Search") oder
    [MSN](http://search.msn.com/ "MSN Search").

Die Idee hinter Referenz-Links ist nicht, dass sie einfacher zu
schreiben sind. Die Idee ist, dass sie Dokumente weitaus lesbarer
machen. Der Beispiel-Absatz ist mit Referenz-Links nur 80 Zeichen lang,
mit Referenz-Links ist er ganze 181 Zeichen lang; als HTML sind es 239
Zeichen, mehr Markup als Inhalt.

Mit Markdowns Referenz-Links Ã¤hnelt das Quell-Dokument eher dem
endgÃ¼ltigen Ausgabeformat, wie es im Browser gezeigt wird. Durch die
MÃ¶glichkeit, Metadaten fÃ¼r Markup aus dem Absatz rauszuholen kÃ¶nnen
Links in den Text integriert werden ohne den Textfluss zu bremsen.



<h3 id="em">Betonung</h3>

Markdown behandelt Sternchen (`*`) und Unterstriche (`_`) als
Indikatoren fÃ¼r Betonung. In einzelne `*` oder `_` gepackter Text wird
mit dem HTML-Tag `<em>` umschlossen, doppelte `*` oder `_` werden mit
dem Tag `<strong>` markiert. Folgender Text zum Beispiel:

    *Einzelne Sternchen*

    _Einzelne Unterstriche_

    **Doppelte Sternchen**

    __Doppelte Unterstriche__

Wird folgendes ausgeben:

    <em>Einzelne Sternchen</em>

    <em>Einzelne Unterstriche</em>

    <strong>Doppelte Sternchen</strong>

    <strong>Doppelte Unterstriche</strong>

Der Stil kann beliebig gewÃ¤hlt werden. Die einzige Begrenzung ist, dass
das gleiche Zeichen zum Ã–ffnen und SchlieÃŸen eines Betonungs-Bereiches
verwendet werden muss.

Betonung kann inmitten eines Wortes verwendet werden:

    Herr*gott*sakrament

Aber wenn ein `*` oder `_` von Leerzeichen umgeben ist, wird es wie ein
einfaches Sternchen oder ein einfacher Unterstrich behandelt.

Um ein Sternchen oder einen Unterstrich zu schreiben an einer Stelle wo
er als Betonung verstanden wÃ¼rde, kann er mit einem Backslash maskiert
werden:

    \*Dieser Text ist von Sternchen umschlossen.\*



<h3 id="code">Code</h3>

Um einen Code-Bereich zu markieren, wird er mit Backtick-Zeichen
umschlossen (`` ` ``). Im Gegensatz zu einem Code-Block formatiert ein
Code-Bereich Code innerhalb eines normalen Absatzes:

    Benutze die Funktion `printf()` um Text auszugeben.

Wird zu:

    <p>Benutze die Funktion <code>printf()</code> um Text auszugeben.</p>

Wenn ein Backtick Im Code-Bereich dargestellt werden soll, dann kÃ¶nnen
mehrere Backticks vor und nach dem Code-Bereich verwendet werden:

    ``Irgendwo hier (`) ist ein Backtick versteckt.``

Dies wird zu:

    <p><code>irgendwo hier (`) ist ein Backtick versteckt.</code></p>

Die Backtick-Trennzeichen um einen Code-Bereich kÃ¶nnen Leerzeichen enthalten --
eins nach den Ã¶ffnenden, eins vor dem schlieÃŸenden Backticks. Dies ermÃ¶glicht
die Verwendung von Backticks innerhalb des Code-Bereiches auch am Anfang oder
Ende:

	Ein einzelner Backtick in einem Code-Bereich: `` ` ``

	Ein Backtick-umschlossener String in einem Code-Bereich: `` `foo` ``

wird zu:

	<p>Ein einzelner Backtick in einem Code-Bereich: <code>`</code></p>
	
	<p>Ein Backtick-umschlossener String in einem Code-Bereich: <code>`foo`</code></p>

In Code-Bereichen werden das kaufmÃ¤nnische Und so wie spitze Klammern
als HTML-Entitiy kodiert.

    Niemand benutzt `<blink>` Tags.

Dies wird zu:

    <p>Niemand benutzt <code>&lt;blink&gt;</code> Tags.</p>

Auch folgendes funktioniert:

    `&#8212;` ist das dezimal kodierte Ã„quivalent von `&mdash;`.

Dies wird zu

    <p><code>&amp;#8212;</code> ist das dezimal kodierte
    Ã„quivalent von <code>&amp;mdash;</code>.</p>



<h3 id="img">Grafiken</h3>

ZugegebenermaÃŸen ist es recht schwierig, eine "natÃ¼rliche" Syntax fÃ¼r
das Einbinden von Grafiken in Text zu finden.

Markdown verwendet hierzu eine Syntax, die dem Stil von Links Ã¤hneln
soll. Dies erlaubt zwei Arten: Inline und Referenz.

Die Inline-Syntax sieht so aus:

    ![Alternativer Text](/pfad/zum/bild.jpg)

    ![Alternativer Text](/pfad/zum/bild.jpg "Optionaler Titel")

Also:

*   Ein Ausrufezeichen: `!`;
*   gefolgt von einem Satz eckiger Klammern die den Wert des
    `alt`-Attributes fÃ¼r die Grafik enthalten;
*   gefolgt von runden Klammern, welche die URL oder den Pfad zur Grafik
    enthalten sowie den Wert eines optionalen `title`-Attributes, verpackt
    in AnfÃ¼hrungsstriche.

Bildverweise im Referenz-Stil sehen wie folgt aus:

    ![Alternativer Text][id]

"id" ist hier der Name einer definierten Bild-Referenz. Bild-Referenzen
werden mit dem gleicher Syntax definiert wie Link-Referenzen:

    [id]: url/zur/grafik  "Optionales title-Attribut"

Zur Zeit besitzt Markdown keine Syntax um die GrÃ¶ÃŸe einer Grafik
anzugeben. Sollte dies nÃ¶tig sein kann einfach der normale HTML-Tag
`<img>` verwendet werden.



* * *



<h2 id="misc">Verschiedenes</h2>

<h3 id="autolink">Automatische Links</h3>

Markdown unterstÃ¼tzt einen einfachen Stil um "automatisch" Links fÃ¼r
URLs und eMail-Adressen zu generieren: Die URL oder eMail-Adresse wird
einfach von spitzen Klammern umschlossen. Wenn man eine URL oder
Mailadresse direkt zeigen will, kann man dies  so lÃ¶sen:

    <http://example.com/>
    
Markdown wird dies in den folgenden HTML-Code umsetzen:

    <a href="http://example.com/">http://example.com/</a>

Automatische Links fÃ¼r eMail-Adressen funktionieren Ã¤hnlich, nur das
Markdown hier per Zufallsprinzip die Adresse in dezimale und
hexadezimale Entity-Kodierung umwandelt um dabei zu helfen, die Adresse
fÃ¼r Spambots unzugÃ¤nglich zu machen. Zum Beispiel:

    <address@example.com>

Dies wird zu so etwas wie dem Folgenden:

    <a href="&#x6D;&#x61;i&#x6C;&#x74;&#x6F;:&#x61;&#x64;&#x64;&#x72;&#x65;
    &#115;&#115;&#64;&#101;&#120;&#x61;&#109;&#x70;&#x6C;e&#x2E;&#99;&#111;
    &#109;">&#x61;&#x64;&#x64;&#x72;&#x65;&#115;&#115;&#64;&#101;&#120;&#x61;
    &#109;&#x70;&#x6C;e&#x2E;&#99;&#111;&#109;</a>

Im Browser wird dies als klickbarer Link zur Adresse
"address@example.com" erscheinen.

(Dieser Trick wird viele, wenn nicht die meisten, Adressen-Sammler
abschmettern. Aber er wird definitiv nicht alle erwischen. Es ist besser
als nichts, aber irgendwann wird eine so publizierte Adresse auch Spam
zugesandt bekommen.)



<h3 id="backslash">Backslash-Maskierung</h3>

Markdown ermÃ¶glicht es, Backslash-Maskierung zu nutzen um Zeichen zu
schreiben, die sonst eine bestimmte Bedeutung in Markdowns Syntax haben.
Wenn man zum Beispiel ein Wort mit Sternchen umgeben will (statt dem
HTML-Tag `<em>`), kann man Backslashes vor die Sternchen stellen:

    \*Von Sternchen umgeben\*

Markdown bietet diese MÃ¶glichkeit fÃ¼r folgende Zeichen:

    \   Backslash
    `   Backtick
    *   Sternchen
    _   Unterstrich
    {}  Geschweifte Klammern
    []  Eckige Klammern
    ()  Runde Klammern
    #   Raute
    +	Plus-Zeichen
    -	Minus-Zeichen (Bindestrich)
    .   Punkt
    !   Ausrufezeichen

<h2 id="interna">Interna</h2>

<h3 id="versionen">Versionen</h3>

- v1, 2005-06-23: Erste Fassung.
- v2, 2005-06-24: Korrekturen und Formatierung.
- v3, Datum unbekannt: Korrekturen.
- v4, 2006-02-13: Ãœberarbeitung auf Basis einer neuen Version des englischen Originals.
- v5, 2010-12-10: Korrekturen.
- v6, 2013-12-15: Einbindung von Ã„nderungen im Englischen Original, Text-Korrekturen (Dank an Wolf Peuker), Erweiterung um Lizenz-Informationen (Dank an Lilo von Hanffstengel) und Versionshistorie.

<h3 id="lizenz">Lizenz</h3>

Dieses Werk ist lizenziert unter einer [Creative Commons Namensnennung -
Weitergabe unter gleichen Bedingungen (BY-SA) 4.0 International Lizenz][by-sa].

[by-sa]: http://creativecommons.org/licenses/by-sa/4.0/deed.de
