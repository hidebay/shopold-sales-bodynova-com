require 'compass/import-once/activate'
# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "code/css"
sass_dir = "code/sass"
images_dir = "code/images"
javascripts_dir = "code/scripts"
fonts_dir = "code/fonts"

output_style = :nested

relative_assets = true

line_comments = false
color_output = false

preferred_syntax = :sass
