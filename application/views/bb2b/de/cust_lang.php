<?php
/**
 * This file is part of OXID eShop Community Edition.
 *
 * OXID eShop Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OXID eShop Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OXID eShop Community Edition.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2014
 * @version   OXID eShop CE
 */

$sLangName  = "Deutsch";
// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = array(
	'charset'                       => 'UTF-8',//'ISO-8859-15',
	'BTN_MY_FAVORITES'              => 'Meine Favoriten',
	'BTN_MYFAVORITES_LISTS'         => 'Meine Favoriten',
	'MYFAVORITES_LISTS_DESC'        => 'Verwalten Sie Ihre Favoriten-Artikel',
	'TO_THE_BASKET'                 => 'In den Warenkorb',
	'SHOW_OPTIONS'                  => 'Varianten anzeigen',
	'ADD_TO_FAVORITE'               => 'Zur Favoritenliste hinzuf&uuml;gen',
	'ADDED_TO_BASKET_CONFIRMATION'  => 'Artikel zum Warenkorb hinzugef&uuml;gt',
	'OPEN_ALL_VARIANTS'             => 'Alle Varianten &ouml;ffnen',
    'SHOW_ARTICLE_DETAILS'          => 'Zeige Artikel Details',
    'AlleArtikel'                   => 'Alle Artikel anzeigen',
    'TOTAL_WEIGHT'                  => 'Gesamtgewicht',
	'ACCOUNT_LOGIN_REQUIRE'         =>  'Sie m&uuml;ssen eingeloggt sein, um den H&auml;ndlershop nutzen zu k&ouml;nnen.',
	'Modal_Title'                   =>  'Artikel Details',
	'THANK_YOU_MESSAGE'             =>  'Vielen Dank f&uuml;r Ihre Nachricht. Wir melden uns schnellstm&ouml;glich bei Ihnen.',
	'AGB_LINK_TITLE'                =>  'AGB',
	'IMPRINT_LINK_TITLE'            =>  'Impressum',
	'BTN_MY_ORDERS'                 =>  'Meine Bestellungen',
	'STREET'                        =>  'Stra&szlig;e',
	'STREETNO'                      =>  'Hausnummer',
	'CITY'                          =>  'Stadt',
	'DATEI'							=>	'Lieferschein anh&auml;ngen',
	'SUBMITFILE'					=>	'hochladen',
	'ALLTOBASKET'					=>	'Alle Artikel in den Warenkorb',
	'RESET_AMOUNT'					=>	'Auswahl zur&uuml;cksetzen',
	'CA'							=>	'ca.',
/* TOOLTIPS */
	'TIP_ALLTOBASKET'				=>	'F&uuml;gt alle zuvor ausgew&auml;hlten Artikel dem Warenkorb hinzu',
	'TIP_SHOW_OPTIONS'				=>	'Zeigt alle verf&uuml;gbaren Varianten des Artikels an',
	'TIP_TO_THE_BASKET'				=>	'Legt die gew&auml;hlte Artikel-Anzahl in den Warenkorb',
	'TIP_MY_ORDERS'					=>	'Schauen Sie sich Ihre bisherigen Bestellungen an und legen die Bestellung einfach erneut in den Warenkorb',
	'TIP_MY_FAVORITES'				=>	'Schauen Sie sich Ihre Artikel-Favoriten an',
	'TIP_QUICKORDER'				=>	'F&uuml;hren Sie White-Label Bestellungen f&uuml;r Ihre Kunden aus',
	'TIP_RESET_AMOUNT'				=>	'Setzt die vorausgew&auml;hlten Artikel-Mengen zur&uuml;ck',
/* Ende TOOLTIPS */
	'PLEASE_CHANGE_PASSWORD'		=>	'Bitte &auml;ndern Sie zu Ihrer Sicherheit Ihr Passwort!',

	'LOGGEDOUT_HEADLINE'			=>	'Automatischer Logout',
    'LOGGEDOUT_TEXT'				=>	'Sie wurden aufgrund l&auml;ngerer Inaktivit&auml;t automatisch ausgeloggt. ',
    'LOGGEDOUT_BUTTON'				=>	'Neu einloggen',
	'MESSAGE_NOT_ON_STOCK'			=>	'',
	'LOW_STOCK'						=>	'',
	'READY_FOR_SHIPPING'			=>	'',
	'WK_TEXT'						=>	'Die Versandkosten werden nach dem Packen der Ware hinzugef&uuml;gt. Wir geben uns gro&szlig;e M&uuml;he, Ihre Sendung so zu packen dass die Versandkosten so kosteng&uuml;nstig wie m&ouml;glich ausfallen und berechnen nur die tats&auml;chlich anfallenden Kosten.',
	'DELTIMEUNIT'					=>	'Tage',
    'DELIVERYTIME_DELIVERYTIME'     =>  'Produktionszeit',
/* Quickorder.tpl */
    'BTN_CLOSE'                     =>  'Schließen',
    'BTN_OK'                        =>  'OK',
    'MODAL_ORDER_COMPLETE_TITLE'    =>  'Bestellung Abgeschlossen',
    'MODAL_ORDER_COMPLETE_BODY'     =>  'Ihre Bestellung wurde ausgeführt.',
    'MODAL_ORDER_PREVIEW_TITLE'     =>  'Bestellung Vorschau',
    'MODAL_ORDER_PREVIEW_BODY'      =>  'Die Vorschau zu ihrer Bestellung.',
    'MODAL_ORDER_BTN_CHANGE'        =>  'Bestellung Ändern',
    'QUICKORDER_BTN_ORDERMANDANTORY'=>  'Verbindlich bestellen',
    'QUICKORDER_ARTICLE_DESC'       =>  'Artikel, Beschreibung, o.&Auml;.',
    'BTN_QUICKORDER'                =>  'Direktversand',
    'QUICKORDER_DESC'               =>  'Hier können Sie eine Direktversand Bestellung aufgeben und diese auf Wunsch dem Kunden in neutraler Versand',
    'SUBMIT_QUICK_ORDER'            =>  'Bestellung absenden',
    'QUICKORDER_PLACEHOLDER_SEARCHFIELD'    =>  'Artikelnummer oder Beschreibung',
    'SITE_TITLE'                    =>  'Positionen',
    'QUICKORDER_ARTNUM'             =>  'Artikelnummer',
    'QUICKORDER_ARTICLE'            =>  'Artikel',
    'QUICKORDER_SINGLE_PRICE'       =>  'Einzelpreis',
    'QUICKORDER_COUNT'              =>  'Anzahl',
    'QUICKORDER_AVAILABILITY'       =>  'Verfügbarkeit',
    'QUICKORDER_WHITE_LABEL'        =>  'Neutrale Verpackung',
    'QUICKORDER_SEARCH_CONTACTS'    =>  'Adressbuch durchsuchen',
    'QUICKORDER_PDF_TXT'            =>  'Wählen Sie ein PDF, welches wir der Bestellung als Ihren Lieferschein beilegen dürfen:',
/* Ende Quickorder */
    'ERROR'                         =>  'Fehler',
    'UVP'                           =>  'UVP',
    'TAGE'                          =>  'Tage',

    'NOTHING_ADDED_TO_BASKET_TITLE'     =>  'Hinweis',
    'NOTHING_ADDED_TO_BASKET_TEXT'      =>  'Es wurden keine Artikel in den Warenkorb gelegt!',
    'NOTHING_ADDED_TO_BASKET_CLOSEBTN'  =>  'Schlie&szlig;en',
    'AUTOSUGGEST_DIDYOUMEAN'            =>  'Meinten sie vielleicht: ',
	'PRINT_PDF'                         =>  'PDF erstellen',
	'ALLENEWS'                          =>  'Alle News',
	'ALLENEWSZEIGEN'                    =>  'Alle News anzeigen',
);
/*
    ''                              =>  '',
[{oxmultilang ident=""}]
*/
