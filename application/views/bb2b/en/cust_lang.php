<?php
/**
 * This file is part of OXID eShop Community Edition.
 *
 * OXID eShop Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OXID eShop Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OXID eShop Community Edition.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2014
 * @version   OXID eShop CE
 */

$sLangName  = "Deutsch";
// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = array(
	'charset'                       =>	'UTF-8',//'ISO-8859-15',
	'BTN_MY_FAVORITES'              =>	'My Favorites',
	'BTN_MYFAVORITES_LISTS'         =>	'My Favorites',
	'MYFAVORITES_LISTS_DESC'        =>	'Manage your favorites',
	'TO_THE_BASKET'                 =>	'Add to basket',
	'SHOW_OPTIONS'                  =>	'Show options',
	'ADD_TO_FAVORITE'               =>	'Add to favorites',
	'ADDED_TO_BASKET_CONFIRMATION'  =>	'Article was added to basket',
	'OPEN_ALL_VARIANTS'             =>	'Open all options',
    'SHOW_ARTICLE_DETAILS'          =>	'Show details',
    'AlleArtikel'                   =>	'Show all articles',
    'TOTAL_WEIGHT'                  =>	'Total weight',
	'ACCOUNT_LOGIN_REQUIRE'         =>	'You must be logged in to use this site.',
	'Modal_Title'                   =>	'Article details',
	'THANK_YOU_MESSAGE'             =>	'Thank you for your message. We will contact you asap.',
	'AGB_LINK_TITLE'                =>	'AGB',
	'IMPRINT_LINK_TITLE'            =>	'Impressum',
	'BTN_MY_ORDERS'                 =>	'My orders',
	'STREET'                        =>	'Street',
	'STREETNO'                      =>	'House number',
	'CITY'                          =>	'City',
	'DATEI'							=>	'Attach delivery slip',
	'SUBMITFILE'					=>	'Upload',
	'ALLTOBASKET'					=>	'All articles in the basket.',
	'RESET_AMOUNT'					=>	'Reset amount',
	'CA'							=>	'ca.',
/* TOOLTIPS */
	'TIP_ALLTOBASKET'				=>	'Add all selected articles to basket',
	'TIP_SHOW_OPTIONS'				=>	'Show all available options',
	'TIP_TO_THE_BASKET'				=>	'Add to basket',
	'TIP_MY_ORDERS'					=>	'Here you can see your previous orders and add to basket',
	'TIP_MY_FAVORITES'				=>	'See your favorites',
	'TIP_QUICKORDER'				=>	'Direct, neutral delivery to your customer.',
	'TIP_RESET_AMOUNT'				=>	'Reset amount',
/* Ende TOOLTIPS */
	'PLEASE_CHANGE_PASSWORD'		=>	'Please change your password as a security precaution.',
	
	'LOGGEDOUT_HEADLINE'			=>	'Automatic logout',
    'LOGGEDOUT_TEXT'				=>	'Logged out due to inactivity',
    'LOGGEDOUT_BUTTON'				=>	'Login again',
	'MESSAGE_NOT_ON_STOCK'			=>	'',
	'LOW_STOCK'						=>	'',
	'READY_FOR_SHIPPING'			=>	'',
	'WK_TEXT'						=>	'Shipping costs will be added after packing. We do our best to keep the shipping costs as low as possible and charge only the actual shipping costs incurred.',
	'DELTIMEUNIT'					=>	'Day',
    'DELIVERYTIME_DELIVERYTIME'     =>  'Production time',
/* Quickorder.tpl */
    'BTN_CLOSE'                     =>  'Close',
    'BTN_OK'                        =>  'OK',
    'MODAL_ORDER_COMPLETE_TITLE'    =>  'Order complete',
    'MODAL_ORDER_COMPLETE_BODY'     =>  'Your order has been successfully completed.',
    'MODAL_ORDER_PREVIEW_TITLE'     =>  'Order preview',
    'MODAL_ORDER_PREVIEW_BODY'      =>  'The preview of your order.',
    'MODAL_ORDER_BTN_CHANGE'        =>  'Change order',
    'QUICKORDER_BTN_ORDERMANDANTORY'=>  'Order now',
    'QUICKORDER_ARTICLE_DESC'       =>  'Article description',
    'BTN_QUICKORDER'                =>  'Direct, neutral shipping',
    'SUBMIT_QUICK_ORDER'            =>  'Submit order',
    'QUICKORDER_PLACEHOLDER_SEARCHFIELD'    =>  'Article number or description',
    'SITE_TITLE'                    =>  'Article',
    'QUICKORDER_ARTNUM'             =>  'Article number',
    'QUICKORDER_ARTICLE'            =>  'Article',
    'QUICKORDER_SINGLE_PRICE'       =>  'Price',
    'QUICKORDER_COUNT'              =>  'Amount',
    'QUICKORDER_AVAILABILITY'       =>  'Availability',
    'QUICKORDER_WHITE_LABEL'        =>  'Neutral shipping',
    'QUICKORDER_SEARCH_CONTACTS'    =>  'Search contacts',
    'QUICKORDER_PDF_TXT'            =>  'Send us your delivery slip(PDF) for this order',
    'UVP'                           =>  'MSRP',
    'TAGE'                          =>  'days',
    'NOTHING_ADDED_TO_BASKET_TITLE'     =>  'Hinweis',
    'NOTHING_ADDED_TO_BASKET_TEXT'      =>  'Nothing has been added to the shopping cart!',
    'NOTHING_ADDED_TO_BASKET_CLOSEBTN'  =>  'Close',
    'AUTOSUGGEST_DIDYOUMEAN'            =>  'Did you mean perhaps: ',
	'PRINT_PDF'                         =>  'Create PDF',
	'ALLENEWS'                          =>  'all News',
	'ALLENEWSZEIGEN'                    =>  'Show all news',
/* Ende Quickorder */
);
/*
    ''                              =>  '',
[{oxmultilang ident=""}]
*/
