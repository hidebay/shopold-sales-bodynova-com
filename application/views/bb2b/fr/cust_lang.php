﻿<?php
/**
 * This file is part of OXID eShop Community Edition.
 *
 * OXID eShop Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OXID eShop Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OXID eShop Community Edition.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2014
 * @version   OXID eShop CE
 */

$sLangName  = "Deutsch";
// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = array(
	'charset'                       =>	'UTF-8',//'ISO-8859-15',
	'BTN_MY_FAVORITES'              =>	'Mes favoris',
	'BTN_MYFAVORITES_LISTS'         =>	'Mes favoris',
	'MYFAVORITES_LISTS_DESC'        =>	'Gérez vos articles favoris',
	'TO_THE_BASKET'                 =>	'Dans le panier',
	'SHOW_OPTIONS'                  =>	'Montrer les variantes',
	'ADD_TO_FAVORITE'               =>	'Ajouter à la liste des favoris',
	'ADDED_TO_BASKET_CONFIRMATION'  =>	'Ajouter l\'article au panier',
	'OPEN_ALL_VARIANTS'             =>	'Montrer toutes les variantes',
    'SHOW_ARTICLE_DETAILS'          =>	'Montrer les détails articles',
    'AlleArtikel'                   =>	'Montrer tous les articles',
    'TOTAL_WEIGHT'                  =>	'Poids total',
	'ACCOUNT_LOGIN_REQUIRE'         =>	'Vous devez être identifié pour utiliser le shop revendeur.',
	'Modal_Title'                   =>	'Détails article',
	'THANK_YOU_MESSAGE'             =>	'Merci pour votre message. Nous reviendrons vers vous dans les meilleurs délais.',
	'AGB_LINK_TITLE'                =>	'CGV',
	'IMPRINT_LINK_TITLE'            =>	'Mentions légales',
	'BTN_MY_ORDERS'                 =>	'Mes commandes',
	'STREET'                        =>	'Rue',
	'STREETNO'                      =>	'Numéro de rue',
	'CITY'                          =>	'Ville',
	'DATEI'							=>	'Attacher le bon de livraison',
	'SUBMITFILE'					=>	'Télécharger',
	'ALLTOBASKET'					=>	'Tous les articles dans le panier',
	'RESET_AMOUNT'					=>	'Réinitialiser la sélection',
	'CA'							=>	'env.',
/* TOOLTIPS */
	'TIP_ALLTOBASKET'				=>	'Ajoute tous les articles présélectionnés dans le panier',
	'TIP_SHOW_OPTIONS'				=>	'Montre toutes les variantes disponibles de l\'article',
	'TIP_TO_THE_BASKET'				=>	'Met la quantité choisie d\'articles dans le panier',
	'TIP_MY_ORDERS'					=>	'Consultez vos commandes précédentes et reprenez la commande à nouveau dans le panier',
	'TIP_MY_FAVORITES'				=>	'Consultez vos articles favoris',
	'TIP_QUICKORDER'				=>	'Passez des commandes avec bon de livraison "neutre" pour vos clients',
	'TIP_RESET_AMOUNT'				=>	'Réinitialiser les quantités pré-choisies d\'articles',
/* Ende TOOLTIPS */
	'PLEASE_CHANGE_PASSWORD'		=>	'Pour votre sécurité, merci de modifier votre mot de passe !',
	
	'LOGGEDOUT_HEADLINE'			=>	'Déconnexion automatique',
    'LOGGEDOUT_TEXT'				=>	'Vous avez été déconnecté automatiquement pour cause d\'inactivité prolongée. ',
    'LOGGEDOUT_BUTTON'				=>	'Vous reconnecter',
	'MESSAGE_NOT_ON_STOCK'			=>	'',
	'LOW_STOCK'						=>	'',
	'READY_FOR_SHIPPING'			=>	'',
	'WK_TEXT'						=>	'Les frais d\'envoi seront ajoutés après emballage de la marchandise. Nous faisons tout notre possible pour emballer votre envoi en maintenant les frais de port les plus bas possibles et nous ne vous facturons que les frais réels.',
	'DELTIMEUNIT'					=>	'Jours',
    'DELIVERYTIME_DELIVERYTIME'     =>  'Délai :',
/* Quickorder.tpl */
    'BTN_CLOSE'                     =>  'Fermer',
    'BTN_OK'                        =>  'OK',
    'MODAL_ORDER_COMPLETE_TITLE'    =>  'Commande terminée',
    'MODAL_ORDER_COMPLETE_BODY'     =>  'Votre commande a été prise en compte.',
    'MODAL_ORDER_PREVIEW_TITLE'     =>  'Aperçu de commande',
    'MODAL_ORDER_PREVIEW_BODY'      =>  'L\'aperçu de votre commande.',
    'MODAL_ORDER_BTN_CHANGE'        =>  'Modifier la commande',
    'QUICKORDER_BTN_ORDERMANDANTORY'=>  'Commande définitive',
    'QUICKORDER_ARTICLE_DESC'       =>  'Article, description, etc.',
    'BTN_QUICKORDER'                =>  'Envoi direct',
    'SUBMIT_QUICK_ORDER'            =>  'Envoyer la commande',
    'QUICKORDER_PLACEHOLDER_SEARCHFIELD'    =>  'Numéro d\'article ou description',
    'SITE_TITLE'                    =>  'Article',
    'QUICKORDER_ARTNUM'             =>  'Numéro d\'article',
    'QUICKORDER_ARTICLE'            =>  'Article',
    'QUICKORDER_SINGLE_PRICE'       =>  'Prix unitaire',
    'QUICKORDER_COUNT'              =>  'Quantité',
    'QUICKORDER_AVAILABILITY'       =>  'Disponibilité',
    'QUICKORDER_WHITE_LABEL'        =>  'Emballage "neutre"',
    'QUICKORDER_SEARCH_CONTACTS'    =>  'Parcourir le carnet d\'adresses',
    'QUICKORDER_PDF_TXT'            =>  'Envoyez nous votre bon de livraison (PDF) pour cette commande :',
    'UVP'                           =>  'prix conseilllé',
    'TAGE'                          =>  'jours',
    'NOTHING_ADDED_TO_BASKET_TITLE'     =>  'Information',
    'NOTHING_ADDED_TO_BASKET_TEXT'      =>  'Aucun produit n\'a été mis dans votre panier!',
    'NOTHING_ADDED_TO_BASKET_CLOSEBTN'  =>  'Fermer',
    'AUTOSUGGEST_DIDYOUMEAN'            =>  'Vouliez-vous dire éventuellement : ',
	'PRINT_PDF'                         =>  'Créer le PDF',
	'ALLENEWS'                          =>  'Tous les messages',
	'ALLENEWSZEIGEN'                    =>  'Montrer tous les messages',
/* Ende Quickorder */
);
/*
    ''                              =>  '',
[{oxmultilang ident=""}]
*/
