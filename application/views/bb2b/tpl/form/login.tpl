[{oxscript include="js/widgets/oxinputvalidator.js" priority=10 }]
[{oxscript add="$('form.js-oxValidate').oxInputValidator();"}]
<form class="js-oxValidate" name="login" action="[{ $oViewConf->getSslSelfLink()}]" method="post">
    [{assign var="aErrors" value=$oView->getFieldValidationErrors()}]
    <div class="form-horizontal">
        <div class="form-group [{if $aErrors}]oxInValid[{/if}]">
            [{$oViewConf->getHiddenSid() }]
            [{$oViewConf->getNavFormParams() }]
            <input type="hidden" name="fnc" value="login_noredirect">
            <input type="hidden" name="cl" value="[{ $oViewConf->getActiveClassName() }]">
            <label class="control-label col-sm-2 short">[{ oxmultilang ident="EMAIL_ADDRESS" }]</label>

            <div class="col-sm-10">
                <input type="text" name="lgn_usr" class="form-control textbox js-oxValidate js-oxValidate_notEmpty"
                       data-fieldsize="pair-xsmall">

                <p class="underInput short oxValidateError">
                    <span class="js-oxError_notEmpty">[{oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                </p>
            </div>
        </div>
        <div class="form-group [{if $aErrors}]oxInValid[{/if}]">
            <label class="control-label short">[{ oxmultilang ident="PASSWORD" }]</label>

            <div class="col-sm-10">
                <input type="password" name="lgn_pwd"
                       class="js-oxValidate js-oxValidate_notEmpty textbox stepsPasswordbox"
                       data-fieldsize="pair-xsmall">
                &nbsp;<strong><a class="forgotPasswordOpener" id="step2PswdOpener" href="#"
                                 title="[{ oxmultilang ident="
                                 FORGOT_PASSWORD" }]">?</a></strong>

                <p class="underInput short oxValidateError">
                    <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                </p>
            </div>
        </div>



        <button type="submit" class="submitButton">[{ oxmultilang ident="LOGIN" }]</button>
    </div>
    </div>
</form>
