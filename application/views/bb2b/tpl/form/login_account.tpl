
<form name="login" action="[{ $oViewConf->getSslSelfLink() }]" method="post" role="form">
	<div>
		[{ $oViewConf->getHiddenSid() }]
		[{ $oViewConf->getNavFormParams() }]
		<input type="hidden" name="fnc" value="login_noredirect">
		<input type="hidden" name="cl" value="[{ $oViewConf->getActiveClassName() }]">
		<input type="hidden" name="tpl" value="[{$oViewConf->getActTplName()}]">
		<input type="hidden" name="oxloadid" value="[{$oViewConf->getActContentLoadId()}]">
		[{if $oView->getArticleId()}]
		  <input type="hidden" name="aid" value="[{$oView->getArticleId()}]">
		[{/if}]
		[{if $oView->getProduct()}]
		  [{assign var="product" value=$oView->getProduct() }]
		  <input type="hidden" name="anid" value="[{ $product->oxarticles__oxnid->value }]">
		[{/if}]
	</div>

	<div class="form-group [{if $aErrors}]has-error[{/if}]">
		<label class="req col-sm-2 control-label">[{ oxmultilang ident="EMAIL" suffix="COLON" }]</label>
		<div class="col-sm-10">
			<input type="email" id="loginUser" name="lgn_usr" class="form-control" placeholder="">
		</div>
	</div>

	<div class="form-group [{if $aErrors}]has-error[{/if}]">
		<label class="req col-sm-2 control-label">[{ oxmultilang ident="PASSWORD" suffix="COLON" }]</label>
		<div class="col-sm-10">
			<input type="password" id="loginPwd" name="lgn_pwd" class="form-control" placeholder="">
		</div>
	</div>

	[{if $oView->showRememberMe()}]
		<div class="form-group">
			<label class="req col-sm-2 control-label">[{ oxmultilang ident="KEEP_LOGGED_IN" suffix="COLON" }]</label>
			<div class="col-sm-10">
				<input id="loginCookie" type="checkbox" class="checkbox" name="lgn_cook" value="1">
			</div>
		</div>
	[{/if}]
	<br><br>
	<div class="form-group" style="margin-top:15px;">
		<div class="col-sm-offset-2 col-sm-10">
			<button id="loginButton" type="submit" class="btn btn-primary submitButton largeButton">[{ oxmultilang ident="LOGIN" }]</button>
		</div>
	</div>
</form>

<div class="clear"></div>

<!--<a id="openAccountLink" href="[{ oxgetseourl ident=$oViewConf->getSslSelfLink()|cat:"cl=register" }]" class="textLink" rel="nofollow">[{ oxmultilang ident="OPEN_ACCOUNT" }]</a><br />-->
<a id="forgotPasswordLink" href="[{ oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=forgotpwd" }]" class="textLink" rel="nofollow">[{ oxmultilang ident="FORGOT_PASSWORD" }]</a>


