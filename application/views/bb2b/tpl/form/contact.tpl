[{assign var="editval" value=$oView->getUserData() }]
[{oxscript include="js/widgets/oxinputvalidator.js" priority=10 }]
[{oxscript add="$('form.js-oxValidate').oxInputValidator();"}]
<form role="form" class="js-oxValidate form-horizontal" action="[{ $oViewConf->getSslSelfLink() }]" method="post">

    [{ $oViewConf->getHiddenSid() }]
    <input type="hidden" name="fnc" value="send"/>
    <input type="hidden" name="cl" value="contact"/>
    [{assign var="oCaptcha" value=$oView->getCaptcha() }]
    <input type="hidden" name="c_mach" value="[{$oCaptcha->getHash()}]"/>

    [{*}]
    <div class="form-group">
        <label class="col-sm-2 control-label">[{ oxmultilang ident="TITLE" }]</label>
        <div class="col-sm-10">
            [{if $oxcmp_user && !$editval.oxuser__oxsal}]
                [{include file="form/fieldset/salutation.tpl" name="editval[oxuser__oxsal]" value=$oxcmp_user->oxuser__oxsal->value }]
            [{else}]
                [{include file="form/fieldset/salutation.tpl" name="editval[oxuser__oxsal]" value=$editval.oxuser__oxsal }]
            [{/if}]
        </div>
    </div>
    [{*}]
    <div class="form-group [{if $aErrors.oxuser__oxfname}]class="oxInValid"[{/if}]">
        <label class="req col-sm-2 control-label">[{ oxmultilang ident="FIRST_NAME" suffix="COLON" }]</label>
        <div class="col-sm-10">
            <input type="text" name="editval[oxuser__oxfname]" size="70" maxlength="40" value="[{if $oxcmp_user && !$editval.oxuser__oxfname}][{$oxcmp_user->oxuser__oxfname->value}][{else}][{$editval.oxuser__oxfname}][{/if}]" class="js-oxValidate js-oxValidate_notEmpty form-control">
        </div>
        <p class="oxValidateError bg-warning">
            <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
            [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxuser__oxfname}]
        </p>
    </div>
    <div class="form-group [{if $aErrors.oxuser__oxlname}]class="oxInValid"[{/if}]">
        <label class="req col-sm-2 control-label">
            [{ oxmultilang ident="LAST_NAME" }]
        </label>
        <div class="col-sm-10">
            <input type="text" name="editval[oxuser__oxlname]" size=70 maxlength=40 value="[{if $oxcmp_user && !$editval.oxuser__oxlname}][{$oxcmp_user->oxuser__oxlname->value}][{else}][{$editval.oxuser__oxlname}][{/if}]" class="js-oxValidate js-oxValidate_notEmpty form-control">
        </div>
        <p class="oxValidateError bg-warning">
            <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
        </p>
    </div>
    <div class="form-group">
        <label class="req col-sm-2 control-label">[{ oxmultilang ident="EMAIL" suffix="COLON" }]</label>
        <div class="col-sm-10">
            <input id="contactEmail" type="text" name="editval[oxuser__oxusername]"  size=70 maxlength=40 value="[{if $oxcmp_user && !$editval.oxuser__oxusername}][{$oxcmp_user->oxuser__oxusername->value}][{else}][{$editval.oxuser__oxusername}][{/if}]" class="js-oxValidate js-oxValidate_notEmpty js-oxValidate_email form-control">
        </div>
        <p class="oxValidateError bg-warning">
            <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
            <span class="js-oxError_email">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOVALIDEMAIL" }]</span>
        </p>
    </div>
    <div class="form-group">
        <label class="req col-sm-2 control-label">[{ oxmultilang ident="SUBJECT" suffix="COLON" }]</label>
        <div class="col-sm-10">
            <input type="text" name="c_subject" size="70" maxlength=80 value="[{$oView->getContactSubject()}]" class="js-oxValidate js-oxValidate_notEmpty form-control">
        </div>
         <p class="oxValidateError bg-warning">
            <span class="js-oxError_notEmpty">[{oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
        </p>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">[{oxmultilang ident="MESSAGE" suffix="COLON" }]</label>
        <div class="col-sm-10">
            <textarea rows="15" cols="70" name="c_message" class="areabox form-control">[{$oView->getContactMessage()}]</textarea>
        </div>
    </div>
    <div class="form-group verify">
        <label class="req col-sm-2 control-label">[{ oxmultilang ident="VERIFICATION_CODE" suffix="COLON" }]</label>
        [{assign var="oCaptcha" value=$oView->getCaptcha() }]
        [{if $oCaptcha->isImageVisible()}]
            <div class="col-sm-10">
                <img src="[{$oCaptcha->getImageUrl()}]" alt="">
            </div>
        [{else}]
            <div class="col-sm-10">
                <span class="verificationCode" id="verifyTextCode">[{$oCaptcha->getText()}]</span>
            </div>
        [{/if}]
        <div class="col-sm-10">
            <input type="text" data-fieldsize="verify" name="c_mac" value="" class="js-oxValidate js-oxValidate_notEmpty form-control">
        </div>
        <p class="oxValidateError bg-warning">
            <span class="js-oxError_notEmpty">[{oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS"}]</span>
        </p>
    </div>
    <div class="form-group formNote">
        <label class="col-sm-2"></label>
        <div class="col-sm-10">
            [{oxmultilang ident="COMPLETE_MARKED_FIELDS"}]
            <button class="submitButton largeButton btn btn-success pull-right" type="submit">[{oxmultilang ident="SEND"}]</button>
        </div>
    </div>
</form>
