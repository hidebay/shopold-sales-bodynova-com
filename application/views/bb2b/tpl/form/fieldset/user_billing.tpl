[{assign var="invadr" value=$oView->getInvoiceAddress()}]
[{assign var="blBirthdayRequired" value=$oView->isFieldRequired(oxuser__oxbirthdate)}]

[{if isset( $invadr.oxuser__oxbirthdate.month ) }]
    [{assign var="iBirthdayMonth" value=$invadr.oxuser__oxbirthdate.month }]
[{elseif $oxcmp_user->oxuser__oxbirthdate->value && $oxcmp_user->oxuser__oxbirthdate->value != "0000-00-00" }]
    [{assign var="iBirthdayMonth" value=$oxcmp_user->oxuser__oxbirthdate->value|regex_replace:"/^([0-9]{4})[-]/":""|regex_replace:"/[-]([0-9]{1,2})$/":"" }]
[{else}]
    [{assign var="iBirthdayMonth" value=0}]
[{/if}]

[{if isset( $invadr.oxuser__oxbirthdate.day ) }]
    [{assign var="iBirthdayDay" value=$invadr.oxuser__oxbirthdate.day}]
[{elseif $oxcmp_user->oxuser__oxbirthdate->value && $oxcmp_user->oxuser__oxbirthdate->value != "0000-00-00"}]
    [{assign var="iBirthdayDay" value=$oxcmp_user->oxuser__oxbirthdate->value|regex_replace:"/^([0-9]{4})[-]([0-9]{1,2})[-]/":"" }]
[{else}]
    [{assign var="iBirthdayDay" value=0}]
[{/if}]

[{if isset( $invadr.oxuser__oxbirthdate.year ) }]
    [{assign var="iBirthdayYear" value=$invadr.oxuser__oxbirthdate.year }]
[{elseif $oxcmp_user->oxuser__oxbirthdate->value && $oxcmp_user->oxuser__oxbirthdate->value != "0000-00-00" }]
    [{assign var="iBirthdayYear" value=$oxcmp_user->oxuser__oxbirthdate->value|regex_replace:"/[-]([0-9]{1,2})[-]([0-9]{1,2})$/":"" }]
[{else}]
    [{assign var="iBirthdayYear" value=0}]
[{/if}]



<div class="form-horizontal">

    [{* Anrede  soll weg !!kein Pflichtfeld!! *}]
    [{*}]
    <div class="form-group ">
        <label class="col-sm-2 control-label [{if $oView->isFieldRequired(oxuser__oxsal)}]req [{else}]norm [{/if}]">[{ oxmultilang ident="TITLE" suffix="COLON" }]</label>
        <div class="col-sm-10">
            [{include file="form/fieldset/salutation.tpl" name="invadr[oxuser__oxsal]" value=$oxcmp_user->oxuser__oxsal->value }]
        </div>
    </div>
    [{*}]

    [{* Vorname soll weg, wenn Firma gefüllt ist !! kein Pflichtfeld!!*}]
    <div class="form-group [{if $aErrors.oxuser__oxfname}]oxInValid[{/if}]">
        <label class="col-sm-2 control-label [{if $oxcmp_user->oxuser__oxcompany->value}] norm [{elseif $oView->isFieldRequired(oxuser__oxfname)}] req [{/if}]">[{ oxmultilang ident="FIRST_NAME" suffix="COLON" }]</label>
        <div class="col-sm-10">
            <input class="form-control [{if $oxcmp_user->oxuser__oxcompany->value}][{if $oView->isFieldRequired(oxuser__oxfname)}]js-oxValidate js-oxValidate_notEmpty [{/if}][{/if}]" type="text" size="37" maxlength="255" name="invadr[oxuser__oxfname]" value="[{if isset( $invadr.oxuser__oxfname ) }][{ $invadr.oxuser__oxfname }][{else }][{ $oxcmp_user->oxuser__oxfname->value }][{/if}]">
            [{if $oxcmp_user->oxuser__oxcompany->value}]
                [{if $oView->isFieldRequired(oxuser__oxfname)}]
                    <p class="oxValidateError">
                        <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                        [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxuser__oxfname}]
                    </p>
                [{/if}]
            [{/if}]
        </div>
    </div>

    [{* Nachname soll weg, wenn Firma gefüllt ist *}]
    <div class="form-group [{if $aErrors.oxuser__oxlname}]oxInValid[{/if}]">
        <label class="col-sm-2 control-label [{if $oxcmp_user->oxuser__oxcompany->value}] norm [{elseif $oView->isFieldRequired(oxuser__oxlname) }]req[{/if}]">[{ oxmultilang ident="LAST_NAME" suffix="COLON" }]</label>
        <div class="col-sm-10">
            <input class="form-control [{if $oxcmp_user->oxuser__oxcompany->value}][{if $oView->isFieldRequired(oxuser__oxlname) }]js-oxValidate js-oxValidate_notEmpty [{/if}][{/if}]" type="text" size="37" maxlength="255" name="invadr[oxuser__oxlname]" value="[{if isset( $invadr.oxuser__oxlname ) }][{ $invadr.oxuser__oxlname }][{else }][{ $oxcmp_user->oxuser__oxlname->value }][{/if}]">
            [{if $oxcmp_user->oxuser__oxcompany->value}]
                [{if $oView->isFieldRequired(oxuser__oxlname)}]
                    <p class="oxValidateError">
                        <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                        [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxuser__oxlname}]
                    </p>
                [{/if}]
            [{/if}]
        </div>
    </div>

    [{* Firma soll kein Pflichtfeld sein, wenn Name oder Vorname gefüllt ist *}]
    <div class="form-group [{if $aErrors.oxuser__oxcompany}]oxInValid[{/if}]">
        <label  class="col-sm-2 control-label [{if $oxcmp_user->oxuser__oxfname->value || isset( $invadr.oxuser__oxfname ) || $oxcmp_user->oxuser__oxlname->value || isset( $invadr.oxuser__oxlname )}] norm [{elseif $oView->isFieldRequired(oxuser__oxcompany) }] req [{/if}]">[{ oxmultilang ident="COMPANY" suffix="COLON"}]</label>
        <div class="col-sm-10">
              <input class="form-control [{if $oxcmp_user->oxuser__oxfname->value || $oxcmp_user->oxuser__oxlname->value}][{if $oView->isFieldRequired(oxuser__oxcompany) }]js-oxValidate js-oxValidate_notEmpty [{/if}][{/if}]" type="text" size="37" maxlength="255" name="invadr[oxuser__oxcompany]" value="[{if isset( $invadr.oxuser__oxcompany ) }][{ $invadr.oxuser__oxcompany }][{else }][{ $oxcmp_user->oxuser__oxcompany->value }][{/if}]">
            [{if $oxcmp_user->oxuser__oxfname->value || $oxcmp_user->oxuser__oxlname->value}]
                [{if $oView->isFieldRequired(oxuser__oxcompany) }]
                    <p class="oxValidateError">
                        <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                        [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxuser__oxcompany}]
                    </p>
                [{/if}]
            [{/if}]
        </div>
    </div>


    [{* Zus. Info kein Pflichtfeld *}]
    <div class="form-group [{if $aErrors.oxuser__oxaddinfo}]oxInValid[{/if}]">
        [{assign var="_address_addinfo_tooltip" value="FORM_FIELDSET_USER_BILLING_ADDITIONALINFO_TOOLTIP"|oxmultilangassign }]
        <label class=" col-sm-2 control-label col-sm-2 control-label [{if $oView->isFieldRequired(oxuser__oxaddinfo) }]req [{else}] norm[{/if}]" [{if $_address_addinfo_tooltip}]title="[{$_address_addinfo_tooltip}]" [{/if}] >[{ oxmultilang ident="ADDITIONAL_INFO" suffix='COLON' }]</label>
        <div class="col-sm-10">
            <input class="form-control [{if $oView->isFieldRequired(oxuser__oxaddinfo) }]js-oxValidate js-oxValidate_notEmpty [{/if}]" type="text" size="37" maxlength="255" name="invadr[oxuser__oxaddinfo]" value="[{if isset( $invadr.oxuser__oxaddinfo ) }][{ $invadr.oxuser__oxaddinfo }][{else }][{ $oxcmp_user->oxuser__oxaddinfo->value }][{/if}]">
              [{if $oView->isFieldRequired(oxuser__oxaddinfo) }]
                  <p class="oxValidateError">
                      <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                      [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxuser__oxaddinfo}]
                  </p>
              [{/if}]
        </div>
    </div>

    [{* Straße & Hausnummer Pflichtfeld *}]
    <div class="form-group [{if $aErrors.oxuser__oxstreet}]oxInValid[{/if}]">
        <label class="col-sm-2 control-label [{if $oView->isFieldRequired(oxuser__oxstreet) || $oView->isFieldRequired(oxuser__oxstreetnr) }]req [{else}] norm [{/if}]">[{ oxmultilang ident="STREET_AND_STREETNO" suffix="COLON" }]</label>
        <div class="form-inline col-sm-10">
              <input class="form-control strasse [{if $oView->isFieldRequired(oxuser__oxstreet) }]js-oxValidate js-oxValidate_notEmpty [{/if}]" type="text" data-fieldsize="pair-xsmall" maxlength="255" name="invadr[oxuser__oxstreet]" value="[{if isset( $invadr.oxuser__oxstreet ) }][{ $invadr.oxuser__oxstreet }][{else }][{ $oxcmp_user->oxuser__oxstreet->value }][{/if}]">
              <input class="form-control hausnr [{if $oView->isFieldRequired(oxuser__oxstreetnr) }]js-oxValidate js-oxValidate_notEmpty [{/if}]" type="text" data-fieldsize="xsmall" maxlength="16" name="invadr[oxuser__oxstreetnr]" value="[{if isset( $invadr.oxuser__oxstreetnr ) }][{ $invadr.oxuser__oxstreetnr }][{else }][{ $oxcmp_user->oxuser__oxstreetnr->value }][{/if}]">
              [{if $oView->isFieldRequired(oxuser__oxstreet) || $oView->isFieldRequired(oxuser__oxstreetnr) }]
                  <p class="oxValidateError">
                      <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                      [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxuser__oxstreet}]
                  </p>
              [{/if}]
        </div>
    </div>

    [{* PLZ & Ort Pflichtfelder *}]
    <div class="form-group [{if $aErrors.oxuser__oxzip}]oxInValid[{/if}]">
        <label class="col-sm-2 control-label [{if $oView->isFieldRequired(oxuser__oxzip) || $oView->isFieldRequired(oxuser__oxcity) }]req [{else}] norm [{/if}]">[{ oxmultilang ident="POSTAL_CODE_AND_CITY" suffix="COLON" }]</label>
        <div class="form-inline col-sm-10">
            <input class="form-control [{if $oView->isFieldRequired(oxuser__oxzip) }]js-oxValidate js-oxValidate_notEmpty [{/if}]" type="text" data-fieldsize="small" maxlength="16" name="invadr[oxuser__oxzip]" value="[{if isset( $invadr.oxuser__oxzip ) }][{ $invadr.oxuser__oxzip }][{else }][{ $oxcmp_user->oxuser__oxzip->value }][{/if}]">
            <input class="form-control strasse [{if $oView->isFieldRequired(oxuser__oxcity) }]js-oxValidate js-oxValidate_notEmpty [{/if}]" type="text" data-fieldsize="pair-small" maxlength="255" name="invadr[oxuser__oxcity]" value="[{if isset( $invadr.oxuser__oxcity ) }][{ $invadr.oxuser__oxcity }][{else }][{ $oxcmp_user->oxuser__oxcity->value }][{/if}]">
            [{if $oView->isFieldRequired(oxuser__oxzip) || $oView->isFieldRequired(oxuser__oxcity) }]
                <p class="oxValidateError">
                    <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                    [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxuser__oxzip}]
                </p>
            [{/if}]
        </div>
    </div>

    [{* USt-ID kein Pflichtfeld *}]
    <div class="form-group [{if $aErrors.oxuser__oxustid}]oxInValid[{/if}]">
        <label class="col-sm-2 control-label [{if $oView->isFieldRequired(oxuser__oxustid) }]req [{else}] norm [{/if}]">[{ oxmultilang ident="VAT_ID_NUMBER" suffix="COLON" }]</label>
        <div class="col-sm-10">
            <input class="form-control [{if $oView->isFieldRequired(oxuser__oxustid) }]js-oxValidate js-oxValidate_notEmpty [{/if}]" type="text" size="37" maxlength="255" name="invadr[oxuser__oxustid]" value="[{if isset( $invadr.oxuser__oxustid ) }][{ $invadr.oxuser__oxustid }][{else}][{ $oxcmp_user->oxuser__oxustid->value }][{/if}]">
            [{if $oView->isFieldRequired(oxuser__oxustid) }]
                <p class="oxValidateError">
                    <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                    [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxuser__oxustid}]
                </p>
            [{/if}]
        </div>
    </div>

    [{* Land Pflichtfeld *}]
    [{block name="form_user_billing_country"}]
        <div class="form-group [{if $aErrors.oxuser__oxcountryid}]oxInValid[{/if}]">
            <label class="col-sm-2 control-label [{if $oView->isFieldRequired(oxuser__oxcountryid) }]req [{else}] norm [{/if}]">[{ oxmultilang ident="COUNTRY" suffix="COLON" }]</label>
            <div class="col-sm-10">
                <select multiple class="form-control [{if $oView->isFieldRequired(oxuser__oxcountryid) }]js-oxValidate js-oxValidate_notEmpty [{/if}]" id="invCountrySelect" name="invadr[oxuser__oxcountryid]" data-fieldsize="normal">
                    <option value="">-</option>
                    [{assign var="blCountrySelected" value=false}]
                    [{foreach from=$oViewConf->getCountryList() item=country key=country_id }]
                        [{assign var="sCountrySelect" value=""}]
                        [{if !$blCountrySelected}]
                            [{if (isset($invadr.oxuser__oxcountryid) && $invadr.oxuser__oxcountryid == $country->oxcountry__oxid->value) || (!isset($invadr.oxuser__oxcountryid) && $oxcmp_user->oxuser__oxcountryid->value == $country->oxcountry__oxid->value) }]
                                [{assign var="blCountrySelected" value=true}]
                                [{assign var="sCountrySelect" value="selected"}]
                            [{/if}]
                        [{/if}]
                        <option value="[{ $country->oxcountry__oxid->value }]" [{$sCountrySelect}]>[{ $country->oxcountry__oxtitle->value }]</option>
                    [{/foreach }]
                </select>
                <br>

                [{* Bundesland soll weg *}]
                [{*include file="form/fieldset/state.tpl" countrySelectId="invCountrySelect" stateSelectName="invadr[oxuser__oxstateid]" selectedStateIdPrim=$invadr.oxuser__oxstateid selectedStateId=$oxcmp_user->oxuser__oxstateid->value*}]

                [{if $oView->isFieldRequired(oxuser__oxcountryid) }]
                    <p class="oxValidateError">
                        <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                        [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxuser__oxcountryid}]
                    </p>
                [{/if}]
            </div>
        </div>
    [{/block}]

    [{* Telefon soll Pflichtfeld sein *}]
    <div class="form-group [{if $aErrors.oxuser__oxfon}]oxInVali[{/if}]">
        <label class="col-sm-2 control-label [{if $oView->isFieldRequired(oxuser__oxfon) }]req [{else}]norm [{/if}]">[{oxmultilang ident="PHONE" suffix="COLON" }]</label>
        <div class="col-sm-10">
            <input class="form-control [{if $oView->isFieldRequired(oxuser__oxfon) }]js-oxValidate js-oxValidate_notEmpty [{/if}]" type="text" size="37" maxlength="128" name="invadr[oxuser__oxfon]" value="[{if isset( $invadr.oxuser__oxfon ) }][{ $invadr.oxuser__oxfon }][{else }][{ $oxcmp_user->oxuser__oxfon->value }][{/if}]">
            [{if $oView->isFieldRequired(oxuser__oxfon) }]
                <p class="oxValidateError">
                    <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                    [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxuser__oxfon}]
                </p>
            [{/if}]
        </div>
    </div>

    [{* Telefax kein Pflichtfeld *}]
    <div class="form-group [{if $aErrors.oxuser__oxfax}]oxInValid[{/if}]">
        <label class="col-sm-2 control-label [{if $oView->isFieldRequired(oxuser__oxfax) }]req [{else}] norm [{/if}]">[{ oxmultilang ident="FAX" suffix="COLON" }]</label>
        <div class="col-sm-10">
            <input class="form-control [{if $oView->isFieldRequired(oxuser__oxfax) }]js-oxValidate js-oxValidate_notEmpty [{/if}]" type="text" size="37" maxlength="128" name="invadr[oxuser__oxfax]" value="[{if isset( $invadr.oxuser__oxfax ) }][{ $invadr.oxuser__oxfax }][{else }][{ $oxcmp_user->oxuser__oxfax->value }][{/if}]">
            [{if $oView->isFieldRequired(oxuser__oxfax) }]
                <p class="oxValidateError">
                    <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                    [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxuser__oxfax}]
                </p>
            [{/if}]
        </div>
    </div>

    [{* Mobiltelefon kein Pflichtfeld *}]
    <div class="form-group [{if $aErrors.oxuser__oxmobfon}]oxInValid[{/if}]">
        <label class="col-sm-2 control-label [{if $oView->isFieldRequired(oxuser__oxmobfon) }]req [{else}] norm [{/if}]">[{ oxmultilang ident="CELLUAR_PHONE" suffix="COLON"}]</label>
        <div class="col-sm-10">
            <input class="form-control [{if $oView->isFieldRequired(oxuser__oxmobfon) }]js-oxValidate js-oxValidate_notEmpty[{/if}]" type="text" size="37" maxlength="64" name="invadr[oxuser__oxmobfon]" value="[{if isset( $invadr.oxuser__oxmobfon ) }][{$invadr.oxuser__oxmobfon }][{else}][{$oxcmp_user->oxuser__oxmobfon->value }][{/if}]">
            [{if $oView->isFieldRequired(oxuser__oxmobfon) }]
                <p class="oxValidateError">
                    <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                    [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxuser__oxmobfon}]
                </p>
            [{/if}]
        </div>
    </div>

    [{* Privat Telefon kein Pflichtfeld *}]
    <div class="form-group [{if $aErrors.oxuser__oxprivfon}]oxInValid[{/if}]">
        <label class="col-sm-2 control-label [{if $oView->isFieldRequired(oxuser__oxprivfon) }]req [{else}] norm [{/if}]">[{ oxmultilang ident="PERSONAL_PHONE" suffix="COLON" }]</label>
        <div class="col-sm-10">
            <input class="form-control [{if $oView->isFieldRequired(oxuser__oxprivfon) }]js-oxValidate js-oxValidate_notEmpty [{/if}]" type="text" size="37" maxlength="64" name="invadr[oxuser__oxprivfon]" value="[{if isset( $invadr.oxuser__oxprivfon ) }][{$invadr.oxuser__oxprivfon }][{else}][{$oxcmp_user->oxuser__oxprivfon->value }][{/if}]">
            [{if $oView->isFieldRequired(oxuser__oxprivfon) }]
                <p class="oxValidateError">
                    <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                    [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxuser__oxprivfon}]
                </p>
            [{/if}]
        </div>
    </div>

    [{* Geburtstag kein Pflichtfeld *}]
    [{*if $oViewConf->showBirthdayFields() }]
        <div class="form-group oxDate[{if $aErrors.oxuser__oxbirthdate}] oxInValid[{/if}]">
            <label class="col-sm-2 control-label [{if $oView->isFieldRequired(oxuser__oxbirthdate) }]req [{else}] norm [{/if}]">[{ oxmultilang ident="BIRTHDATE" suffix="COLON" }]</label>

            [{ Tag: }]
            <div class="form-inline col-sm-3">
                <label class="norm" for="oxDay">[{oxmultilang ident="DAY" }]</label>
                <input id="oxDay" class="form-control oxDay js-oxValidate" name="invadr[oxuser__oxbirthdate][day]" type="text" data-fieldsize="xsmall" maxlength="2" value="[{if $iBirthdayDay > 0 }][{$iBirthdayDay }][{/if}]" />
            </div>

            [{ Monat: }]
            <div class="form-inline col-sm-3">
                <label class="norm" for="oxDay">[{oxmultilang ident="MONTH" }]</label>
                <select class="form-control oxMonth js-oxValidate js-oxValidate_date [{if $oView->isFieldRequired(oxuser__oxbirthdate) }] js-oxValidate_notEmpty [{/if}]" name="invadr[oxuser__oxbirthdate][month]">
                    <option value="" >-</option>
                    [{section name="month" start=1 loop=13 }]
                        <option value="[{$smarty.section.month.index}]" [{if $iBirthdayMonth == $smarty.section.month.index}] selected="selected" [{/if}]>
                            [{oxmultilang ident="MONTH_NAME_"|cat:$smarty.section.month.index}]
                        </option>
                    [{/section}]
                </select>
            </div>

            [{ Jahr: }]
            <div class="form-inline col-sm-3">
                <label class="norm" for="oxYear">[{ oxmultilang ident="YEAR" }]</label>
                    <input id="oxYear" class="form-control oxYear js-oxValidate" name="invadr[oxuser__oxbirthdate][year]" type="text" data-fieldsize="small" maxlength="4" value="[{if $iBirthdayYear }][{$iBirthdayYear }][{/if}]" />

            </div>
            <p class="oxValidateError">
                <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                <span class="js-oxError_incorrectDate">[{ oxmultilang ident="ERROR_MESSAGE_INCORRECT_DATE" }]</span>
                [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxuser__oxbirthdate}]
            </p>
        </div>
    [{/if*}]

    [{* Hinweis das alle fetten Felder auszufüllen sind *}]

    [{if !$noFormSubmit}]
        <div class="form-group formNote">
            <label class="col-sm-2 control-label">[{oxmultilang ident="ERROR"}]</label>
            <div class="col-sm-10">
                <p class="form-control-static">[{oxmultilang ident="COMPLETE_MARKED_FIELDS"}]</p>
            </div>
        </div>
    [{/if}]
<div class="form-group"></div>

    <div class="form-group formSubmit">
        <label class="col-sm-2 control-label"></label>
        <div class="col-sm-10">
            <button id="accUserSaveTop" type="submit" name="save" class="submitButton btn btn-success">[{oxmultilang ident="SAVE"}]</button>
        </div>
    </div>
</div>


[{*}]
dre_bug Vorname ist Pflichtfeld:    [{$oView->isFieldRequired(oxuser__oxlname)|var_dump}]
dre_bug2 Firma hat inhalt::         [{$oxcmp_user->oxuser__oxcompany->value|var_dump}]
dre_bug3 Firma ist Pflichtfeld:     [{$oView->isFieldRequired(oxuser__oxcompany)|var_dump}]
dre_bug4 Array gefüllt:             [{$invadr.oxuser__oxcompany|var_dump}]
[{if $oxcmp_user->oxuser__oxcompany->value == 0}]
    Leer ist gleich null!
[{/if}]
[{*}]
