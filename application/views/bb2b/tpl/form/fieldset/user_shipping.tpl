[{if $oxcmp_user}]
    [{assign var="delivadr" value=$oxcmp_user->getSelectedAddress()}]
[{/if}]


    [{*Neutraler Versand
    <div class="form-group">
        <label class="col-sm-2 control-label norm">[{oxmultilang ident="QUICKORDER_WHITE_LABEL" suffix="COLON"}]</label>
        <div class="col-sm-10">
            <input class="form-control" name="iswhitelabel" type="checkbox">
        </div>
    </div>
*}]
[{* Adressauswahl *}]
<div class="form-group">
    <label class="col-sm-2 control-label norm" >[{oxmultilang ident="ADDRESSES" }]</label>
    <div class="col-sm-10">
        <input type="hidden" name="changeClass" value="[{$onChangeClass|default:'account_user'}]">
        [{oxscript include="js/widgets/oxusershipingaddressselect.js" priority=10 }]
        [{oxscript add="$( '#addressId' ).oxUserShipingAddressSelect();"}]
        <select multiple class="col-sm-10 form-control" id="addressId" name="oxaddressid">
            <option value="-1">[{ oxmultilang ident="NEW_ADDRESS" }]</option>
            [{if $oxcmp_user }]
                [{foreach from=$oxcmp_user->getUserAddresses() item=address }]
                    <option value="[{$address->oxaddress__oxid->value}]" [{if $address->isSelected()}]SELECTED[{/if}]>[{$address}]</option>
                [{/foreach }]
            [{/if}]
        </select>
    </div>
</div>


[{if $delivadr }]
    <div class="form-group" id="shippingAddressText">
        [{include file="widget/address/shipping_address.tpl" delivadr=$delivadr}]
        [{oxscript add="$('#userChangeShippingAddress').click( function() { $('#shippingAddressForm').show();$('#shippingAddressText').hide(); $('#userChangeShippingAddress').hide();return false;});"}]
    </div>
[{/if}]


<div id="shippingAddressForm" class="form-horizontal" [{if $delivadr }]style="display: none;"[{/if}]>
    [{*
    <li>
        <label [{if $oView->isFieldRequired(oxaddress__oxsal) }]class="req"[{/if }]>[{ oxmultilang ident="TITLE" suffix="COLON" }]</label>
          [{include file="form/fieldset/salutation.tpl" name="deladr[oxaddress__oxsal]" value=$delivadr->oxaddress__oxsal->value value2=$deladr.oxaddress__oxsal }]
    </li>
    *}]

    [{* Vorname *}]
    <div class="form-group [{if $aErrors.oxaddress__oxfname}]oxInValid[{/if}]">
        <label class="col-sm-2 control-label [{if $oView->isFieldRequired(oxaddress__oxfname)}]req [{else}]norm [{/if}]">[{oxmultilang ident="FIRST_NAME" suffix="COLON" }]</label>
        <div class="col-sm-10">
            <input class="form-control [{if $oView->isFieldRequired(oxaddress__oxfname) }]js-oxValidate js-oxValidate_notEmpty[{/if }]" type="text" maxlength="255" name="deladr[oxaddress__oxfname]" value="[{if isset( $deladr.oxaddress__oxfname ) }][{ $deladr.oxaddress__oxfname }][{else}][{ $delivadr->oxaddress__oxfname->value }][{/if }]">
            [{if $oView->isFieldRequired(oxaddress__oxfname)}]
                <p class="oxValidateError">
                    <span class="js-oxError_notEmpty">[{oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                    [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxaddress__oxfname}]
                </p>
            [{/if }]
        </div>
    </div>

    [{* Nachname *}]
    <div class="form-group [{if $aErrors.oxaddress__oxlname}]oxInValid[{/if}]">
        <label class="col-sm-2 control-label [{if $oView->isFieldRequired(oxaddress__oxlname)}]req [{else}]norm [{/if}]">[{ oxmultilang ident="LAST_NAME" suffix="COLON" }]</label>
        <div class="col-sm-10">
            <input class="form-control [{if $oView->isFieldRequired(oxaddress__oxlname)}]js-oxValidate js-oxValidate_notEmpty[{/if }]" type="text" maxlength="255" name="deladr[oxaddress__oxlname]" value="[{if isset( $deladr.oxaddress__oxlname ) }][{ $deladr.oxaddress__oxlname }][{else}][{ $delivadr->oxaddress__oxlname->value }][{/if }]">
            [{if $oView->isFieldRequired(oxaddress__oxlname)}]
                <p class="oxValidateError">
                    <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                    [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxaddress__oxlname}]
                </p>
            [{/if }]
        </div>
    </div>

    [{* Firma *}]
    <div class="form-group [{if $aErrors.oxaddress__oxcompany}] oxInValid[{/if}]">
        <label class="col-sm-2 control-label [{if $oView->isFieldRequired(oxaddress__oxcompany) }]req [{else}]norm [{/if}]">[{ oxmultilang ident="COMPANY" suffix="COLON" }]</label>
        <div class="col-sm-10">
            <input class="form-control [{if $oView->isFieldRequired(oxaddress__oxcompany) }]js-oxValidate js-oxValidate_notEmpty [{/if }]" type="text" size="37" maxlength="255" name="deladr[oxaddress__oxcompany]" value="[{if isset( $deladr.oxaddress__oxcompany ) }][{ $deladr.oxaddress__oxcompany }][{else}][{ $delivadr->oxaddress__oxcompany->value }][{/if }]">
             [{if $oView->isFieldRequired(oxaddress__oxcompany) }]
             <p class="oxValidateError">
                <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxaddress__oxcompany}]
            </p>
             [{/if }]
        </div>
    </div>

    [{* Zusätzliche Infos *}]
    <div class="form-group [{if $aErrors.oxaddress__oxaddinfo}]oxInValid[{/if}]">
        [{assign var="_address_addinfo_tooltip" value="FORM_FIELDSET_USER_SHIPPING_ADDITIONALINFO2_TOOLTIP"|oxmultilangassign }]
        <label class="col-sm-2 control-label [{if $oView->isFieldRequired(oxaddress__oxaddinfo) }]req [{else}]norm [{/if}]" [{if $_address_addinfo_tooltip}]title="[{$_address_addinfo_tooltip}]" [{/if}] >[{oxmultilang ident="ADDITIONAL_INFO" suffix='COLON'}]</label>
        <div class="col-sm-10">
            <input class="form-control [{if $oView->isFieldRequired(oxaddress__oxaddinfo) }]js-oxValidate js-oxValidate_notEmpty[{/if }]" type="text" size="37" maxlength="255" name="deladr[oxaddress__oxaddinfo]" value="[{if isset( $deladr.oxaddress__oxaddinfo ) }][{ $deladr.oxaddress__oxaddinfo }][{else}][{ $delivadr->oxaddress__oxaddinfo->value }][{/if }]">
            [{if $oView->isFieldRequired(oxaddress__oxaddinfo) }]
                <p class="oxValidateError">
                    <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                    [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxaddress__oxaddinfo}]
                </p>
            [{/if }]
        </div>
    </div>

    [{* Straße und Hausnr *}]
    <div class="form-group [{if $aErrors.oxaddress__oxstreet}]oxInValid[{/if}]">
        <label class="col-sm-2 control-label [{if $oView->isFieldRequired(oxaddress__oxstreet) || $oView->isFieldRequired(oxaddress__oxstreetnr) }]req [{else}]norm [{/if}]">[{oxmultilang ident="STREET_AND_STREETNO" suffix="COLON" }]</label>
        <div class="form-inline col-sm-10">
            <input class="form-control strasse [{if $oView->isFieldRequired(oxaddress__oxstreet) }] js-oxValidate js-oxValidate_notEmpty [{/if }]" type="text" data-fieldsize="pair-xsmall" maxlength="255" name="deladr[oxaddress__oxstreet]" value="[{if isset( $deladr.oxaddress__oxstreet ) }][{$deladr.oxaddress__oxstreet }][{else}][{$delivadr->oxaddress__oxstreet->value }][{/if }]">
            <input class="form-control hausnr [{if $oView->isFieldRequired(oxaddress__oxstreetnr) }] js-oxValidate js-oxValidate_notEmpty [{/if }]" type="text" data-fieldsize="xsmall" maxlength="16" name="deladr[oxaddress__oxstreetnr]" value="[{if isset( $deladr.oxaddress__oxstreetnr ) }][{$deladr.oxaddress__oxstreetnr }][{else}][{$delivadr->oxaddress__oxstreetnr->value }][{/if }]">
            [{if $oView->isFieldRequired(oxaddress__oxstreet) || $oView->isFieldRequired(oxaddress__oxstreetnr)}]
                <p class="oxValidateError">
                    <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                    [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxaddress__oxstreet}]
                </p>
            [{/if }]
        </div>
    </div>

    [{* PLZ und Ort *}]
    <div class="form-group[{if $aErrors.oxaddress__oxzip}]oxInValid[{/if}]">
        <label class="col-sm-2 control-label [{if $oView->isFieldRequired(oxaddress__oxzip) || $oView->isFieldRequired(oxaddress__oxcity) }]req [{else}]norm [{/if}]">[{oxmultilang ident="POSTAL_CODE_AND_CITY" suffix="COLON" }]</label>
        <div class="form-inline col-sm-10">
            <input class="form-control [{if $oView->isFieldRequired(oxaddress__oxzip) }]js-oxValidate js-oxValidate_notEmpty [{/if }]" type="text" data-fieldsize="small" maxlength="50" name="deladr[oxaddress__oxzip]" value="[{if isset( $deladr.oxaddress__oxzip ) }][{ $deladr.oxaddress__oxzip }][{else}][{ $delivadr->oxaddress__oxzip->value }][{/if }]">
            <input class="form-control strasse [{if $oView->isFieldRequired(oxaddress__oxcity) }] js-oxValidate js-oxValidate_notEmpty[{/if }]" type="text" data-fieldsize="pair-small" maxlength="255" name="deladr[oxaddress__oxcity]" value="[{if isset( $deladr.oxaddress__oxcity ) }][{$deladr.oxaddress__oxcity }][{else}][{$delivadr->oxaddress__oxcity->value }][{/if }]">
            [{if $oView->isFieldRequired(oxaddress__oxzip) }]
                <p class="oxValidateError">
                    <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                    [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxaddress__oxzip}]
                </p>
            [{/if }]
       </div>
    </div>

    [{* Land *}]
    [{block name="form_user_shipping_country"}]
        <div class="form-group [{if $aErrors.oxaddress__oxcountryid}]oxInValid[{/if}]">
            <label class="col-sm-2 control-label [{if $oView->isFieldRequired(oxaddress__oxcountryid)}]req [{else}]norm [{/if}]">[{oxmultilang ident="COUNTRY" suffix="COLON" }]</label>
            <div class="col-sm-10">
                <select multiple class="form-control [{if $oView->isFieldRequired(oxaddress__oxcountryid)}]js-oxValidate js-oxValidate_notEmpty [{/if }]" id="delCountrySelect" name="deladr[oxaddress__oxcountryid]">
                    <option value="">-</option>
                    [{assign var="blCountrySelected" value=false}]
                    [{foreach from=$oViewConf->getCountryList() item=country key=country_id }]
                        [{assign var="sCountrySelect" value=""}]
                        [{if !$blCountrySelected}]
                            [{if (isset($deladr.oxaddress__oxcountryid) && $deladr.oxaddress__oxcountryid == $country->oxcountry__oxid->value) ||
                                (!isset($deladr.oxaddress__oxcountryid) && ($delivadr->oxaddress__oxcountry->value == $country->oxcountry__oxtitle->value or
                                $delivadr->oxaddress__oxcountry->value == $country->oxcountry__oxid->value or
                                $delivadr->oxaddress__oxcountryid->value == $country->oxcountry__oxid->value)) }]
                                [{assign var="blCountrySelected" value=true}]
                                [{assign var="sCountrySelect" value="selected"}]
                            [{/if}]
                      [{/if}]
                      <option value="[{$country->oxcountry__oxid->value}]" [{$sCountrySelect}]>[{$country->oxcountry__oxtitle->value}]</option>
                    [{/foreach }]
                </select>
                [{if $oView->isFieldRequired(oxaddress__oxcountryid) }]
                    <p class="oxValidateError">
                        <span class="js-oxError_notEmpty">[{oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS"}]</span>
                        [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxaddress__oxcountryid}]
                    </p>
                [{/if }]
            </div>
        </div>

        [{* Bundesland *}]
        [{*
        <div class="form-group stateBox">
            <label class="col-sm-2 control-label [{if $oView->isFieldRequired(oxaddress__oxcountryid) }]req[{/if}]">[{oxmultilang ident="STATE" suffix="COLON" }]</label>
            <div class="col-sm-10">
            [{include file="form/fieldset/state.tpl" countrySelectId="delCountrySelect" stateSelectName="deladr[oxaddress__oxstateid]" selectedStateIdPrim=$deladr.oxaddress__oxstateid selectedStateId=$delivadr->oxaddress__oxstateid->value}]
            </div>
        </div>
        *}]
    [{/block}]

    [{* Telefon *}]
    <div class="form-group  [{if $aErrors.oxaddress__oxfon}]oxInValid[{/if}]">
        <label class="col-sm-2 control-label [{if $oView->isFieldRequired(oxaddress__oxfon) }]req [{else}]norm [{/if}]">[{oxmultilang ident="PHONE" suffix="COLON" }]</label>
        <div class="col-sm-10">
            <input class="form-control [{if $oView->isFieldRequired(oxaddress__oxfon)}]js-oxValidate js-oxValidate_notEmpty [{/if }]" type="text" size="37" maxlength="128" name="deladr[oxaddress__oxfon]" value="[{if isset( $deladr.oxaddress__oxfon ) }][{$deladr.oxaddress__oxfon }][{else}][{$delivadr->oxaddress__oxfon->value }][{/if }]">
            [{if $oView->isFieldRequired(oxaddress__oxfon) }]
                <p class="oxValidateError">
                    <span class="js-oxError_notEmpty">[{oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS"}]</span>
                    [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxaddress__oxfon}]
                </p>
            [{/if }]
        </div>
    </div>

    [{* Fax *}]
    <div class="form-group  [{if $aErrors.oxaddress__oxfax}]oxInValid[{/if}]">
        <label class="col-sm-2 control-label [{if $oView->isFieldRequired(oxaddress__oxfax) }]req [{else}]norm [{/if}]">[{oxmultilang ident="FAX" suffix="COLON"}]</label>
        <div class="col-sm-10">
            <input class="form-control [{if $oView->isFieldRequired(oxaddress__oxfax) }]js-oxValidate js-oxValidate_notEmpty [{/if }]" type="text" size="37" maxlength="128" name="deladr[oxaddress__oxfax]" value="[{if isset( $deladr.oxaddress__oxfax ) }][{ $deladr.oxaddress__oxfax }][{else}][{ $delivadr->oxaddress__oxfax->value }][{/if }]">
            [{if $oView->isFieldRequired(oxaddress__oxfax) }]
                <p class="oxValidateError">
                    <span class="js-oxError_notEmpty">[{oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS"}]</span>
                    [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxaddress__oxfax}]
                </p>
            [{/if }]
        </div>
    </div>

    [{if !$noFormSubmit}]
        <div class="form-group formNote">
            <label class="col-sm-2 control-label">[{oxmultilang ident="ERROR" }]</label>
            <div class="col-sm-10">
                <p class="form-control-static">[{oxmultilang ident="COMPLETE_MARKED_FIELDS" }]</p>
            </div>
        </div>
    [{/if}]
    <div class="form-group formSubmit">
        <label class="control-label col-sm-2"></label>
        <div class="col-sm-10">
            <button id="accUserSaveBottom" type="submit" class="btn btn-large btn-success submitButton largeButton nextStep  pull-left" name="save">[{ oxmultilang ident="SAVE" }]</button>
        </div>
    </div>
</div>
[{*$noFormSubmit|var_dump*}]
