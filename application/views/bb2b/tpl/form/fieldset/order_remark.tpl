[{if $blOrderRemark}]
    [{*oxscript include="js/widgets/oxinnerlabel.js" priority=10 }]
    [{oxscript add="$( '#orderRemark' ).oxInnerLabel();"*}]
    <div class="form-group">
        <label class="col-sm-2 control-label norm">[{oxmultilang ident="WHAT_I_WANTED_TO_SAY" suffix="COLON"}]</label>
        <div class="col-sm-10">
            <textarea id="orderRemark" rows="7" name="order_remark" class="areabox form-control" aria-describedby="mitteilungsText" >[{$oView->getOrderRemark()}]</textarea>
            <span class="help-block" id="mitteilungsText">[{oxmultilang ident="HERE_YOU_CAN_ENETER_MESSAGE"}]</span>
        </div>
        [{*}]
        <label for="orderRemark" class="innerLabel textArea">[{oxmultilang ident="HERE_YOU_CAN_ENETER_MESSAGE"}]</label>
        [{*}]
    </div>
[{/if}]
