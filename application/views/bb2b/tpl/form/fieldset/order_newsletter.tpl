[{if $blSubscribeNews}]
    [{block name="user_billing_newsletter"}]
        <div class="form-group">
            <label class="col-sm-2 control-label norm" for="subscribeNewsletter">[{oxmultilang ident="NEWSLETTER_SUBSCRIPTION" suffix="COLON" }]</label>
            <div class="col-sm-10">
                <input type="hidden" name="blnewssubscribed" value="0">
                <input id="subscribeNewsletter" type="checkbox" name="blnewssubscribed" value="1" [{if $oView->isNewsSubscribed()}]checked[{/if}]>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label norm"></label>
            <div class="col-sm-10">
                <!--<input type="text" id="eingabefeldHilfeText" class="form-control" aria-describedby="hilfeText">-->
                <span id="hilfeText" class="help-block">[{oxmultilang ident="MESSAGE_NEWSLETTER_SUBSCRIPTION" }]</span>
                [{*}]
                <textarea class="note form-control" rows="3">[{oxmultilang ident="MESSAGE_NEWSLETTER_SUBSCRIPTION" }]</textarea>
                [{*}]
            </div>
         </div>
    [{/block}]
[{/if}]
