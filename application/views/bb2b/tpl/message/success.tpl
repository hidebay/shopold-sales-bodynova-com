<div class="alert alert-success" role="alert">
    <p>[{ $statusMessage }]</p>
</div>