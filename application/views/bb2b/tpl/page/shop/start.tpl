[{capture append="oxidBlock_content"}]
    <div class="starttext">
        [{oxifcontent ident="startbild" object="oContent"}]

                [{$oContent->oxcontents__oxcontent->value}]

        [{/oxifcontent}]
    </div>
    [{* News *}]

    [{if $oxcmp_news|count }]
        [{include file="widget/news.tpl" oNews=$oxcmp_news}]
    [{/if}]

    <div class="startbtns">
        <a class="btn btn-default" href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account_order" }]" data-toggle="tooltip" data-placement="bottom" title="[{oxmultilang ident="TIP_MY_ORDERS"}]"><span class="ladda-label">[{oxmultilang ident="BTN_MY_ORDERS"}]</span></a>
        <a class="btn btn-default" href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=myfavorites&fnc=showLists" }]" data-toggle="tooltip" data-placement="bottom" title="[{oxmultilang ident="TIP_MY_FAVORITES"}]"><span class="ladda-label">[{oxmultilang ident="BTN_MY_FAVORITES"}]</span></a>
        <a class="btn btn-default" href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=quickorder&fnc=showForm" }]" data-toggle="tooltip" data-placement="bottom" title="[{oxmultilang ident="TIP_QUICKORDER"}]"><span class="ladda-label">[{oxmultilang ident="BTN_QUICKORDER"}]</span></a>
    </div>

    <div class="startbtns">
    [{oxifcontent ident="starttext" object="oContent"}]

            [{*}]<h3 class="sectionHead">
                [{$oContent->oxcontents__oxtitle->value}]
            </h3>[{*}]

                [{$oContent->oxcontents__oxcontent->value}]
    [{/oxifcontent}]
    </div>
    <div class="startbtns">
        [{oxifcontent ident="startneuheiten" object="oContent"}]
                [{$oContent->oxcontents__oxcontent->value}]
        [{/oxifcontent}]
    </div>





    [{assign var="oFirstArticle" value=$oView->getFirstArticle()}]
    [{if $oView->getCatOfferArticleList()|@count > 0}]
        [{foreach from=$oView->getCatOfferArticleList() item=actionproduct name=CatArt}]
            [{if $smarty.foreach.CatArt.first}]
            [{assign var="oCategory" value=$actionproduct->getCategory()}]
                [{if $oCategory }]
                    [{assign var="promoCatTitle" value=$oCategory->oxcategories__oxtitle->value}]
                    [{assign var="promoCatImg" value=$oCategory->getPromotionIconUrl()}]
                    [{assign var="promoCatLink" value=$oCategory->getLink()}]
                [{/if}]
            [{/if}]
        [{/foreach}]
    [{/if}]
    [{if $oView->getBargainArticleList()|@count > 0 || ($promoCatTitle && $promoCatImg)}]
        <div class="promoBoxes clear">
            [{if count($oView->getBargainArticleList()) > 0 }]
                <div id="specBox" class="specBox">
                    [{include file="widget/product/bargainitems.tpl"}]
                </div>
            [{/if}]
            [{if $promoCatTitle && $promoCatImg}]
                <div id="specCatBox" class="specCatBox">
                    <h2 class="sectionHead">[{$promoCatTitle}]</h2>
                    <a href="[{$promoCatLink}]" class="viewAllHover glowShadow corners"><span>[{ oxmultilang ident="VIEW_ALL_PRODUCTS" }]</span></a>
                    <img src="[{$promoCatImg}]" alt="[{$promoCatTitle}]">
                </div>
            [{/if}]
        </div>
    [{/if}]
    [{include file="widget/manufacturersslider.tpl" }]
    [{if $oView->getNewestArticles() }]
        [{assign var='rsslinks' value=$oView->getRssLinks() }]
        [{include file="widget/product/list.tpl" type=$oViewConf->getViewThemeParam('sStartPageListDisplayType') head="JUST_ARRIVED"|oxmultilangassign listId="newItems" products=$oView->getNewestArticles() rsslink=$rsslinks.newestArticles rssId="rssNewestProducts" showMainLink=true}]
    [{/if}]
    [{ insert name="oxid_tracker"}]
[{/capture}]
[{include file="layout/page.tpl" sidebar="Right"}]

