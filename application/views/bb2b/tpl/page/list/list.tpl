[{assign var="actCategory" value=$oView->getActiveCategory()}]

[{capture append="oxidBlock_sidebar"}]
    [{assign var="listType" value=$oView->getListType()}]
    [{if $listType=='manufacturer' || $listType=='vendor'}]
        [{if $actCategory && $actCategory->getIconUrl()}]
        <div class="box">
            <h3>
                [{if $listType=='manufacturer'}]
                    [{oxmultilang ident="BRAND"}]
                [{elseif $listType=='vendor'}]
                    [{oxmultilang ident="VENDOR"}]
                [{/if}]
            </h3>
            <div class="featured icon">
                <img src="[{$actCategory->getIconUrl()}]" alt="[{$actCategory->getTitle()}]">
            </div>
        </div>
        [{/if}]
    [{/if}]
[{/capture}]

[{capture append="oxidBlock_content"}]
        [{if $actCategory->oxcategories__oxthumb->value && $actCategory->getThumbUrl()}]
            <img src="[{$actCategory->getThumbUrl()}]" alt="[{$actCategory->oxcategories__oxtitle->value}]" class="categoryPicture">
        [{/if}]

        [{if $listType!='tag' && $actCategory && $actCategory->getShortDescription() }]
            <div class="categoryTopDescription" id="catDesc">[{$actCategory->getShortDescription()}]</div>
        [{/if}]
        [{if $actCategory->oxcategories__oxlongdesc->value }]
            <div class="categoryTopLongDescription" id="catLongDesc">[{oxeval var=$actCategory->oxcategories__oxlongdesc}]</div>
        [{/if}]

        [{if $oView->hasVisibleSubCats()}]
			<ul class="nav nav-pills">
                [{foreach from=$oView->getSubCatList() item=category name=MoreSubCat}]
					[{*
                    [{ if $category->getContentCats() }]
                        [{foreach from=$category->getContentCats() item=ocont name=MoreCms}]
                            [{assign var="iSubCategoriesCount" value=$iSubCategoriesCount+1}]
							<a id="moreSubCms_[{$smarty.foreach.MoreSubCat.iteration}]_[{$smarty.foreach.MoreCms.iteration}]" href="[{$ocont->getLink()}]">[{ $ocont->oxcontents__oxtitle->value }]</a>
                            <ul class="content"></ul>
                        [{/foreach}]
                    [{/if }]
					*}]
                    [{if $category->getIsVisible()}]
                        [{assign var="iconUrl" value=$category->getIconUrl()}]
						<li class="label [{*active*}]">
							<a id="moreSubCat_[{$smarty.foreach.MoreSubCat.iteration}]" class="btn btn-lg" href="[{ $category->getLink() }]">
								[{$category->oxcategories__oxtitle->value }][{ if $oView->showCategoryArticlesCount() && ($category->getNrOfArticles() > 0) }] <span class="badge">[{ $category->getNrOfArticles() }]</span>[{/if}]
							</a>
						</li>
						[{*
						[{if $category->getHasVisibleSubCats()}]
							<ul class="content">
								[{if $iconUrl}]
									<li class="subcatPic">
										<a href="[{ $category->getLink() }]">
											<img src="[{$category->getIconUrl() }]" alt="[{ $category->oxcategories__oxtitle->value }]">
										</a>
									</li>
								[{/if}]
								[{foreach from=$category->getSubCats() item=subcategory}]
									[{if $subcategory->getIsVisible() }]
										[{ foreach from=$subcategory->getContentCats() item=ocont name=MoreCms}]
											<li>
												<a href="[{$ocont->getLink()}]"><strong>[{ $ocont->oxcontents__oxtitle->value }]</strong></a>
											</li>
										[{/foreach }]
										<li>
											<a href="[{ $subcategory->getLink() }]">
												<strong>[{ $subcategory->oxcategories__oxtitle->value }]</strong>[{ if $oView->showCategoryArticlesCount() && ($subcategory->getNrOfArticles() > 0) }] ([{ $subcategory->getNrOfArticles() }])[{/if}]
											</a>
										</li>
									 [{/if}]
								[{/foreach}]
							</ul>
						[{else}]
							<div class="content catPicOnly">
								<div class="subcatPic">
								[{if $iconUrl}]
									<a href="[{ $category->getLink() }]">
										<img src="[{$category->getIconUrl() }]" alt="[{ $category->oxcategories__oxtitle->value }]">
									</a>
								 [{/if}]
								</div>
							</div>
						[{/if}]
						*}]
                    [{/if}]
                [{/foreach}]
            </ul>
        [{/if}]

    [{if $oView->getArticleList()|@count > 0}]
        <h1 class="pageHead">[{$oView->getTitle()}]
            [{assign var='rsslinks' value=$oView->getRssLinks() }]
            [{ if $rsslinks.activeCategory}]
                <a class="rss js-external" id="rssActiveCategory" href="[{$rsslinks.activeCategory.link}]" title="[{$rsslinks.activeCategory.title}]"><img src="[{$oViewConf->getImageUrl('rss.png')}]" alt="[{$rsslinks.activeCategory.title}]"><span class="FXgradOrange corners glowShadow">[{$rsslinks.activeCategory.title}]</span></a>
            [{/if }]
        </h1>

        <div class="modal fade" id="nothingaddedtobasketmodal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">[{oxmultilang ident="NOTHING_ADDED_TO_BASKET_TITLE" }]</h4>
                    </div>
                    <div class="modal-body">
                        <p>[{oxmultilang ident="NOTHING_ADDED_TO_BASKET_TEXT" }]</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">[{oxmultilang ident="NOTHING_ADDED_TO_BASKET_CLOSEBTN" }]</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="listRefine clear bottomRound">
            [{include file="widget/locator/listlocator.tpl" locator=$oView->getPageNavigationLimitedTop() attributes=$oView->getAttributes() listDisplayType=true itemsPerPage=true sort=true }]
        </div>
        [{* List types: grid|line|infogrid *}]
        [{include file="widget/product/list.tpl" type=$oView->getListDisplayType() listId="productList" products=$oView->getArticleList()}]
        [{include file="widget/locator/listlocator.tpl" locator=$oView->getPageNavigationLimitedBottom() place="bottom"}]
    [{/if}]
    [{insert name="oxid_tracker"}]
[{/capture}]
[{include file="layout/page.tpl" sidebar="Left" tree_path=$oView->getTreePath()}]
