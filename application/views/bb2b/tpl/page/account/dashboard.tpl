[{assign var="template_title" value="MY_ACCOUNT"|oxmultilangassign }]
[{assign var="dropship" value=$oxcmp_user->oxuser__dropship->value}]
[{capture append="oxidBlock_content"}]
    <div class="accountDashboardView">
        <h1 id="accountMain" class="pageHead">[{ oxmultilang ident="MY_ACCOUNT" }] - "[{ $oxcmp_user->oxuser__oxusername->value }]"</h1>
        <div class="col">
            [{block name="account_dashboard_col1"}]
                <dl>
                    <dt><a id="linkAccountPassword" href="[{ oxgetseourl ident=$oViewConf->getSslSelfLink()|cat:"cl=account_password" }]" rel="nofollow">[{ oxmultilang ident="CHANGE_PASSWORD" }]</a></dt>
                </dl>
                <dl>
                    <dt><a id="linkAccountNewsletter" href="[{ oxgetseourl ident=$oViewConf->getSslSelfLink()|cat:"cl=account_newsletter" }]" rel="nofollow">[{ oxmultilang ident="NEWSLETTER_SETTINGS" }]</a></dt>
                    <dd>[{ oxmultilang ident="NEWSLETTER_SUBSCRIBE_CANCEL" }]</dd>
                </dl>
                <dl>
                    <dt><a id="linkAccountBillship" href="[{ oxgetseourl ident=$oViewConf->getSslSelfLink()|cat:"cl=account_user" }]" rel="nofollow">[{ oxmultilang ident="BILLING_SHIPPING_SETTINGS" }]</a></dt>
                    <dd>[{ oxmultilang ident="UPDATE_YOUR_BILLING_SHIPPING_SETTINGS" }]</dd>
                </dl>
                <dl>
                    <dt><a id="linkAccountOrder" href="[{ oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account_order" }]" rel="nofollow">[{ oxmultilang ident="ORDER_HISTORY" }]</a></dt>
                    <dd>[{ oxmultilang ident="ORDERS" suffix="COLON" }] [{ $oView->getOrderCnt() }]</dd>
                </dl>
				<dl>
					<dt><a id="linkMyShoppingLists" href="[{ oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=myfavorites&fnc=showLists" }]" rel="nofollow">[{ oxmultilang ident="BTN_MYFAVORITES_LISTS" }]</a></dt>
					<dd>[{ oxmultilang ident="MYFAVORITES_LISTS_DESC" }]</dd>
				</dl>
                [{if $dropship == 1}]
                    <dl>
                        <dt><a id="linkQuickorder" href="[{ oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=quickorder&fnc=showForm" }]" rel="nofollow">[{ oxmultilang ident="BTN_QUICKORDER" }]</a></dt>
                        <dd>[{ oxmultilang ident="QUICKORDER_DESC" }]</dd>
                    </dl>
                [{/if}]
            [{/block}]
        </div>
        <div class="clear"></div>
    </div>
        
            <a href="[{ $oViewConf->getLogoutLink() }]" class="submitButton largeButton">[{ oxmultilang ident="LOGOUT" }]</a>
        
    [{insert name="oxid_tracker" title=$template_title }]
[{/capture}]


[{capture append="oxidBlock_sidebar"}]
    [{include file="page/account/inc/account_menu.tpl"}]
[{/capture}]
[{include file="layout/page.tpl" sidebar="Left"}]
