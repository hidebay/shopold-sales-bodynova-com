[{capture append="oxidBlock_content"}]
    [{block name="checkout_order_errors"}]
        [{if $oView->isConfirmAGBError() == 1}]
            [{include file="message/error.tpl" statusMessage="READ_AND_CONFIRM_TERMS"|oxmultilangassign}]
        [{/if}]
        [{assign var="iError" value=$oView->getAddressError()}]
        [{if $iError == 1}]
           [{include file="message/error.tpl" statusMessage="ERROR_DELIVERY_ADDRESS_WAS_CHANGED_DURING_CHECKOUT"|oxmultilangassign}]
        [{/if}]
    [{/block}]
    [{* ordering steps *}]
    [{include file="page/checkout/inc/steps.tpl" active=4}]
    [{block name="checkout_order_main"}]
        [{if !$oView->showOrderButtonOnTop()}]
            <div class="alert alert-info" role="alert">
                [{oxmultilang ident="MESSAGE_SUBMIT_BOTTOM"}]
            </div>
        [{/if}]
        [{block name="checkout_order_details"}]
            [{if !$oxcmp_basket->getProductsCount()}]
                [{block name="checkout_order_emptyshippingcart"}]
                    <div class="alert alert-danger" role="alert">[{oxmultilang ident="BASKET_EMPTY"}]</div>
                [{/block}]
            [{else}]
                [{assign var="currency" value=$oView->getActCurrency()}]
                [{if $oView->isLowOrderPrice()}]
                    [{block name="checkout_order_loworderprice_top"}]
                        <div class="alert alert-warning" role="alert">[{oxmultilang ident="MIN_ORDER_PRICE"}] [{$oView->getMinOrderPrice()}] [{$currency->sign}]</div>
                    [{/block}]
                [{else}]
                    [{if $oView->showOrderButtonOnTop()}]
                        <div id="orderAgbTop">
                            <form role="form" action="[{$oViewConf->getSslSelfLink()}]" method="post" id="orderConfirmAgbTop">
                                <div class="form-group">
                                    [{$oViewConf->getHiddenSid()}]
                                    [{$oViewConf->getNavFormParams()}]
                                    <input type="hidden" name="cl" value="order">
                                    <input type="hidden" name="fnc" value="[{$oView->getExecuteFnc()}]">
                                    <input type="hidden" name="challenge" value="[{$challenge}]">
                                    <input type="hidden" name="sDeliveryAddressMD5" value="[{$oView->getDeliveryAddressMD5()}]">

                                    [{include file="page/checkout/inc/agb.tpl"}]

                                    <div class="lineBox clear">
                                        <a href="[{oxgetseourl ident=$oViewConf->getPaymentLink()}]" class="prevStep submitButton largeButton">[{oxmultilang ident="PREVIOUS_STEP"}]</a>
                                        <button type="submit" class="submitButton nextStep largeButton btn btn-success">[{oxmultilang ident="SUBMIT_ORDER"}]</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    [{/if}]
                [{/if}]
                [{block name="checkout_order_vouchers"}]
                    [{if $oViewConf->getShowVouchers() && $oxcmp_basket->getVouchers()}]
                        [{oxmultilang ident="USED_COUPONS"}]
                        <div>
                            [{foreach from=$Errors.basket item=oEr key=key}]
                                [{if $oEr->getErrorClassType() == 'oxVoucherException'}]
                                    [{oxmultilang ident="COUPON_NOT_ACCEPTED" args=$oEr->getValue('voucherNr')}]<br>
                                    [{oxmultilang ident="REASON" suffix="COLON"}]
                                    [{$oEr->getOxMessage()}]<br>
                                [{/if}]
                            [{/foreach}]
                            [{foreach from=$oxcmp_basket->getVouchers() item=sVoucher key=key name=aVouchers}]
                                [{$sVoucher->sVoucherNr}]<br>
                            [{/foreach}]
                        </div>
                    [{/if}]
                [{/block}]
                [{block name="checkout_order_address"}]
                    <div id="orderAddress">
                        <form role="form" action="[{$oViewConf->getSslSelfLink()}]" method="post">
                            <div class="form-group">
                                <h3 class="section">
                                    <strong>[{oxmultilang ident="ADDRESSES"}]</strong>
                                    [{$oViewConf->getHiddenSid()}]
                                    <input type="hidden" name="cl" value="user">
                                    <input type="hidden" name="fnc" value="">
                                    <button type="submit" class="submitButton largeButton btn btn-info">[{oxmultilang ident="EDIT"}]</button>
                                </h3>
                            </div>

                        </form>
                        <dl>
                            <dt>[{oxmultilang ident="BILLING_ADDRESS"}]</dt>
                            <dd>
                                [{include file="widget/address/billing_address.tpl"}]
                            </dd>
                        </dl>
                        [{assign var="oDelAdress" value=$oView->getDelAddress()}]
                        [{if $oDelAdress}]
                            <dl class="shippingAddress">
                                <dt>[{oxmultilang ident="SHIPPING_ADDRESS"}]</dt>
                                <dd>
                                    [{include file="widget/address/shipping_address.tpl" delivadr=$oDelAdress}]
                                </dd>
                            </dl>
                        [{/if}]
                        [{if $oView->getOrderRemark()}]
                            <dl class="orderRemarks">
                                <dt>[{oxmultilang ident="WHAT_I_WANTED_TO_SAY"}]</dt>
                                <dd>
                                    [{$oView->getOrderRemark()}]
                                </dd>
                            </dl>
                        [{/if}]
                    </div>
                    <div style="clear:both;"></div>
                [{/block}]
                [{block name="shippingAndPayment"}]
                    <div id="orderShipping">
                    <form role="form" action="[{$oViewConf->getSslSelfLink()}]" method="post">
                        <div class="form-group">
                            <h3 class="section">
                                <strong>[{oxmultilang ident="SHIPPING_CARRIER"}]</strong>
                                [{$oViewConf->getHiddenSid()}]
                                <input type="hidden" name="cl" value="payment">
                                <input type="hidden" name="fnc" value="">
                                <button type="submit" class="submitButton largeButton btn btn-info">[{oxmultilang ident="EDIT"}]</button>
                            </h3>
                        </div>
                    </form>
                    [{assign var="oShipSet" value=$oView->getShipSet()}]
                    [{$oShipSet->oxdeliveryset__oxtitle->value}]
                    </div>
                    <div id="orderPayment">
                        <form role="form" action="[{$oViewConf->getSslSelfLink()}]" method="post">
                            <div class="form-group">
                                <h3 class="section">
                                    <strong>[{oxmultilang ident="PAYMENT_METHOD"}]</strong>
                                    [{$oViewConf->getHiddenSid()}]
                                    <input type="hidden" name="cl" value="payment">
                                    <input type="hidden" name="fnc" value="">
                                    <button type="submit" class="submitButton largeButton btn btn-info">[{oxmultilang ident="EDIT"}]</button>
                                </h3>
                            </div>
                        </form>
                        [{assign var="payment" value=$oView->getPayment()}]
                        [{$payment->oxpayments__oxdesc->value}]
                    </div>
                [{/block}]
                [{if $oView->isLowOrderPrice()}]
                    [{block name="checkout_order_loworderprice_bottom"}]
                        <div>[{oxmultilang ident="MIN_ORDER_PRICE"}] [{$oView->getMinOrderPrice()}] [{$currency->sign}]</div>
                    [{/block}]
                [{else}]
                    [{block name="checkout_order_btn_confirm_bottom"}]
                        <form role="form" action="[{$oViewConf->getSslSelfLink()}]" method="post" id="orderConfirmAgbBottom" enctype="multipart/form-data">
                            <div class="form-group">
                                [{$oViewConf->getHiddenSid()}]
                                [{$oViewConf->getNavFormParams()}]
                                <input type="hidden" name="cl" value="order">
                                <input type="hidden" name="fnc" value="[{$oView->getExecuteFnc()}]">
                                <input type="hidden" name="challenge" value="[{$challenge}]">
                                <input type="hidden" name="sDeliveryAddressMD5" value="[{$oView->getDeliveryAddressMD5()}]">
                                [{if $oViewConf->isFunctionalityEnabled("blShowTSInternationalFeesMessage")}]
                                    [{oxifcontent ident="oxtsinternationalfees" object="oTSIFContent"}]
                                        <hr/>
                                        <div class="clear">
                                            <span class="title">[{$oTSIFContent->oxcontents__oxcontent->value}]</span>
                                        </div>
                                    [{/oxifcontent}]
                                [{/if}]


                                [{*Neutraler Versand*}]
                                    <label class="col-sm-2 control-label">[{oxmultilang ident="QUICKORDER_WHITE_LABEL" suffix="COLON"}]</label>
                                    <div class="col-sm-10">
                                        <input name="iswhitelabel" type="checkbox">
                                    </div>
                                <br><br>

                                [{* Lieferschein Datei Upload *}]

                                    <label class="col-sm-2 control-label">[{oxmultilang ident="DATEI" suffix="COLON" }]</label>
                                    <div class="col-sm-10">
                                        [{oxmultilang ident="QUICKORDER_PDF_TXT"}]
                                            <input name="lieferschein" type="file" accept=".pdf"/>
                                    </div>


                                [{if $payment->oxpayments__oxid->value eq "oxidcashondel" && $oViewConf->isFunctionalityEnabled("blShowTSCODMessage")}]
                                    [{oxifcontent ident="oxtscodmessage" object="oTSCODContent"}]
                                        <hr/>
                                        <div class="clear">
                                            <span class="title">[{$oTSCODContent->oxcontents__oxcontent->value}]</span>
                                        </div>
                                    [{/oxifcontent}]
                                [{/if}]
                                <hr/>
                                [{if !$oView->showOrderButtonOnTop()}]
                                    [{include file="page/checkout/inc/agb.tpl"}]
                                    <hr/>
                                [{else}]
                                    [{include file="page/checkout/inc/agb.tpl" hideButtons=true}]
                                [{/if}]

                            <a href="[{oxgetseourl ident=$oViewConf->getOrderLink()}]" class="prevStep submitButton largeButton btn btn-warning">[{oxmultilang ident="PREVIOUS_STEP"}]</a>
                            <button type="submit" class="submitButton nextStep largeButton btn btn-success pull-right">[{oxmultilang ident="SUBMIT_ORDER"}]</button>
                            </div>
                        </form>
                    [{/block}]
                [{/if}]
                <div id="orderEditCart">
                    <form role="form" action="[{$oViewConf->getSslSelfLink()}]" method="post">
                        <div class="form-group">
                            <h3 class="section">
                                <strong>[{oxmultilang ident="CART"}]</strong>
                                [{$oViewConf->getHiddenSid()}]
                                <input type="hidden" name="cl" value="basket">
                                <input type="hidden" name="fnc" value="">
                                <button type="submit" class="submitButton largeButton btn btn-info">[{oxmultilang ident="EDIT"}]</button>
                            </h3>
                        </div>
                    </form>
                </div>
                <div class="lineBox clear">


                    [{block name="order_basket"}]
                        [{include file="page/checkout/inc/basketcontents.tpl" editable=false}]
                    [{/block}]
                </div>
            [{/if}]
        [{/block}]
    [{/block}]
    [{insert name="oxid_tracker" title=$template_title}]
[{/capture}]
[{assign var="template_title" value="REVIEW_YOUR_ORDER"|oxmultilangassign}]
[{include file="layout/page.tpl" title=$template_title location=$template_title}]
