[{* basket contents *}]
[{oxscript include="js/widgets/oxbasketchecks.js" priority=10 }]
[{oxscript include="js/widgets/dre_warenkorb.js" priority=10 }]
[{oxscript add="$('#checkAll, #basketRemoveAll').oxBasketChecks();"}]
[{assign var="currency" value=$oView->getActCurrency()}]
<form name="basket[{$basketindex}]" action="[{$oViewConf->getSelfActionLink()}]" method="post">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="cl" value="basket">
    <input type="hidden" name="fnc" value="changebasket">
    <input type="hidden" name="CustomError" value='basket'>

    [{* Funktionalität *}]
    <div class="clear">
        [{block name="checkout_basketcontents_basketfunctions"}]
            [{if $editable }]
                <div id="basketFn" class="basketFunctions">
                    [{*  basket update/delete buttons  *}]
                    <input type="checkbox" name="checkAll" id="checkAll" title="[{ oxmultilang ident=" ALL" }]">
                    <button type="button" class="btn btn-danger" id="basketRemoveAll" name="removeAllBtn"><span>[{ oxmultilang ident="ALL" }]</span>
                    </button>
                    <button type="submit" class="btn btn-warning" id="basketRemove" name="removeBtn"><span>[{ oxmultilang ident="REMOVE" }]</span>
                    </button>
                    <button type="submit" class="btn btn-info" id="basketUpdate" name="updateBtn"><span>[{ oxmultilang ident="UPDATE" }]</span>
                    </button>
                </div>
                <br/>
            [{/if}]
        [{/block}]
    </div>

    [{* Warenkorbinhalt *}]
    <div class="table-responsive">
        <table id="basket"
               class="table table-striped table-hover basketitems[{if $oViewConf->getActiveClassName() == 'order'}] orderBasketItems[{/if}]">
            <colgroup>
                [{if $editable}]
                    <col class="editCol">
                [{/if}]
                <col class="thumbCol">
                <col class="title">
                [{*if $oView->isWrapping()}]
                    <col class="wrappingCol">
                [{/if*}]
                <col class="countCol">
                <col class="weightCol">
                <col class="priceCol">
                <col class="uvp">
                <col class="totalCol">
            </colgroup>
            [{* basket header *}]
            <thead>
            <tr>
                [{* Checkbox *}]
                [{if $editable }]
                    <th></th>
                [{/if}]
                [{* Produkt Bild *}]
                <th></th>
                [{* Artikel *}]
                <th>[{ oxmultilang ident="PRODUCT" }]</th>
                [{* Geschenkpapier *}]
                [{*if $oView->isWrapping() }]
                    <th>[{ oxmultilang ident="WRAPPING" }]</th>
                [{/if*}]
                [{* Menge *}]
                <th>[{oxmultilang ident="QUANTITY" }]</th>
                [{* Gewicht *}]
                <th class="weight">[{oxmultilang ident="WEIGHT" }]</th>
                [{* Einzelpreis *}]
                <th class="unitPrice">[{oxmultilang ident="UNIT_PRICE" }]</th>
                [{* UVP *}]
                <th class="unitPrice">[{oxmultilang ident="UVP"}]</th>
                [{* Gesamtbetrag *}]
                <th class="unitPrice">[{oxmultilang ident="TOTAL" }]</th>
            </tr>
            </thead>

            [{* basket items *}]
            <tbody>
            [{assign var="basketitemlist" value=$oView->getBasketArticles() }]
            [{assign var="oMarkGenerator" value=$oView->getBasketContentMarkGenerator() }]
            [{assign var="intWeightSum" value=0 }]

            [{foreach key=basketindex from=$oxcmp_basket->getContents() item=basketitem name=basketContents}]
                [{block name="checkout_basketcontents_basketitem"}]
                    [{assign var="basketproduct" value=$basketitemlist.$basketindex }]
                    [{assign var="oArticle" value=$basketitem->getArticle()}]
                    [{assign var="oAttributes" value=$oArticle->getAttributesDisplayableInBasket()}]
                    [{assign var="intWeightSum" value=$intWeightSum+$basketitem->getWeight() }]
                    <tr id="cartItem_[{$smarty.foreach.basketContents.iteration}]">

                        [{block name="checkout_basketcontents_basketitem_removecheckbox"}]
                            [{if $editable }]
                                <td>
                                    <div class="checkbox warenkorb">
                                        <label class="warenkorb">
                                            <input class="warenkorb" type="checkbox"
                                                   name="aproducts[[{ $basketindex }]][remove]" value="1">
                                        </label>
                                    </div>
                                </td>
                            [{/if}]
                        [{/block}]

                        [{block name="checkout_basketcontents_basketitem_image"}]
                            [{* product image *}]
                            <td class="hidden-xs basketImage">
                                <div class="pictureBox">
                                    <a class="viewAllHover glowShadow corners" href="[{*$basketitem->getLink()*}]#"
                                       onclick="loadArticleDetails('[{$oArticle->oxarticles__oxid}]')" rel="nofollow">
                                        [{if $oArticle->oxarticles__oxpic1!=''}]
                                            <img src="[{$oArticle->getThumbnailUrl()}]" alt="[{$oArticle->oxarticles__oxtitle->value}] [{$oArticle->oxarticles__oxvarselect->value}]" class="img-thumbnail">
                                        [{else}]
                                            <img src="https://www.bodynova.de/out/imagehandler.php?artnum=[{$oArticle->oxarticles__oxartnum}]&size=450_450_100" class="img-thumbnail">
                                        [{/if}]
                                    </a>
                                </div>
                            </td>
                        [{/block}]

                        [{block name="checkout_basketcontents_basketitem_titlenumber"}]
                            [{* product title & number *}]
                            <td>
                                <div>
                                    <a rel="nofollow" href="[{*$basketitem->getLink()*}]#"
                                       onclick="loadArticleDetails('[{$oArticle->oxarticles__oxid}]')"><b>[{$basketitem->getTitle()}]</b></a>

                                    [{if $basketitem->isSkipDiscount() }] <sup><a rel="nofollow"
                                                                                  href="#noteWithSkippedDiscount">[{$oMarkGenerator->getMark('skippedDiscount')}]</a>
                                        </sup>[{/if}]
                                    [{if $oViewConf->getActiveClassName() == 'order' && $oViewConf->isFunctionalityEnabled('blEnableIntangibleProdAgreement')}]
                                        [{if $oArticle->hasDownloadableAgreement() }] <sup><a rel="nofollow"
                                                                                              href="#noteForDownloadableArticles">[{$oMarkGenerator->getMark('downloadable')}]</a>
                                            </sup>[{/if}]
                                        [{if $oArticle->hasIntangibleAgreement() }] <sup><a rel="nofollow"
                                                                                            href="#noteForIntangibleArticles">[{$oMarkGenerator->getMark('intangible')}]</a>
                                            </sup>[{/if}]
                                    [{/if}]
                                </div>
                                <div class="smallFont">
                                    [{ oxmultilang ident="PRODUCT_NO" suffix="COLON" }] [{
                                    $basketproduct->oxarticles__oxartnum->value }]
                                </div>
                                <div class="smallFont">
                                    [{assign var=sep value=", "}]
                                    [{assign var=result value=""}]
                                    [{foreach key=oArtAttributes from=$oAttributes->getArray() item=oAttr name=attributeContents}]
                                        [{assign var=temp value=$oAttr->oxattribute__oxvalue->value}]
                                        [{assign var=result value=$result$temp$sep}]
                                    [{/foreach}]
                                    <b>[{$result|trim:$sep}]</b>
                                </div>

                                [{if !$basketitem->isBundle() || !$basketitem->isDiscountArticle()}]
                                    [{assign var="oSelections" value=$basketproduct->getSelections(null,$basketitem->getSelList())}]
                                    [{if $oSelections}]
                                        <div class="selectorsBox clear"
                                             id="cartItemSelections_[{$smarty.foreach.basketContents.iteration}]">
                                            [{foreach from=$oSelections item=oList name=selections}]
                                                [{if $oViewConf->showSelectListsInList()}]
                                                    [{include file="widget/product/selectbox.tpl" oSelectionList=$oList sFieldName="aproducts[`$basketindex`][sel]" iKey=$smarty.foreach.selections.index blHideDefault=true sSelType="seldrop"}]
                                                [{else}]
                                                    [{assign var="oActiveSelection" value=$oList->getActiveSelection()}]
                                                    [{if $oActiveSelection}]
                                                        <input type="hidden"
                                                               name="aproducts[[{$basketindex}]][sel][[{$smarty.foreach.selections.index}]]"
                                                               value="[{if $oActiveSelection }][{$oActiveSelection->getValue()}][{/if}]">
                                                        <div>[{$oList->getLabel()}]
                                                            : [{$oActiveSelection->getName()}]</div>
                                                    [{/if}]
                                                [{/if}]
                                            [{/foreach}]
                                        </div>
                                    [{/if}]
                                [{/if}]

                                [{if !$editable }]
                                    <p class="persparamBox">
                                        [{foreach key=sVar from=$basketitem->getPersParams() item=aParam name=persparams }]
                                            [{if !$smarty.foreach.persparams.first}]<br/>[{/if}]
                                            <strong>
                                                [{if $smarty.foreach.persparams.first && $smarty.foreach.persparams.last}]
                                                    [{ oxmultilang ident="LABEL" suffix="COLON" }]
                                                [{else}]
                                                    [{ $sVar }] :
                                                [{/if}]
                                            </strong>
                                            [{ $aParam }]
                                        [{/foreach}]
                                    </p>
                                [{else}]
                                    [{if $basketproduct->oxarticles__oxisconfigurable->value}]
                                        [{if $basketitem->getPersParams()}]
                                            <br/>
                                            [{foreach key=sVar from=$basketitem->getPersParams() item=aParam name=persparams }]
                                                <p>
                                                    <label class="persParamLabel">
                                                        [{if $smarty.foreach.persparams.first && $smarty.foreach.persparams.last}]
                                                            [{ oxmultilang ident="LABEL" suffix="COLON" }]
                                                        [{else}]
                                                            [{ $sVar }]:
                                                        [{/if}]
                                                    </label>
                                                    <input class="textbox persParam" type="text"
                                                           name="aproducts[[{ $basketindex }]][persparam][[{ $sVar }]]"
                                                           value="[{ $aParam }]">
                                                </p>
                                            [{/foreach}]
                                        [{else}]
                                            <p>[{oxmultilang ident="LABEL" suffix="COLON" }] <input
                                                        class="textbox persParam" type="text"
                                                        name="aproducts[[{ $basketindex }]][persparam][details]"
                                                        value=""></p>
                                        [{/if}]
                                    [{/if}]
                                [{/if}]

                            </td>
                        [{/block}]

                        [{block name="checkout_basketcontents_basketitem_wrapping"}]
                            [{* product wrapping *}]
                            [{*if $oView->isWrapping() }]
                                <td>
                                    [{if !$basketitem->getWrappingId() }]
                                        [{if $editable }]
                                            <a class="wrappingTrigger" rel="nofollow" href="#"
                                               title="[{oxmultilang ident="ADD"}]">[{ oxmultilang ident="ADD" }]</a>
                                        [{else}]
                                            [{ oxmultilang ident="NONE" }]
                                        [{/if}]
                                    [{else}]
                                        [{assign var="oWrap" value=$basketitem->getWrapping() }]
                                        [{if $editable }]
                                            <a class="wrappingTrigger" rel="nofollow" href="#"
                                               title="[{oxmultilang ident="ADD"}]">[{$oWrap->oxwrapping__oxname->value}]</a>
                                        [{else}]
                                            [{$oWrap->oxwrapping__oxname->value}]
                                        [{/if}]
                                    [{/if}]
                                </td>
                            [{/if*}]
                        [{/block}]

                        [{block name="checkout_basketcontents_basketitem_quantity"}]
                            [{* product quantity manager *}]
                            <td class="quantity">
                                [{if $editable }]
                                    <input type="hidden" name="aproducts[[{ $basketindex }]][aid]"
                                           value="[{ $basketitem->getProductId() }]">
                                    <input type="hidden" name="aproducts[[{ $basketindex }]][basketitemid]"
                                           value="[{ $basketindex }]">
                                    <input type="hidden" name="aproducts[[{ $basketindex }]][override]" value="1">
                                    [{if $basketitem->isBundle() }]
                                        <input type="hidden" name="aproducts[[{ $basketindex }]][bundle]" value="1">
                                    [{/if}]

                                    [{if !$basketitem->isBundle() || !$basketitem->isDiscountArticle()}]
                                        <p>
                                            <input id="am_[{$smarty.foreach.basketContents.iteration}]" type="text"
                                                   class="textbox amountinput" name="aproducts[[{ $basketindex }]][am]"
                                                   value="[{ $basketitem->getAmount() }]" size="2" autocomplete="off">
                                            <br>
                                            <button type="submit" class="btn btn-info updateBtn btn-sm"
                                                    id="basketUpdate_[{$basketindex}]" name="updateBtn"
                                                    style="display: none;" title="[{oxmultilang ident="UPDATE"}]"
                                                    data-toggle="tooltip" data-placement="bottom">
                                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                                [{*}]<span>[{oxmultilang ident="UPDATE"}]</span>[{*}]
                                            </button>
                                        </p>
                                    [{/if}]
                                [{else}]
                                    [{$basketitem->getAmount()}]
                                [{/if}]
                                [{if $basketitem->getdBundledAmount() > 0 && ($basketitem->isBundle() || $basketitem->isDiscountArticle()) }]
                                    +[{ $basketitem->getdBundledAmount() }]
                                [{/if}]
                            </td>
                        [{/block}]

                        [{block name="checkout_basketcontents_basketitem_vat"}]
                            [{* product VAT percent *}]
                            <td class="weight">
                                [{*oxmultilang ident="CA" suffix="COLON"*}] [{$basketitem->getWeight()}] kg
                                <br>
                                [{*oxmultilang ident="CA" suffix="COLON"*}] [{math equation="x/y" x=$basketitem->getWeight() y=$basketitem->getAmount() }]
                                kg
                            </td>
                        [{/block}]

                        [{block name="checkout_basketcontents_basketitem_unitprice"}]
                            [{* product price *}]
                            <td class="unitPrice">
                                [{if $basketitem->getUnitPrice() }]
                                    [{oxprice price=$basketitem->getUnitPrice() currency=$currency }]
                                [{/if}]

                            </td>
                            [{* UVP product price *}]
                            <td class="unitPrice">
                                [{*if $basketitem->getUnitPrice() }]
                                    [{oxprice price=$basketitem->getUnitPrice() currency=$currency }]
                                [{/if*}]
                                [{if !$basketitem->isBundle() }]
                                    [{assign var=dRegUnitPrice value=$basketitem->getRegularUnitPrice()}]
                                    [{assign var=dUnitPrice value=$basketitem->getUnitPrice()}]
                                    [{*if $dRegUnitPrice->getPrice() > $dUnitPrice->getPrice() *}]
                                    [{oxprice price=$basketproduct->getTPrice() currency=$currency }]
                                    [{*/if*}]
                                [{/if}]
                            </td>
                        [{/block}]
                        [{block name="checkout_basketcontents_basketitem_totalprice"}]
                            [{* product quantity * price *}]
                            <td class="totalPrice">
                                [{oxprice price=$basketitem->getPrice() currency=$currency}]
                            </td>
                        [{/block}]
                    </tr>
                [{/block}]

                [{* packing unit *}]

                [{block name="checkout_basketcontents_itemerror"}]
                    [{foreach from=$Errors.basket item=oEr key=key }]
                        [{if $oEr->getErrorClassType() == 'oxOutOfStockException'}]
                            [{* display only the exceptions for the current article *}]
                            [{if $basketindex == $oEr->getValue('basketIndex') }]
                                <tr class="basketError">
                                    [{if $editable }]
                                        <td></td>
                                    [{/if}]
                                    <td colspan="5">
                                        <span class="inlineError">[{ $oEr->getOxMessage() }] <strong>[{
                                                $oEr->getValue('remainingAmount') }]</strong></span>
                                    </td>
                                    [{*if $oView->isWrapping() }]
                                        <td></td>
                                    [{/if*}]
                                    <td></td>
                                </tr>
                            [{/if}]
                        [{/if}]
                        [{if $oEr->getErrorClassType() == 'oxArticleInputException'}]
                            [{if $basketitem->getProductId() == $oEr->getValue('productId') }]
                                <tr class="basketError">
                                    [{if $editable }]
                                        <td></td>
                                    [{/if}]
                                    <td colspan="5">
                                        <span class="inlineError">[{ $oEr->getOxMessage() }]</span>
                                    </td>
                                    [{*if $oView->isWrapping() }]
                                        <td></td>
                                    [{/if*}]
                                    <td></td>
                                </tr>
                            [{/if}]
                        [{/if}]
                    [{/foreach}]
                [{/block}]
                [{*  basket items end  *}]
            [{/foreach}]

            [{block name="checkout_basketcontents_giftwrapping"}]
            [{/block}]


            </tbody>
        </table>
    </div>



    [{* Zweite Tabelle unter den Basketbuttons *}]
    [{block name="checkout_basketcontents_summary"}]
    <div id="basketSummary"
         class="table-responsive summary[{if $oViewConf->getActiveClassName() == 'order' }] orderSummary[{/if}]">
        [{*  basket summary  *}]
        <table class="table table-striped table-hover summary">
            <colgroup>
                [{if $editable}]
                    <col class="sumeditCol">
                [{/if}]
                <col class="sumthumbCol">
                <col class="sumtitle">
                <col class="sumcountCol">
                <col class="sumweightCol">
                <col class="sumpriceCol">
                <col class="sumuvp">
                <col class="sumtotalCol">
            </colgroup>

            [{if !$oxcmp_basket->getDiscounts() }]
                [{block name="checkout_basketcontents_nodiscounttotalnet"}]
                    <tr class="nodiscounts">
                        [{if $editable}]
                            <td></td>
                        [{/if}]
                        <td></td>
                        <td>[{oxmultilang ident="TOTAL_NET" suffix="COLON"}]</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="totalCol" id="basketTotalProductsNettoPrice">
                            <strong>[{oxprice price=$oxcmp_basket->getNettoSum() currency=$currency}]</strong></td>
                    </tr>
                [{/block}]
            [{else}]
                [{if $oxcmp_basket->isPriceViewModeNetto() }]
                    [{block name="checkout_basketcontents_discounttotalnet"}]
                        <tr>
                            <th>[{ oxmultilang ident="TOTAL_NET" suffix="COLON" }]</th>
                            <td>[{oxprice price=$oxcmp_basket->getNettoSum() currency=$currency }]</td>
                        </tr>
                    [{/block}]
                [{else}]
                    [{block name="checkout_basketcontents_discounttotalgross"}]
                        <tr>
                            <th>[{ oxmultilang ident="TOTAL_GROSS" suffix="COLON" }]</th>
                            <td>[{oxprice price=$oxcmp_basket->getBruttoSum() currency=$currency}]</td>
                        </tr>
                    [{/block}]
                [{/if}]

                [{block name="checkout_basketcontents_discounts"}]
                    [{foreach from=$oxcmp_basket->getDiscounts() item=oDiscount name=test_Discounts}]
                        <tr>
                            <th>
                                <b>[{if $oDiscount->dDiscount < 0 }][{ oxmultilang ident="SURCHARGE" }][{else}][{ oxmultilang ident="DISCOUNT" }][{/if}]
                                    &nbsp;</b>[{ $oDiscount->sDiscount }]
                            </th>
                            <td>[{oxprice price=$oDiscount->dDiscount*-1 currency=$currency}]</td>
                        </tr>
                    [{/foreach}]
                [{/block}]

                [{if !$oxcmp_basket->isPriceViewModeNetto() }]
                    [{block name="checkout_basketcontents_totalnet"}]
                        <tr>
                            <th>[{oxmultilang ident="TOTAL_NET" suffix="COLON" }]</th>
                            <td>[{oxprice price=$oxcmp_basket->getNettoSum() currency=$currency }]</td>
                            <td></td>
                        </tr>
                    [{/block}]
                [{/if}]

            [{/if}]
            <tr class="summeweight">
                [{if $editable}]
                    <td></td>
                [{/if}]
                <td></td>
                <td>[{oxmultilang ident="TOTAL_WEIGHT"}]</td>
                <td></td>
                <td class="weight">[{oxmultilang ident="CA"}]
                    &nbsp;[{math equation="x" format="%.0f" x=$intWeightSum}] kg
                </td>
                <td></td>
                <td></td>
                <td></td>

            </tr>
        </table>

        [{/block}]
    </div>
</form>
