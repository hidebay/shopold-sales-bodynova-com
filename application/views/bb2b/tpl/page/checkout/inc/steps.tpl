[{block name="checkout_steps_main"}]
	<ul class="nav nav-tabs" role="tablist">
        [{if $oxcmp_basket->getProductsCount()}]
            [{assign var=showStepLinks value=true}]
        [{/if}]

        [{block name="checkout_steps_basket"}]
            <li class="step1[{if $active == 1}] active [{elseif $active > 1}] passed [{/if}]">
				<a rel="nofollow" href="[{if $showStepLinks}][{oxgetseourl ident=$oViewConf->getBasketLink()}][{else}]#[{/if}]">
					[{oxmultilang ident="STEPS_BASKET"}]
				</a>
            </li>
        [{/block}]

        [{assign var=showStepLinks value=false}]
        [{if !$oView->isLowOrderPrice() && $oxcmp_basket->getProductsCount()}]
            [{assign var=showStepLinks value=true}]
        [{/if}]

        [{block name="checkout_steps_send"}]
            <li class="step2[{if $active == 2}] active [{elseif $active > 2}] passed [{/if}]">
				<a rel="nofollow" href="[{if $showStepLinks}][{oxgetseourl ident=$oViewConf->getOrderLink()}][{else}]#[{/if}]">
					[{oxmultilang ident="STEPS_SEND"}]
				</a>
            </li>
        [{/block}]

        [{assign var=showStepLinks value=false}]
        [{if $active != 1 && $oxcmp_user && !$oView->isLowOrderPrice() && $oxcmp_basket->getProductsCount()}]
            [{assign var=showStepLinks value=true}]
        [{/if}]

		[{*
        [{block name="checkout_steps_pay"}]
            <li class="step3[{if $active == 3}] active [{elseif $active > 3}] passed [{/if}]">
				<a rel="nofollow" [{if $oViewConf->getActiveClassName() == "user"}]id="paymentStep"[{/if}] href="[{if $showStepLinks}][{oxgetseourl ident=$oViewConf->getPaymentLink()}][{else}]#[{/if}]">
                    [{oxmultilang ident="STEPS_PAY"}]
				</a>
            </li>
            [{oxscript add="$('#paymentStep').click( function() { $('#userNextStepBottom').click();return false;});"}]
        [{/block}]

        $oView->getPaymentList() &&
		*}]

        [{assign var=showStepLinks value=false}]
        [{if $active != 1 && $oxcmp_user && $oxcmp_basket->getProductsCount() && !$oView->isLowOrderPrice()}]
            [{assign var=showStepLinks value=true}]
        [{/if}]

        [{block name="checkout_steps_order"}]
            <li class="step4[{if $active == 4}] active [{elseif $active > 4}] passed [{/if}]">
				<a rel="nofollow" [{if $oViewConf->getActiveClassName() == "payment"}]id="orderStep"[{/if}] href="[{if $showStepLinks}][{if $oViewConf->getActiveClassName() == "payment"}]javascript:document.forms.order.submit();[{else}][{oxgetseourl ident=$oViewConf->getOrderConfirmLink()}][{/if}][{else}]#[{/if}]">
                    [{oxmultilang ident="STEPS_ORDER"}]
				</a>
			</li>
            [{oxscript add="$('#orderStep').click( function() { $('#paymentNextStepBottom').click();return false;});"}]
        [{/block}]

        [{block name="checkout_steps_laststep"}]
            <li class="step5[{if $active == 5}] activeLast [{else}] defaultLast [{/if}] ">
                <a href="#">[{oxmultilang ident="READY"}]</a>
            </li>
        [{/block}]
    </ul>
[{/block}]