[{block name="widget_product_listitem_line"}]
    [{assign var="product"              value=$oView->getProduct()         }]
    [{assign var="owishid"              value=$oView->getWishId()          }]
    [{assign var="removeFunction"       value=$oView->getRemoveFunction()  }]
    [{assign var="recommid"             value=$oView->getRecommId()        }]
    [{assign var="iIndex"               value=$oView->getIndex()           }]
    [{assign var="showMainLink"         value=$oView->getShowMainLink()    }]
    [{assign var="blDisableToCart"      value=$oView->getDisableToCart()   }]
    [{assign var="toBasketFunction"     value=$oView->getToBasketFunction()}]
    [{assign var="altproduct"           value=$oView->getAltProduct()      }]
	[{assign var="aVariantSelections"   value=$product->getVariantSelections(null,null,1)}]

	[{*debug*}]
	[{* $oViewConf->getNavFormParams() *}]
	[{* $oViewConf->getHiddenSid() *}]

	[{if $showMainLink}]
		[{assign var='_productLink' value=$product->getMainLink()}]
	[{else}]
		[{assign var='_productLink' value=$product->getLink()}]
	[{/if}]


	<tr id="article_[{$product->oxarticles__oxid|replace:".":"_"}]" class="tabellenspalte">
		<td class="hidden-xs imagecolumn">
			<a name="[{$product->getID()|replace:".":"_"}]"></a>
			<div class="pictureBox">
				<a href="[{*$_productLink*}]#" onclick="loadArticleDetails('[{$product->oxarticles__oxid}]')" class="viewAllHover glowShadow corners" title="[{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]">
                    [{if $product->oxarticles__oxpic1!=''}]
                        <img src="[{$product->getThumbnailUrl()}]" alt="[{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]" class="img-thumbnail">
                    [{else}]
                        <img src="https://www.bodynova.de/out/imagehandler.php?artnum=[{$product->oxarticles__oxartnum}]&size=450_450_100" class="img-thumbnail">
                    [{/if}]
				</a>
			</div>
		</td>
		<td class="ebenetitel">
			<span class="headline">
				[{if $product->isBuyable()}]
					[{$product->oxarticles__oxtitle->value }]
				[{else}]
					[{$product->oxarticles__oxtitle->value }] [{$product->oxarticles__oxvarselect->value}]
				[{/if}]
			</span>
			[{*
			<a id="[{$iIndex}]" href="[{$_productLink}]" class="title" title="[{ $product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]">
				<span>[{ $product->oxarticles__oxtitle->value }] [{$product->oxarticles__oxvarselect->value}]</span>
			</a>
			<br>
			[{$product->oxarticles__oxshortdesc->value|truncate:160:"..."}]
			<br>*}]<br/>
			[{*<a id="mehrinfobn" class="submitButton largeButton" href="[{ $_productLink }]" >[{ oxmultilang ident="MORE_INFO" }]</a>*}]

			<form>
				[{* Standartartikel *}]
				[{if $product->isBuyable()}]
					<div class="input-line">

                        [{* Mengenwähler *}]
						<div class="amount_input spinner input-group bootstrap-touchspin">
							<span class="input-group-btn"><button type="button" class="btn btn-default bootstrap-touchspin-down">-</button></span>
							<span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
							<input class="form-control amount" type="numeric" name="amount[[{$product->oxarticles__oxid}]]" value="[{if $product->orderedamount}][{$product->orderedamount}][{else}]0[{/if}]" style="display: block;">
							<span class="input-group-btn"><button type="button" class="btn btn-default bootstrap-touchspin-up">+</button></span>
						</div>

						<!-- btn-primary -->

                        [{* Info Button *}]
						<button type="button" class="btn btn-info btn btn-default ladda-button" data-style="expand-right" onclick="loadArticleDetails('[{$product->oxarticles__oxid}]')" data-toggle="tooltip" data-placement="bottom" title="[{oxmultilang ident="SHOW_ARTICLE_DETAILS"}]"><span class="fa fa-info-circle"></span></button>

                        [{* Herz Button *}]
						<button type="button" class="btn btn-favorite btn btn-default ladda-button [{if $product->isFavorite()}]btn-primary[{/if}]" data-style="expand-right" onclick="toggleFavoritEntry('[{$product->oxarticles__oxid}]')" data-toggle="tooltip" data-placement="bottom" title="[{oxmultilang ident="ADD_TO_FAVORITE" }]"><span class="fa fa-heart-o"></span></button>

                        <div class="artnr">Artnr: [{$product->oxarticles__oxartnum}]</div>

                        [{* Warenkorb Platzhalter *}]
                        [{*}]<div style="width: 52px;"></div>[{*}]



                        [{*$product->oxarticles__oxartnum*}]
					</div>
					[{* Vaterartikel *}]
				[{else}]
					<div class="input-line">

                        [{* Warenkorb Platzhalter *}]
						[{*}]<div style="width: 52px;"></div>[{*}]

                        [{* Optionen Button *}]
						<button type="button" class="btn btn-default btn-show-variants ladda-button optionen" data-style="expand-right" onclick="loadProductOptions('[{$product->oxarticles__oxid}]')" data-toggle="tooltip" data-placement="bottom" title="[{oxmultilang ident="TIP_SHOW_OPTIONS"}]"><span class="glyphicon glyphicon-list"></span>[{*}]<span class="ladda-label">[{oxmultilang ident="SHOW_OPTIONS" }]</span>[{*}]</button>

                        [{* Info Button *}]
						<button type="button" class="btn btn-info btn btn-default ladda-button" data-style="expand-right" onclick="loadArticleDetails('[{$product->oxarticles__oxid}]')" data-toggle="tooltip" data-placement="bottom" title="[{oxmultilang ident="SHOW_ARTICLE_DETAILS"}]"><span class="fa fa-info-circle"></span></button>

                        [{* Herz Button *}]
						<button type="button" class=" btn btn-favorite btn btn-default ladda-button [{if $product->isFavorite()}]btn-primary[{/if}]" data-style="expand-right" onclick="toggleFavoritEntry('[{$product->oxarticles__oxid}]')" data-toggle="tooltip" data-placement="bottom" title="[{oxmultilang ident="ADD_TO_FAVORITE"}]"><span class="fa fa-heart-o"></span></button>

                        <div class="artnr">Artnr: [{$product->oxarticles__oxartnum}]</div>

						[{*$product->oxarticles__oxartnum*}]
					</div>
				[{/if}]

			</form>
		</td>
        <td class="Warenkorb">
            <form>
                <div class="input-line">
                    [{if $product->isBuyable()}]
                        [{* Warenkorbbutton *}]
                        <button type="button" class="btn btn-default btn-prima btn-basket ladda-button btn-success pull-right" data-style="expand-right" onclick="addToBasket('[{$product->oxarticles__oxid}]')" data-toggle="tooltip" data-placement="bottom" title="[{oxmultilang ident="TIP_TO_THE_BASKET"}]"><span class="glyphicon glyphicon-shopping-cart"></span>[{*}]<span class="ladda-label">[{oxmultilang ident="TO_THE_BASKET"}]</span>[{*}]</button>
                    [{/if}]
                </div>
            </form>
        </td>
		<td class="selprice">
			<label id="productPrice_[{$iIndex}]" class="price">
				<span>
					[{if $product->isRangePrice()}]
						[{oxmultilang ident="PRICE_FROM" }]
						[{if !$product->isParentNotBuyable() }]
							[{assign var="oPrice" value=$product->getMinPrice()}]
						[{else}]
							[{assign var="oPrice" value=$product->getVarMinPrice()}]
						[{/if}]
					[{else}]
						[{if !$product->isParentNotBuyable() }]
							[{assign var="oPrice" value=$product->getPrice()}]
						[{else}]
							[{assign var="oPrice" value=$product->getVarMinPrice() }]
						[{/if}]
					[{/if}]
				</span>
				[{oxprice price=$oPrice currency=$oView->getActCurrency()}]*
				[{if $oView->isVatIncluded() }]
					[{if !($product->hasMdVariants() || ($oViewConf->showSelectListsInList() && $product->getSelections(1)) || $product->getVariants())}]*[{/if}]
				[{/if}]
			</label>
		</td>
		<td class="ampelcolumn">
			[{*if $product->isBuyable()*}]
				[{oxid_include_dynamic file="widget/product/stockinfo.tpl"}]
                [{*oxid_include_dynamic file="widget/product/stockinfodebug.tpl"*}]
			[{*/if*}]
		</td>
	</tr>
	[{if !$product->isBuyable()}]
		<tr id="article_selected_[{$product->oxarticles__oxid|replace:".":"_"}]" class="articleselect">
			<td colspan="12">
				<!-- empty for viewing selection area -->
			</td>
		</tr>
	[{/if}]
[{/block}]

[{*debug*}]
