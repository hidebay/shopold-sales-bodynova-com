[{* Väter und Standart *}]
[{if $product}]

    [{if $_product}]
        [{* Lieferzeit der Varianten beim Vater anzeigen *}]
        [{if $_product->getmindeltime() != 0 || $_product->getmaxdeltime() != 0 }]

            [{*$_product|var_dump*}]

            [{* Standartartikel *}]
            [{if $_product->isBuyable()}]
                <div class="stockFlagBox [{if $_product->showCssFlag()}][{$_product->showCssFlag()}][{/if}]"></div>

                [{if $_product->getmindeltimeS() > 0}]
                    [{if $_product->getBnFlag() !='0'}]
                        [{oxmultilang ident="DELIVERYTIME_DELIVERYTIME"}] [{$_product->getmindeltimeS()}]-[{$_product->getmaxdeltimeS()}] [{oxmultilang ident="TAGE"}]
                    [{/if}]
                [{/if}]

                [{*$_product->getDeliveryDate()|var_dump*}]

                [{* Lieferbar ab:  Beim Vater keine Ampel aber bei den Varianten Ampel mit Lieferdatum.*}]
                [{if $_product->getDeliveryDate()}]
                    [{if $_product->getBnFlag() == '2'}]
                        [{oxmultilang ident="AVAILABLE_ON"}] [{$_product->getDeliveryDate()}]
                    [{/if}]
                [{/if}]

            [{/if}]

            [{* Stockflag *}]
            [{*}]<div class="stockFlagBox [{if $_product->showCssFlag()}][{$_product->showCssFlag()}][{/if}]"></div>[{*}]

            [{* Lieferdauer Anfertigungszeit Nur beim Vater nicht bei Varianten bei Varianten keine Ampel anzeigen auch keinen Grünen
                *}]

            [{* Variantenväter *}]
            [{if !$_product->isBuyable()}]

                [{if $_product->getmindeltimeS() > 0}]
                    [{* Stockflag *}]
                    <div class="stockFlagBox [{if $_product->showCssFlag()}][{$_product->showCssFlag()}][{/if}]"></div>
                    [{oxmultilang ident="DELIVERYTIME_DELIVERYTIME"}] [{$_product->getmindeltimeS()}]-[{$_product->getmaxdeltimeS()}] [{oxmultilang ident="TAGE"}]

                [{/if}]

                [{* Lieferbar ab:  Beim Vater keine Ampel aber bei den Varianten Ampel mit Lieferdatum.*}]
                [{if $_product->getDeliveryDate()}]
                    [{* Stockflag *}]
                    <div class="stockFlagBox [{if $_product->showCssFlag()}][{$_product->showCssFlag()}][{/if}]"></div>
                    [{if $_product->getBnFlag() == '2'}]
                        [{oxmultilang ident="AVAILABLE_ON"}] [{$_product->getDeliveryDate()}]
                    [{/if}]
                [{/if}]
            [{/if}]
        [{/if}]
    [{/if}]



    [{* Anzeige bei den Varianten *}]
[{else}]
    [{* Wenn das Array gefüllt ist: *}]
    [{if $_aProduct}]

        [{* Wenn die Mindest oder Maximallieferzeit gleich null ist *}]
        [{if $_aProduct.mindeltime == 0 || $_aProduct.maxdeltime == 0}]

            [{*$_aProduct|@debug_print_var*}]

            [{* Stockflag *}]
            <div class="stockFlagBox [{if $_aProduct.bncssflag}][{$_aProduct.bncssflag}][{/if}]"></div>

            [{* Lieferbar ab: nur anzeigen wenn Ampel rot*}]
            [{if $_aProduct.deliveryDate && $_aProduct.bncssflag == "notOnStock"}]
                [{oxmultilang ident="AVAILABLE_ON"}] [{$_aProduct.deliveryDate}]
            [{/if}]
        [{/if}]
    [{/if}]
[{/if}]

