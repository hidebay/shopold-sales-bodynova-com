[{* Väter und Standart *}]
[{if $product}]

    [{if $_product}]
        [{* Lieferzeit der Varianten beim Vater anzeigen *}]
        [{if $_product->getmindeltime() !=0 || $_product->getmaxdeltime() != 0 }]

            [{* Lieferdauer *}]
            [{if $_product->getmindeltimeS() > 0}]
                Lieferdauer [{$_product->getmindeltimeS()}]-[{$_product->getmaxdeltimeS()}] Tage
            [{/if}]

            [{* Lieferbar ab: *}]
            [{if $_product->getDeliveryDate()}]
                [{oxmultilang ident="AVAILABLE_ON"}] [{$_product->getDeliveryDate()}]
            [{/if}]

            [{* Stockflag *}]
            <div class="stockFlagBox [{if $_product->showCssFlag()}][{$_product->showCssFlag()}][{/if}]"></div>

        [{/if}]
    [{/if}]


[{* Anzeige bei den Varianten *}]
[{else}]
    [{* Wenn das Array gefüllt ist: *}]
    [{if $_aProduct}]

        [{* Wenn die Mindest oder Maximallieferzeit gleich null ist *}]
        [{if $_aProduct.mindeltime == 0 || $_aProduct.maxdeltime == 0}]
            [{*$_aProduct|@debug_print_var*}]

            [{* Lieferbar ab: *}]
            [{if $_aProduct.deliveryDate}]
                [{oxmultilang ident="AVAILABLE_ON"}] [{$_aProduct.deliveryDate}]
            [{/if}]

            [{* Stockflag *}]
            <div class="stockFlagBox [{if $_aProduct.bncssflag}][{$_aProduct.bncssflag}][{/if}]"></div>
        [{/if}]
    [{/if}]
[{/if}]

