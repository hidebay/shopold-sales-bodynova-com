[{* Debug *}]
<h3>Debug:</h3>
[{assign var="objekt" value=$product}]
<pre>
        <b>get_template_vars<b>
                <hr>
                [{php}]
                    print_r($this->get_template_vars('objekt'));
                    #exit;
                [{/php}]
                <hr>
    </pre>


<pre>
        <b>get_object_vars<b>
                <hr>
                [{php}]
                    print_r($this->get_object_vars('objekt'));
                    #exit;
                [{/php}]
                <hr>
    </pre>
<h5> Statis: </h5>
[{if $product}]
    $product existiert beim Vater und bei dem Standartartikel
    $product existiert:<br/>
    [{if !$product->isNotBuyable()}]
        kaufbar<br/>
        <pre>
            [{*$product|@debug_print_var*}]
        </pre>
    [{/if}]
    [{if $product->getProductStatus()}]
        $product = [{$product->getProductStatus()}]<div class="stockFlagBox [{if $product->showCssFlag()}][{$product->showCssFlag()}][{/if}]"></div><br/>
    [{/if}]

[{/if}]
<hr><hr>
[{if $_product}]
    [{* $product existiert beim Vater und bei dem Standartartikel *}]
    $_product existiert:<br/>
    [{if !$_product->isNotBuyable()}]
        kaufbar<br/>
        <pre>
                [{*$product|@debug_print_var*}]
            </pre>
    [{/if}]
    [{if $_product->getProductStatus()}]
        $_product = [{$_product->getProductStatus()}]<div class="stockFlagBox [{if $_product->showCssFlag()}][{$_product->showCssFlag()}][{/if}]"></div><br/>
    [{/if}]
[{/if}]

[{*}]
<pre>
    <b>get_template_vars<b>
    <hr>
        [{php}]
            print_r($this->get_template_vars('$_debug_keys'));
            #exit;
        [{/php}]
    <hr>
</pre>


<pre>
    <b>get_object_vars<b>
    <hr>
        [{php}]
            print_r($this->get_object_vars('objekt'));
            #exit;
        [{/php}]
    <hr>
</pre>
[{ /Debug *}]


[{* /Debug *}]


