[{assign var="dropship" value=$oxcmp_user->oxuser__dropship->value}]

[{block name="footer_services"}]
	<span class="headline">[{oxmultilang ident="SERVICES" }]</span>
	<ul>
		<li><a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=contact" }]">[{ oxmultilang ident="CONTACT" }]</a></li>
		<li><a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account" }]" rel="nofollow">[{ oxmultilang ident="ACCOUNT" }]</a></li>
		<li><a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=myfavorites&fnc=showLists" }]" rel="nofollow">[{ oxmultilang ident="BTN_MYFAVORITES_LISTS" }]</a></li>
        [{if $dropship == 1}]
            <li><a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=quickorder&fnc=showForm" }]" rel="nofollow">[{ oxmultilang ident="BTN_QUICKORDER" }]</a></li>
        [{/if}]
        <li><a href="[{$oViewConf->getLogoutLink() }]" class="submitButton largeButton">[{oxmultilang ident="LOGOUT"}]</a></li>
	</ul>
    [{*debug
         <pre>
            [{$oxcmp_user->oxuser__dropship->value|var_dump}]
        </pre>*}]
[{/block}]
