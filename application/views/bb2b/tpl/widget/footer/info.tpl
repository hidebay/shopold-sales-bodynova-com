[{assign var="aServices" value=$oView->getServicesList()}]
[{assign var="aServiceItems" value=$oView->getServicesKeys()}]
[{block name="footer_information"}]
	<span class="headline">[{oxmultilang ident="INFORMATION" }]</span>
	<ul>
		[{foreach from=$aServiceItems item=sItem}]
			[{if isset($aServices.$sItem)}]
				<li><a href="[{$aServices.$sItem->getLink()}]">[{$aServices.$sItem->oxcontents__oxtitle->value}]</a></li>
			[{/if}]
		[{/foreach}]
	</ul>
[{/block}]