<!-- Logo -->
<li role="presentation" class="logo hidden-xs hidden-sm">
    <a href="[{$oViewConf->getHomeLink()}]" title="[{$oxcmp_shop->oxshops__oxtitleprefix->value}]">
        <img alt="Bodynova Logo" src="[{$oViewConf->getImageUrl('logo-small.png')}]">
    </a>
    [{if !$oViewConf->getUser()}]
        <!-- Flaggen-Box -->
        <div id="languageSwitcher" class="dropdown symbol-flags">
            <button id="btnLangaugeMenue" class="btn btn-default dropdown-toggle" aria-expanded="true" data-toggle="dropdown" type="button">
                [{foreach from=$oxcmp_lang item=_lng}]
                    [{if $_lng->selected}]
                        [{assign var="sLangImg" value=""|cat:$_lng->abbr|cat:".png"}]
                        <img src="[{$oViewConf->getImageUrl($sLangImg)}]">
                        [{*$_lng->name*}]
                    [{/if}]
                [{/foreach}]
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="btnLangaugeMenue" role="menu">
                [{foreach from=$oxcmp_lang item=_lng}]
                    [{assign var="sLangImg" value="lang/"|cat:$_lng->abbr|cat:".png"}]
                    <li>
                        <a class="flag [{$_lng->abbr }] [{if $_lng->selected}]selected[{/if}]" title="[{$_lng->name}]" href="[{$_lng->link|oxaddparams:$oView->getDynUrlParams()}]" hreflang="[{$_lng->abbr }]">
                            <img src="[{$oViewConf->getImageUrl($sLangImg)}]">
                            [{*$_lng->name*}]
                        </a>
                    </li>
                [{/foreach}]
            </ul>
        </div><!-- Ende: Flaggen-Box -->
    [{/if}]
</li>
