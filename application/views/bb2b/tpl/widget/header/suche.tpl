[{* Wird überschrieben von:
    modules/autosuggest/views/blocks/search.tpl *}]
[{block name="widget_header_search_form"}]
    [{if $oView->showSearch() }]
        <!-- Suche  lala-->
        <li role="presentation">
            <form class="navbar-form" role="search" action="[{$oViewConf->getSelfActionLink() }]" method="get" name="search">
                <div class="form-group form-group-suche">
                    [{$oViewConf->getHiddenSid()}]
                    <input type="hidden" name="cl" value="search">
                    [{block name="header_search_field"}]
                        <input type="text" class="form-control breite-100pc" id="searchParam" name="searchparam"  placeholder="[{oxmultilang ident="SEARCH"}]" value="[{$oView->getSearchParamForHtml()}]">
                    [{/block}]
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span>
                    </button>
                    <div id="results"></div>
                </div>
            </form>
        </li>
    [{/if}]
[{/block}]
[{oxid_include_widget cl="oxwMiniBasket" nocookie=$blAnon force_sid=$force_sid}]
