[{if $oView->isLanguageLoaded()}]

    [{* Abmeldebutton *}]
    <div class="btn-logout pull-right">
        <div id="languageSwitcher" class="dropdown">
            <button id="btnLangaugeMenue" class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
                [{foreach from=$oxcmp_lang item=_lng}]
                    [{if $_lng->selected}]
                        [{assign var="sLangImg" value=""|cat:$_lng->abbr|cat:".png"}]
                        <img src="[{$oViewConf->getImageUrl($sLangImg)}]">
                        [{*$_lng->name*}]
                    [{/if}]
                [{/foreach}]
                <span class="caret"></span>
            </button>
            [{*$_lng|var_dump*}]
            <ul class="dropdown-menu" role="menu" aria-labelledby="btnLangaugeMenue">
                [{foreach from=$oxcmp_lang item=_lng}]
                    [{assign var="sLangImg" value="lang/"|cat:$_lng->abbr|cat:".png"}]
                    <li>
                        <a class="flag [{$_lng->abbr }] [{if $_lng->selected}]selected[{/if}]" title="[{$_lng->name}]" href="[{$_lng->link|oxaddparams:$oView->getDynUrlParams()}]" hreflang="[{$_lng->abbr }]">
                            <img src="[{$oViewConf->getImageUrl($sLangImg)}]">
                            [{*$_lng->name*}]
                        </a>
                    </li>
                [{/foreach}]
            </ul>

        </div>
        <div class="symbol-logout">
            [{*}]<a href="[{$oViewConf->getLogoutLink()}]" class="btn btn-default btn-sm btn-danger"><span class="glyphicon glyphicon-off" aria-hidden="true"></span>&nbsp;[{oxmultilang ident="LOGOUT"}]</a>[{*}]
            <a href="[{$oViewConf->getLogoutLink()}]" class="btn btn-default btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="[{oxmultilang ident="LOGOUT"}]"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a>

        </div>

    </div>
    [{*
                    [{if $_lng->selected}]
                        [{capture name="languageSelected"}]
                            <a class="flag [{$_lng->abbr }]" title="[{$_lng->name}]" href="[{$_lng->link|oxaddparams:$oView->getDynUrlParams()}]" hreflang="[{$_lng->abbr }]"><span style="background-image:url('[{$oViewConf->getImageUrl($sLangImg)}]')" >[{$_lng->name}]</span></a>
                        [{/capture}]
                    [{/if}]



        [{capture name="languageList"}]x
        <p id="languageTrigger" class="selectedValue">

        </p>
        <div class="flyoutBox">
        <ul id="languages" class="corners">
            <li class="active">[{$smarty.capture.languageSelected}]</li>
            [{$smarty.capture.languageList}]
        </ul>
        </div>
    </div>
    *}]

[{/if}]
[{oxscript widget=$oView->getClassName()}]
