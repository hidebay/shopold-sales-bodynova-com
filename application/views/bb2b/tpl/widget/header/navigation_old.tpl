<!-- Fixierte Navbar -->
<!-- erl:
<div id="header" class="navbar navbar-default navbar-fixed-top container-fluid" role="navigation">
    [{if $oViewConf->getUser()}]
        <div class="logo">
            <a id="logo" href="[{$oViewConf->getHomeLink()}]" title="[{$oxcmp_shop->oxshops__oxtitleprefix->value}]" class="pull-left"></a>
        </div>

        <div class="navbar-collapse collapse">
            <div class="sn-div">
                [{* Suche *}]
                <div class="searchdiv">
                    [{include file="widget/header/search.tpl"}]
                </div>
                [{* /searchdiv*}]
-->
                [{* Navigation *}]
                <div class="nav-div">
                    <ul id="menu" class="nav navbar-nav">
                        [{foreach from=$oxcmp_categories item=ocat key=catkey name=root}]
                            [{if $ocat->getIsVisible() }]
                                [{foreach from=$ocat->getContentCats() item=oTopCont name=MoreTopCms}]
                                    [{assign var="iCatCnt" value=$iCatCnt+1 }]
                                    [{assign var="iAllCatCount" value=$iAllCatCount+1 }]
                                    [{if !$bHasMore && ($iCatCnt >= $oView->getTopNavigationCatCnt())}]
                                        [{assign var="bHasMore" value="true"}]
                                        [{assign var="iCatCnt" value=$iCatCnt+1}]
                                    [{/if}]

                                    [{if $iCatCnt <= $oView->getTopNavigationCatCnt()}]
                                        <li><a href="[{$oTopCont->getLink()}]">[{$oTopCont->oxcontents__oxtitle->value}]</a></li>
                                    [{else}]
                                        [{capture append="moreLinks"}]
                                            <li><a href="[{$oTopCont->getLink()}]">[{$oTopCont->oxcontents__oxtitle->value}]</a></li>
                                        [{/capture}]
                                    [{/if}]
                                [{/foreach}]

                                [{assign var="iCatCnt" value=$iCatCnt+1 }]
                                [{if !$bHasMore && ($iCatCnt >= $oView->getTopNavigationCatCnt())}]
                                    [{assign var="bHasMore" value="true"}]
                                    [{assign var="iCatCnt" value=$iCatCnt+1}]
                                [{/if}]

                                [{if $iCatCnt <= $oView->getTopNavigationCatCnt()}]
                                    <li class="dropdown [{if $homeSelected == 'false' && $ocat->expanded}]current[{/if}]">
                                        [{if $ocat->getSubCats()}]
                                            <a class="[{if $homeSelected == 'false' && $ocat->expanded}]current[{/if}] " data-toggle="dropdown" href="[{$ocat->getLink()}]">[{$ocat->oxcategories__oxtitle->value}] <span class="caret"></span></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a class="[{if $homeSelected == 'false' && $ocat->expanded}]current[{/if}] " href="[{$ocat->getLink()}]">[{oxmultilang ident="AlleArtikel"}] <span class="caret"></span></a></li>
                                                [{foreach from=$ocat->getSubCats() item=osubcat key=subcatkey name=SubCat}]
                                                    [{*foreach from=$osubcat->getContentCats() item=ocont name=MoreCms}]
                                                        <li><a href="[{$ocont->getLink()}]">[{$ocont->oxcontents__oxtitle->value}]</a></li>
                                                    [{/foreach*}]
                                                    [{if $osubcat->getIsVisible() }]
                                                        <li class="[{if $homeSelected == 'false' && $osubcat->expanded}]current[{/if}] [{if $osubcat->getSubCats()}]dropdown-submenu[{/if}]" ><a [{if $homeSelected == 'false' && $osubcat->expanded}]class="current"[{/if}] href="[{$osubcat->getLink()}]">[{$osubcat->oxcategories__oxtitle->value}]</a>
                                                            [{if $osubcat->getSubCats()}]
                                                                <ul class="dropdown-menu" role="menu">
                                                                    [{foreach from=$osubcat->getSubCats() item=osubsubcat key=subsubcatkey name=SubSubCat}]
                                                                        <li><a class="dropdown-toggle"  href="[{$osubsubcat->getLink()}]">[{$osubsubcat->oxcategories__oxtitle->value}]</a></li>
                                                                    [{/foreach}]
                                                                </ul>
                                                            [{/if}]
                                                        </li>
                                                    [{/if}]
                                                [{/foreach}]
                                            </ul>
                                        [{else}]
                                            <a  [{if $homeSelected == 'false' && $ocat->expanded}]class="current"[{/if}] href="[{$ocat->getLink()}]">[{$ocat->oxcategories__oxtitle->value}]</a>
                                        [{/if}]
                                    </li>
                                [{else}]
                                    [{capture append="moreLinks"}]
                                        <li [{if $homeSelected == 'false' && $ocat->expanded}]class="current"[{/if}]>
                                            <a href="[{$ocat->getLink()}]">[{$ocat->oxcategories__oxtitle->value}]</a>
                                        </li>
                                    [{/capture}]
                                [{/if}]
                            [{/if}]
                        [{/foreach}]
                    </ul>
                    <!--</div>-->
                </div>
                [{* /nav-div *}]


              <!--  [{* Service *}]
                <div class="service-div">
                    [{oxid_include_widget cl="oxwMiniBasket" nocookie=$blAnon force_sid=$force_sid}]
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Navigation ein-/ausblenden</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                </div>
                [{* /service-div *}]-->


            </div><!--sn-div-->



            [{*}] Original:
                        <div class="sn-div">
                            <div class="searchdiv">
                                [{include file="widget/header/search.tpl"}]
                            </div>

                            <div class="service-div">
                                [{oxid_include_widget cl="oxwMiniBasket" nocookie=$blAnon force_sid=$force_sid}]

                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="sr-only">Navigation ein-/ausblenden</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                            </div>

                            <div class="nav-div">
                                <ul id="menu" class="nav navbar-nav">
                                    [{foreach from=$oxcmp_categories item=ocat key=catkey name=root}]
                                        [{if $ocat->getIsVisible() }]
                                            [{foreach from=$ocat->getContentCats() item=oTopCont name=MoreTopCms}]
                                                [{assign var="iCatCnt" value=$iCatCnt+1 }]
                                                [{assign var="iAllCatCount" value=$iAllCatCount+1 }]
                                                [{if !$bHasMore && ($iCatCnt >= $oView->getTopNavigationCatCnt())}]
                                                    [{assign var="bHasMore" value="true"}]
                                                    [{assign var="iCatCnt" value=$iCatCnt+1}]
                                                [{/if}]
                                                [{if $iCatCnt <= $oView->getTopNavigationCatCnt()}]
                                                    <li><a href="[{$oTopCont->getLink()}]">[{$oTopCont->oxcontents__oxtitle->value}]</a></li>
                                                [{else}]
                                                    [{capture append="moreLinks"}]
                                                        <li><a href="[{$oTopCont->getLink()}]">[{$oTopCont->oxcontents__oxtitle->value}]</a></li>
                                                    [{/capture}]
                                                [{/if}]
                                            [{/foreach}]

                                            [{assign var="iCatCnt" value=$iCatCnt+1 }]
                                            [{if !$bHasMore && ($iCatCnt >= $oView->getTopNavigationCatCnt())}]
                                                [{assign var="bHasMore" value="true"}]
                                                [{assign var="iCatCnt" value=$iCatCnt+1}]
                                            [{/if}]
                                            [{if $iCatCnt <= $oView->getTopNavigationCatCnt()}]
                                                <li class="dropdown [{if $homeSelected == 'false' && $ocat->expanded}]current[{/if}]">
                                                    [{if $ocat->getSubCats()}]
                                                        <a class="[{if $homeSelected == 'false' && $ocat->expanded}]current[{/if}] " data-toggle="dropdown" href="[{$ocat->getLink()}]">[{$ocat->oxcategories__oxtitle->value}] <span class="caret"></span></a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a class="[{if $homeSelected == 'false' && $ocat->expanded}]current[{/if}] " href="[{$ocat->getLink()}]">[{oxmultilang ident="AlleArtikel"}] <span class="caret"></span></a></li>
                                                            [{foreach from=$ocat->getSubCats() item=osubcat key=subcatkey name=SubCat}]

                                                                [{*foreach from=$osubcat->getContentCats() item=ocont name=MoreCms}]
                                                                    <li><a href="[{$ocont->getLink()}]">[{$ocont->oxcontents__oxtitle->value}]</a></li>
                                                                [{/foreach*}]
            [{*}]
                                                                [{if $osubcat->getIsVisible() }]
                                                                    <li class="[{if $homeSelected == 'false' && $osubcat->expanded}]current[{/if}] [{if $osubcat->getSubCats()}]dropdown-submenu[{/if}]" ><a [{if $homeSelected == 'false' && $osubcat->expanded}]class="current"[{/if}] href="[{$osubcat->getLink()}]">[{$osubcat->oxcategories__oxtitle->value}]</a>
                                                                        [{if $osubcat->getSubCats()}]
                                                                            <ul class="dropdown-menu" role="menu">
                                                                                [{foreach from=$osubcat->getSubCats() item=osubsubcat key=subsubcatkey name=SubSubCat}]
                                                                                    <li><a class="dropdown-toggle"  href="[{$osubsubcat->getLink()}]">[{$osubsubcat->oxcategories__oxtitle->value}]</a></li>
                                                                                [{/foreach}]
                                                                            </ul>
                                                                        [{/if}]
                                                                    </li>
                                                                [{/if}]
                                                            [{/foreach}]
                                                        </ul>
                                                    [{else}]
                                                        <a  [{if $homeSelected == 'false' && $ocat->expanded}]class="current"[{/if}] href="[{$ocat->getLink()}]">[{$ocat->oxcategories__oxtitle->value}]</a>
                                                    [{/if}]
                                                </li>
                                            [{else}]
                                                [{capture append="moreLinks"}]
                                                    <li [{if $homeSelected == 'false' && $ocat->expanded}]class="current"[{/if}]>
                                                        <a href="[{$ocat->getLink()}]">[{$ocat->oxcategories__oxtitle->value}]</a>
                                                    </li>
                                                [{/capture}]
                                            [{/if}]
                                        [{/if}]
                                    [{/foreach}]
                                </ul>
                            </div>
                        </div>
            [{*}]


        </div><!--/.nav-collapse -->
    [{/if}]
</div>
