<!-- Navbar oben fixiert: -->
<nav class="navbar navbar-default navbar-fixed-top nav nav-tabs nav-justified" id="header">

        <!-- Navigation -->
        <ul class="nav nav-tabs nav-justified">


                [{* Logo: *}]
                [{include file="widget/header/logo.tpl"}]

            [{if $oViewConf->getUser()}]
                [{* Suche: und Servicebox *}]
                [{include file="widget/header/search.tpl"}]

                [{* Navi-Leiste *}]
                [{include file="widget/header/navi.tpl"}]

             [{else}]

            [{/if}]

        </ul><!-- Ende: Navigation -->

</nav><!-- Ende: Navbar oben fixiert: -->
