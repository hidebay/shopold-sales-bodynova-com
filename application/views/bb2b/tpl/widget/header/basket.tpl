[{if $oView->isLanguageLoaded()}]
    <li class="rest" role="presentation">
        <form class="navbar-form" role="form">
            <!-- Minibasket -->
            <div class="form-group form-group-service " id="[{$_prefix}]miniBasket">
                <a class="minib" href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=basket" }]">
                    <span class="counter" style="[{if $oxcmp_basket->getItemsCount() < 1}]display: none;[{/if}]">
                        <span class="badge" data-placement="bottom" data-toggle="tooltip" title="[{oxmultilang ident="ADDED_TO_BASKET_CONFIRMATION" }]">[{$oxcmp_basket->getItemsCount()}]</span>
                    </span>
                    <img id="[{$_prefix}]minibasketIcon" class="basket" alt="Basket" src="[{$oViewConf->getImageUrl('basket.png')}]" data-original-title="" title="">
                </a>



                    <!-- Flaggen-Box -->
                    <div id="languageSwitcher" class="dropdown symbol-flags">
                        <button id="btnLangaugeMenue" class="btn btn-default dropdown-toggle" aria-expanded="true" data-toggle="dropdown" type="button">
                            [{foreach from=$oxcmp_lang item=_lng}]
                                [{if $_lng->selected}]
                                    [{assign var="sLangImg" value=""|cat:$_lng->abbr|cat:".png"}]
                                    <img src="[{$oViewConf->getImageUrl($sLangImg)}]">
                                    [{*$_lng->name*}]
                                [{/if}]
                            [{/foreach}]
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="btnLangaugeMenue" role="menu">
                            [{foreach from=$oxcmp_lang item=_lng}]
                                [{assign var="sLangImg" value="lang/"|cat:$_lng->abbr|cat:".png"}]
                                <li>
                                    <a class="flag [{$_lng->abbr }] [{if $_lng->selected}]selected[{/if}]" title="[{$_lng->name}]" href="[{$_lng->link|oxaddparams:$oView->getDynUrlParams()}]" hreflang="[{$_lng->abbr }]">
                                        <img src="[{$oViewConf->getImageUrl($sLangImg)}]">
                                        [{*$_lng->name*}]
                                    </a>
                                </li>
                            [{/foreach}]
                        </ul>
                    </div><!-- Ende: Flaggen-Box -->

                <!-- Logout Symbol -->
                <a class="btn btn-default btn-sm btn-danger minilogout" title="[{oxmultilang ident="LOGOUT"}]" data-placement="bottom" data-toggle="tooltip" href="[{$oViewConf->getLogoutLink()}]">
                    <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                </a><!-- Ende: Logout Symbol -->

                [{* Schalter: *}]
                [{include file="widget/header/schalter.tpl"}]

            </div><!-- Ende: Minibasket -->
        </form>
    </li>
[{/if}]
[{oxscript widget=$oView->getClassName()}]
