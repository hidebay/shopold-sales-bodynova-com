<div id="footer" class="container">
	<hr>
	<div class="row">
		<div class="col-lg-12">
			<div class="col-md-6 pull-left" style="vertical-align: bottom;">
				<span class="headline">&copy; 2015 Bodynova GmbH</span>
				<br>
				<a href="[{oxgetseourl ident="oxagb" type="oxcontent" }]">[{ oxmultilang ident="AGB_LINK_TITLE" }]</a>
				&nbsp;|&nbsp;
				<a href="[{oxgetseourl ident="oximpressum" type="oxcontent" }]">[{ oxmultilang ident="IMPRINT_LINK_TITLE" }]</a>
			</div>
			<div class="col-md-3">
				[{oxid_include_widget cl="oxwServiceList" noscript=1 nocookie=1}]
			</div>
			<div class="col-md-3">
				[{oxid_include_widget cl="oxwInformation" noscript=1 nocookie=1}]
			</div>
		</div>
	</div>
</div>