[{capture append="oxidBlock_pageBody"}]
    [{if $oView->showRDFa()}]
        [{include file="rdfa/rdfa.tpl"}]
    [{/if}]
	[{include file="layout/header.tpl"}]
	[{*
	[{if $sidebar}]
		<div id="sidebar">
			[{include file="layout/sidebar.tpl"}]
		</div>
	[{/if}]
	*}]
    <div id="content" class="container">
        [{* Banner-Slider *}]
        <div class="slidercontent">
            [{* Banner *}]
            [{if $oView->getClassName()=='start' && $oView->getBanners()|@count > 0 }]
                <div class="oxSlider">
                    [{include file="widget/promoslider.tpl" }]
                </div>
            [{/if}]
        </div>

        [{if $oView->getClassName() ne "start" && !$blHideBreadcrumb}]
		    [{include file="widget/breadcrumb.tpl"}]
		[{/if}]

		[{include file="message/errors.tpl"}]

		[{foreach from=$oxidBlock_content item="_block"}]
			[{$_block}]<br>
		[{/foreach}]
	</div>
	[{include file="layout/footer.tpl"}]
	[{*<div id="page" class="container [{if $sidebar}] sidebar[{$sidebar}][{/if}]"></div>*}]

    [{*include file="widget/facebook/init.tpl"*}]
    [{*if $oView->isPriceCalculated() *}]
        [{block name="layout_page_vatinclude"}]
        [{oxifcontent ident="oxdeliveryinfo" object="oCont"}]
            [{assign var="tsBadge" value=""}]
            [{*if $oView->getTrustedShopId()}]
                [{assign var="tsBadge" value="TsBadge"}]
            [{/if*}]

            <div id="incVatMessage[{$tsBadge}]">
                * <span class="deliveryInfo">[{ oxmultilang ident="PLUS" }]<a href="" rel="nofollow">[{oxmultilang ident="PLUS_SHIPPING2"}]</a></span>
                [{if $oView->isVatIncluded()}]
                    * <span class="deliveryInfo">[{ oxmultilang ident="PLUS_SHIPPING" }]<a href="[{$oCont->getLink() }]" rel="nofollow">[{oxmultilang ident="PLUS_SHIPPING2"}]</a></span>
                [{else}]
                    * <span class="deliveryInfo">[{ oxmultilang ident="PLUS" }]<a href="[{$oCont->getLink() }]" rel="nofollow">[{oxmultilang ident="PLUS_SHIPPING2"}]</a></span>
                [{/if}]
            </div>
        [{/oxifcontent }]
        <div id="incVatMessage">
            <a href="#" class="back-to-top">
                <i class="fa fa-arrow-circle-up"></i>
            </a>
            <span class="deliveryInfo">[{oxmultilang ident="INCL_TAX_AND_PLUS_SHIPPING"}]</span>
        </div>
        [{/block}]
    [{*/if*}]


[{/capture}]
[{include file="layout/base.tpl"}]
