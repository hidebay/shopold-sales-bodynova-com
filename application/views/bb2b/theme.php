<?php

/**
 * Theme Information
 */
$aTheme = array(
    'id'           => 'bb2b',
    'title'        => 'BodynovaB2B',
    'description'  => 'Bodynova Busines 2 Bussines Theme',
    'thumbnail'    => 'theme.jpg',
    'version'      => '1.0',
    'author'       => 'Bodynova',
	'parentTheme'  => 'azure',
	'parentVersions'=>array('1.3.1'),
);