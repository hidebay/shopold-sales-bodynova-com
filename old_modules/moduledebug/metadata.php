<?php
/**
 *    This file is part of OXID eShop Community Edition.
 *
 *    OXID eShop Community Edition is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    OXID eShop Community Edition is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with OXID eShop Community Edition.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2012
 */

/**
 * Metadata version
 */
$sMetadataVersion = '1.0';

/**
 * Module information
 */
$aModule = array(
    'id'           => 'moduledebug',
    'title'        => 'Module Debug',
    'description'  => array(
            'de' => 'Modul für erweiterte Kontrolle und Fehlerbehebung',
            'en' => 'Module for advanced control and troubleshooting',
    ),
    'thumbnail'    => 'module.png',
    'version'      => '1.0',
    'author'       => 'OXID eSales AG',
    'url'          => 'http://www.oxid-sales.com',
    'email'        => 'info@oxid-esales.com',
    'extend'       => array(),
    'files'        => array("module_debug" => "moduledebug/admin/module_debug.php"),
    'templates'    => array("module_debug.tpl" => "moduledebug/out/admin/tpl/module_debug.tpl")
);
