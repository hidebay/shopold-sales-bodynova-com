<?php
/**
 *    This file is part of OXID eShop Community Edition.
 *
 *    OXID eShop Community Edition is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    OXID eShop Community Edition is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with OXID eShop Community Edition.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2012
 */

class module_debug extends shop_config {
    protected $_sThisTemplate = 'module_debug.tpl';

    public function getEditObjectId()
    {
        return $this->getConfig()->getShopId();
    }

    public function render()
    {
        parent::render();
        $this->_aViewData["oxid"] = parent::getEditObjectId();
        return $this->_sThisTemplate;
    }

    public function save()
    {
        $this->saveConfVars();
    }

    /**
     * Serialize config var depending on it's type
     *
     * @param string $sType  var type
     * @param string $sName  var name
     * @param mixed  $mValue var value
     *
     * @return string
     */
    public function _serializeConfVar($sType, $sName, $mValue)
    {
        if (is_array($mValue)) {
            unset($mValue[-1]);
        }

        if($sName == 'aLegacyModules') {
            $oStr = getStr();
            $aData  = array();
            foreach ($mValue as $sModuleId => $aModule) {
                foreach ($aModule as $sKey => $mVal) {
                    if (strpos($mVal, "=>") !== false) {
                        $aModule[$sKey] = $this->_multilineToAarray($mVal);
                    } else {
                        $aModule[$sKey] = $mVal;
                    }

                    if ($sKey == 'id') {
                        $sModuleId = $mVal;
                        $aModule['id'] = $sModuleId;
                    }
                }

                if ($sModuleId) {
                    $aData[$sModuleId] = $aModule;
                }
            }
            $sData = serialize($aData);
        }

        if($sName == 'aModuleFiles' || $sName == 'aModuleTemplates') {
            $oStr = getStr();
            $aData  = array();
            foreach ($mValue as $sModuleId => $aModule) {
                $sModuleId = $aModule['id'];
                $sModuleArray = $this->_multilineToAarray($aModule['array']);

                if ($sModuleId) {
                    $aData[$sModuleId] = $sModuleArray;
                }
            }
            $sData = serialize($aData);
        }
        if (!$sData) {
            $sData = parent::_serializeConfVar($sType, $sName, $mValue);
        }
        return $sData;
    }

    /**
     * Unserialize config var depending on it's type
     *
     * @param string $sType  var type
     * @param string $sName  var name
     * @param string $sValue var value
     *
     * @return mixed
     */
    public function _unserializeConfVar($sType, $sName, $sValue)
    {
        $mData = parent::_unserializeConfVar($sType, $sName, $sValue);

        if($sName == 'aLegacyModules') {
            $oStr = getStr();
            $mData  = array();
            $aValue = unserialize( $sValue );
            foreach ($aValue as $sModuleId => $aModule) {
                foreach ($aModule as $sKey => $mValue) {
                    if (is_array($mValue)) {
                        $aModule[$sKey] = $oStr->htmlentities($this->_aarrayToMultiline($mValue));
                    } else {
                        $aModule[$sKey] = $oStr->htmlentities($mValue);
                    }
                }
                $mData[$sModuleId] = $aModule;
            }
        }

        if( $sName == 'aModuleFiles' || $sName == 'aModuleTemplates') {
            $oStr = getStr();
            $mData  = array();
            $aValue = unserialize( $sValue );
            foreach ($aValue as $sModuleId => $aModule) {
                $mData[$sModuleId] = $oStr->htmlentities($this->_aarrayToMultiline($aModule));
            }
        }

        return $mData;
    }


}