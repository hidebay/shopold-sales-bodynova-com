<?php
/**
 *    This file is part of OXID eShop Community Edition.
 *
 *    OXID eShop Community Edition is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    OXID eShop Community Edition is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with OXID eShop Community Edition.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2012
 */

$sLangName  = "English";
$iLangNr    = 1;
// -------------------------------
// RESOURCE IDENTITFIER = STRING
// -------------------------------
$aLang = array(

'charset'                                         => 'ISO-8859-15',
'tbclmodule_debug'                                => 'Debug',
'MODULE_DEBUG_INSTALLED'                          => 'Installed Modules',
'MODULE_DEBUG_DISABLED'                           => 'Disabled Modules',
'MODULE_DEBUG_PATHS'                              => 'Module Paths',
'MODULE_DEBUG_FILES'                              => 'Module Files',
'MODULE_DEBUG_FILES_ID'                           => 'ID',
'MODULE_DEBUG_FILES_FILES'                        => 'Files',
'MODULE_DEBUG_TEMPLATES'                          => 'Module Templates',
'MODULE_DEBUG_TEMPLATES_ID'                       => 'ID',
'MODULE_DEBUG_TEMPLATES_TEMPLATES'                => 'Templates',
'MODULE_DEBUG_LEGACY'                             => 'Legacy Modules',
'MODULE_DEBUG_LEGACY_ID'                          => 'ID',
'MODULE_DEBUG_LEGACY_TITLE'                       => 'Module name',
'MODULE_DEBUG_LEGACY_EXTEND'                      => 'Extended classes',
);

