[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign}]

<script type="text/javascript">
<!--
function _groupExp(el) {
    var _cur = el.parentNode;

    if (_cur.className == "exp") _cur.className = "";
      else _cur.className = "exp";
}
[{if $smarty.const.OXID_VERSION_EE}]
function editThis( sID )
{
    var oTransfer = top.basefrm.edit.document.getElementById( "transfer" );
    oTransfer.oxid.value = '';
    oTransfer.cl.value = top.oxid.admin.getClass( sID );

    //forcing edit frame to reload after submit
    top.forceReloadingEditFrame();

    var oSearch = top.basefrm.list.document.getElementById( "search" );
    oSearch.oxid.value = sID;
    oSearch.updatenav.value = 1;
    oSearch.submit();
}
[{/if}] [{* OXID_VERSION_EE *}]
//-->
</script>

[{ if $readonly}]
    [{assign var="readonly" value="readonly disabled"}]
[{else}]
    [{assign var="readonly" value=""}]
[{/if}]

[{cycle assign="_clear_" values=",2" }]

<form name="transfer" id="transfer" action="[{ $oViewConf->getSelfLink() }]" method="post">
    [{ $oViewConf->getHiddenSid() }]
    <input type="hidden" name="oxid" value="[{ $oxid }]">
    <input type="hidden" name="cl" value="module_debug">
    <input type="hidden" name="fnc" value="">
    <input type="hidden" name="actshop" value="[{$oViewConf->getActiveShopId()}]">
    <input type="hidden" name="updatenav" value="">
    <input type="hidden" name="editlanguage" value="[{ $editlanguage }]">
</form>

<form name="myedit" id="myedit" action="[{ $oViewConf->getSelfLink() }]" method="post">
[{ $oViewConf->getHiddenSid() }]
<input type="hidden" name="cl" value="module_debug">
<input type="hidden" name="fnc" value="save">
<input type="hidden" name="oxid" value="[{ $oxid }]">
<input type="hidden" name="editval[oxshops__oxid]" value="[{ $oxid }]">




    <div class="groupExp">
        <div class="">
            <a class="rc" onclick="_groupExp(this);return false;" href="#"><b>[{ oxmultilang ident="MODULE_DEBUG_INSTALLED" }]</b></a>
            <dl>
                <dt>
                    <textarea wrap="off" class="txtfield" style="width: 430px;" name=confaarrs[aModules] [{ $readonly }]>[{$confaarrs.aModules}]</textarea>
                </dt>
                <div class="spacer"></div>
            </dl>
        </div>
    </div>
    <div class="groupExp">
        <div class="">
            <a class="rc" onclick="_groupExp(this);return false;" href="#"><b>[{ oxmultilang ident="MODULE_DEBUG_DISABLED" }]</b></a>
            <dl>
                <dt>
                    <textarea wrap="off" class="txtfield" style="width: 430px;" name=confarrs[aDisabledModules] [{ $readonly }]>[{$confarrs.aDisabledModules}]</textarea>
                </dt>
                <div class="spacer"></div>
            </dl>
        </div>
    </div>
    <div class="groupExp">
        <div class="">
            <a class="rc" onclick="_groupExp(this);return false;" href="#"><b>[{ oxmultilang ident="MODULE_DEBUG_PATHS" }]</b></a>
            <dl>
                <dt>
                    <textarea wrap="off" class="txtfield" style="width: 430px;" name=confaarrs[aModulePaths] [{ $readonly }]>[{$confaarrs.aModulePaths}]</textarea>
                </dt>
                <div class="spacer"></div>
            </dl>
        </div>
    </div>
        <div class="groupExp">
        <div class="">
            <a class="rc" onclick="_groupExp(this);return false;" href="#"><b>[{ oxmultilang ident="MODULE_DEBUG_FILES" }]</b></a>
            [{foreach from=$confaarrs.aModuleFiles key=id item=aFiles}]
            <dl>
                <dt>
                    <input class="txt" style="width: 430px;" name=confaarrs[aModuleFiles][[{$id}]][id]" [{ $readonly }] value="[{$id}]" >
                </dt>
                <dd>
                    [{ oxmultilang ident="MODULE_DEBUG_FILES_ID" }]
                </dd>
            </dl>
            <dl>
                <dt>
                    <textarea wrap="off" class="txtfield" style="width: 430px;" name=confaarrs[aModuleFiles][[{$id}]][array]" [{ $readonly }]>[{$aFiles}]</textarea>
                </dt>
                <dd>
                    [{ oxmultilang ident="MODULE_DEBUG_FILES_FILES" }]
                </dd>
                <div class="spacer"></div>
            <dl>
            [{/foreach}]
            <dl>
                <dt>
                    <input class="txt" style="width: 430px;" name=confaarrs[aModuleFiles][-1][id]" [{ $readonly }] value="" >
                </dt>
                <dd>
                    [{ oxmultilang ident="MODULE_DEBUG_FILES_ID" }]
                </dd>
            </dl>
            <dl>
                <dt>
                    <textarea wrap="off" class="txtfield" style="width: 430px;" name=confaarrs[aModuleFiles][-1][array]" [{ $readonly }]></textarea>
                </dt>
                <dd>
                    [{ oxmultilang ident="MODULE_DEBUG_FILES_FILES" }]
                </dd>
                <div class="spacer"></div>
            <dl>
         </div>
    </div>
    <div class="groupExp">
        <div class="">
            <a class="rc" onclick="_groupExp(this);return false;" href="#"><b>[{ oxmultilang ident="MODULE_DEBUG_TEMPLATES" }]</b></a>
            [{foreach from=$confaarrs.aModuleTemplates key=id item=aTemplates}]
            <dl>
                <dt>
                    <input class="txt" style="width: 430px;" name=confaarrs[aModuleTemplates][[{$id}]][id]" [{ $readonly }] value="[{$id}]" >
                </dt>
                <dd>
                    [{ oxmultilang ident="MODULE_DEBUG_TEMPLATES_ID" }]
                </dd>
            </dl>
            <dl>
                <dt>
                    <textarea wrap="off" class="txtfield" style="width: 430px;" name=confaarrs[aModuleTemplates][[{$id}]][array]" [{ $readonly }]>[{$aTemplates}]</textarea>
                </dt>
                <dd>
                    [{ oxmultilang ident="MODULE_DEBUG_TEMPLATES_TEMPLATES" }]
                </dd>
                <div class="spacer"></div>
            <dl>
            [{/foreach}]
            <dl>
                <dt>
                    <input class="txt" style="width: 430px;" name=confaarrs[aModuleTemplates][-1][id]" [{ $readonly }] value="" >
                </dt>
                <dd>
                    [{ oxmultilang ident="MODULE_DEBUG_TEMPLATES_ID" }]
                </dd>
            </dl>
            <dl>
                <dt>
                    <textarea wrap="off" class="txtfield" style="width: 430px;" name=confaarrs[aModuleTemplates][-1][array]" [{ $readonly }]></textarea>
                </dt>
                <dd>
                    [{ oxmultilang ident="MODULE_DEBUG_TEMPLATES_TEMPLATES" }]
                </dd>
                <div class="spacer"></div>
            <dl>
         </div>
    </div>
    <div class="groupExp">
        <div class="">
            <a class="rc" onclick="_groupExp(this);return false;" href="#"><b>[{ oxmultilang ident="MODULE_DEBUG_LEGACY" }]</b></a>
            [{foreach from=$confaarrs.aLegacyModules key=id item=aLegacy}]
            <dl>
                <dt>
                    <input class="txt" style="width: 430px;" name=confaarrs[aLegacyModules][[{$id}]][id]" [{ $readonly }] value="[{$id}]" >
                </dt>
                <dd>
                    [{ oxmultilang ident="MODULE_DEBUG_LEGACY_ID" }]
                </dd>
            </dl>
            <dl>
                <dt>
                    <input class="txt" style="width: 430px;" name=confaarrs[aLegacyModules][[{$id}]][title]" [{ $readonly }] value="[{$aLegacy.title}]" >
                </dt>
                <dd>
                    [{ oxmultilang ident="MODULE_DEBUG_LEGACY_TITLE" }]
                </dd>
            </dl>
            <dl>
                <dt>
                    <textarea wrap="off" class="txtfield" style="width: 430px;" name=confaarrs[aLegacyModules][[{$id}]][extend]" [{ $readonly }]>[{$aLegacy.extend}]</textarea>
                </dt>
                <dd>
                    [{ oxmultilang ident="MODULE_DEBUG_LEGACY_EXTEND" }]
                </dd>
                <div class="spacer"></div>
            <dl>
            [{/foreach}]
            <dl>
                <dt>
                    <input class="txt" style="width: 430px;" name=confaarrs[aLegacyModules][-1][id]" [{ $readonly }] value="" >
                </dt>
                <dd>
                    [{ oxmultilang ident="MODULE_DEBUG_LEGACY_ID" }]
                </dd>
            </dl>
            <dl>
                <dt>
                    <input class="txt" style="width: 430px;" name=confaarrs[aLegacyModules][-1][title]" [{ $readonly }] value="" >
                </dt>
                <dd>
                    [{ oxmultilang ident="MODULE_DEBUG_LEGACY_TITLE" }]
                </dd>
            </dl>
            <dl>
                <dt>
                    <textarea wrap="off" class="txtfield" style="width: 430px;" name=confaarrs[aLegacyModules][-1][extend]" [{ $readonly }]></textarea>
                </dt>
                <dd>
                    [{ oxmultilang ident="MODULE_DEBUG_LEGACY_EXTEND" }]
                </dd>
                <div class="spacer"></div>
            <dl>
         </div>
    </div>

    <br>
    <input type="submit" class="confinput" name="save" value="[{ oxmultilang ident="GENERAL_SAVE" }]" onClick="Javascript:document.myedit.fnc.value='save'"" [{ $readonly }]>
</form>
[{include file="bottomnaviitem.tpl"}]
[{include file="bottomitem.tpl"}]