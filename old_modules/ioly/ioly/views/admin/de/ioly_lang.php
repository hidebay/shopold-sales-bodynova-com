<?php

/**
 * @author smxsm
 * @package ioly-oxid-ce-487
 * 
 * Encoding: UTF-8
 * Date: 02.11.2014
 * 
 * Description of ioly_lang
 */
$sLangName  = "Deutsch";
// -------------------------------
// RESOURCE IDENTITFIER = STRING
// -------------------------------
$aLang = array(
'charset'                                   => 'UTF-8',
'mxioly'                                    => 'ioly',    
'IOLY_MAIN_HEADLINE'                        => 'ioly',
'IOLY_MAIN_TITLE'                        	=> 'ioly Modulmanager',
'IOLY_MODULE_NAME'                          => 'Modulname',     
'IOLY_MODULE_DOWNLOAD'                      => '',    
'IOLY_IOLY_UPDATE_BUTTON'                   => 'ioly Core aktualisieren',    
'IOLY_RECIPE_UPDATE_BUTTON'                 => 'ioly Rezepte aktualisieren',    
'IOLY_MODULE_OXID_VERSION'                  => 'unterst&uuml;tzte OXID-Version',    
'IOLY_MODULE_DOWNLOAD_SUCCESS'              => 'Modul-Download erfolgreich!',    
'IOLY_MODULE_UNINSTALL_SUCCESS'             => 'Modul-Dateien erfolgreich entfernt!',    
'IOLY_MAIN_INFOTEXT'                        => 'Mit ioly kannst du mit einem Klick OXID Module herunterladen und in deinem Shop installieren.<br>Mehr Informationen zu ioly findest du unter <a href="https://github.com/ioly/ioly" target="_blank">github.com/ioly/ioly</a>.<br>',    
'IOLY_RECIPE_UPDATE_SUCCESS'                => 'Rezepte erfolgreich aktualisiert!',    
'IOLY_RECIPE_UPDATE_ERROR'                  => 'Problem beim Aktualisieren der Rezepte: ',    
'IOLY_IOLY_UPDATE_SUCCESS'                  => 'ioly Core Lib erfolgreich aktualisiert!',    
'IOLY_IOLY_UPDATE_ERROR'                    => 'Problem beim Aktualisieren der ioly Core Lib: ',    
'IOLY_IOLY_UPDATE_HINT'                     => 'ioly Core aktualisieren',    
'IOLY_RECIPE_UPDATE_HINT'                   => 'ioly Rezepte aktualisieren',    
'IOLY_INSTALL_MODULE_HINT'                  => 'Modul-Dateien in das Shopverzeichnis herunterladen und entpacken', 
'IOLY_REINSTALL_MODULE_HINT'                => 'Modul-Dateien in das Shopverzeichnis erneut herunterladen und entpacken', 
'IOLY_UNINSTALL_MODULE_HINT'                => 'Modul-Dateien aus dem Shopverzeichnis entfernen', 
'IOLY_MODULE_INSTALLED'                     => 'Modul installiert',  
'IOLY_EXCEPTION_CORE_NOT_LOADED'            => "ioly Core konnte nicht geladen werden!<br>Folgende Verzeichnisse ben&ouml;tigen Schreibrechte:<br><br><i>&lsaquo;shoproot&rsaquo;/ioly/<br>&lsaquo;shoproot&rsaquo;/modules/<br>&lsaquo;shoproot&rsaquo;/modules/ioly/ioly/</i>", 
'IOLY_PROJECT_URL'                          => 'Moduldetails',    
'IOLY_OXID_VERSIONS'                        => 'OXID',    
'IOLY_TOGGLE_INFO'                          => 'Infos anzeigen',    
'IOLY_ERROR_BASELIB_MISSING'                => "JS Bibliotheken nocht nicht vorhanden. Installiere '%s' in Version '%s' in 'ioly/libs' ...",    
'IOLY_BUTTON_DOWNLOAD_VERSION_1'            => 'Version',   
'IOLY_BUTTON_DOWNLOAD_VERSION_2'            => 'installieren',    
'IOLY_BUTTON_DOWNLOAD_VERSION_3'            => 'erneut installieren',  
'IOLY_BUTTON_REMOVE_VERSION_1'              => 'Version',    
'IOLY_BUTTON_REMOVE_VERSION_2'              => 'deinstallieren',  
'IOLY_OXID_MAPPINGS'                        => 'Datei-Mappings',  
'IOLY_EXCEPTION_MESSAGE_CODE_1022'          => 'Das Modul wurde deinstalliert, aber folgende Dateien konnten nicht gel&oum;scht werden:',    
'IOLY_EXCEPTION_MESSAGE_CODE_1023'          => 'Das Modul wurde deinstalliert, aber folgende Dateien wurden modifiziert und konnten nicht gel&oum;scht werden:',    
'IOLY_EXCEPTION_MESSAGE_CODE_1021'          => 'Konnte die angegebene Digest-Version nicht finden.',    
'IOLY_EXCEPTION_MESSAGE_CODE_1020'          => 'Konnte keine Digest-Version finden.',    
'IOLY_EXCEPTION_MESSAGE_CODE_1008'          => 'Bitte pr&uuml;fen Sie die Schreibrechte folgender Dateien/Ordner:',    
'IOLY_EXCEPTION_MESSAGE_CODE_1007'          => 'Der Download-Server antwortete mit Fehler:',    
'IOLY_EXCEPTION_MESSAGE_CODE_1010'          => 'Bitte pr&uuml;fen Sie die Schreibrechte folgender Dateien/Ordner:',    
'IOLY_EXCEPTION_MESSAGE_CODE_1011'          => 'Bitte pr&uuml;fen Sie die Schreibrechte folgender Dateien/Ordner:',    
'IOLY_EXCEPTION_MESSAGE_CODE_1000'          => 'Bitte rufen Sie zuerst \$ioly->setSystemBasePath(\$path) auf oder f&uuml;hren Sie folgendes aus: export IOLY_SYSTEM_BASE=%path%',    
'IOLY_EXCEPTION_MESSAGE_CODE_1001'          => 'Bitte rufen Sie zuerst \$ioly->setSystemVersion(\$version) auf oder f&uuml;hren Sie folgendes aus: export IOLY_SYSTEM_VERSION=%version%',    
'IOLY_EXCEPTION_MESSAGE_CODE_1003'          => 'Konnte Modul-Version nicht finden:',    
'IOLY_EXCEPTION_MESSAGE_CODE_1005'          => 'Konnte Modul nicht finden:',    
'IOLY_EXCEPTION_MESSAGE_CODE_1006'          => 'Bitte benutzen Sie vendor/package um ein Modul zu installieren.',    
 
);
?>
