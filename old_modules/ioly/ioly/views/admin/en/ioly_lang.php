<?php

/**
 * @author smxsm
 * @package ioly-oxid-ce-487
 * 
 * Encoding: UTF-8
 * Date: 02.11.2014
 * 
 * Description of ioly_lang
 */
$sLangName  = "English";
// -------------------------------
// RESOURCE IDENTITFIER = STRING
// -------------------------------
$aLang = array(
'charset'                                   => 'UTF-8',
'mxioly'                                    => 'ioly',    
'IOLY_MAIN_HEADLINE'                        => 'ioly',
'IOLY_MAIN_TITLE'                        	=> 'ioly module manager',
'IOLY_MODULE_NAME'                          => 'Module-Name',       
'IOLY_MODULE_DOWNLOAD'                      => '',    
'IOLY_IOLY_UPDATE_BUTTON'                   => 'Update ioly core',    
'IOLY_RECIPE_UPDATE_BUTTON'                 => 'Update ioly recipes',    
'IOLY_MODULE_OXID_VERSION'                  => 'supported OXID-Versions',    
'IOLY_MODULE_DOWNLOAD_SUCCESS'              => 'Module downloaded successfully!',    
'IOLY_MODULE_UNINSTALL_SUCCESS'             => 'Module files removed successfully!',    
'IOLY_MAIN_INFOTEXT'                        => 'With ioly you can download and install any module or lib via "recipes".',    
'IOLY_RECIPE_UPDATE_SUCCESS'                => 'updated recipes successfully!',    
'IOLY_RECIPE_UPDATE_ERROR'                  => 'problem updating recipes: ',    
'IOLY_IOLY_UPDATE_SUCCESS'                  => 'updated ioly core lib successfully!',    
'IOLY_IOLY_UPDATE_ERROR'                    => 'problem updating ioly core lib: ',    
'IOLY_IOLY_UPDATE_HINT'                     => 'update ioly core',    
'IOLY_RECIPE_UPDATE_HINT'                   => 'update ioly recipes',    
'IOLY_INSTALL_MODULE_HINT'                  => 'download module or lib files to the shop directory and extract them',    
'IOLY_REINSTALL_MODULE_HINT'                => 'download module or lib files to the shop directory and extract them again',    
'IOLY_UNINSTALL_MODULE_HINT'                => 'remove module files from shop directory?', 
'IOLY_MODULE_INSTALLED'                     => 'Module installed',    
'IOLY_EXCEPTION_CORE_NOT_LOADED'            => "Unable to load ioly core!<br>Please check write permissions in the following folders:<i>&lsaquo;shoproot&rsaquo;/ioly/<br>&lsaquo;shoproot&rsaquo;/modules/<br>&lsaquo;shoproot&rsaquo;/modules/ioly/ioly/</i>",    
'IOLY_PROJECT_URL'                          => 'module details',    
'IOLY_OXID_VERSIONS'                        => 'OXID',    
'IOLY_TOGGLE_INFO'                          => 'Toggle info',    
'IOLY_ERROR_BASELIB_MISSING'                => "JS libs not found. Installing '%s' in version '%s' to 'ioly/libs' ...",    
'IOLY_BUTTON_DOWNLOAD_VERSION_1'            => 'install version',   
'IOLY_BUTTON_DOWNLOAD_VERSION_2'            => '',    
'IOLY_BUTTON_DOWNLOAD_VERSION_3'            => 'reinstall version',  
'IOLY_BUTTON_REMOVE_VERSION_1'              => '',    
'IOLY_BUTTON_REMOVE_VERSION_2'              => 'remove version',   
'IOLY_OXID_MAPPINGS'                        => 'File mappings',    
'IOLY_EXCEPTION_MESSAGE_CODE_1022'          => 'The module was uninstalled, but the following files could not be deleted:',    
'IOLY_EXCEPTION_MESSAGE_CODE_1023'          => 'The module was uninstalled, but the following files have been modified and could not be deleted:',    
'IOLY_EXCEPTION_MESSAGE_CODE_1021'          => 'Could not find specified digest version.',    
'IOLY_EXCEPTION_MESSAGE_CODE_1020'          => 'Could not find any digest version.',    
'IOLY_EXCEPTION_MESSAGE_CODE_1008'          => 'Please check permissions on the following files/folders:',    
'IOLY_EXCEPTION_MESSAGE_CODE_1007'          => 'Download server responded with error:',    
'IOLY_EXCEPTION_MESSAGE_CODE_1010'          => 'Please check permissions on the following files/folders:',    
'IOLY_EXCEPTION_MESSAGE_CODE_1011'          => 'Please check permissions on the following files/folders:',    
'IOLY_EXCEPTION_MESSAGE_CODE_1000'          => 'Please call \$ioly->setSystemBasePath(\$path) run: export IOLY_SYSTEM_BASE=%path%',    
'IOLY_EXCEPTION_MESSAGE_CODE_1001'          => 'Please call \$ioly->setSystemVersion(\$version) or run: export IOLY_SYSTEM_VERSION=%version%',    
'IOLY_EXCEPTION_MESSAGE_CODE_1003'          => 'Could not find module version:',    
'IOLY_EXCEPTION_MESSAGE_CODE_1005'          => 'Could not find module:',    
'IOLY_EXCEPTION_MESSAGE_CODE_1006'          => 'Please use vendor/package to install a module.',    
);
?>
