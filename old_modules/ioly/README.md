# ioly OXID Connector

With the ioly OXID Connector you can install modules directly via the OXID-Admin.

[![ioly OXID Connector](https://raw.github.com/ioly/ioly/gh-pages/assets/img/ioly-oxid-connector.png)](https://www.youtube.com/embed/8JnDZxhWETo?vq=hd720)

## Installation

1. download the [ioly oxid connector](https://github.com/ioly/ioly/archive/connector-oxid.zip) module
2. copy connector in your oxid modules directory
3. activate the ioly module using the OXID Admin login
4. done


for more information about ioly read [master/README.md](https://github.com/ioly/ioly/blob/master/README.md)

