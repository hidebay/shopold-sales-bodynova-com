/**
 * @license AngularJS v1.0.8
 * (c) 2010-2012 Google, Inc. http://angularjs.org
 * License: MIT
 */
!function(o,n,e){"use strict";n.module("ngCookies",["ng"]).factory("$cookies",["$rootScope","$browser",function(o,i){function t(){var o,t,f,s;for(o in c)a(r[o])&&i.cookies(o,e);for(o in r)t=r[o],n.isString(t)?t!==c[o]&&(i.cookies(o,t),s=!0):n.isDefined(c[o])?r[o]=c[o]:delete r[o];if(s){s=!1,f=i.cookies();for(o in r)r[o]!==f[o]&&(a(f[o])?delete r[o]:r[o]=f[o],s=!0)}}var r={},c={},f,s=!1,u=n.copy,a=n.isUndefined;return i.addPollFn(function(){var n=i.cookies();f!=n&&(f=n,u(n,c),u(n,r),s&&o.$apply())})(),s=!0,o.$watch(t),r}]).factory("$cookieStore",["$cookies",function(o){return{get:function(e){var i=o[e];return i?n.fromJson(i):i},put:function(e,i){o[e]=n.toJson(i)},remove:function(n){delete o[n]}}}])}(window,window.angular);