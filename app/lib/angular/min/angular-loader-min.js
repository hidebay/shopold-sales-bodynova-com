/**
 * @license AngularJS v1.0.8
 * (c) 2010-2012 Google, Inc. http://angularjs.org
 * License: MIT
 */
!function r(e){function n(r,e,n){return r[e]||(r[e]=n())}return n(n(e,"angular",Object),"module",function(){var r={};return function e(o,i,t){return i&&r.hasOwnProperty(o)&&(r[o]=null),n(r,o,function(){function r(r,n,o){return function(){return e[o||"push"]([r,n,arguments]),c}}if(!i)throw Error("No module: "+o);var e=[],n=[],u=r("$injector","invoke"),c={_invokeQueue:e,_runBlocks:n,requires:i,name:o,provider:r("$provide","provider"),factory:r("$provide","factory"),service:r("$provide","service"),value:r("$provide","value"),constant:r("$provide","constant","unshift"),filter:r("$filterProvider","register"),controller:r("$controllerProvider","register"),directive:r("$compileProvider","directive"),config:u,run:function(r){return n.push(r),this}};return t&&u(t),c})}})}(window),angular.Module;