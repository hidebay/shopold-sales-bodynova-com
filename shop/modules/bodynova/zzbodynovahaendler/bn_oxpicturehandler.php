<?php

class bn_oxpicturehandler extends bn_oxpicturehandler_parent {
	public function getPicUrl( $sPath, $sFile, $sSize, $sIndex = null, $sAltPath = false, $bSsl = null )
	{
		$sDirName = '450_450_' . $this->getConfig()->getConfigParam( 'sDefaultImageQuality' );
		return 'https://bodynova.de/out/pictures/generated/product/1/'.$sDirName.'/'.$sFile;
	}
}
