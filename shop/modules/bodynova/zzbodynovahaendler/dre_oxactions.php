<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 02.03.15
 * Time: 10:36
 */

class dre_oxactions extends dre_oxactions_parent {
    /**
     * Returns assigned banner article picture url
     *
     * @return string
     */
    public function getBannerPictureUrl()
    {
        if ( isset( $this->oxactions__oxpic ) && $this->oxactions__oxpic->value ) {

            $sPromoDir = oxRegistry::get("oxUtilsFile")->normalizeDir( oxUtilsFile::PROMO_PICTURE_DIR );

            $sPath = "https://bodynova.de/out/pictures/".$sPromoDir.$this->oxactions__oxpic->value;

            #return $this->getConfig()->getPictureUrl( $sPromoDir.$this->oxactions__oxpic->value, false );
            return $sPath;
        }
    }
}
