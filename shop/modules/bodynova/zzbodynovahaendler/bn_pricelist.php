<?php

class pricelist extends oxUBase
{

    protected $strImageSize = '100_100_75';

    /**
     *
     */
    public function getPDF()
    {

        // bekomme alle artikel
        $objArticleListe = new oxArticleList();
        $objArticleListe->loadCategoryArticles($_GET['cat'], null);
        // array zum speichern der artikel
        $arrElemente = array();
        // laufe über alel artikel und speichere sie in dem array
        foreach($objArticleListe AS $objArticle)
        {
            // bei avraibtebn: lade diese und speichere sie in dem array andernfalls den einfachen artikel
            if($objArticle->getVariantsCount())
            {
                foreach($objArticle->getVariants() AS $objVariante)
                {
                    $arrElemente[] = array(
                        'name'           =>  $objArticle->oxarticles__oxtitle->value.': '.$objVariante->oxarticles__oxvarselect->value,
                        'bild'           =>  null, //$objVariante->getPictureUrl(),
                        //'bild'              =>  ($objVariante->oxarticles__oxpic1->value ? $objVariante->oxarticles__oxpic1->value : 'https://bodynova.de/out/imagehandler.php?artnum='.$objVariante->oxarticles__oxartnum->value.'&size='.$this->strImageSize),
                        'artikelnummer'  =>  $objVariante->oxarticles__oxartnum->value,
                        'preis'          =>  $objVariante->getPrice()->getPrice(),
                        'uvp'            =>  $objVariante->oxarticles__oxprice->value,
                    );
                }
            }
            else
            {
                $arrElemente[] = array(
                    'name'              =>  $objArticle->oxarticles__oxtitle->value,
                    //'bild'              =>  ($objArticle->oxarticles__oxpic1->value ? $objArticle->oxarticles__oxpic1->value : 'https://bodynova.de/out/imagehandler.php?artnum='.$objArticle->oxarticles__oxartnum->value.'&size='.$this->strImageSize),
                    'artikelnummer'     =>  $objArticle->oxarticles__oxartnum->value,
                    'preis'             =>  $objArticle->getPrice()->getPrice(),
                    'uvp'               =>  $objArticle->oxarticles__oxprice->value,
                );
            }
        }

        usort($arrElemente, function($a,$b) {
            return $a['name']>$b['name'];
        });



        /*
        //
        $strHTML = $strHTMLHeader = '<table border="1" cellspacing="0" cellpadding="2" style="font-size: x-small; margin-top:20px;"> ';

        //
        $strHTMLHeader.='<tr> ';
        $strHTMLHeader.='    <td width="380"><strong>Name '.$_GET['name'].'</strong></td> ';
        $strHTMLHeader.='    <td width="50"><strong>Artnum:</strong></td> ';
        $strHTMLHeader.='    <td width="40"><strong>UVP/€</strong></td> ';
        $strHTMLHeader.='    <td width="40"><strong>EK/€</strong></td> ';
        $strHTMLHeader.='</tr> ';
        $strHTMLHeader.='</table> ';
        */




        /*
        //
        $strHTML.='</table>';
*/
        //
        require_once dirname(__FILE__).'/bn_pricelistpdf.php';

        // initiating pdf engine
        $oPdf = new bn_pricelistpdf();

        //
        foreach($arrElemente AS $arrElement)
        {
            $oPdf->writeBnCell(
                $arrElement['name'],
                $arrElement['artikelnummer'],
                number_format($arrElement['uvp'], 2),
                number_format($arrElement['preis'], 2)
            );
        }

        //
        $oPdf->lastPage();
        $oPdf->Output('preisliste.pdf', 'I');
    }
}
