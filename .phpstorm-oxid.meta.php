<?php
/**
 * An helper file for OXID, to provide autocomplete information to your IDE
 * Generated with PhpStorm / PS-143.382 / OXID Plugin 0.4 on 2015-12-11 15:50:56.
 *
 * @author Daniel Espendiller <daniel@espendiller.net>
 * @see https://github.com/Haehnchen/idea-php-oxid-plugin
 */

namespace {
 exit("This file should not be included, only analyzed by your IDE");

 class oePayPalDeliverySet_Main_parent extends DeliverySet_Main
 {
 }

 class oeThemeSwitcherVendorList_parent extends VendorList
 {
 }

 class bn_oxinputvalidator_parent extends oxInputValidator
 {
 }

 class dre_oxactions_parent extends oxActions
 {
 }

 class dre_oxcategorylist_parent extends oxCategoryList
 {
 }

 class oeThemeSwitcherContent_parent extends Content
 {
 }

 class oePayPalOxUser_parent extends oxUser
 {
 }

 class bn_oxshopcontrol_parent extends oxShopControl
 {
  use \dre_cleartmp_oxshopcontrolTrait, \mude_oxrelic_oxshopcontrolTrait;
 }

 class mude_oxrelic_oxshopcontrol_parent extends oxShopControl
 {
  use \dre_cleartmp_oxshopcontrolTrait, \bn_oxshopcontrolTrait;
 }

 class dre_cleartmp_oxshopcontrol_parent extends oxShopControl
 {
  use \mude_oxrelic_oxshopcontrolTrait, \bn_oxshopcontrolTrait;
 }

 class mude_oxrelic_oxwidgetcontrol_parent extends oxWidgetControl
 {
 }

 class dre_newsletter_parent extends oxNewsletter
 {
 }

 class oeThemeSwitcherViewConfig_parent extends oxViewConfig
 {
  use \oePayPalOxViewConfigTrait;
 }

 class oePayPalOxViewConfig_parent extends oxViewConfig
 {
  use \oeThemeSwitcherViewConfigTrait;
 }

 class dre_cleartmp_navigation_parent extends Navigation
 {
 }

 class oeThemeSwitcherRss_parent extends Rss
 {
 }

 class oePayPalOxwArticleDetails_parent extends oxwArticleDetails
 {
 }

 class bn_oxcmp_user_parent extends oxcmp_user
 {
 }

 class category_main_ajax extends category_main_ajax
 {
 }

 class oeThemeSwitcherReview_parent extends Review
 {
 }

 class oePayPalOxBasketItem_parent extends oxBasketItem
 {
 }

 class bn_oxsearch_parent extends oxSearch
 {
 }

 class oePayPalPayment_parent extends Payment
 {
  use \bn_paymentTrait;
 }

 class bn_payment_parent extends Payment
 {
  use \oePayPalPaymentTrait;
 }

 class oeThemeSwitcherDetails_parent extends Details
 {
 }

 class oeThemeSwitcherTag_parent extends Tag
 {
 }

 class bn_article_main_parent extends Article_Main
 {
 }

 class oePayPalOrder_parent extends order
 {
  use \bn_orderTrait;
 }

 class bn_order_parent extends order
 {
  use \oePayPalOrderTrait;
 }

 class oeThemeSwitcherLang_parent extends oxLang
 {
 }

 class oeThemeSwitcherConfig_parent extends oxConfig
 {
 }

 class oePayPalOxPaymentGateway_parent extends oxPaymentGateway
 {
 }

 class oeThemeSwitcherManufacturerList_parent extends ManufacturerList
 {
 }

 class oeThemeSwitcherStart_parent extends Start
 {
 }

 class bn_oxpicturehandler_parent extends oxPictureHandler
 {
 }

 class oePayPalWrapping_parent extends Wrapping
 {
 }

 class oeThemeSwitcherTheme_parent extends oxTheme
 {
 }

 class bn_oxarticlelist_parent extends oxArticleList
 {
 }

 class oePayPalOxOrder_parent extends oxOrder
 {
  use \bn_oxorderTrait;
 }

 class bn_oxorder_parent extends oxOrder
 {
  use \oePayPalOxOrderTrait;
 }

 class bn_account_password_parent extends Account_Password
 {
 }

 class oxprobs_articles extends oxprobs_articles
 {
 }

 class oeThemeSwitcherAList_parent extends aList
 {
 }

 class oePayPalOxcmp_Basket_parent extends oxcmp_basket
 {
 }

 class oePayPalOxBasket_parent extends oxBasket
 {
  use \bn_oxbasketTrait;
 }

 class bn_oxbasket_parent extends oxBasket
 {
  use \oePayPalOxBasketTrait;
 }

 class bn_oxaddress_parent extends oxAddress
 {
  use \oePayPalOxAddressTrait;
 }

 class oePayPalOxAddress_parent extends oxAddress
 {
  use \bn_oxaddressTrait;
 }

 class oePayPalOxArticle_parent extends oxArticle
 {
  use \bn_oxarticleTrait;
 }

 class bn_oxarticle_parent extends oxArticle
 {
  use \oePayPalOxArticleTrait;
 }

 class oePayPalOrder_List_parent extends Order_List
 {
 }

 trait oeThemeSwitcherAListTrait
 {
  /**
   * Check if filter was selected
   *
   * @return bool
   * @see \oeThemeSwitcherAList::getShowFilter
   */
  public function getShowFilter()
  {
  }

  /**
   * Returns view ID (for template engine caching).
   *
   * @return string   $this->_sViewId view id
   * @see \oeThemeSwitcherAList::getViewId
   */
  public function getViewId()
  {
  }

  /**
   * If filter should be displayed
   *
   * @var bool
   * @see \oeThemeSwitcherAList::_blShowFilter
   */
  protected $_blShowFilter;
 }

 trait oePayPalOxBasketTrait
 {
  /**
   * Checks if products in basket ar virtual and does not require real delivery.
   * Returns TRUE if virtual
   *
   * @return bool
   * @see \oePayPalOxBasket::isVirtualPayPalBasket
   */
  public function isVirtualPayPalBasket()
  {
  }

  /**
   * Returns wrapping cost value
   *
   * @return double
   * @see \oePayPalOxBasket::getPayPalWrappingCosts
   */
  public function getPayPalWrappingCosts()
  {
  }

  /**
   * Returns greeting card cost value
   *
   * @return double
   * @see \oePayPalOxBasket::getPayPalGiftCardCosts
   */
  public function getPayPalGiftCardCosts()
  {
  }

  /**
   * Returns payment costs netto or brutto value
   * @return double
   * @see \oePayPalOxBasket::getPayPalPaymentCosts
   */
  public function getPayPalPaymentCosts()
  {
  }

  /**
   * Returns Trusted shops costs netto or brutto value
   * @return double
   * @see \oePayPalOxBasket::getPayPalTsProtectionCosts
   */
  public function getPayPalTsProtectionCosts()
  {
  }

  /**
   * Collects all basket discounts (basket, payment and vouchers)
   * and returns sum of collected discounts.
   *
   * @return double
   * @see \oePayPalOxBasket::getDiscountSumPayPalBasket
   */
  public function getDiscountSumPayPalBasket()
  {
  }

  /**
   * Calculates basket costs (payment, GiftCard and gift card)
   * and returns sum of all costs.
   *
   * @return double
   * @see \oePayPalOxBasket::getSumOfCostOfAllItemsPayPalBasket
   */
  public function getSumOfCostOfAllItemsPayPalBasket()
  {
  }

  /**
   * Returns absolute VAT value
   * @return float
   * @see \oePayPalOxBasket::getPayPalBasketVatValue
   */
  public function getPayPalBasketVatValue()
  {
  }

  /**
   * Return products VAT.
   * @return double
   * @see \oePayPalOxBasket::getPayPalProductVat
   */
  public function getPayPalProductVat()
  {
  }

  /**
   * Return wrapping VAT.
   * @return double
   * @see \oePayPalOxBasket::getPayPalWrappingVat
   */
  public function getPayPalWrappingVat()
  {
  }

  /**
   * Return gift card VAT.
   * @return double
   * @see \oePayPalOxBasket::getPayPalGiftCardVat
   */
  public function getPayPalGiftCardVat()
  {
  }

  /**
   * Return payment VAT.
   * @return double
   * @see \oePayPalOxBasket::getPayPalPayCostVat
   */
  public function getPayPalPayCostVat()
  {
  }

  /**
   * Return payment VAT.
   * @return double
   * @see \oePayPalOxBasket::getPayPalTsProtectionCostVat
   */
  public function getPayPalTsProtectionCostVat()
  {
  }

 }

 trait oePayPalDeliverySet_MainTrait
 {
  /**
   * Add default PayPal mobile payment.
   *
   * @return string
   * @see \oePayPalDeliverySet_Main::render
   */
  public function render()
  {
  }

  /**
   * Saves default PayPal mobile payment.
   *
   * @return mixed
   * @see \oePayPalDeliverySet_Main::save
   */
  public function save()
  {
  }

  /**
   * Save default shipping id.
   *
   * @param oxConfig $oConfig config object to save.
   * @param string $sShippingId shipping id.
   * @param oePayPalConfig $oPayPalConfig PayPal config.
   * @see \oePayPalDeliverySet_Main::_saveECDefaultShippingId
   */
  protected function _saveECDefaultShippingId($oConfig, $sShippingId, $oPayPalConfig)
  {
  }

 }

 trait dre_cleartmp_oxshopcontrolTrait
 {
  /**
   * @see \dre_cleartmp_oxshopcontrol::_runOnce
   */
  protected function _runOnce()
  {
  }

 }

 trait mude_oxrelic_oxshopcontrolTrait
 {
  /**
   * @see \mude_oxrelic_oxshopcontrol::_process
   */
  protected function _process($sClass, $sFunction, $aParams = null, $aViewsChain = null)
  {
  }

 }

 trait dre_cleartmp_navigationTrait
 {
  /**
   * @see \dre_cleartmp_navigation::render
   */
  public function render()
  {
  }

  /**
   * @see \dre_cleartmp_navigation::cleartmp
   */
  public function cleartmp()
  {
  }

  /**
   * @see \dre_cleartmp_navigation::isDevMode
   */
  public function isDevMode()
  {
  }

  /**
   * @see \dre_cleartmp_navigation::deleteFiles
   */
  public function deleteFiles()
  {
  }

 }

 trait bn_oxarticlelistTrait
 {
  /**
   * @param $arrOXIDs
   * @see \bn_oxarticlelist::loadByArticlesIDs
   */
  public function loadByArticlesIDs($arrOXIDs)
  {
  }

  /**
   * @see \bn_oxarticlelist::output
   */
  public function output()
  {
  }

 }

 trait dre_oxcategorylistTrait
 {
  /**
   * constructs the sql string to get the category list
   *
   * @param bool $blReverse list loading order, true for tree, false for simple list (optional, default false)
   * @param array $aColumns required column names (optional)
   * @param string $sOrder order by string (optional)
   *
   * @return string
   * @see \dre_oxcategorylist::_getSelectString
   */
  protected function _getSelectString($blReverse = false, $aColumns = null, $sOrder = null)
  {
  }

 }

 trait bn_oxshopcontrolTrait
 {
  /**
   * @param $sClass
   * @param $sFunction
   * @param null $aParams
   * @param null $aViewsChain
   * @see \bn_oxshopcontrol::_process
   */
  protected function _process($sClass, $sFunction, $aParams = null, $aViewsChain = null)
  {
  }

  /**
   * @param $oViewObject
   * @see \bn_oxshopcontrol::_render
   */
  protected function _render($oViewObject)
  {
  }

 }

 trait bn_article_mainTrait
 {
  /**
   * @see \bn_article_main::render
   */
  public function render()
  {
  }

 }

 trait bn_oxcmp_userTrait
 {
  /**
   * Add flag "forcechangepassword=1" to url an redirects to password change form to a force a password
   * change when the user passwor still is "bodynova"
   *
   * @param $oUser
   * @see \bn_oxcmp_user::_afterLogin
   */
  protected function _afterLogin($oUser)
  {
  }

  /**
   * to avoid the basked moved to a non user session (per default the haendlershop forces a logged in user
   * we do not move the user basket back into a session where no user is present
   * @see \bn_oxcmp_user::_afterLogout
   */
  protected function _afterLogout()
  {
  }

  /**
   *
   * @see \bn_oxcmp_user::logout
   */
  public function logout()
  {
  }

 }

 trait oeThemeSwitcherThemeTrait
 {
  /**
   * Get theme info item
   *
   * @param string $sName name of info item to retrieve
   *
   * @return mixed
   * @see \oeThemeSwitcherTheme::getInfo
   */
  public function getInfo($sName)
  {
  }

 }

 trait bn_orderTrait
 {
  /**
   * @see \bn_order::getPayment
   */
  public function getPayment()
  {
  }

 }

 trait oePayPalOxViewConfigTrait
 {
  /**
   * @param oePayPalPaymentValidator $oPaymentValidator
   * @see \oePayPalOxViewConfig::setPaymentValidator
   */
  public function setPaymentValidator($oPaymentValidator)
  {
  }

  /**
   * @return oePayPalPaymentValidator
   * @see \oePayPalOxViewConfig::getPaymentValidator
   */
  public function getPaymentValidator()
  {
  }

  /**
   * Returns TRUE if express checkout is enabled
   * Does payment amount or user country/group check.
   *
   * @return bool
   * @see \oePayPalOxViewConfig::isExpressCheckoutEnabled
   */
  public function isExpressCheckoutEnabled()
  {
  }

  /**
   * Returns TRUE if express checkout and displaying it in mini basket is enabled
   *
   * @return bool
   * @see \oePayPalOxViewConfig::isExpressCheckoutEnabledInMiniBasket
   */
  public function isExpressCheckoutEnabledInMiniBasket()
  {
  }

  /**
   * Returns TRUE if express checkout is enabled
   * Does payment amount or user country/group check.
   *
   * @return bool
   * @see \oePayPalOxViewConfig::isExpressCheckoutEnabledInDetails
   */
  public function isExpressCheckoutEnabledInDetails()
  {
  }

  /**
   * Returns TRUE if standard checkout is enabled.
   * Does payment amount or user country/group check.
   *
   * @return bool
   * @see \oePayPalOxViewConfig::isStandardCheckoutEnabled
   */
  public function isStandardCheckoutEnabled()
  {
  }

  /**
   * Checks if PayPal standard or express checkout is enabled.
   * Does not do payment amount or user country/group check.
   *
   * @return bool
   * @see \oePayPalOxViewConfig::isPayPalActive
   */
  public function isPayPalActive()
  {
  }

  /**
   * Returns PayPal payment description text
   *
   * @return string
   * @see \oePayPalOxViewConfig::getPayPalPaymentDescription
   */
  public function getPayPalPaymentDescription()
  {
  }

  /**
   * Returns PayPal payment object
   *
   * @return oxPayment
   * @see \oePayPalOxViewConfig::getPayPalPayment
   */
  public function getPayPalPayment()
  {
  }

  /**
   * Returns state if order info should be send to PayPal
   *
   * @return bool
   * @see \oePayPalOxViewConfig::sendOrderInfoToPayPal
   */
  public function sendOrderInfoToPayPal()
  {
  }

  /**
   * Returns default (on/off) state if order info should be send to PayPal
   *
   * @return bool
   * @see \oePayPalOxViewConfig::sendOrderInfoToPayPalDefault
   */
  public function sendOrderInfoToPayPalDefault()
  {
  }

  /**
   * Returns PayPal config
   *
   * @return oePayPalConfig
   * @see \oePayPalOxViewConfig::_getPayPalConfig
   */
  protected function _getPayPalConfig()
  {
  }

  /**
   * Returns current URL
   *
   * @return string
   * @see \oePayPalOxViewConfig::getCurrentUrl
   */
  public function getCurrentUrl()
  {
  }

  /**
   * Check if module is active.
   *
   * @param string $sModuleId module id.
   * @param string $sVersionFrom module from version.
   * @param string $sVersionTo module to version.
   *
   * @return  bool
   * @see \oePayPalOxViewConfig::oePayPalIsModuleActive
   */
  public function oePayPalIsModuleActive($sModuleId, $sVersionFrom = null, $sVersionTo = null)
  {
  }

  /**
   * Checks if module exists.
   *
   * @param $sModuleId
   * @param $aModules
   * @return bool
   * @see \oePayPalOxViewConfig::_oePayPalModuleExists
   */
  protected function _oePayPalModuleExists($sModuleId, $aModules)
  {
  }

  /**
   * Checks whether module is enabled.
   *
   * @param $sModuleId
   * @return bool
   * @see \oePayPalOxViewConfig::_oePayPalIsModuleEnabled
   */
  protected function _oePayPalIsModuleEnabled($sModuleId)
  {
  }

  /**
   * Checks whether module version is between given range.
   *
   * @param $sModuleId
   * @param $sVersionFrom
   * @param $sVersionTo
   * @return bool
   * @see \oePayPalOxViewConfig::_oePayPalIsModuleVersionCorrect
   */
  protected function _oePayPalIsModuleVersionCorrect($sModuleId, $sVersionFrom, $sVersionTo)
  {
  }

  /**
   * @see \oePayPalOxViewConfig::_oPayPalConfig
   */
  protected $_oPayPalConfig;
  /**
   * PayPal payment object
   * @var bool
   * @see \oePayPalOxViewConfig::_oPayPalPayment
   */
  protected $_oPayPalPayment;
  /**
   * Status if Express checkout is ON
   * @var bool
   * @see \oePayPalOxViewConfig::_blExpressCheckoutEnabled
   */
  protected $_blExpressCheckoutEnabled;
  /**
   * Status if Standard checkout is ON
   * @var bool
   * @see \oePayPalOxViewConfig::_blStandardCheckoutEnabled
   */
  protected $_blStandardCheckoutEnabled;
  /**
   * Status if PayPal is ON
   * @var bool
   * @see \oePayPalOxViewConfig::_blPayPalEnabled
   */
  protected $_blPayPalEnabled;
  /**
   * PayPal Payment Validator object
   * @var oePayPalPaymentValidator
   * @see \oePayPalOxViewConfig::_oPaymentValidator
   */
  protected $_oPaymentValidator;
 }

 trait oeThemeSwitcherReviewTrait
 {
  /**
   * Returns view ID (for template engine caching).
   *
   * @return string   $this->_sViewId view id
   * @see \oeThemeSwitcherReview::getViewId
   */
  public function getViewId()
  {
  }

 }

 trait bn_oxpicturehandlerTrait
 {
  /**
   * @see \bn_oxpicturehandler::getPicUrl
   */
  public function getPicUrl($sPath, $sFile, $sSize, $sIndex = null, $sAltPath = false, $bSsl = null)
  {
  }

 }

 trait oePayPalOxPaymentGatewayTrait
 {
  /**
   * Sets order.
   *
   * @param oxOrder $oOrder
   * @see \oePayPalOxPaymentGateway::setPayPalOxOrder
   */
  public function setPayPalOxOrder($oOrder)
  {
  }

  /**
   * Gets order.
   *
   * @return oxOrder
   * @see \oePayPalOxPaymentGateway::getPayPalOxOrder
   */
  public function getPayPalOxOrder()
  {
  }

  /**
   * Executes payment, returns true on success.
   *
   * @param double $dAmount Goods amount
   * @param object &$oOrder User ordering object
   *
   * @return bool
   * @see \oePayPalOxPaymentGateway::executePayment
   */
  public function executePayment($dAmount, & $oOrder)
  {
  }

  /**
   * Executes "DoExpressCheckoutPayment" to PayPal
   *
   * @return bool
   * @see \oePayPalOxPaymentGateway::doExpressCheckoutPayment
   */
  public function doExpressCheckoutPayment()
  {
  }

  /**
   * @param $oBasket
   * @return string
   * @see \oePayPalOxPaymentGateway::_getTransactionMode
   */
  protected function _getTransactionMode($oBasket)
  {
  }

  /**
   * Return PayPal config
   *
   * @return oePayPalConfig
   * @see \oePayPalOxPaymentGateway::getPayPalConfig
   */
  public function getPayPalConfig()
  {
  }

  /**
   * Set PayPal config
   *
   * @param oePayPalConfig $oPayPalConfig config
   * @see \oePayPalOxPaymentGateway::setPayPalConfig
   */
  public function setPayPalConfig($oPayPalConfig)
  {
  }

  /**
   * Sets PayPal service
   *
   * @param oePayPalService $oCheckoutService
   * @see \oePayPalOxPaymentGateway::setPayPalCheckoutService
   */
  public function setPayPalCheckoutService($oCheckoutService)
  {
  }

  /**
   * Returns PayPal service
   *
   * @return oePayPalService
   * @see \oePayPalOxPaymentGateway::getPayPalCheckoutService
   */
  public function getPayPalCheckoutService()
  {
  }

  /**
   * Returns PayPal order object
   *
   * @return oxOrder
   * @see \oePayPalOxPaymentGateway::_getPayPalOrder
   */
  protected function _getPayPalOrder()
  {
  }

  /**
   * Returns PayPal user
   *
   * @return oxUser
   * @see \oePayPalOxPaymentGateway::_getPayPalUser
   */
  protected function _getPayPalUser()
  {
  }

  /**
   * PayPal config.
   *
   * @var null
   * @see \oePayPalOxPaymentGateway::_oPayPalConfig
   */
  protected $_oPayPalConfig;
  /**
   * PayPal config.
   *
   * @var null
   * @see \oePayPalOxPaymentGateway::_oCheckoutService
   */
  protected $_oCheckoutService;
  /**
   * Order.
   *
   * @var oxOrder
   * @see \oePayPalOxPaymentGateway::_oPayPalOxOrder
   */
  protected $_oPayPalOxOrder;
 }

 trait oeThemeSwitcherDetailsTrait
 {
  /**
   * Returns view ID (for template engine caching).
   *
   * @return string   $this->_sViewId view id
   * @see \oeThemeSwitcherDetails::getViewId
   */
  public function getViewId()
  {
  }

 }

 trait bn_paymentTrait
 {
  /**
   * @see \bn_payment::init
   */
  public function init()
  {
  }

 }

 trait bn_oxarticleTrait
 {
  /**
   * Return price suffix
   *
   * @return null
   * @see \bn_oxarticle::_getUserPriceSufix
   */
  protected function _getUserPriceSufix()
  {
  }

  /**
   * Favorite Handling
   * @see \bn_oxarticle::isFavorite
   */
  public function isFavorite()
  {
  }

  /**
   *
   * @see \bn_oxarticle::getUVPPrice
   */
  public function getUVPPrice()
  {
  }

  /**
   * holt die Lieferzeit vom Parentartikel
   * @see \bn_oxarticle::getParentLieferzeit
   */
  public function getParentLieferzeit()
  {
  }

  /**
   * holt die Mindestlieferzeit
   * @return array
   * @see \bn_oxarticle::getmindeltime
   */
  public function getmindeltime()
  {
  }

  /**
   * holt die Maximale Lieferzeit
   * @return array
   * @see \bn_oxarticle::getmaxdeltime
   */
  public function getmaxdeltime()
  {
  }

  /**
   * gibt die maximale Lieferdauer als String zurück
   * @return mixed
   * @see \bn_oxarticle::getmaxdeltimeS
   */
  public function getmaxdeltimeS()
  {
  }

  /**
   * gibt die mindest Lieferdauer als String zurück
   * @return mixed
   * @see \bn_oxarticle::getmindeltimeS
   */
  public function getmindeltimeS()
  {
  }

  /**
   * gibt den Wert von bnFlagStatus zurück
   * @return mixed
   * @see \bn_oxarticle::getBnFlag
   */
  public function getBnFlag()
  {
  }

  /**
   * Ermittelt die StockFlag Farbe und gibt die Css Klasse zurück
   * @return null|string
   * @see \bn_oxarticle::showCssFlag
   */
  public function showCssFlag()
  {
  }

  /**
   * Gibt als String zurück ob das Produkt Varianten hat
   * @return null|string
   * @see \bn_oxarticle::getProductStatus
   */
  public function getProductStatus()
  {
  }

  /**
   * Returns formatted delivery date. If the date is not set ('0000-00-00') returns false.
   *
   * @return string | bool
   * @see \bn_oxarticle::getDeliveryDate
   */
  public function getDeliveryDate()
  {
  }

  /**
   * @see \bn_oxarticle::isFavorite
   */
  protected $isFavorite;
 }

 trait oePayPalOxArticleTrait
 {
  /**
   * Check if Product is virtual: is non material and is downloadable
   *
   * @return bool
   * @see \oePayPalOxArticle::isVirtualPayPalArticle
   */
  public function isVirtualPayPalArticle()
  {
  }

  /**
   * Gets stock amount for article
   * @return float
   * @see \oePayPalOxArticle::getStockAmount
   */
  public function getStockAmount()
  {
  }

 }

 trait oeThemeSwitcherTagTrait
 {
  /**
   * Returns view ID (for template engine caching).
   *
   * @return string   $this->_sViewId view id
   * @see \oeThemeSwitcherTag::getViewId
   */
  public function getViewId()
  {
  }

 }

 trait bn_oxinputvalidatorTrait
 {
  /**
   * Checking if all required fields were filled. In case of error
   * exception is thrown
   *
   * @param oxUser $oUser active user
   * @param array $aInvAddress billing address
   * @param array $aDelAddress delivery address
   *
   * @return null
   * @see \bn_oxinputvalidator::checkRequiredFields
   */
  public function checkRequiredFields($oUser, $aInvAddress, $aDelAddress)
  {
  }

 }

 trait oeThemeSwitcherLangTrait
 {
  /**
   * Returns array with paths where frontend language files are stored
   *
   * @param int $iLang active language
   *
   * @return array
   * @see \oeThemeSwitcherLang::_getLangFilesPathArray
   */
  protected function _getLangFilesPathArray($iLang)
  {
  }

  /**
   * Returns language cache file name
   *
   * @param bool $blAdmin admin or not
   * @param int $iLang current language id
   * @param array $aLangFiles language files to load [optional]
   *
   * @return string
   * @see \oeThemeSwitcherLang::_getLangFileCacheName
   */
  protected function _getLangFileCacheName($blAdmin, $iLang, $aLangFiles = null)
  {
  }

  /**
   * Returns language map array
   *
   * @param int $iLang language index
   * @param bool $blAdmin admin mode [default NULL]
   *
   * @return array
   * @see \oeThemeSwitcherLang::_getLanguageMap
   */
  protected function _getLanguageMap($iLang, $blAdmin = null)
  {
  }

 }

 trait oeThemeSwitcherViewConfigTrait
 {
  /**
   * User Agent getter.
   *
   * @return oeThemeSwitcherUserAgent
   * @see \oeThemeSwitcherViewConfig::oeThemeSwitcherGetUserAgent
   */
  public function oeThemeSwitcherGetUserAgent()
  {
  }

  /**
   * Return shop edition (EE|CE|PE)
   *
   * @return string
   * @see \oeThemeSwitcherViewConfig::oeThemeSwitcherGetEdition
   */
  public function oeThemeSwitcherGetEdition()
  {
  }

  /**
   * Check if module is active.
   *
   * @param string $sModuleId module id.
   * @param string $sVersionFrom module from version.
   * @param string $sVersionTo module to version.
   *
   * @return  bool
   * @see \oeThemeSwitcherViewConfig::oeThemeSwitcherIsModuleActive
   */
  public function oeThemeSwitcherIsModuleActive($sModuleId, $sVersionFrom = null, $sVersionTo = null)
  {
  }

  /**
   * Checks if module exists.
   *
   * @param $sModuleId
   * @param $aModules
   * @return bool
   * @see \oeThemeSwitcherViewConfig::_oeThemeSwitcherModuleExists
   */
  protected function _oeThemeSwitcherModuleExists($sModuleId, $aModules)
  {
  }

  /**
   * Checks whether module is enabled.
   *
   * @param $sModuleId
   * @return bool
   * @see \oeThemeSwitcherViewConfig::_oeThemeSwitcherIsModuleEnabled
   */
  protected function _oeThemeSwitcherIsModuleEnabled($sModuleId)
  {
  }

  /**
   * Checks whether module version is between given range.
   *
   * @param $sModuleId
   * @param $sVersionFrom
   * @param $sVersionTo
   * @return bool
   * @see \oeThemeSwitcherViewConfig::_oeThemeSwitcherIsModuleVersionCorrect
   */
  protected function _oeThemeSwitcherIsModuleVersionCorrect($sModuleId, $sVersionFrom, $sVersionTo)
  {
  }

  /**
   * User Agent.
   *
   * @var object
   * @see \oeThemeSwitcherViewConfig::_oUserAgent
   */
  protected $_oUserAgent;
 }

 trait dre_newsletterTrait
 {
  /**
   * @see \dre_newsletter::send
   */
  public function send()
  {
  }

 }

 trait oePayPalOxcmp_BasketTrait
 {
  /**
   * Method returns URL to checkout products OR to show popup.
   *
   * @return string
   * @see \oePayPalOxcmp_Basket::actionExpressCheckoutFromDetailsPage
   */
  public function actionExpressCheckoutFromDetailsPage()
  {
  }

  /**
   * Returns whether ECS popup should be shown
   *
   * @return bool
   * @see \oePayPalOxcmp_Basket::shopECSPopUp
   */
  public function shopECSPopUp()
  {
  }

  /**
   * Action method to add product to basket and return checkout URL.
   *
   * @return string
   * @see \oePayPalOxcmp_Basket::actionAddToBasketAndGoToCheckout
   */
  public function actionAddToBasketAndGoToCheckout()
  {
  }

  /**
   * Action method to return checkout URL.
   *
   * @return string
   * @see \oePayPalOxcmp_Basket::actionNotAddToBasketAndGoToCheckout
   */
  public function actionNotAddToBasketAndGoToCheckout()
  {
  }

  /**
   * Returns express checkout URL
   *
   * @return string
   * @see \oePayPalOxcmp_Basket::_getExpressCheckoutUrl
   */
  protected function _getExpressCheckoutUrl()
  {
  }

  /**
   * Method returns serialized current article params.
   *
   * @return string
   * @see \oePayPalOxcmp_Basket::getCurrentArticleInfo
   */
  public function getCurrentArticleInfo()
  {
  }

  /**
   * Method sets params for article and returns it's object.
   *
   * @return oePayPalArticleToExpressCheckoutCurrentItem
   * @see \oePayPalOxcmp_Basket::_getCurrentArticle
   */
  protected function _getCurrentArticle()
  {
  }

  /**
   * Method returns request object.
   *
   * @return oePayPalRequest
   * @see \oePayPalOxcmp_Basket::_getRequest
   */
  protected function _getRequest()
  {
  }

  /**
   * Method sets params for validator and returns it's object.
   *
   * @return oePayPalArticleToExpressCheckoutValidator
   * @see \oePayPalOxcmp_Basket::_getValidator
   */
  protected function _getValidator()
  {
  }

  /**
   * Changes oePayPalCancelURL by changing popup showing parameter.
   *
   * @return string
   * @see \oePayPalOxcmp_Basket::getPayPalCancelURL
   */
  public function getPayPalCancelURL()
  {
  }

  /**
   * Formats Redirect URL to normal url
   *
   * @param string $sUnformedUrl
   * @return string
   * @see \oePayPalOxcmp_Basket::_formatUrl
   */
  protected function _formatUrl($sUnformedUrl)
  {
  }

  /**
   * Show ECS PopUp
   *
   * @var bool
   * @see \oePayPalOxcmp_Basket::_blShopPopUp
   */
  protected $_blShopPopUp;
 }

 trait oeThemeSwitcherContentTrait
 {
  /**
   * Returns view ID (for template engine caching).
   *
   * @return string   $this->_sViewId view id
   * @see \oeThemeSwitcherContent::getViewId
   */
  public function getViewId()
  {
  }

 }

 trait oePayPalPaymentTrait
 {
  /**
   * Detects is current payment must be processed by PayPal and instead of standard validation
   * redirects to standard PayPal dispatcher
   *
   * @return bool
   * @see \oePayPalPayment::validatePayment
   */
  public function validatePayment()
  {
  }

  /**
   * Detects if current payment was already successfully processed by PayPal
   *
   * @param oxBasket $oBasket basket object
   *
   * @return bool
   * @see \oePayPalPayment::isConfirmedByPayPal
   */
  public function isConfirmedByPayPal($oBasket)
  {
  }

 }

 trait oeThemeSwitcherRssTrait
 {
  /**
   * Returns view ID (for template engine caching).
   *
   * @return string   $this->_sViewId view id
   * @see \oeThemeSwitcherRss::getViewId
   */
  public function getViewId()
  {
  }

 }

 trait oePayPalOxwArticleDetailsTrait
 {
  /**
   * Returns products amount to .tpl pages.
   *
   * @return int
   * @see \oePayPalOxwArticleDetails::oePayPalGetArticleAmount
   */
  public function oePayPalGetArticleAmount()
  {
  }

  /**
   * Returns persistent parameter.
   *
   * @return string
   * @see \oePayPalOxwArticleDetails::oePayPalGetPersistentParam
   */
  public function oePayPalGetPersistentParam()
  {
  }

  /**
   * Returns selections array.
   *
   * @return array
   * @see \oePayPalOxwArticleDetails::oePayPalGetSelection
   */
  public function oePayPalGetSelection()
  {
  }

  /**
   * Checks if showECSPopup parameter was passed.
   *
   * @return bool
   * @see \oePayPalOxwArticleDetails::oePayPalShowECSPopup
   */
  public function oePayPalShowECSPopup()
  {
  }

  /**
   * Checks if showECSPopup parameter was passed.
   *
   * @return bool
   * @see \oePayPalOxwArticleDetails::oePayPalGetCancelUrl
   */
  public function oePayPalGetCancelUrl()
  {
  }

  /**
   * Checks if displayCartInPayPal parameter was passed.
   *
   * @return bool
   * @see \oePayPalOxwArticleDetails::oePayPalDisplayCartInPayPal
   */
  public function oePayPalDisplayCartInPayPal()
  {
  }

  /**
   * Method returns request object.
   *
   * @return oePayPalRequest
   * @see \oePayPalOxwArticleDetails::_oePayPalGetRequest
   */
  protected function _oePayPalGetRequest()
  {
  }

  /**
   * Gets ECSArticle, unserializes and returns it.
   *
   * @return array
   * @see \oePayPalOxwArticleDetails::_oePayPalGetECSArticle
   */
  protected function _oePayPalGetECSArticle()
  {
  }

 }

 trait bn_account_passwordTrait
 {
  /**
   * @see \bn_account_password::render
   */
  public function render()
  {
  }

 }

 trait bn_oxbasketTrait
 {
  /**
   * Returns basket items array
   *
   * @return array
   * @see \bn_oxbasket::getContents
   */
  public function getContents()
  {
  }

 }

 trait oeThemeSwitcherStartTrait
 {
  /**
   * Returns view ID (for template engine caching).
   *
   * @return string   $this->_sViewId view id
   * @see \oeThemeSwitcherStart::getViewId
   */
  public function getViewId()
  {
  }

 }

 trait oePayPalOxUserTrait
 {
  /**
   * Check if exist real user (with password) for passed email
   *
   * @param string $sUserEmail - email
   *
   * @return bool
   * @see \oePayPalOxUser::isRealPayPalUser
   */
  public function isRealPayPalUser($sUserEmail)
  {
  }

  /**
   * Check if the shop user is the same as PayPal user.
   * Fields: first name, last name, street, street nr, city - must be equal.
   *
   * @param oePayPalResponseGetExpressCheckoutDetails $oDetails - data returned from PayPal
   *
   * @return bool
   * @see \oePayPalOxUser::isSamePayPalUser
   */
  public function isSamePayPalUser($oDetails)
  {
  }

  /**
   * Check if the shop user address is the same in PayPal.
   * Fields: street, street nr, city - must be equal.
   *
   * @param oePayPalResponseGetExpressCheckoutDetails $oDetails - data returned from PayPal
   *
   * @return bool
   * @see \oePayPalOxUser::isSameAddressPayPalUser
   */
  public function isSameAddressPayPalUser($oDetails)
  {
  }

  /**
   * Check if the shop user address user name is the same in PayPal.
   * Fields: name, lname.
   *
   * @param oePayPalResponseGetExpressCheckoutDetails $oDetails - data returned from PayPal
   *
   * @return bool
   * @see \oePayPalOxUser::isSameAddressUserPayPalUser
   */
  public function isSameAddressUserPayPalUser($oDetails)
  {
  }

  /**
   * Returns user from session associated with current PayPal order
   *
   * @return oxUser
   * @see \oePayPalOxUser::loadUserPayPalUser
   */
  public function loadUserPayPalUser()
  {
  }

  /**
   * Creates user from PayPal data
   *
   * @param oePayPalResponseGetExpressCheckoutDetails $oPayPalData - data returned from PayPal
   *
   * @see \oePayPalOxUser::createPayPalUser
   */
  public function createPayPalUser($oPayPalData)
  {
  }

  /**
   * Prepare address data array from PayPal response data
   *
   * @param oePayPalResponseGetExpressCheckoutDetails $oPayPalData - PayPal data
   *
   * @return array
   * @see \oePayPalOxUser::_prepareDataPayPalUser
   */
  protected function _prepareDataPayPalUser($oPayPalData)
  {
  }

  /**
   * Check required fields
   *
   * @param array $aAddressData - PayPal data
   *
   * @return bool
   * @see \oePayPalOxUser::_checkRequiredFieldsPayPalUser
   */
  protected function _checkRequiredFieldsPayPalUser($aAddressData)
  {
  }

  /**
   * Split street nr from address
   *
   * @param string $sShipToStreet address string
   *
   * @return array
   * @see \oePayPalOxUser::_splitShipToStreetPayPalUser
   */
  protected function _splitShipToStreetPayPalUser($sShipToStreet)
  {
  }

  /**
   * Returns true if user has callback state
   *
   * @return bool
   * @see \oePayPalOxUser::isCallBackUserPayPalUser
   */
  public function isCallBackUserPayPalUser()
  {
  }

  /**
   * Returns user group list
   *
   * @param $sOxId oxId identifier
   *
   * @return oxList
   * @see \oePayPalOxUser::getUserGroups
   */
  public function getUserGroups($sOxId = null)
  {
  }

  /**
   * Initializes call back user
   *
   * @param array $aPayPalData callback user data
   *
   * @return null
   * @see \oePayPalOxUser::initializeUserForCallBackPayPalUser
   */
  public function initializeUserForCallBackPayPalUser($aPayPalData)
  {
  }

  /**
   * CallBack user mode
   * @var bool
   * @see \oePayPalOxUser::_blCallBackUser
   */
  protected $_blCallBackUser;
 }

 trait oePayPalOxAddressTrait
 {
  /**
   * Creates user shipping address from PayPal data and set to session
   *
   * @param oePayPalResponseGetExpressCheckoutDetails $oDetails - PayPal data
   * @param string $sUserId - user id
   *
   * @see \oePayPalOxAddress::createPayPalAddress
   */
  public function createPayPalAddress($oDetails, $sUserId)
  {
  }

  /**
   * Prepare address data array from PayPal response data
   *
   * @param oePayPalResponseGetExpressCheckoutDetails $oDetails - PayPal data
   *
   * @return array
   * @see \oePayPalOxAddress::_prepareDataPayPalAddress
   */
  protected function _prepareDataPayPalAddress($oDetails)
  {
  }

  /**
   * Check required fields
   *
   * @param array $aAddressData - PayPal data
   *
   * @return bool
   * @see \oePayPalOxAddress::_checkRequiredFieldsPayPalAddress
   */
  protected function _checkRequiredFieldsPayPalAddress($aAddressData)
  {
  }

  /**
   * @see \oePayPalOxAddress::_existPayPalAddress
   */
  protected function _existPayPalAddress($aAddressData)
  {
  }

  /**
   *  Split street nr from address
   *
   * @param string $sShipToStreet address string
   *
   * @return array
   * @see \oePayPalOxAddress::splitShipToStreetPayPalAddress
   */
  public function splitShipToStreetPayPalAddress($sShipToStreet)
  {
  }

 }

 trait oePayPalOrder_ListTrait
 {
  /**
   * Executes parent method parent::render() and returns name of template
   * file "order_list.tpl".
   *
   * @return string
   * @see \oePayPalOrder_List::render
   */
  public function render()
  {
  }

  /**
   * Builds and returns SQL query string. Adds additional order check.
   *
   * @param object $oListObject list main object
   *
   * @return string
   * @see \oePayPalOrder_List::_buildSelectString
   */
  protected function _buildSelectString($oListObject = null)
  {
  }

  /**
   * Adding folder check
   *
   * @param array $aWhere SQL condition array
   * @param string $sqlFull SQL query string
   *
   * @return string
   * @see \oePayPalOrder_List::_prepareWhereQuery
   */
  protected function _prepareWhereQuery($aWhere, $sqlFull)
  {
  }

 }

 trait mude_oxrelic_oxwidgetcontrolTrait
 {
  /**
   * @see \mude_oxrelic_oxwidgetcontrol::_process
   */
  protected function _process($sClass, $sFunction, $aParams = null, $aViewsChain = null)
  {
  }

 }

 trait oePayPalOxOrderTrait
 {
  /**
   * Loads order associated with current PayPal order
   *
   * @return bool
   * @see \oePayPalOxOrder::loadPayPalOrder
   */
  public function loadPayPalOrder()
  {
  }

  /**
   *
   *
   * @return bool
   * @see \oePayPalOxOrder::oePayPalUpdateOrderNumber
   */
  public function oePayPalUpdateOrderNumber()
  {
  }

  /**
   * Delete order created by current PayPal ordering process
   *
   * @return bool
   * @see \oePayPalOxOrder::deletePayPalOrder
   */
  public function deletePayPalOrder()
  {
  }

  /**
   * Delete order together with PayPal order data
   *
   * @param null $sOxId
   * @return bool|void
   * @see \oePayPalOxOrder::delete
   */
  public function delete($sOxId = null)
  {
  }

  /**
   * Updates order transaction status, ID and date.
   *
   * @param string $sTransactionId order transaction ID
   * @param string $sDate order transaction date
   *
   * @see \oePayPalOxOrder::_setPaymentInfoPayPalOrder
   */
  protected function _setPaymentInfoPayPalOrder($sTransactionId, $sDate)
  {
  }

  /**
   * Finalizes PayPal order
   *
   * @param  oePayPalResponseDoExpressCheckoutPayment $oResult PayPal results array
   * @param oxUser $oUser User object
   * @param oxBasket $oBasket Basket object
   * @param oxPayment $oPayment Payment object
   * @param string $sTransactionMode transaction mode Sale|Authorization
   *
   * @return null
   * @see \oePayPalOxOrder::finalizePayPalOrder
   */
  public function finalizePayPalOrder($oResult, $oBasket, $sTransactionMode)
  {
  }

  /**
   * Checks if delivery set used for current order is available and active.
   * Throws exception if not available
   *
   * @param oxbasket $oBasket basket object
   *
   * @return null
   * @see \oePayPalOxOrder::validateDelivery
   */
  public function validateDelivery($oBasket)
  {
  }

  /**
   * Returns PayPal order object
   *
   * @param null $sOxId
   *
   * @return oePayPalPayPalOrder|null
   * @see \oePayPalOxOrder::getPayPalOrder
   */
  public function getPayPalOrder($sOxId = null)
  {
  }

  /**
   * Get payment status
   *
   * @return string
   * @see \oePayPalOxOrder::getPayPalPaymentStatus
   */
  public function getPayPalPaymentStatus()
  {
  }

  /**
   * Returns PayPal Authorization id
   * @see \oePayPalOxOrder::getAuthorizationId
   */
  public function getAuthorizationId()
  {
  }

  /**
   * Checks whether PayPal payment is available
   * @param $oUser
   * @param $dBasketPrice
   * @param $sShippingId
   *
   * @return bool
   * @see \oePayPalOxOrder::_isPayPalPaymentValid
   */
  protected function _isPayPalPaymentValid($oUser, $dBasketPrice, $sShippingId)
  {
  }

  /**
   * Checks whether Empty payment is available.
   * @param $sShippingId
   * @param $dBasketPrice
   * @param $oUser
   *
   * @return bool
   * @see \oePayPalOxOrder::_isEmptyPaymentValid
   */
  protected function _isEmptyPaymentValid($oUser, $dBasketPrice, $sShippingId)
  {
  }

  /**
   * PayPal order information
   *
   * @var oePayPalPayPalOrder
   * @see \oePayPalOxOrder::_oPayPalOrder
   */
  protected $_oPayPalOrder;
 }

 trait oePayPalOxBasketItemTrait
 {
  /**
   * Checks if validation should be skipped when getting article object.
   * This is done only when PayPal finalizes order.
   *
   * @param bool $blCheckProduct checks if product is buyable and visible
   * @param string $sProductId product id
   * @param bool $blDisableLazyLoading disable lazy loading
   *
   * @throws oxArticleException, oxNoArticleException exception
   *
   * @return oxArticle
   * @see \oePayPalOxBasketItem::getArticle
   */
  public function getArticle($blCheckProduct = true, $sProductId = null, $blDisableLazyLoading = false)
  {
  }

 }

 trait bn_oxsearchTrait
 {
  /**
   *
   *
   * @param bool $sSearchParamForQuery
   * @param bool $sInitialSearchCat
   * @param bool $sInitialSearchVendor
   * @param bool $sInitialSearchManufacturer
   * @param bool $sSortBy
   * @return oxarticlelist
   * @see \bn_oxsearch::getSearchArticles
   */
  public function getSearchArticles($sSearchParamForQuery = false, $sInitialSearchCat = false, $sInitialSearchVendor = false, $sInitialSearchManufacturer = false, $sSortBy = false)
  {
  }

  /**
   * Gibt die Summe der Gefundenen ergebenisse Zurück
   *
   * @param bool $sSearchParamForQuery
   * @param bool $sInitialSearchCat
   * @param bool $sInitialSearchVendor
   * @param bool $sInitialSearchManufacturer
   * @return integer
   * @see \bn_oxsearch::getSearchArticleCount
   */
  public function getSearchArticleCount($sSearchParamForQuery = false, $sInitialSearchCat = false, $sInitialSearchVendor = false, $sInitialSearchManufacturer = false)
  {
  }

  /**
   * Statement um Datenätze aus der Datenbank abzufragen
   *
   * @param $sSearchParamForQuery
   * @param $strFields
   * @return string
   * @see \bn_oxsearch::getSelectStatement
   */
  public function getSelectStatement($sSearchParamForQuery, $strFields)
  {
  }

  /**
   * @return string
   * @see \bn_oxsearch::getAutosuggestSelectColumns
   */
  public function getAutosuggestSelectColumns()
  {
  }

  /**
   *
   * @see \bn_oxsearch::getFaultCorrectedSearch
   */
  public function getFaultCorrectedSearch($strSearchedString)
  {
  }

 }

 trait bn_oxorderTrait
 {
  /**
   * @param $oBasket
   * @see \bn_oxorder::validateDelivery
   */
  public function validateDelivery($oBasket)
  {
  }

  /**
   * @param $oBasket
   * @see \bn_oxorder::validatePayment
   */
  public function validatePayment($oBasket)
  {
  }

  /**
   * Updates order transaction status. Faster than saving whole object
   *
   * @param string $sStatus order transaction status
   *
   * @return null
   * @see \bn_oxorder::_setOrderStatus
   */
  protected function _setOrderStatus($sStatus)
  {
  }

  /**
   * @see \bn_oxorder::setWhitelabel
   */
  public function setWhitelabel($wert)
  {
  }

  /**
   * @see \bn_oxorder::lieferschein
   */
  public function lieferschein()
  {
  }

  /**
   * Whitelabel Bestellungen.
   * @var null
   * @see \bn_oxorder::_isWhitelabel
   */
  protected $_isWhitelabel;
 }

 trait bn_oxaddressTrait
 {
  /**
   * @see \bn_oxaddress::filterAdresses
   */
  public function filterAdresses($strSearch)
  {
  }

 }

 trait oePayPalWrappingTrait
 {
  /**
   * Checks if payment action is processed by PayPal
   *
   * @return bool
   * @see \oePayPalWrapping::isPayPal
   */
  public function isPayPal()
  {
  }

  /**
   * Detects is current payment must be processed by PayPal and instead of standard validation
   * redirects to standard PayPal dispatcher
   *
   * @return bool
   * @see \oePayPalWrapping::changeWrapping
   */
  public function changeWrapping()
  {
  }

 }

 trait dre_oxactionsTrait
 {
  /**
   * Returns assigned banner article picture url
   *
   * @return string
   * @see \dre_oxactions::getBannerPictureUrl
   */
  public function getBannerPictureUrl()
  {
  }

 }

 trait oeThemeSwitcherVendorListTrait
 {
  /**
   * Returns view ID (for template engine caching).
   *
   * @return string   $this->_sViewId view id
   * @see \oeThemeSwitcherVendorList::getViewId
   */
  public function getViewId()
  {
  }

 }

 trait oeThemeSwitcherManufacturerListTrait
 {
  /**
   * Returns view ID (for template engine caching).
   *
   * @return string   $this->_sViewId view id
   * @see \oeThemeSwitcherManufacturerList::getViewId
   */
  public function getViewId()
  {
  }

 }

 trait oeThemeSwitcherConfigTrait
 {
  /**
   * Returns config parameter value if such parameter exists
   *
   * @param string $sName config parameter name
   *
   * @return mixed
   * @see \oeThemeSwitcherConfig::getConfigParam
   */
  public function getConfigParam($sName)
  {
  }

  /**
   * Return current active theme
   *
   * @return string
   * @see \oeThemeSwitcherConfig::oeThemeSwitcherGetActiveThemeId
   */
  public function oeThemeSwitcherGetActiveThemeId()
  {
  }

  /**
   * Return theme manager
   *
   * @return oeThemeSwitcherThemeManager
   * @see \oeThemeSwitcherConfig::oeThemeSwitcherGetThemeManager
   */
  public function oeThemeSwitcherGetThemeManager()
  {
  }

  /**
   * Bool variable true if modules configs are loaded, otherwise false
   *
   * @var bool
   * @see \oeThemeSwitcherConfig::_blIsModuleConfigLoaded
   */
  protected $_blIsModuleConfigLoaded;
  /**
   * Theme manager object
   *
   * @var oeThemeSwitcherThemeManager
   * @see \oeThemeSwitcherConfig::_oThemeManager
   */
  protected $_oThemeManager;
 }

 trait oePayPalOrderTrait
 {
  /**
   * Checks if payment action is processed by PayPal
   *
   * @return bool
   * @see \oePayPalOrder::isPayPal
   */
  public function isPayPal()
  {
  }

  /**
   * Returns PayPal user
   *
   * @return oxUser
   * @see \oePayPalOrder::getUser
   */
  public function getUser()
  {
  }

  /**
   * Returns PayPal payment object if PayPal is on, or returns parent::getPayment()
   *
   * @return oxPayment
   * @see \oePayPalOrder::getPayment
   */
  public function getPayment()
  {
  }

  /**
   * Returns current order object
   *
   * @return oxOrder
   * @see \oePayPalOrder::_getOrder
   */
  protected function _getOrder()
  {
  }

  /**
   * Checks if order payment is PayPal and redirects to payment processing part
   *
   * @param int $iSuccess order state
   *
   * @return string
   * @see \oePayPalOrder::_getNextStep
   */
  protected function _getNextStep($iSuccess)
  {
  }

 }

 trait bn_oxprobsarticlesTrait
 {
  /**
   * @see \bn_oxprobsarticles::render
   */
  public function render()
  {
  }

 }

 trait bn_category_main_ajaxTrait
 {
  /**
   *
   * @see \bn_category_main_ajax::__construct
   */
  public function __construct()
  {
  }

 }

}
