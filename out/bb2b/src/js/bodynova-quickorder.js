var objSearchTimeout = null;

$('#OXARTNUM').keyup(function() {
    if(objSearchTimeout)
        window.clearInterval(objSearchTimeout);
    objSearchTimeout = window.setTimeout(lookupForItems, 500);
})


function lookupForItems() {
    //
    var res = $.ajax({
        type: "GET",
        dataType: "json",
        url: '/index.php?cl=quickorder&fnc=getArticles&artnum=' + $('#OXARTNUM').val(),
        async : true,
        success : function(res) {
            //
            $('#newEntryAutoComplete').html('');
            //
            if(res.articles!=undefined)
            {
                $.each(res.articles, function(key,val) {
                    $('#newEntryAutoComplete').append(
                        '<table>' +
                        '   <tr class="tr ' + (key % 2 == 0 ? 'even' : 'odd') + '" onclick="addQuickorderItem(\'' + addslashes(JSON.stringify(val)) + '\');">' +
                        '       <td class="td artnum">' + this.artnum + '</td>' +
                        '       <td class="td title">' + this.title + '</td>' +
                        '       <td class="td price">' + this.price + '</td>' +
                        '       <td class="td amount"></td>' +
                        '       <td class="td stock"><div class="stock' + this.bnflagbestand + '"></div></td>' +
                        '   </tr>' +
                        '</table>'
                    );
                });
            }
            else if(res.suggestions!=undefined)
            {
                var strSuggestion = strSuggestionHtml = '';
                //
                $.each(res.suggestions, function(key, val) {
                    strSuggestion+= val.word + ' ';
                    strSuggestionHtml+=(val.changed ? '<b><i>' : '') + val.word + (val.changed ? '</i></b>' : '') + ' ';
                });
                //
                $('#newEntryAutoComplete').append(
                    res.suggesttext
                    +
                    '<a href="#" onclick="$(\'#OXARTNUM\').val(\'' + strSuggestion +'\');lookupForItems();">' + strSuggestionHtml + '</a>'
                );
            }
        }
    });
}

/**
 *
 * @param params
 */
function addQuickorderItem(params) {
    //
    eval('params = ' + params);
    // clear result list
    $('#newEntryAutoComplete').html('');
    // clear inputs
    $('#OXARTNUM').val('');
    $('#OXTITLE').val('');
    // add row to table
    var strEntry =  '<tr class="tr item">' +
        '   <td class="td artnum">' + params.artnum + '</td>' +
        '   <td class="td title">' + params.title + '</td>' +
        '   <td class="td price">' + params.price + '</td>' +
        '   <td class="td amount"><input type="text" name="amount[' + params.oxid + ']" value="1"></td>' +
        '   <td class="td stock"><div class="stock' + params.bnflagbestand + '"></div></td>' +
        '</tr>';
    //
    $(strEntry).insertBefore('#QuickPositions #newEntry');
    // ende
}

/**
 *
 */
function placeQuickOrder() {
    // disable button with ladda
    var ladda = Ladda.create(document.querySelector('#btnPlaceOrder'));
    ladda.start();
    //
    $('#frmQuickOrder').ajaxForm({
        type: "POST",
        dataType: 'json',
        url: '/index.php?cl=quickorder&fnc=submitBasket',
        success:function(res) {
            //
            ladda.stop();
            // close preview modal and open confirmation when successful
            if(res.state==1) {
                $('#previeworder').modal('hide');
                $('#ordercomplete').modal({});
            } else {
                //
                var strErrorMsg = '';
                switch(res.state) {
                    case 0:
                        strErrorMsg = 'Sie haben keine Artikel gewählt.';
                    break;
                    case -1:
                        strErrorMsg = 'Bitte füllen Sie die Adresse vollständig aus!';
                    break;
                }
                //
                $('#previeworder .modal-body').prepend('<div class="alert alert-danger" role="alert">' + strErrorMsg + '</div>');
                // ende
            }
            // ende
        }
    }).submit();
    // ende
}


/**
 *
 */
function showConfirmQuickOrder() {
    //
    var strOverview = '<table id="orderPreview">'
        strOverview+= ' <tr class="headline">';
        strOverview+= '     <td class="amount">Anzahl</td>';
        strOverview+= '     <td class="title">Artikel</td>';
        strOverview+= '     <td class="price">Preis</td>';
        strOverview+= ' </tr>';
    //
    $('.item').each(function() {
        strOverview+='<tr>';
        strOverview+='  <td class="amount">' + $(this).children('.amount').children('input').val() + '</td>';
        strOverview+='  <td class="title">' + $(this).children('.title').text() + '</td>';
        strOverview+='  <td class="price">' + $(this).children('.price').text() + '</td>';
        strOverview+='</tr>';
    });
    strOverview+='</table>';
    //
    $('#previeworder .modal-body').html(strOverview);
    $('#previeworder').modal({});
    // ende
}


//
//
//
//
//

/**
 *
 */
function setAdress(oxid, company, sal, fname, lname, street, streetnr, addinfo, zip, city, countryid, fon ) {
    //
    $('#addressbookid').val(oxid);
    $('#oxcompany').val(company);
    $('#oxsal').val(sal);
    $('#oxfname').val(fname);
    $('#oxlname').val(lname);
    $('#oxstreet').val(street);
    $('#oxstreetnr').val(streetnr);
    $('#oxaddinfo').val(addinfo);
    $('#oxzip').val(zip);
    $('#oxcity').val(city);
    $('#oxfon').val(fon);
    $('#oxcountryid').val(countryid);
    //
    $('#adressBookAutocomplete').html('');
    $('#searchadressbook').val('');
    // ende
}


/**
 *
 */
function searchInAdressbook() {
    //
    var res = $.ajax({
        type: "GET",
        dataType: "json",
        url: '/index.php?cl=quickorder&fnc=getAdressAutocomplete&search=' + $('#searchadressbook').val(),
        async : false
    }).responseJSON;
    //
    $('#adressBookAutocomplete').html('');
    //
    $.each(res, function() {
        //
        $('#adressBookAutocomplete').append(
            '<div onclick="setAdress(\'' + this.oxid + '\',\'' + this.company + '\',\'' + this.sal + '\',\'' + this.fname + '\',\'' + this.lname + '\',\'' + this.street + '\',\'' + this.streetnr + '\',\'' + this.addinfo + '\',\'' + this.zip + '\',\'' + this.city + '\',\'' + this.countryid + '\',\'' + this.fon + '\')" class="addressBookItem">' +
                (this.company!='' ? this.company + '<br>': '') +
                this.fname + ' ' + this.lname + '<br>' +
                this.street + ' ' + this.streetnr + '<br>' +
                this.zip + ' ' + this.city + '<br>' +
            '</div>'
        );
        // ende
    });
    // ende
}

var timeOutHolderAdressbookSearch = null;

$(document).ready(function() {
    $('#searchadressbook').keyup(function() {
        //
        if(timeOutHolderAdressbookSearch)
            window.clearTimeout(timeOutHolderAdressbookSearch);
        //
        timeOutHolderAdressbookSearch = window.setTimeout('searchInAdressbook()', 500);
        // ende
    });
    //
    $('#addressbookid, #oxcompany, #oxsal, #oxfname, #oxlname, #oxstreet, #oxstreetnr, #oxaddinfo, #oxzip, #oxcity, #oxfon, #oxcountryid').change(function() {
        $('#addressbookid').val('');
    });
    //
    /*
    $('#searchadressbook').val('geil').keyup();
    addQuickorderItem('{\'oxid\':\'edae7ac1f920327673822d2c2bc1f404\',\'artnum\':\'hms1\',\'title\':\'Hamam Set I zum Sonderpreis\',\'price\':4.96,\'bnflagbestand\':\'0\'}');
    addQuickorderItem('{\'oxid\':\'eda3ce1925f2b48e3947eadc87bcb196\',\'artnum\':\'hms2b\',\'title\':\'Hamam Set II blau, zum Sonderpreis\',\'price\':10,\'bnflagbestand\':\'0\'}');
    addQuickorderItem('{\'oxid\':\'8894ac5e442372d96.61335702\',\'artnum\':\'ms88b\',\'title\':\'Meditations Set: Zafu & Zabuton(80x80 cm) | blau\',\'price\':62.94,\'bnflagbestand\':\'0\'}');
    */
    // ende
});

/**
 * @todo remove development helpers
 */
$(document).ready(function() {
    //
    /*
    //$('#OXARTNUM').val('hms');
    //$('#OXARTNUM').keyup();
    //window.setTimeout("$('#newEntryAutoComplete .tr:first').click()", 1000)

    addQuickorderItem('{\'oxid\':\'edae7ac1f920327673822d2c2bc1f404\',\'artnum\':\'hms1\',\'title\':\'Hamam Set I zum Sonderpreis\',\'price\':4.96,\'bnflagbestand\':\'0\'}');
    addQuickorderItem('{\'oxid\':\'eda3ce1925f2b48e3947eadc87bcb196\',\'artnum\':\'hms2b\',\'title\':\'Hamam Set II blau, zum Sonderpreis\',\'price\':10,\'bnflagbestand\':\'0\'}');
    addQuickorderItem('{\'oxid\':\'8894ac5e442372d96.61335702\',\'artnum\':\'ms88b\',\'title\':\'Meditations Set: Zafu & Zabuton(80x80 cm) | blau\',\'price\':62.94,\'bnflagbestand\':\'0\'}');

    $('input[name=\'invadr[oxuser__oxfname]\']').val('Manuel');
    $('input[name=\'invadr[oxuser__oxlname]\']').val('Geil');
    $('input[name=\'invadr[oxuser__oxcompany]\']').val('Geil.PM');
    $('input[name=\'invadr[oxuser__oxstreet]\']').val('Schloßstraße');
    $('input[name=\'invadr[oxuser__oxstreetnr]\']').val('28');
    $('input[name=\'invadr[oxuser__oxzip]\']').val('52379');
    $('input[name=\'invadr[oxuser__oxcity]\']').val('Langerwehe');
    $('input[name=\'invadr[oxuser__oxfon]\']').val('024239048721');
    $('input[name=\'shipaswhitelabel\']').attr('checked', 'checked');
    //
    // showConfirmQuickOrder();
    */
    // ende
});

