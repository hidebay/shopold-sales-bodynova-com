$(document).ready(function() {
    // einmal an alle elemente den ursprungswert speichern
    $('.amountinput').each(function() {
        $(this).attr('original-value', $(this).val());
    });
    //
    $('.amountinput').keyup(function() {
        if($(this).val() != $(this).attr('original-value')) {
            $(this).siblings('.updateBtn').show();
        } else {
            $(this).siblings('.updateBtn').hide();
        }
    });
});