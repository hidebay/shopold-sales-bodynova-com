/**
 *  execution functions on document ready
 */
$(document).ready(function() {
    // Bind Scroll shrink/unshrink class binding on scrolling
    $(window).scroll(function() {
        setTimeout( function() {
            if ( (window.pageYOffset || document.scrollTop) >= 150 ) {
                $('body').removeClass('unshrinked' );
                $('body').addClass('shrinked' );
                //$('.logo').removeClass('hidden-xs, hidden-sm ,hidden-md, hidden-lg');
                /*$('.logo').addClass('hidden-md hidden-lg');
                $('.suche').addClass('hidden-md hidden-lg');
                $('.rest').addClass('hidden-md hidden-lg');*/

            } else {
                $('body').removeClass('shrinked' );
                $('body').addClass('unshrinked' );
                /*$('.logo').removeClass('hidden-md hidden-lg');
                $('.suche').removeClass('hidden-md hidden-lg');
                $('.rest').removeClass('hidden-md hidden-lg');*/
            }
        }, 250 );
    });
    // trigger scroll for first time execution
    $(window).scroll();
    //
    /*
    $('[data-toggle="tooltip"]').tooltip();
    // close tooltips on click otherwise you cause a tooltip inside the popovers
    $('[data-toggle="tooltip"]').click(function() {
        $(this).tooltip('hide');
    });
    */
    //
    $('#header .suche button').click(function(){
        if($('body').hasClass('shrinked')) {
            //

            //orig:
            /*
             $('#header .searchBox #searchparam').toggle();
            if($('#header .searchBox #searchparam').css('display')=='block') {
                $('#header .searchBox #results').show();
            } else {
                $('#header .searchBox #results').hide();
            }

            if($('#header .searchBox #searchparam').css('display')=='block') {
                $('#header .searchBox #results').show();
            } else {
                $('#header .searchBox #results').hide();
            }
             */

            // neu test:
            $('#header .suche #searchparam').toggle();
            $('#header .suche #results').hide();
            // ende
        }
        return false;
    });
    // end
});


/**
 * Fixes Oxid to match css-id criteria
 *
 * @param oxid
 * @returns {XML|string|void}
 */
function fixOxId(oxid) {
    return oxid.replace(/\./g, '_');
}


/**
 * Bind spinner
 */
function bindSpinner() {
    // first unbind to avoid count clash
    $('.spinner .bootstrap-touchspin-up').unbind('click');
    $('.spinner .bootstrap-touchspin-up').on('click', function(e) {
        $(this).parent().siblings('input:first').val( parseInt($(this).parent().siblings('input:first').val(), 10) + 1);
        return false;
    });
    //
    $('.spinner .bootstrap-touchspin-down').unbind('click');
    $('.spinner .bootstrap-touchspin-down').on('click', function() {
        if(parseInt($(this).parent().siblings('input:first').val())>0)
            $(this).parent().siblings('input:first').val( parseInt($(this).parent().siblings('input:first').val(), 10) - 1);
        return false;
    });
    // ende
}
// bind spinners on page ready
$(document).ready(bindSpinner);


/**
 *
 */
function handleAjaxLogout(response) {
    //
    if(response.success!=undefined && !response.success) {
        //
        window.scrollTo(0,0);
        //
        $('body').append(
            '<div id="darkInizer"></div>' +
            '<div id="alertLoggedOut" role="alert" class="alert alert-danger alert-dismissible fade in">' +
            '   <h4>' + translations.loggedOutHeadline+ '</h4>' +
            '   <p>' + translations.loggedOutText + '</p>' +
            '   <p>' +
            '       <button class="btn btn-default" type="button" onclick="location.href=\'/index.php?cl=account&sourcecl=start\';">' + translations.loginButton + '</button>' +
            '    </p>' +
            '</div>'
        );
        //
        $('#alertLoggedOut').alert();
        // ende
    }
    // ende
}


/**
 *
 * @param oxid
 */
function loadProductOptions(oxid, highlightoxid) {
    // remove and hide options on second click
    if($('#article_selected_' + fixOxId(oxid)).css('display')!='none') {
        $('#article_selected_' + fixOxId(oxid) + ' td').html('');
        $('#article_selected_' + fixOxId(oxid)).hide();
        return;
    }
    //
    var ladda = Ladda.create( document.querySelector( '#article_' + fixOxId(oxid) + ' .btn-show-variants' ) );
    //
    ladda.start();
    //
    var res = $.ajax({
        type: "GET",
        url: '/index.php?cl=getproductselections&fnc=getList&oxid=' + oxid,
        async : false,
        dataType: "json",
        error: handleAjaxLogout,
        success : function (res) {
            //
            handleAjaxLogout(res);
            // show area for select and inject htmlcode
            $('#article_selected_' + fixOxId(oxid) + ' td').html(res.content);
            //
            bindSpinner();
            //
            ladda.stop();
            //
            $('#article_selected_' + fixOxId(oxid)).show();
            //
            if(highlightoxid!=undefined) {
                $('#article_' + fixOxId(fixOxId(highlightoxid))).addClass('highlight');
            }
            // reorganize tab order
            makeTabOrder();
            // ende
        }
    });
    //end
}

/**
 *
 */
function makeTabOrder() {
    $('input.amount').each(function(key,val) {
        $(this).attr('tabindex', key+1);
    });
}


/**
 *
 */
function loadArticleDetails(oxid) {
    //
    var res = $.ajax({
        type: "GET",
        url: '/index.php?cl=ajaxbasket&fnc=getArticleDetails&oxid=' + oxid,
        async: false,
        dataType: "json",
        error: handleAjaxLogout
    }).responseJSON;
    //
    $('body').append(res.content);
    //
    $('#articledetails').modal();
    //
    $('#articledetails').on('hide.bs.modal', function (e) {
        $('#articledetails').remove();
    });
    // ende
}


/**
 *
 * @param oxid
 */
function jumpToArticle(oxparentid, oxid) {
    //
    var res = $.ajax({
        type: "GET",
        url: '/index.php?cl=ajaxbasket&fnc=getCategoryURL&oxid=' + oxid,
        async: false,
        dataType: "json",
        error: handleAjaxLogout
    }).responseJSON;
    //
    window.location.href = res.url + (res.url.indexOf('?') <0 ? '?' : '') + '&open=' + oxparentid +'&highlight=' + oxid;
    // ende
}

/**
 *
 * @param sParam
 * @returns {*}
 */
function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
}

/**
 * check if we need to highlight a oxid
 */
$(document).ready(function() {
    if(getUrlParameter('open')) {
        //
        location.hash = '#' + fixOxId(getUrlParameter('open'));
        //
        $(document).scrollTop($(document).scrollTop() - 175);
        //
        if(getUrlParameter('open')!=getUrlParameter('highlight')) {
            loadProductOptions(getUrlParameter('open'), fixOxId(getUrlParameter('highlight')));
        } else {
            $('#article_' + fixOxId(fixOxId(getUrlParameter('open')))).addClass('highlight');
            loadProductOptions(getUrlParameter('open'));
        }
        // ende
    }
});


/**
 *
 */
function openAllVariants() {
    $('.btn-show-variants').each(function() {
        $(this).click();
    });
}


/**
 * organize tab order on page load
 */
$(document).ready(function() {
    makeTabOrder();
});

/**
 *
 * @param response
 */
function handleAddTobasketResponse(response) {
    //
    handleAjaxLogout(response);
    //
    if(response.added.length)
    {
        // update the badge
        $('#miniBasket .badge').text(response.itemscount);
        $('#miniBasket .counter').show();
        //
        $('#miniBasket .badge').tooltip('show');
        window.setTimeout("$('#miniBasket .badge').tooltip('hide');", 2500);
        //
        if(response.oxid) {
            $('#article_selected_' + fixOxId(response.oxid)).hide();
        }
        //
        $('input[name^=\'amount\']').val('0');
    }
    else
    {
        $('#nothingaddedtobasketmodal').modal({});
    }
    // ende
}

/**
 * adds a bulkform to the basket
 * @param oxid
 */
function addBulkCart(oxid) {
    $.ajax({
        type: "POST",
        url: '/index.php?cl=ajaxbasket&fnc=addBulk&oxid=' + oxid,
        data: $('#article_selected_' + fixOxId(oxid) + ' form').serialize(),
        dataType: "json",
        success: handleAddTobasketResponse,
        error: handleAjaxLogout
    });
}


/**
 * Adds a Single Item to the Basket
 * @param oxid
 */
function addToBasket(oxid) {
    //
    var data = {};
    data['amount'] = {};
    data['amount'][oxid] = $('#article_' + fixOxId(oxid) + ' input[name=\'amount[' + oxid + ']\']').val();
    //
    if(data['amount'][oxid]==0)
        return;
    //
    $.ajax({
        type: "POST",
        url: '/index.php?cl=ajaxbasket&fnc=addBulk&oxid=' + oxid,
        data: data,
        dataType: "json",
        success: handleAddTobasketResponse,
        error: handleAjaxLogout
    });
    // ende
}


/**
 *
 */
function allItemsIntoTheBasket() {
    //
    var data = {}, count = 0;
    data['amount'] = {};
    //
    $('input[name^=\'amount\']').each(function() {
        //
        if($(this).val()<1)
            return;
        //
        var strOXID = $(this).attr('name').substr(7);
        strOXID = strOXID.substr(0, strOXID.length-1);
        //
        count+= $(this).val();
        data['amount'][strOXID] = $(this).val();
        // ende
    });
    //
    if(count) {
        $.ajax({
            type: "POST",
            url: '/index.php?cl=ajaxbasket&fnc=addBulk',
            data: data,
            dataType: "json",
            success: handleAddTobasketResponse,
            error: handleAjaxLogout
        });
    }
    // ende
}


/**
 *
 */
function resetBasketSelection() {
    $('input[name^=\'amount\']').val('0');
}


/**
 * Handle the response from toggleFavoritEntry
 */
function handleFavoriteToggle(response) {
    //
    handleAjaxLogout(response);
    //
    if(response.added) {
        $('#article_' + response.oxid + ' .btn-favorite').addClass('btn-primary');
    } else {
        $('#article_' + response.oxid + ' .btn-favorite').removeClass('btn-primary');
    }
    // ende
}

/**
 * Add or Remove a Item from the favorite list
 * @param oxid
 */
function toggleFavoritEntry(oxid) {
    $.ajax({
        type: "POST",
        url: '/index.php?cl=myfavorites&fnc=toggleFavoritEntry&oxid=' + oxid,
        dataType: "json",
        success: handleFavoriteToggle,
        error: handleAjaxLogout
    });
}


/**
 * Add slahes arround " and '
 * @param str
 * @returns {string}
 */
function addslashes(str) {
    return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0').replace(/"/g, '\'');
}


$(document).ready(function() {
    //
    $('#minibasketIcon').popover({
        trigger : 'hover',
        placement : 'auto',
        content : function () {
            //
            var response = $.ajax({
                async : false,
                type: "POST",
                url: '/index.php?cl=ajaxbasket&fnc=getCurrentBasket',
                dataType: "json"
            }).responseJSON;
            //
            var strRet = '<table>';
            $.each(response.articles, function() {
                strRet+='<tr>';
                strRet+='   <td class="amount">' + this.amount + 'x</td>';
                strRet+='   <td class="title">' + this.title + '</td>';
                strRet+='   <td>' + this.price + '</td>';
                strRet+='</tr>';
            });
            strRet+='</table>';
            strRet+='<div class="minbsum">Gesamtsumme: ' + response.pricesum + '</div>';
            //
            return strRet;
            // ende
        },
        html: true,
        delay: { "hide": 2000 }
    });
    //
    $(document).click(function(e) {
        if (e.target.id == undefined || e.target.id != 'minibasketIcon') {
            $('#minibasketIcon').popover('hide');
        }
    });
    // ende
});

/** Back to top Button: */
$(document).ready(function() {
    var offset = 250;
    var duration = 300;

    $(window).scroll(function() {
        if ($(this).scrollTop() > offset) {
            $('.back-to-top').fadeIn(duration);
        } else {
            $('.back-to-top').fadeOut(duration);
        }
    });

    $('.back-to-top').click(function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, duration);
        return false;
    });
});

/**
 * Basket Content zweite Tabelle positionierung
 */
$(document).ready(function() {
    $('.sumeditCol').width($('.editCol').width());
    $('.sumthumbCol').width($('.thumbCol').width());
    $('.sumtitle').width($('.title').width());
    $('.sumcountCol').width($('.countCol').width());
    $('.sumweightCol').width($('.weightCol').width());
    $('.sumpriceCol').width($('.priceCol').width());
    $('.sumuvp').width($('.uvp').width());
    $('.sumtotalCol').width($('.totalCol').width());
});

/**
 * Newsbox Modal
 */
$('#newsboxmodal').on('shown.bs.modal', function () {
    $('#meinEingabefeld').focus();
});

