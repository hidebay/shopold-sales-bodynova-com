<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop PE
 */

    /** @name database information */
    /*
    $this->dbHost = 'localhost'; // database host name
    #$this->dbName = 'dev_haendler_db'; // database name
    $this->dbName = 'dev_haendler_livedb'; // database name
    $this->dbUser = 'devHaendler'; // database user name
    $this->dbPwd = 'peanuts30'; // database user password
    $this->dbType = 'mysql';
    $this->sShopURL = 'http://bodhi-yoga.local.dev/shop'; // eShop base url, required
    $this->sSSLShopURL  = null;            // eShop SSL url, optional
    $this->sAdminSSLURL = null;            // eShop Admin SSL url, optional
    $this->sShopDir = '/Users/andre/Sites/bodhi-yoga/webroot/shop';
    $this->sCompileDir = '/Users/andre/Sites/bodhi-yoga/webroot/shop/tmp';

	*/
	switch(isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : gethostname()) {
        case 'www.bodhi-yoga.de':   // dev
            //
            /*
            $this->dbHost = '92.51.132.203';
            $this->dbName = 'devdb';
            $this->dbUser = 'devuser';
            $this->dbPwd = 'Arane2Umeb85';
            */
            $this->dbHost = 'localhost';
            $this->dbName = 'livedb';
            $this->dbUser = 'liveuser';
            $this->dbPwd = 'Uguka8Obir75';
            // ende
        break;
        case 'lvps92-51-132-203.dedicated.hosteurope.de':
        case 'sales.bodynova.com':   // live
            //
            $this->dbHost = 'localhost';
            $this->dbName = 'livedb';
            $this->dbUser = 'liveuser';
            $this->dbPwd = 'Uguka8Obir75';
            // ende
        break;
        default:
            // local @andres mac
            if(function_exists('getenv') && getenv('SERVER_ADDR')=='192.168.40.44') {
                $this->dbHost = 'localhost';
                $this->dbName = 'devdb';           // usrdb_yoghxhwh22 // livedb
                $this->dbUser = 'liveuser';         // root
                $this->dbPwd =  'Uguka8Obir75';     // root
                // ende
            } else {
                $this->dbHost = 'localhost';
                $this->dbName = 'haendler';
                $this->dbUser = 'haendler';
                $this->dbPwd  = 'peanuts30';
            }
        break;
    }
    /**
     * @name localeVagrantEinstellungen:
     */
    #$this->dbHost   = 'localhost';
    #$this->dbName   = 'livedb';
    #$this->dbUser   = 'liveuser';
    #$this->dbPwd    = 'Uguka8Obir75';

    $this->sShopURL = 'http'.(getenv('HTTPS')=='on' ? 's' : '').'://'.getenv('HTTP_HOST'); // eShop base url, required
    $this->dbType = 'mysql';
    $this->sSSLShopURL  = null;                      // eShop SSL url, optional
    $this->sAdminSSLURL = null;                      // eShop Admin SSL url, optional
    $this->sShopDir = dirname(__FILE__);//'/Users/andre/Sites/haendlershop'; //dirname(__FILE__);
    $this->sCompileDir = dirname(__FILE__).'/tmp';//'/Users/andre/Sites/haendlershop/tmp'; //dirname(__FILE__).'/tmp';

    /**
     * Debug der config.inc.php
     * @name dre_debug
     */
    $this->debugconf = 1;
    $this->showserver = 1;

    if($_SERVER['HTTP_HOST']!='sales.bodynova.com'){
        // ausschalten, sonst werden keine Daten in den Benutzerzuordnungen angezeigt.
        ini_set('display_errors', 0);
        error_reporting(E_ALL);
        ini_set('display_startup_errors', 1);
        ini_set('log_errors', 1);
        $this->blSkipViewUsage = true;
        if($this->showserver == 1){
            echo 'SERVER_ADDR: '.getenv('SERVER_ADDR').'<br>';
            echo '$_SERVER[\'HTTP_HOST\'] : '.$_SERVER['HTTP_HOST'].'<br>';
            echo '<pre>';
            print_r(array(
                'sShopURL: '    =>$this->sShopURL,
                'sShopDir: '    =>$this->sShopDir,
                'sCompileDir'   =>$this->sCompileDir,
                'dbHost'        =>$this->dbHost,
                'dbName'        =>$this->dbName,
                'dbUser'        =>$this->dbUser,
                'dbPwd'         =>$this->dbPwd,
            ));
            echo '</pre>';
        }
    }
    // UTF-8 mode in shop 0 - off, 1 - on
    $this->iUtfMode = 1;

    // File type whitelist for file upload
    $this->aAllowedUploadTypes = array('jpg', 'gif', 'png', 'pdf', 'mp3', 'avi', 'mpg', 'mpeg', 'doc', 'xls', 'ppt');

    // timezone information
    date_default_timezone_set('Europe/Berlin');

    // Search engine friendly URL processor
    // After changing this value, you should rename oxid.php file as well
    // Always leave .php extension here unless you know what you are doing
    $this->sOXIDPHP = "oxid.php";

    //  enable debug mode for template development or bugfixing
    // -1 = Logger Messages internal use only
    //  0 = off
    //  1 = smarty
    //  2 = SQL The SQL logging feature is deprecated since v5.3.0 (2016-06-07). This feature will be removed.
    //  3 = SQL + smarty The SQL logging feature is deprecated since v5.3.0 (2016-06-07). This feature will be removed.
    //  4 = SQL + smarty + shoptemplate data The SQL logging feature is deprecated since v5.3.0 (2016-06-07). This feature will be removed.
    //  5 = Delivery Cost calculation info
    //  6 = SMTP Debug Messages
    //  7 = oxDbDebug SQL parser The SQL logging feature is deprecated since v5.3.0 (2016-06-07). This feature will be removed.
    //  8 = display smarty template names (requires /tmp cleanup)
    $this->iDebug = 0;

    #$this->debugconf = 1;
    $this->showserver = 0;

	/*echo '<pre>';
	print_r($_SERVER);
	echo '</pre>';
	die();*/

    if($_SERVER['HTTP_HOST']!== 'sales.bodynova.com'){
        // ausschalten, sonst werden keine Daten in den Benutzerzuordnungen angezeigt.
        ini_set('display_errors', 0);
        error_reporting(E_ALL);
        ini_set('display_startup_errors', 1);
        ini_set('log_errors', 1);
        #$this->blSkipViewUsage = true;
        if($this->showserver == 1){
            echo 'SERVER_ADDR: '.getenv('SERVER_ADDR').'<br>';
            echo '$_SERVER[\'HTTP_HOST\'] : '.$_SERVER['HTTP_HOST'].'<br>';
            echo '<pre>';
            print_r(array(
                'sShopURL: '    =>$this->sShopURL,
                'sShopDir: '    =>$this->sShopDir,
                'sCompileDir'   =>$this->sCompileDir,
                'dbHost'        =>$this->dbHost,
                'dbName'        =>$this->dbName,
                'dbUser'        =>$this->dbUser,
                'dbPwd'         =>$this->dbPwd,
            ));
            echo '</pre>';
        }
    }

    // Log all modifications performed in Admin
    $this->blLogChangesInAdmin = false;

    // Force admin email
    $this->sAdminEmail = '';

    // in case session must be started on first user page visit (not only on
    // session required action) set this option value 1
    $this->blForceSessionStart = false;

    // Use browser cookies to store session id (no sid parameter in URL)
    $this->blSessionUseCookies = true;

    // The domain that the cookie is available: array( _SHOP_ID_ => _DOMAIN_ );
    // check setcookie() documentation for more details @php.net
    $this->aCookieDomains = null;

    // The path on the server in which the cookie will be available on: array( _SHOP_ID_ => _PATH_ );
    // check setcookie() documentation for more details @php.net
    $this->aCookiePaths = null;

    // uncomment the following line if you want to leave euro sign unchanged in output
    // by default is set to convert euro sign symbol to html entity
    // $this->blSkipEuroReplace = true;


    // List of all Search-Engine Robots
    $this->aRobots = array(
                        'googlebot',
                        'ultraseek',
                        'crawl',
                        'spider',
                        'fireball',
                        'robot',
                        'slurp',
                        'fast',
                        'altavista',
                        'teoma',
                        'msnbot',
                        'bingbot',
                        'yandex',
                        'gigabot',
                        'scrubby'
                        );

    // Deactivate Static URL's for these Robots
    $this->aRobotsExcept = array();

    // IP addresses for which session/cookie id match and user agent change checks are off
    $this->aTrustedIPs = array();

    /**
     * Works only if basket reservations feature is enabled in admin.
     *
     * The number specifies how many expired basket reservations are
     * cleaned per one request (to the eShop).
     * Cleaning a reservation basically means returning the reserved
     * stock to the articles.
     *
     * Keeping this number too low may cause article stock being returned too
     * slowly, while too high value may have spiking impact on the performance.
     */
    $this->iBasketReservationCleanPerRequest = 200;

    // Trusted shops buyer protection wsdl url
    $this->sTsProtectionUrl = "https://protection.trustedshops.com/ts/protectionservices/ApplicationRequestService?wsdl";
    // This is only needed for testing during integration
    $this->sTsTestProtectionUrl = "https://protection-qa.trustedshops.com/ts/protectionservices/ApplicationRequestService?wsdl";

    // Trusted Shops Ratings login info
    // The public username and password are provided in TrustedShops documentation and helps to identify the functionality provided by OXID eShop
    // Do not change this information unless instructed otherwise
    $this->sTsUser = "oxid_esales";
    $this->sTsPass = "V1AoGEXm";

    // Trusted Shops Ratings configuration array
    $this->aTsConfig = array( "blTestMode"   => false, // set TRUE to enable testing mode
                              "sTsUrl"       => "https://www.trustedshops.com", // Trusted Shops Rating main url
                              "sTsTestUrl"   => "https://qa.trustedshops.com",  // Trusted Shops Rating test url
                              "sTsWidgetUri" => array( "bewertung/widget/widgets/%s.gif" ), // rating widget url
                              "sTsInfoUri"   => array( "de" => "bewertung/info_%s.html",  // DE rating info url
                                                       "en" => "buyerrating/info_%s.html" // EN rating info url
                                                     ),
                              "sTsRatingUri" => array( "de" => "bewertung/bewerten_%s.html", // DE rating url
                                                       "en" => "buyerrating/rate_%s.html"    // EN rating url
                                                     )
                             );
    // Trusted Shops Ratings service wsdl
    $this->sTsServiceWsdl = "https://www.trustedshops.de/ts/services/TsRating?wsdl";

    // Trusted Shops Ratings test service wsdl
    $this->sTsServiceTestWsdl = "https://qa.trustedshops.de/ts/services/TsRating?wsdl";

    /**
     * should template blocks be highlighted in frontend ?
     * this is mainly intended for module writers in non productive environment
     */
    $this->blDebugTemplateBlocks = false;

    /**
     * should requests, coming via stdurl and not redirected to seo url be logged to seologs db table?
     * note: only active if in productive mode, as the eShop in non productive more will always log such urls
     */
    $this->blSeoLogging = false;

    /**
     * To override oxubase::_aUserComponentNames use this array option:
     * array keys are component(class) names and array values defines if component is cacheable (true/false)
     * e.g. array("user_class" => false);
     */
    $this->aUserComponentNames = null;

    /**
     * Default database connection character set. This option is only used when config option iUtfMode is set to 0.
     */
    $this->sDefaultDatabaseConnection = 'latin1';

    /**
     * Additional multi language tables
     */
    $this->aMultiLangTables = null;

    /**
     * Instructs shop that price update is perfomed by cron (time based job sheduler)
     */
    $this->blUseCron = false;


    /**
     * Do not disable module if class from extension path does not exist.
     */
    $this->blDoNotDisableModuleOnError = false;

    /**
     * Captcha encryption key.
     */
    $this->captchaKey = '54d13e6b21c282894c1ad08d734b718e';
