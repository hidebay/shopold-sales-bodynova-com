<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 15.04.15
 * Time: 14:48
 */

array_push( $aIncReports, array('name'  => 'artikelhaendlerpreis',
                                'title' => array('de' => 'Artikel ohne Haendlerpreise'),
                                'desc'  => array('de' => 'Bei den nachfolgenden Artikeln ist ein oder mehrere Händlerpreise nicht gefüllt.')
));

if ($cReportType == 'artikelhaendlerpreis') {
    $sSql1 ='
SELECT
          oxarticles.oxactive AS oxactive,
          oxarticles.oxstock AS oxstock,
          oxarticles.oxprice AS oxprice,
          oxarticles.oxid,
          oxarticles.oxartnum,
          oxarticles.oxean,
          oxarticles.oxid,
          IF(oxarticles.oxparentid=\'\',oxarticles.oxtitle,(SELECT a1.oxtitle FROM oxarticles a1 WHERE a1.oxid=oxarticles.oxparentid)) AS oxtitle,
          IF(oxarticles.oxparentid=\'\',(SELECT m.oxtitle FROM oxmanufacturers m WHERE oxarticles.oxmanufacturerid = m.oxid),(SELECT m.oxtitle FROM oxarticles a1, oxmanufacturers m WHERE oxarticles.oxparentid = a1.oxid AND a1.oxmanufacturerid = m.oxid)) AS oxmantitle,
          oxarticles.oxvarselect
FROM oxarticles
WHERE
	oxarticles.OXACTIVE = 1
AND (
	(oxarticles.OXPRICEA = 0)
OR
	(oxarticles.OXPRICEB = 0)
OR
	(oxarticles.OXPRICEC = 0)
OR
	(oxarticles.OXPRICED = 0)
OR
	(oxarticles.OXPRICEE = 0)
)';
    $sSql2 = '';
    // ende
}

?>
