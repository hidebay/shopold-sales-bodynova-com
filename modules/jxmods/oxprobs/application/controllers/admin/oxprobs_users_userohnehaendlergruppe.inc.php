<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 15.04.15
 * Time: 17:00
 */
array_push( $aIncReports, array('name'  => 'userohnehaendlergruppe',
                                'title' => array('de' => 'User ohne Zuordnung zu einer Händlergruppe'),
                                'desc'  => array('de' => 'Nachfolgende User haben keine Händlerpreisgruppenzuordnung.')
));
if ($cReportType == 'userohnehaendlergruppe') {
    $sSql1 =
    '
    SELECT
        oxid AS oxid,
        oxactive AS oxactive,
        oxusername AS name
    FROM
    oxuser
    WHERE
    (SELECT count(*) FROM oxobject2group WHERE oxobject2group.OXOBJECTID=oxuser.OXID AND oxobject2group.OXGROUPSID IN (\'oxidpricea\',\'oxidpriceb\',\'oxidpricec\',\'oxidpriced\',\'oxidpricee\'))=0
    ';

    $sSql2 = '';
    // ende
}
