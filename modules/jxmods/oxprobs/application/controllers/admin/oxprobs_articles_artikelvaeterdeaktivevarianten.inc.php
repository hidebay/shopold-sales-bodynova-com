<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 16.04.15
 * Time: 11:58
 */
array_push( $aIncReports, array('name'  => 'artikelvaeterdeaktivevarianten',
                                'title' => array('de' => 'Artikelväter, deaktive Varianten'),
                                'desc'  => array('de' => 'Nachfolgende Artikelväter, haben alle Varianten deaktiviert.')
));

if ($cReportType == 'artikelvaeterdeaktivevarianten') {
    $sSql1 =
        '
SELECT
          vater.oxactive AS oxactive,
          oxstock AS oxstock,
          vater.oxprice AS oxprice,
          vater.oxid AS oxid,
          oxartnum,
          vater.OXTITLE AS oxtitle,
          oxean
FROM
oxarticles AS vater
WHERE
vater.OXPARENTID=\'\'
AND
vater.OXACTIVE=1
AND
(SELECT count(*) From
	oxarticles AS kinder WHERE kinder.OXPARENTID=vater.OXID AND kinder.OXACTIVE=1)=0';
}
