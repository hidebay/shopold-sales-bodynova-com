<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 16.04.15
 * Time: 12:53
 */
array_push( $aIncReports, array('name'  => 'artikelvariantendeaktivervater',
                                'title' => array('de' => 'Artikelvarianten aktiv mit deaktiviertem Vater'),
                                'desc'  => array('de' => 'Nachfolgende Artikelvarianten sind aktiv, obwohl deren Vater deaktiviert ist.')
));

if ($cReportType == 'artikelvariantendeaktivervater') {
    $sSql1 =
        '
SELECT
distinct(kind.OXPARENTID),
kind.OXARTNUM AS oxartnum,
kind.oxactive AS oxactive,
kind.OXID AS oxid
FROM
oxarticles AS kind,
oxarticles AS vater
WHERE
kind.OXPARENTID!=\'\'
AND
kind.OXACTIVE=1
AND
vater.oxid=kind.OXPARENTID
AND
vater.OXACTIVE=0
        ';
}
