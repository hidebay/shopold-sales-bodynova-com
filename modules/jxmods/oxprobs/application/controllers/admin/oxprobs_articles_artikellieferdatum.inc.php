<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 16.04.15
 * Time: 14:00
 */
array_push( $aIncReports, array('name'  => 'artikellieferdatum',
                                'title' => array('de' => 'Artikel mit vergangenem Lieferdatum'),
                                'desc'  => array('de' => 'Nachfolgende Artikel haben ein in der Vergangenheit liegendes Lieferdatum, sind aber noch gelb oder rot.')
));

if ($cReportType == 'artikellieferdatum') {
    $sSql1 =
        '
        SELECT
oxactive AS oxactive,
oxstock AS oxstock,
oxprice AS oxprice,
oxid AS oxid,
oxartnum
FROM
oxarticles
WHERE
oxarticles.bnflagbestand!=0
AND
oxarticles.OXDELIVERY<=now()
AND
oxarticles.OXDELIVERY>0
AND
oxarticles.OXACTIVE=1
        ';
}
