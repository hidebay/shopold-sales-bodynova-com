<?php


array_push( $aIncReports, array('name'  => 'artikelhaendlerendkunden',
    'title' => array('de' => 'Alle Artikel die nicht im Händlershop sind aber im Endkundenshop'),
    'desc'  => array('de' => 'Alle Artikel die nicht im Händlershop sind aber im Endkundenshop')
));

if ($cReportType == 'artikelhaendlerendkunden') {
    // bekomme daten von endkundenshop
    require_once 'oxprobs_articles_getarticlesvomendkundenshop.php';

    //$sWhereActive = 'a.oxactivefrom != '0000-00-00 00:00:00' AND a.oxactivefrom < NOW() AND a.oxactiveto != '0000-00-00 00:00:00' AND a.oxactiveto < NOW() ';
    $sWhereActive = 'a.oxactivefrom > NOW() ';

    //
    $sSql1 ='
         SELECT
          oxarticlenumsendkunden.oxid,
          oxarticlenumsendkunden.oxartnum,
          oxarticlenumsendkunden.oxactive,
          oxarticlenumsendkunden.oxtitle AS oxtitle,
          oxarticlenumsendkunden.oxvarselect AS oxvarselect,
          \'b2c\' AS shop
         FROM
          oxarticlenumsendkunden
         LEFT JOIN
          oxarticles ON oxarticles.oxartnum=oxarticlenumsendkunden.oxartnum
         WHERE
            oxarticles.oxid IS NULL
          AND
            oxarticlenumsendkunden.oxactive>0
    ';
    $sSql2 = '';
    // ende
}

?>
