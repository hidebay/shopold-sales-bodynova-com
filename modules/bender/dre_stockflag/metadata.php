<?php
/**
 */

/**
 * Metadata version
 */
$sMetadataVersion = '0.1';

/**
 * Module information
 *	Dieses Modul erweitert den Head im Admin um den Cache zu leeren.
 */
$aModule = array(
    'id'           => 'dre_stockflag',
    'title'        => '<img src="../favicon.ico" title="Body Stock Flag">ody Stock Flag',
    'description'  => array(
            'de' => 'Modul zur erweiterten Darstellung des Versandstatus.',
            'en' => 'Modul extended view of delivery status.',
    ),
    'thumbnail'    => 'picture.png',
    'version'      => '0.1',
    'author'       => 'Andre Bender',
    'url'          => 'www.bodynova.de',
    'email'        => 'a.bender@bodynova.de',
    'blocks'       => array(
            array(
                'template'  => 'page/details/inc/productmain.tpl',
                'block'     => 'details_productmain_stockstatus',
                'file'      => 'views/blocks/page/details/inc/dre_productmain.tpl',
                'position'  => 1,
            )
    )
);
