<?php
/**
 */

/**
 * Metadata version
 */
$sMetadataVersion = '0.1';

/**
 * Module information
 *	Dieses Modul erweitert den Head im Admin um den Cache zu leeren.
 */
$aModule = array(
    'id'           => 'dre_navileiste',
    'title'        => '<img src="../favicon.ico" title="Navileiste">ody Navileiste',
    'description'  => array(
        'de' => 'Modul erweitert das Model  oxcategory.',
        'en' => 'Modul see german.',
    ),
    'thumbnail'    => 'picture.png',
    'version'      => '0.1',
    'author'       => 'Andre Bender',
    'url'          => 'www.bodynova.de',
    'email'        => 'a.bender@bodynova.de',
    'extend'	   => array(
	    'oxcategorylist'	=> 'bender/dre_navi/models/dre_oxcategorylist',
	),
);
