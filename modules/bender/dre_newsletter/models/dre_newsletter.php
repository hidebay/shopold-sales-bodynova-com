<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 04.12.15
 * Time: 17:18
 */
class dre_newsletter extends dre_newsletter_parent{

	public function send()
	{
		// überprüfen ob der Kunde bereits diesen Newsletter erhalten hat.
		$oDb = oxDb::getDb();
		$select = "select * from oxnewssend where OXID = '".$this->_oUser->oxuser__oxid->value."'";
		$vorhanden[] = $oDb->getOne($select);

		// Wenn ja brich ab.
		if($vorhanden[0] == $this->_oUser->oxuser__oxid->value){
			echo 'Newsletter bereits verschickt.';
			return 1;
		}else{

			//Wenn nein Newsletter versenden.
			$oxEMail = oxNew( 'oxemail' );
			$blSend = $oxEMail->sendNewsletterMail( $this, $this->_oUser, $this->oxnewsletter__oxsubject->value );

			// Log in DB.
			$oDb = oxDb::getDb();
			$insert = "insert into oxnewssend (OXID , OXUSERNAME , OXSEND , OXCUSTNR , OXTIMESTAMP) values ('".$this->_oUser->oxuser__oxid->value."', '".$this->_oUser->oxuser__oxusername->value."' , '1' ,'".$this->_oUser->oxuser__oxcustnr->value."' , '".date("Y-m-d H:i:s")."')";
			$oDb->execute($insert);

			// Log in Textdatei.
			$inhalt = $this->_oUser->oxuser__oxid->value.' '.$this->_oUser->oxuser__oxusername->value.' '.$this->_oUser->oxuser__oxcustnr->value."\n";
			$fp = fopen( dirname(__FILE__).'/newsletterversand.txt', 'a+');
			fwrite($fp, $inhalt);
			fclose($fp);
			return $blSend;
		}
	}
}
