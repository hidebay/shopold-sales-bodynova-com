<?php
/**
 */

/**
 * Metadata version
 */
$sMetadataVersion = '0.1';

/**
 * Module information
 *	Dieses Modul erweitert den Head im Admin um den Cache zu leeren.
 */
$aModule = array(
    'id'           => 'dre_newsletter',
    'title'        => '<img src="../modules/bodynova/img/favicon.ico" title="Body Stock Flag">ody Newsletter',
    'description'  => array(
            'de' => 'Modul zur Logerstellung beim Newsletter Versand.',
            'en' => 'Modul to show financial monthly options.',
    ),
    'thumbnail'    => 'picture.png',
    'version'      => '0.1',
    'author'       => 'Andre Bender',
    'url'          => 'www.bodynova.de',
    'email'        => 'a.bender@bodynova.de',
    'extend'	   => array(
        'oxnewsletter'	=> 'bender/dre_newsletter/models/dre_newsletter',
    ),
);
