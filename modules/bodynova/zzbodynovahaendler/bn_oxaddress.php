<?php
class bn_oxaddress extends bn_oxaddress_parent
{
    public function filterAdresses($strSearch)
    {
        //
        $oAdressList = oxNew( "oxlist" );
        $oAdressList->init( "oxbase" );
        //
        $oDb = oxDb::getDb();
        $sSelect  = "select * from oxaddress ";
        $sSelect .= "where OXUSERID='".$this->getUser()->getId()."' ";
        $sSelect .= "AND MATCH (`OXCOMPANY`, `OXFNAME`, `OXLNAME`, `OXCITY`) AGAINST (".$oDb->quote($strSearch.'*')." IN BOOLEAN MODE)";
        //
        $oAdressList->selectString( $sSelect );
        //
        return $oAdressList;
        // ende
    }
}
