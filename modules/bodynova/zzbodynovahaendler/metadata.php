<?php
/**
 * This file is part of OXID eShop Community Edition.
 *
 * OXID eShop Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OXID eShop Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OXID eShop Community Edition.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2014
 * @version   OXID eShop CE
 */

/**
 * Metadata version
 */
$sMetadataVersion = '1.0';

/**
 * Module information
 */
$aModule = array(
    'id'           => 'zzbodynovahaendler',
    'title'        => 'Boynova Haendler Funktionen',
    'description'  => 'Boynova Haendler Funktionen',
    'thumbnail'    => 'picture.png',
    'version'      => '0.1',
    'author'       => 'Murphy & Marly',
    'extend'       => array(
	    'oxarticlelist'     => 'bodynova/zzbodynovahaendler/bn_oxarticlelist',
		'account_password'	=> 'bodynova/zzbodynovahaendler/bn_account_password',
		'oxcmp_user'        => 'bodynova/zzbodynovahaendler/bn_oxcmp_user',
		'order'             => 'bodynova/zzbodynovahaendler/bn_order',
	    'payment'           => 'bodynova/zzbodynovahaendler/bn_payment',
	    'oxshopcontrol'     => 'bodynova/zzbodynovahaendler/bn_oxshopcontrol',
		'oxarticle'         => 'bodynova/zzbodynovahaendler/bn_oxarticle',
		'oxaddress'         => 'bodynova/zzbodynovahaendler/bn_oxaddress',
	    'oxorder'           => 'bodynova/zzbodynovahaendler/bn_oxorder',
	    'article_main'      => 'bodynova/zzbodynovahaendler/bn_article_main',
	    'oxpicturehandler'  => 'bodynova/zzbodynovahaendler/bn_oxpicturehandler',
		'oxbasket'         	=> 'bodynova/zzbodynovahaendler/bn_oxbasket',
        'oxinputvalidator'  => 'bodynova/zzbodynovahaendler/bn_oxinputvalidator',
        'oxactions'        	=> 'bodynova/zzbodynovahaendler/dre_oxactions',
        'oxprobs_articles'  => 'bodynova/zzbodynovahaendler/bn_oxprobsarticles',
		'category_main_ajax'	=> 'bodynova/zzbodynovahaendler/bn_category_main_ajax',
    ),
    'files' => array(
	    'article_main'      =>  'bodynova/zzbodynovahaendler/bn_article_main.php',
	    'getproductselections' => 'bodynova/zzbodynovahaendler/bn_getproductselections.php',
	    'ajaxbasket'        => 'bodynova/zzbodynovahaendler/bn_ajaxbasket.php',
	    'myfavorites'       => 'bodynova/zzbodynovahaendler/bn_myfavorites.php',
		'quickorder'        => 'bodynova/zzbodynovahaendler/bn_quickorder.php',
		'pricelist'       	=> 'bodynova/zzbodynovahaendler/bn_pricelist.php',
    ),
    'templates' => array(
	    'bn_article_main.tpl'  => 'bodynova/zzbodynovahaendler/views/admin/tpl/bn_article_main.tpl',
	    'bn_json.tpl'          => 'bodynova/zzbodynovahaendler/views/tpl/bn_json.tpl',
	    'bn_selections.tpl'    => 'bodynova/zzbodynovahaendler/views/tpl/bn_selections.tpl',
	    'bn_myfavorites.tpl'   => 'bodynova/zzbodynovahaendler/views/tpl/bn_myfavorites.tpl',
	    'bn_quickorder_form.tpl'   => 'bodynova/zzbodynovahaendler/views/tpl/bn_quickorder_form.tpl',
	    'bn_acrticledetails.tpl'   => 'bodynova/zzbodynovahaendler/views/tpl/bn_acrticledetails.tpl',
        'oxprobs_articles.tpl' => 'bodynova/zzbodynovahaendler/views/admin/tpl/oxprobs_articles.tpl',
    ),
);