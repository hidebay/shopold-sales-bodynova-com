<?php

class bn_oxinputvalidator extends bn_oxinputvalidator_parent
{
    /**
     * Checking if all required fields were filled. In case of error
     * exception is thrown
     *
     * @param oxUser $oUser       active user
     * @param array  $aInvAddress billing address
     * @param array  $aDelAddress delivery address
     *
     * @return null
     */
    public function checkRequiredFields( $oUser, $aInvAddress, $aDelAddress )
    {
        //
        $arrCheck = array('oxuser' => $aInvAddress);

        // check delivery address ?
        $blCheckDel = false;
        if ( count( $aDelAddress ) )
        {
            $arrCheck = array('oxaddress' => $aDelAddress);
        }

        //
        foreach($arrCheck AS $strKey => $arrAdressRecord)
        {
            // collecting info about required fields
            $aMustFields = array(
                'oxstreetnr',
                'oxstreet',
                'oxzip',
                'oxcity'
            );
            //
            if(!$aInvAddress[$strKey.'__oxcompany'] )
            {
                $aMustFields[] = 'oxfname';
                $aMustFields[] = 'oxlname';
            }
            else
            {
                $aMustFields[] = 'oxcompany';
            }

            // checking
            foreach ( $aMustFields as $sMustField ) {
                //
                $strFieldKey = $strKey.'__'.$sMustField;

                echo '$sMustField: '.$sMustField.'<br>';
                echo '$strFieldKey: '.$strFieldKey.'<br>';
                echo 'value: '.$arrAdressRecord[$strKey.'__'.$sMustField].'<br>';
                echo 'notisste: '.(!isset( $arrAdressRecord[$strKey.'__'.$sMustField] ) ? 'a' : 'b').'<br>';



                if ( isset( $arrAdressRecord[$strFieldKey] ) && is_array( $arrAdressRecord[$strFieldKey] ) )
                {
                    $this->checkRequiredArrayFields( $oUser, $strFieldKey, $arrAdressRecord[$strFieldKey] );
                }
                elseif (!isset( $arrAdressRecord[$strFieldKey] ) || !trim( $arrAdressRecord[$strFieldKey] ) )
                {
                    $oEx = oxNew( 'oxInputException' );
                    $oEx->setMessage('ERROR_MESSAGE_INPUT_NOTALLFIELDS');

                    $this->_addValidationError($strFieldKey, $oEx );
                }
            }
        }
    }
}
