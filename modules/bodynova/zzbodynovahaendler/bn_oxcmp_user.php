<?php

class bn_oxcmp_user extends bn_oxcmp_user_parent
{
	/**
	 * Add flag "forcechangepassword=1" to url an redirects to password change form to a force a password
	 * change when the user passwor still is "bodynova"
	 *
	 * @param $oUser
	 */
	protected function _afterLogin( $oUser )
	{
		// call parent
		$res = parent::_afterLogin( $oUser );
		//
		if($_POST['lgn_pwd']=='bodynova') {
			//
			oxRegistry::getUtils()->redirect( $this->getConfig()->getShopHomeURL() . 'cl=account_password&forcechangepassword=1' );
			return;
			// ende
		}
		//
		return $res;
		// ende
	}


	/**
	 * to avoid the basked moved to a non user session (per default the haendlershop forces a logged in user
	 * we do not move the user basket back into a session where no user is present
	 */
	protected function _afterLogout()
	{
		//
		parent::_afterLogout();
		// resetting & recalc basket
		if ( ( $oBasket = $this->getSession()->getBasket() ) ) {
			$oBasket->resetUserInfo();
			$oBasket->onUpdate();
			$oBasket->deleteBasket();
		}
		// ende
	}


	/**
	 *
	 */
	public function logout()
	{
		parent::logout();
		oxRegistry::getUtils()->redirect( $this->getConfig()->getShopHomeURL() .'cl=account&sourcecl=start' );
	}
}