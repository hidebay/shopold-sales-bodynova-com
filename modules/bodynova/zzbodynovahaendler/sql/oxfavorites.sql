/*
 Navicat MySQL Data Transfer

 Source Server         : www.bodhi-yoga.de
 Source Server Type    : MySQL
 Source Server Version : 50540
 Source Host           : www.bodhi-yoga.de
 Source Database       : usrdb_yoghxhwh2

 Target Server Type    : MySQL
 Target Server Version : 50540
 File Encoding         : utf-8

 Date: 11/20/2014 12:39:51 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `oxfavorites`
-- ----------------------------
DROP TABLE IF EXISTS `oxfavorites`;
CREATE TABLE `oxfavorites` (
  `OXUSER` char(32) DEFAULT NULL,
  `OXARTICLE` char(32) DEFAULT NULL,
  UNIQUE KEY `OXUSER` (`OXUSER`,`OXARTICLE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;
