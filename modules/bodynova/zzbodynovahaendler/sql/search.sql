
ALTER TABLE oxarticles DROP INDEX oxfulltext_2;
ALTER TABLE `oxarticles` ADD FULLTEXT KEY `oxfulltext_2` (`OXARTNUM`,`OXTITLE_2`,`OXSEARCHKEYS_2`);


ALTER TABLE oxarticles DROP INDEX oxfulltext_1;
ALTER TABLE `oxarticles` ADD FULLTEXT KEY `oxfulltext_1` (`OXARTNUM`,`OXTITLE_1`,`OXSEARCHKEYS_1`);


ALTER TABLE oxarticles DROP INDEX oxfulltext_0;
ALTER TABLE `oxarticles` ADD FULLTEXT KEY `oxfulltext_0` (`OXARTNUM`,`OXTITLE`,`OXSEARCHKEYS`);
