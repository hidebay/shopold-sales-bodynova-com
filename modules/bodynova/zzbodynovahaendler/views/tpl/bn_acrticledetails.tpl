<div id="articledetails" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">[{ oxmultilang ident="Modal_Title" }]</h4>
			</div>
			<div class="modal-body">

                <!-- Title -->
                <h1 id="productTitle"><span>[{$oProduct->oxarticles__oxtitle->value}] [{$oProduct->oxarticles__oxvarselect->value}]</span></h1>

                <!-- Detail Picture with Zoom -->
                <div class="pictureBoxDetails">
                    <a href="[{$oProductLink}]" class="viewAllHover glowShadow corners" title="[{ $product->oxarticles__oxtitle->value}] [{$oProduct->oxarticles__oxvarselect->value}]">
                        [{if $oProduct->oxarticles__oxpic1!=''}]
                            <img src="[{$oProduct->getThumbnailUrl()}]" alt="[{$oProduct->oxarticles__oxtitle->value}] [{$oProduct->oxarticles__oxvarselect->value}]" class="img-thumbnail">
                        [{else}]
                            <img src="https://www.bodynova.de/out/imagehandler.php?artnum=[{$oProduct->oxarticles__oxartnum}]&size=380_340_100" class="img-thumbnail">
                        [{/if}]
                    </a>
                </div>

                <!-- Long description -->
                <span>[{$oProduct->getLongDesc()}]</span>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
