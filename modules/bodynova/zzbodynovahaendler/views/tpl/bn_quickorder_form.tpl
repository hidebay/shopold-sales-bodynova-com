[{capture append="oxidBlock_content"}]
    [{assign var="dropship" value=$oxcmp_user->oxuser__dropship->value}]
    [{if $dropship == 1}]
        [{oxscript include=$oViewConf->getResourceUrl('js/bodynova-quickorder.js') priority=10}]
        <div id="ordercomplete" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">[{oxmultilang ident="BTN_CLOSE"}]</span></button>
                        <h4 class="modal-title">[{oxmultilang ident="MODAL_ORDER_COMPLETE_TITLE"}]</h4>
                    </div>
                    <div class="modal-body">
                        <p>[{oxmultilang ident="MODAL_ORDER_COMPLETE_BODY"}]</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" onclick="location.href='/index.php?cl=quickorder&fnc=showForm';">[{oxmultilang ident="BTN_OK"}]</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div id="previeworder" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">[{oxmultilang ident="BTN_CLOSE"}]</span></button>
                        <h4 class="modal-title">[{oxmultilang ident="MODAL_ORDER_PREVIEW_TITLE"}]</h4>
                    </div>
                    <div class="modal-body">
                        <p>[{oxmultilang ident="MODAL_ORDER_PREVIEW_BODY"}]</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">[{oxmultilang ident="MODAL_ORDER_BTN_CHANGE"}]</button>
                        <button id="btnPlaceOrder" type="button" class="btn btn-primary ladda-button" data-style="expand-left" onclick="placeQuickOrder();"><span class="ladda-label">[{oxmultilang ident="QUICKORDER_BTN_ORDERMANDANTORY"}]</span></button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <div class="form-horizontal">
            <form id="frmQuickOrder" method="POST" action="index.php?cl=quickorder&fnc=addToBasket" role="form" enctype="multipart/form-data">
          <h1>[{oxmultilang ident="SITE_TITLE"}]</h1>
          <div class="form-group">
            <label class="col-sm-2 control-label"></label>
                <div class="col-sm-10">
                <table id="QuickPositions" class="table-responsive">
                    <tr class="tr headline">
                        <td class="artnum td">[{oxmultilang ident="QUICKORDER_ARTNUM"}]</td>
                        <td class="title td">[{oxmultilang ident="QUICKORDER_ARTICLE"}]</td>
                        <td class="td price">[{oxmultilang ident="QUICKORDER_SINGLE_PRICE"}]</td>
                        <td class="td amount">[{oxmultilang ident="QUICKORDER_COUNT"}]</td>
                        <td class="td stock">[{oxmultilang ident="QUICKORDER_AVAILABILITY"}]</td>
                    </tr>
                    <tr id="newEntry" class="tr">
                        <td class="td" colspan="2">
                            <input type="text" id="OXARTNUM" name="OXARTNUM" placeholder="[{oxmultilang ident="QUICKORDER_PLACEHOLDER_SEARCHFIELD"}]">
                        </td>
                        <td class="td price"></td>
                        <td class="td amount"></td>
                    </tr>
                    <tr><td id="newEntryAutoComplete" colspan="10"></td></tr>
                </table>
                </div>
            </div>
            <br><br><br><br>

            <h3>[{oxmultilang ident="SHIPPING_ADDRESS" }]</h3>

            <div class="form-group">
                <label class="col-sm-2 control-label">[{oxmultilang ident="QUICKORDER_WHITE_LABEL" suffix="COLON"}]</label>
                <div class="col-sm-1">
                    <input class="form-control" name="iswhitelabel" type="checkbox">
                </div>
            </div>
            [{* Adressbuch durchsuchen *}]
            <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-10">
                    <input class="form-control" id="searchadressbook" placeholder="[{oxmultilang ident="QUICKORDER_SEARCH_CONTACTS"}]">
                    <input type="hidden" id="addressbookid" name="addressbookid">
                    <div id="adressBookAutocomplete"></div>
                </div>
            </div>
            [{* Anrede raus ? *}]
            <div class="form-group">
                <label class="col-sm-2 control-label">[{oxmultilang ident="TITLE" suffix="COLON"}]</label>
                <div class="col-sm-10">
                    <select class="form-control" id="iswhitelabel" id="oxsal" name="invadr[oxuser__oxsal]">
                        <option value="MR"  [{if $value|lower  == "mr"  or $value2|lower == "mr" }]SELECTED[{/if}]>[{oxmultilang ident="MR"  }]</option>
                        <option value="MRS" [{if $value|lower  == "mrs" or $value2|lower == "mrs"}]SELECTED[{/if}]>[{oxmultilang ident="MRS" }]</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="firma">[{oxmultilang ident="FIRST_NAME" suffix="COLON" }]</label>
                <div class="col-sm-10">
                    <input class="form-control" id="oxfname" name="invadr[oxuser__oxfname]" placeholder="[{oxmultilang ident="FIRST_NAME" }]">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">[{oxmultilang ident="LAST_NAME" suffix="COLON" }]</label>
                <div class="col-sm-10">
                    <input class="form-control" id="oxlname" name="invadr[oxuser__oxlname]" placeholder="[{oxmultilang ident="LAST_NAME" }]">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">[{oxmultilang ident="COMPANY" suffix="COLON" }]</label>
                <div class="col-sm-10">
                    <input class="form-control" id="oxcompany" name="invadr[oxuser__oxcompany]" placeholder="[{oxmultilang ident="COMPANY" }]">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">[{oxmultilang ident="ADDITIONAL_INFO" suffix="COLON" }]</label>
                <div class="col-sm-10">
                    <input class="form-control" id="oxaddinfo" name="invadr[oxuser__oxaddinfo]" placeholder="[{oxmultilang ident="ADDITIONAL_INFO" }]">
                </div>
            </div>
            <div class="form-group street">
                <label class="col-sm-2 control-label">[{oxmultilang ident="STREET_AND_STREETNO" suffix="COLON" }]</label>
                <div class="col-sm-10">
                    <input class="form-control big" id="oxstreet" name="invadr[oxuser__oxstreet]" placeholder="[{oxmultilang ident="STREET" }]">
                    <input class="form-control small" id="oxstreetnr" name="invadr[oxuser__oxstreetnr]" placeholder="[{oxmultilang ident="STREETNO"  }]">
                </div>
            </div>
            <div class="form-group plzcity">
                <label class="col-sm-2 control-label">[{oxmultilang ident="POSTAL_CODE_AND_CITY" suffix="COLON" }]</label>
                <div class="col-sm-10">
                    <input class="form-control small" id="oxzip" name="invadr[oxuser__oxzip]" placeholder="[{oxmultilang ident="POSTAL_CODE"}]">
                    <input class="form-control big"id="oxcity"  name="invadr[oxuser__oxcity]" placeholder="[{oxmultilang ident="CITY"}]">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">[{oxmultilang ident="COUNTRY" suffix="COLON" }]</label>
                <div class="col-sm-10">
                    <select id="oxcountryid" name="invadr[oxuser__oxcountryid]" id="invCountrySelect" class="form-control">
                        <option value="">-</option>
                        [{assign var="blCountrySelected" value=false}]
                        [{foreach from=$oViewConf->getCountryList() item=country key=country_id }]
                            [{assign var="sCountrySelect" value=""}]
                            [{if !$blCountrySelected}]
                                [{if (isset($invadr.oxuser__oxcountryid) && $invadr.oxuser__oxcountryid == $country->oxcountry__oxid->value) || (!isset($invadr.oxuser__oxcountryid) && $oxcmp_user->oxuser__oxcountryid->value == $country->oxcountry__oxid->value) }]
                                    [{assign var="blCountrySelected" value=true}]
                                    [{assign var="sCountrySelect" value="selected"}]
                                [{/if}]
                            [{/if}]
                            <option value="[{$country->oxcountry__oxid->value }]" [{$sCountrySelect}]>[{$country->oxcountry__oxtitle->value }]</option>
                        [{/foreach }]
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">[{oxmultilang ident="PHONE" suffix="COLON" }]</label>
                <div class="col-sm-10">
                    <input class="form-control" id="oxfon" name="invadr[oxuser__oxfon]" placeholder="[{ oxmultilang ident="PHONE"}]">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">[{oxmultilang ident="DATEI" suffix="COLON" }]</label>
                <div class="col-sm-10">
                    <p>[{oxmultilang ident="QUICKORDER_PDF_TXT"}]<br><br><br>
                        <input name="lieferschein" type="file" accept=".pdf"/>
                    </p>
                </div>
            </div>
            <button type="button" class="btn btn-default btn-primary pull-right btn-success" onclick="showConfirmQuickOrder()"><span class="ladda-label">[{oxmultilang ident="SUBMIT_QUICK_ORDER"}]</span></button>
        </div>
        </form>
        <br><br><br><br>
    [{/if}]
[{/capture}]

[{include file="layout/page.tpl" sidebar="Left"}]
