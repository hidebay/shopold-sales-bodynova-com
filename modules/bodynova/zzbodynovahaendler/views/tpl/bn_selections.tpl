<form>
	<table class="tableselections table" border="0">
		[{foreach from=$arrSelections item=_aProduct}]
			<tr id="article_[{$_aProduct.oxid}]" class="tabellenspalte">
				<td class="hidden-xs imagecolumn"></td>
				<td class="ebenetitel">
                    <span class="headline"></span>
					<div class="pull-left input-line">
						<div class="amount_input spinner input-group bootstrap-touchspin">
							<span class="input-group-btn"><button type="button" class="btn btn-default bootstrap-touchspin-down">-</button></span>
							<span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
							<input class="form-control amount" type="numeric" name="amount[[{$_aProduct.oxid}]]" value="0" style="display: block;">
							<span class="input-group-btn"><button type="button" class="btn btn-default bootstrap-touchspin-up">+</button></span>
						</div>
					</div>
					<div class="pull-left selection-text">
                        <strong>[{*oxmultilang ident="ARTNUM" suffix="COLON"*}][{$_aProduct.artnum}]</strong>
                        &nbsp;[{$_aProduct.title}]
                        [{*
                            <div class="selection-artnum-text">
                                <strong>[{oxmultilang ident="ARTNUM" suffix="COLON"}][{$_aProduct.artnum}]</strong>
                            </div>
                            <div class="selection-title-text">
                                [{$_aProduct.title}]
                            </div>

                            &nbsp;
                        *}]
						[{*$oView|var_dump*}]

						[{if $_aProduct.lists}]
							<table>
								[{foreach from=$_aProduct.lists item=_list key=_fkey}]
								<tr>
									<td>[{$_list.name}]</td>
									<td>
										<select name="sel[[{$_aProduct.oxid}]][[{$_fkey}]]">
											[{foreach from=$_list item=_item key=_skey}]
											[{if $_item->name}]
											<option value="[{$_skey}]">[{$_item->name}]</option>
											[{/if}]
											[{/foreach}]
										</select>
									</td>
								</tr>
								[{/foreach}]
							</table>
						[{/if}]
					</div>
				</td>
				[{*}]
					<td class="uvptitle">UVP</td>
					<td class="uvpprice">
						[{oxprice price=$_aProduct.uvpprice currency=$oView->getActCurrency()}]
					</td>
				[{*}]
                <td class="Warenkorb"></td>
				<td class="selprice">
                    <label id="productPrice_[{$iIndex}]" class="price">
					    [{oxprice price=$_aProduct.price currency=$oView->getActCurrency()}]
                    </label>
				</td>
                <td class="ampelcolumn">
					[{oxid_include_dynamic file="widget/product/stockinfo.tpl"}]
				</td>
			</tr>
		[{/foreach}]
		<tr class="tabellenspalte">
			<td class="hidden-xs imagecolumn"></td>
            <td class="ebenetitel"></td>
            <td class="Warenkorb">
                <div class="input-line">
                    <button onclick="addBulkCart('[{$oxid}]')" type="button" class="btn btn-default btn-prima btn-basket ladda-button btn-success pull-right"><span class="glyphicon glyphicon-shopping-cart"></span></button>
                </div>
            </td>
            <td class="selprice"></td>
			<td class="ampelcolumn"></td>
		</tr>
	</table>
</form>
