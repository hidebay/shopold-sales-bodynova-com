<?php
class myfavorites extends oxUBase
{

	// teplate for the view
	protected $_sThisTemplate = 'bn_myfavorites.tpl';


	/**
	 *
	 */
	public function showLists()
	{
		//
		$oFavoritesList = oxNew( "oxlist" );
		$oFavoritesList->init( "oxbase" );
		$strQuery = 'SELECT OXARTICLE FROM oxfavorites WHERE OXUSER=\''.$this->getUser()->getId().'\'';
		$oFavoritesList->selectString( $strQuery );

		// speichere die favorisierten artikel in ein array
		$arrFavoritesArticles = array();
		if ( $oFavoritesList->count() ) {
			foreach ( $oFavoritesList as $oItem ) {
				$arrFavoritesArticles[] = $oItem->__oxarticle->rawValue;
			}
		}

		$oArtList = oxNew( 'oxarticlelist' );
		$oArtList->loadByArticlesIDs($arrFavoritesArticles);

		//
		$this->_aViewData['type'] = 'line';
		$this->_aViewData['products'] = $oArtList;
		// ende
	}


	/**
	 *
	 */
	public function toggleFavoritEntry()
	{
		//
		$oDb = oxDb::getDb( oxDb::FETCH_MODE_ASSOC );
		//
		$sSelect =  'SELECT * FROM oxfavorites WHERE OXARTICLE= \''. oxDb::getInstance()->escapeString( $_GET['oxid'] ). '\' AND '.
				' OXUSER= \''. oxDb::getInstance()->escapeString( $this->getUser()->getId() ). '\'';
		//und
		$aData = $oDb->getRow( $sSelect );
		//
		$res = false;
		//
		if(count($aData)) {
			//
			$sStatement = 'DELETE FROM oxfavorites WHERE '
				. ' OXARTICLE= \''. oxDb::getInstance()->escapeString( $_GET['oxid'] ). '\' AND '
				. ' OXUSER= \''. oxDb::getInstance()->escapeString( $this->getUser()->getId() ). '\' ';
			//
			$oDb->Execute($sStatement);
			// ende
		} else {
			//
			$sStatement = 'INSERT into oxfavorites SET '
			. ' OXARTICLE= \''. oxDb::getInstance()->escapeString( $_GET['oxid'] ). '\', '
			. ' OXUSER= \''. oxDb::getInstance()->escapeString( $this->getUser()->getId() ). '\' ';
			//
			$oDb->Execute($sStatement);
			//
			$res = true;
			// ende
		}
		//
		die(json_encode(array('added'=>$res, 'oxid'=>$_GET['oxid'])));
		// ende
	}


	/**
	 *
	 */
	function showOrderedArticles()
	{
		//
		$oDb = oxDb::getDb( oxDb::FETCH_MODE_ASSOC );
		//
		$sQ = 'select oxid from oxorder where oxuserid = '.$oDb->quote( $this->getUser()->getId() ).' and oxid = ' . $oDb->quote( $_GET['orderid'] );
		//
		$res = oxDb::getDb()->getOne( $sQ );
		//
		if($res!=$_GET['orderid']) {
			oxRegistry::getUtils()->redirect( $this->getConfig()->getShopHomeURL() . 'cl=account_order', false, 301 );
			exit;
		}

		//
		$oOrder = oxNew( "oxorder" );
		$oOrder->load($_GET['orderid']);
		$oOrderArticleList = $oOrder->getOrderArticles();

		//
		$arrArticleIDs = $arrArticleIDAmounts = array();
		foreach($oOrderArticleList AS $oOrderArticle) {
			$arrArticleIDs[] = $oOrderArticle->oxorderarticles__oxartid->getRawValue();
			$arrArticleIDAmounts[$oOrderArticle->oxorderarticles__oxartid->getRawValue()] = $oOrderArticle->oxorderarticles__oxamount->rawValue;
		}

		//
		$oArtList = oxNew( 'oxarticlelist' );
		$oArtList->loadByArticlesIDs($arrArticleIDs);

		//
		$this->_aViewData['type'] = 'line';

		// merge amounts into articles set
		foreach($oArtList AS $key=>$oArt) {
			$oArt->orderedamount = $arrArticleIDAmounts[$oArt->getId()];
			$oArtList[$key] = $oArt;
		}

		//
        $this->_aViewData['isorder'] = true;
		$this->_aViewData['products'] = $oArtList;
		// ende
	}
}