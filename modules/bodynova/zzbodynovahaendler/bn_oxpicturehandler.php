<?php

class bn_oxpicturehandler extends bn_oxpicturehandler_parent {
	public function getPicUrl( $sPath, $sFile, $sSize, $sIndex = null, $sAltPath = false, $bSsl = null )
	{
		$sDirName = "380_340_" . $this->getConfig()->getConfigParam( 'sDefaultImageQuality' );
		return 'https://www.bodynova.de/out/pictures/generated/product/1/'.$sDirName.'/'.$sFile;
	}
}
