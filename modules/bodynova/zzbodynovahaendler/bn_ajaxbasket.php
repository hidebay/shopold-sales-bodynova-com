<?php
/**
 * Created by PhpStorm.
 * User: manuelgeil
 * Date: 06.10.14
 * Time: 12:43
 */

class ajaxbasket extends oxUBase {

	// teplate for the view
	protected $_sThisTemplate = 'bn_json.tpl';

	/**
	 * Adds a whole bulk (array) of items to the basket:
	 *
	 *  key => oxid
	 *  val => amount
	 */
	public function addBulk() {
		//
		$arrResponse = array(
			'oxid'              =>  $_GET['oxid'],
			'added'             =>  array(),
			'itemcount_before'  =>  $this->getSession()->getBasket()->getItemsCount(true),
		);

		// add items if present
		$intCount = 0;
		if(isset($_POST['amount'])) {
			$oBasket = oxnew('oxcmp_basket');
			// walk through each item and add it
			foreach ( $_POST['amount'] AS $oxid => $intAmount) {
				if($intAmount) {
					$intCount+= $intAmount;
					$oBasket->tobasket( $oxid, $intAmount, $_POST['sel'][$oxid] );
					$arrResponse['added'] = array($oxid, $intAmount, $_POST['sel'][$oxid]);
				}
			}
		}
		//
		$arrResponse['itemscount'] = $arrResponse['itemcount_before'] + $intCount;
		//
		$this->_aViewData['response'] = $arrResponse;
		// ende
	}


	/**
	 *
	 */
	public function getCategoryURL()
	{
		//
		$ret = array();
		//
		$oProduct = oxNew( 'oxarticle' );
		$oProduct->load( $_GET['oxid'] );
		//
		$ret['url'] = $oProduct->getCategory()->getLink();
		//
		die(json_encode($ret));
		// ende
	}


	/**
	 *
	 */
	public function getArticleDetails()
	{
		//
		$this->_sThisTemplate = 'bn_acrticledetails.tpl';
		//
		$oProduct = oxNew( 'oxarticle' );
		$oProduct->load( $_GET['oxid'] );
		//
		$this->_aViewData['oProduct'] = $oProduct;
		// ende
	}


	/**
	 *
	 */
	function getCurrentBasket()
	{
		//
		$oLang = oxRegistry::getLang();
		$oCurrency = $this->getConfig()->getActShopCurrencyObject();
		$sCurrencySign = $oCurrency->sign;
		$sCurrencySide = $oCurrency->side;
		//
		$arrReturn = array(
			'articlecount'	=>	0,
			'pricesum'		=>	0,
		);
		//
		foreach($this->getSession()->getBasket()->getContents() AS $objBasketItem) {
			//
			$strPrice = $oLang->formatCurrency(oxPrice::getPriceInActCurrency($objBasketItem->getUnitPrice()->getNettoPrice()), $oCurrency);
			//
			$arrReturn['articles'][] = array(
				'amount'    =>  $objBasketItem->getAmount(),
				'title'     =>  $objBasketItem->getTitle(),
				'price'     =>  $sCurrencySide == 'Front' ? $sCurrencySign . ' ' . $strPrice : $strPrice . ' ' . $sCurrencySign,
			);
			//
			$arrReturn['articlecount']+= $objBasketItem->getAmount();
			$arrReturn['pricesum']+= ($objBasketItem->getUnitPrice()->getNettoPrice() * $objBasketItem->getAmount());
			// ende
		}
		//
		$arrReturn['pricesum'] = $oLang->formatCurrency(oxPrice::getPriceInActCurrency($arrReturn['pricesum']), $oCurrency);
		$arrReturn['pricesum'] = $sCurrencySide == 'Front' ? $sCurrencySign . ' ' . $arrReturn['pricesum'] : $arrReturn['pricesum']. ' ' . $sCurrencySign;
		//
		die(json_encode($arrReturn));
		// ende
	}
}