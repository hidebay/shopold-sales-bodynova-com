<?php
/**
 *
 */
class bn_oxshopcontrol extends bn_oxshopcontrol_parent
{
	/**
	 * @param $sClass
	 * @param $sFunction
	 * @param null $aParams
	 * @param null $aViewsChain
	 */

	protected function _process( $sClass, $sFunction, $aParams = null, $aViewsChain = null )
	{
		if($this->isAdmin())
		{
			return parent::_process( $sClass, $sFunction );
		}

		$myConfig = $this->getConfig();
		$oUser = $this->getUser();

		if(!$oUser && $sClass != 'account' && $sClass != 'forgotpwd' && $sClass != 'login' && $sClass != 'contact' && $sClass!='content')
		{
            //
            $arrErrors = oxRegistry::getSession()->getVariable( 'Errors' );
            if(!count($arrErrors['default']))
            {
    			oxUtilsView::getInstance()->addErrorToDisplay( oxLang::getInstance()->translateString('ACCOUNT_LOGIN_REQUIRE') );
            }
            //
            oxUtils::getInstance()->redirect( $myConfig->getShopHomeURL() .'cl=account&sourcecl=start' );
			return;
            // ende
		}

		if(!($sClass != 'account' && $sClass != 'forgotpwd' && $sClass != 'register' && $sClass != 'login'))
		{
			return parent::_process( $sClass, $sFunction );
		}

		return parent::_process( $sClass, $sFunction, $aParams, $aViewsChain);
	}

	/**
	 * @param $oViewObject
	 */
	protected function _render($oViewObject)
	{
		if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
            $res = parent::_render($oViewObject);
            //
			$strFirstChar = substr($res, 0, 1);
			//
			if(isset($_GET['redirected']) ||($strFirstChar!='[' && $strFirstChar!='{')) {
				die(json_encode(array('success' => !$_GET['redirected'], 'content' => $res)));
			}
		}
		//
		return parent::_render($oViewObject);
	}
}
