<?php
/**
 * Created by PhpStorm.
 * User: manuelgeil
 * Date: 06.10.14
 * Time: 12:43
 */

class getproductselections extends oxUBase {


	// teplate for the view
	protected $_sThisTemplate = 'bn_selections.tpl';

	/**
	 *
	 */
	public function getList()
	{
		//
		$this->_aViewData['oxid'] = $_GET['oxid'];

		//
		$myConfig = $this->getConfig();
		$myConfig->setConfigParam('blLoadVariants', true);

		//
		$this->_oProduct = oxNew('oxarticle');
		$this->_oProduct->load($_GET['oxid']);

		//
		$arrSelections = array();
		$arrVariants = $this->_oProduct->getvariants();

		//
		foreach($arrVariants AS $oxid => $objVariant) {
			//
			#echo '<pre>';
			#print_r($objVariant);
			#die;
			$arrSelections[$oxid] = array(
				'oxid'      	=> $oxid,
				'title'     	=> $objVariant->oxarticles__oxvarselect->value,
				'artnum'		=> $objVariant->oxarticles__oxartnum->value,
				'price'     	=> $objVariant->getPrice()->getNettoPrice(),
				'uvpprice'  	=> $objVariant->getUVPPrice(),
				'deliveryDate'	=> $objVariant->getDeliveryDate(),
				'mindeltime'	=> $objVariant->oxarticles__oxmindeltime->value,
				'maxdeltime'	=> $objVariant->oxarticles__oxmaxdeltime->value,
				'delunit'		=> $objVariant->oxarticles__oxdeltimeunit->value,
                'bnstockflag'   => $objVariant->oxarticles__bnflagbestand->value,
                'bncssflag'     => $objVariant->showCssFlag(),
			);

            #echo '<pre>';
            #print_r($arrSelections);
            #echo '</pre>';
            #die;

			//
			$objProduct = oxNew('oxarticle');
			$objProduct->load($oxid);
			$arrSelectLists = $objProduct->getSelectLists();

			//
			if(count($arrSelectLists)) {
				$arrSelections[$oxid]['lists'] = $arrSelectLists;
			}
			// ende
		}
		//
		$this->_aViewData['arrSelections'] = $arrSelections;
		// ende
	}
}
