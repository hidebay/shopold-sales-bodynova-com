<?php
class quickorder extends oxUBase
{

	// teplate for the view
	protected $_sThisTemplate = 'bn_quickorder_form.tpl';


	public function __construct()
	{
		// load user specific prices
		$sPriceSufix = '';
		$oUser = $this->getUser();
		//
		if ( $oUser->inGroup( 'oxidpricea' ) ) {
			$sPriceSufix = 'a';
		} elseif ( $oUser->inGroup( 'oxidpriceb' ) ) {
			$sPriceSufix = 'b';
		} elseif ( $oUser->inGroup( 'oxidpricec' ) ) {
			$sPriceSufix = 'c';
		} elseif ( $oUser->inGroup( 'oxidpriced' ) ) {
			$sPriceSufix = 'd';
		} elseif ( $oUser->inGroup( 'oxidpricee' ) ) {
			$sPriceSufix = 'e';
		}

		// get language id
		$iLanguage = oxRegistry::getLang()->getBaseLanguage();

		// set values
		$this->strPriceKey = '__oxprice'.$sPriceSufix;
		$this->strVariantTitleKey = '__oxvarselect'.($iLanguage ? '_'.$iLanguage : '');
		$this->strArticleTitleKey = '__oxtitle'.($iLanguage ? '_'.$iLanguage : '');
		//print_r($this->strArticleTitleKey);
		// ende
	}


	/**
	 *
	 */
	public function showForm()
	{
		//
		/**
		 * nothing in here ... just show the form
		 */
		// ende
	}


	/**
	 *
	 */
	function getArticles()
	{
		// searching ..
		$oSearchHandler = oxNew( 'oxsearch' );
		$strSearchString = $oSearchHandler->getSelectStatement($_GET['artnum'], 'IF(a.oxparentid!=\'\', a.oxparentid, a.oxid) AS oxid').' LIMIT 0, 30	';

		// filter results for uniqe oxids
		$arrOXIDs = array();
		foreach(oxDb::getDb( oxDb::FETCH_MODE_ASSOC )->getAll( $strSearchString) AS $arrRecord)
		{
			$arrOXIDs[] = $arrRecord['oxid'];
		}
		//
		$arrOXIDs = array_unique($arrOXIDs);

		// query databse um die oxids zu bekommen
		$strSearchString = 'SELECT '.$oSearchHandler->getAutosuggestSelectColumns().', (SELECT count(oxarticlessub.oxid) AS count FROM oxarticles AS oxarticlessub WHERE oxarticlessub.OXPARENTID=a.OXID AND oxarticlessub.OXACTIVE=1) AS isparent FROM oxarticles AS a WHERE oxid IN (\''.implode('\',\'', $arrOXIDs).'\')';

		//
		$oArticlesList = oxNew( "oxlist" );
		$oArticlesList->init( "oxbase" );
		$oArticlesList->selectString( $strSearchString );

		// speichere die artikel in ein temporäres array
		$arrTmpArticles = array();
		if ( $oArticlesList->count() )
        {
			//
			$oLang = oxRegistry::getLang();
			$oCurrency = $this->getConfig()->getActShopCurrencyObject();
			$sCurrencySign = $oCurrency->sign;
			$sCurrencySide = $oCurrency->side;

			//
			$arrArticles = array();
			//
			foreach ( $oArticlesList as $oItem )
			{

				//
				if($oItem->__isparent->rawValue)
				{
                        //
                        $oVariantList = oxNew( "oxlist" );
                        $oVariantList->init( "oxbase" );
                        //
                        $strQuery = '
                            SELECT
                                OXID,
                                oxartnum,
                                oxvarselect,
                                oxvarselect_1,
                                oxvarselect_2,
                                bnflagbestand,
                                oxvat,
                                oxprice,
                                oxpricea,
                                oxpriceb,
                                oxpricec,
                                oxpriced,
                                oxpricee
                            FROM
                                oxarticles AS base
                            WHERE
                                    OXPARENTID=\''.$oItem->__oxid->rawValue.'\'
                                AND
                                    OXACTIVE=1';
                        //LIMIT 10';

                        // query databse um die oxids zu bekommen
                        $oVariantList->selectString( $strQuery );
                        foreach ( $oVariantList as $oVariantItem ) {

                            $strPrice = $oLang->formatCurrency(oxPrice::getPriceInActCurrency(($oVariantItem->{$this->strPriceKey}->rawValue ? $oVariantItem->{$this->strPriceKey}->rawValue : round((($oVariantItem->__oxprice->rawValue  / (100 + $oVariantItem->__oxvat->rawValue ))*100), 2))), $oCurrency);

                            $arrArticles[] = array(
                                'oxid'      =>  $oVariantItem->__oxid->rawValue,
                                'artnum'    =>  $oVariantItem->__oxartnum ->rawValue,

                                'title'     =>  $oItem->__oxtitle->rawValue .
	                                ' | '.
	                                $oVariantItem->{$this->strVariantTitleKey}->rawValue,/*(
                                                    $oItem->{$this->strArticleTitleKey}->rawValue
                                                    ?
                                                    $oItem->{$this->strArticleTitleKey}->rawValue
                                                    :
                                                    $oVariantItem->{$this->strArticleTitleKey}->rawValue
                                                ).
                                                ' | '.
                                                $oVariantItem->{$this->strVariantTitleKey}->rawValue,*/

                                'price'     =>  $sCurrencySide == 'Front' ? $sCurrencySign . ' ' . $strPrice : $strPrice . ' ' . $sCurrencySign,
                                'bnflagbestand' =>  $oVariantItem->__bnflagbestand->rawValue,
                            );
                        }
				}
				else
				{
					//
					$strPrice = $oLang->formatCurrency(oxPrice::getPriceInActCurrency(($oItem->{$this->strPriceKey}->rawValue ? $oItem->{$this->strPriceKey}->rawValue : round((($oItem->__oxprice->rawValue  / (100 + $oItem->__oxvat->rawValue ))*100), 2))), $oCurrency);
					//
					$arrArticles[] = array(
						'oxid'          =>  $oItem->__oxid->rawValue,
						'artnum'        =>  $oItem->__oxartnum->rawValue,
						'title'         =>  ($oItem->__parenttitle->rawValue ? $oItem->__parenttitle->rawValue : '').($oItem->__oxparentid->rawValue ? $oItem->{$this->strArticleTitleKey}->rawValue. ' | '.$oItem->{$this->strVariantTitleKey}->rawValue : $oItem->{$this->strArticleTitleKey}->rawValue),
						'price'         =>  $sCurrencySide == 'Front' ? $sCurrencySign . ' ' . $strPrice : $strPrice . ' ' . $sCurrencySign,
						'bnflagbestand' =>  $oItem->__bnflagbestand->rawValue,
					);
					// ende
				}
			}
	        #print_r($this);
            //
            die(json_encode(array('articles' => $arrArticles)));
			// ende
		}
        else
        {
            die(json_encode(array(
                'suggestions' => oxNew( 'oxsearch' )->getFaultCorrectedSearch($_GET['artnum']),
                'suggesttext' => oxRegistry::getLang()->translateString('AUTOSUGGEST_DIDYOUMEAN', oxRegistry::getLang()->getBaseLanguage()),
            )));
        }
		// ende
	}


	/**
	 *
	 */
	public function submitBasket()
	{
		//
		$oBasket = oxnew('oxbasket');
		$oUser = $this->getUser();

		// walk through each item and add it
		if(isset($_POST['amount']) && is_array($_POST['amount'])) {
			foreach ( $_POST['amount'] AS $sProductID => $dAmount) {
				$res = $oBasket->addToBasket( $sProductID, $dAmount);
			}
		} else {
			die(json_encode(array('state'=>0)));
		}
		$oBasket->calculateBasket();

		//
		$aMustFields = array( 'oxfname', 'oxlname', 'oxstreetnr', 'oxstreet', 'oxzip', 'oxcity' );
		$boolError = false;
		$arrErrorFields = array();
		foreach($aMustFields AS $strField) {
			if(!$_POST['invadr']['oxuser__'.$strField]) {
				$boolError = true;
				$arrErrorFields[] = $strField;
			}
		}
		//
		if($boolError)
			die(json_encode(array('state'=>-1, 'fields'=>$arrErrorFields)));

		//
		$strDelAdrId = $_POST['addressbookid'];
		$oAddress = oxNew( 'oxaddress' );
		//
		if(!$strDelAdrId) {
			$oAddress->oxaddress__oxuserid = new oxField( $oUser->getID() );
			foreach($_POST['invadr'] AS $key=>$val) {
				$key = str_replace('oxuser', 'oxaddress', $key);
				$oAddress->$key = new oxField( $val );
			}
			$strDelAdrId = $oAddress->save();
		}

		//
		oxSession::setVar( 'deladrid' , $strDelAdrId);
		$oAddress->load($strDelAdrId);
		$_GET['sDeliveryAddressMD5'] = $oUser->getEncodedDeliveryAddress().$oAddress->getEncodedDeliveryAddress();
		$_GET['deladrid'] = $strDelAdrId;

		//
		$oOrder = oxNew('oxorder');
		oxSession::setVar( 'sess_challenge' , '');
		$iSuccess = $oOrder->finalizeOrder($oBasket, $oUser);
		//
		if(isset($_FILES['lieferschein']['tmp_name'])){


			move_uploaded_file(
				$_FILES['lieferschein']['tmp_name'],
				//dirname(__FILE__).'/lieferscheine/'.$oOrder->getId().substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'))
				dirname(__FILE__).'/lieferscheine/'.$oOrder->oxorder__oxordernr->value.substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'))
			);

			// FTP Upload auf fritz.nas
			$ftp_server= '92.50.75.170';
			$ftp_user= 'haendlershop';
			$ftp_pass= 'peanuts30';
			$file = dirname(__FILE__).'/lieferscheine/'.$oOrder->oxorder__oxordernr->value.substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'));
			$destination_file = 'JetFlash-Transcend16GB-01/FRITZ/salespdf/'.$oOrder->oxorder__oxordernr->value.substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'));
			$cid=ftp_connect($ftp_server);
			ftp_login($cid, $ftp_user, $ftp_pass);
			ftp_pasv($cid, true);
			ftp_put($cid, $destination_file, $file, FTP_BINARY);
			ftp_close($cid);
		}

		die(json_encode(array(
			'state'     =>  $iSuccess,
			'oxorderid' =>  $oOrder->getId(),
		)));
		// ende
	}


	/**
	 *
	 */
	public function getAdressAutocomplete()
	{
		//
		$arrRet = array();
		//
		$oAddress = oxNew( 'oxaddress' );
		//
		$arrResults = $oAddress->filterAdresses($_GET['search']);
		//
		foreach ( $arrResults as $oItem ) {
			$arrRet[] = array(
				//
				'oxid'		=>	$oItem->__oxid->rawValue,
				'company'	=>	$oItem->__oxcompany->rawValue ? $oItem->__oxcompany->rawValue : '',
				'sal'		=>	$oItem->__oxsal->rawValue ? $oItem->__oxsal->rawValue : '',
				'fname'		=>	$oItem->__oxfname->rawValue ? $oItem->__oxfname->rawValue : '',
				'lname'		=>	$oItem->__oxlname->rawValue ? $oItem->__oxlname->rawValue : '',
				'street'	=>	$oItem->__oxstreet->rawValue ? $oItem->__oxstreet->rawValue : '',
				'addinfo'	=>	$oItem->__addinfo->rawValue ? $oItem->__addinfo->rawValue : '',
				'streetnr'	=>	$oItem->__oxstreetnr->rawValue ? $oItem->__oxstreetnr->rawValue : '',
				'zip'		=>	$oItem->__oxzip->rawValue ? $oItem->__oxzip->rawValue : '',
				'city'		=>	$oItem->__oxcity->rawValue ? $oItem->__oxcity->rawValue : '',
				'countryid' =>	$oItem->__oxcountryid->rawValue ? $oItem->__oxcountryid->rawValue : '',
				'fon'		=>	$oItem->__oxfon->rawValue ? $oItem->__oxfon->rawValue : '',
				// ende
			);
		}
		//
		die(json_encode($arrRet));
		// ende
	}
}
