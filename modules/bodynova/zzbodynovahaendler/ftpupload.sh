#!/bin/bash

HOST=92.50.75.170  #This is the FTP servers host or IP address.
USER=haendlershop             #This is the FTP user that has access to the server.
PASS=peanuts30          #This is the password for the FTP user.

# Call 1. Uses the ftp command with the -inv switches.
#-i turns off interactive prompting.
#-n Restrains FTP from attempting the auto-login feature.
#-v enables verbose and progress.
cd lieferscheine

ftp -inv $HOST << EOF
user $USER $PASS
put $1
bye
EOF
