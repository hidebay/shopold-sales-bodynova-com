<?php
/**
 * Created by PhpStorm.
 * User: manuelgeil
 * Date: 06.10.14
 * Time: 14:09
 */

class bn_oxarticle extends bn_oxarticle_parent {

    protected $isFavorite = null;

	/**
	 * Return price suffix
	 *
	 * @return null
	 */
	protected function _getUserPriceSufix()
	{
		$sPriceSufix = '';
		$oUser = $this->getArticleUser();

		if ( $oUser ) {
			if ( $oUser->inGroup( 'oxidpricea' ) ) {
				$sPriceSufix = 'a';
			} elseif ( $oUser->inGroup( 'oxidpriceb' ) ) {
				$sPriceSufix = 'b';
			} elseif ( $oUser->inGroup( 'oxidpricec' ) ) {
				$sPriceSufix = 'c';
			} elseif ( $oUser->inGroup( 'oxidpriced' ) ) {
				$sPriceSufix = 'd';
			} elseif ( $oUser->inGroup( 'oxidpricee' ) ) {
				$sPriceSufix = 'e';
			}
		}

		# echo '$sPriceSufix: '.$sPriceSufix.'<br>';

		return $sPriceSufix;
	}

	/**
	 * Favorite Handling
	 */
	public function isFavorite()
	{
		if($this->isFavorite === null) {
			$strSQL = 'select 1 from oxfavorites WHERE OXUSER=\''.$this->getUser()->getId().'\' AND OXARTICLE=\''.$this->getId().'\'';
			$this->isFavorite = (bool) oxDb::getDb()->getOne( $strSQL );
		}
		return $this->isFavorite;
	}

	/**
	 *
	 */
	public function getUVPPrice()
	{
		return $this->oxarticles__oxprice->value;
	}

	/**
	 * holt die Lieferzeit vom Parentartikel
	 */
	public function getParentLieferzeit(){

		$arvariants  = $this->_getVariantsIds();
		$amindeltime = $this->getmindeltime();
		$amaxdeltime = $this->getaxdeltime();
        $mindeltime = $amindeltime[0];
        $maxdeltime = $amaxdeltime[0];

		/*
		echo '<pre>';
		print_r($amaxdeltime);
		echo '</pre>';
		*/

		// OXID = $this->oxarticles__oxid->value;

		if ($amaxdeltime[0] > 0){
			return "$mindeltime"." - "."$maxdeltime";
		}
		return false;

	}



	/**
	 * holt die Mindestlieferzeit
	 * @return array
	 */
	public function getmindeltime()
	{
		$aSelect = array();
		if ( ( $sId = $this->getId() ) ) {
			$oDb = oxDb::getDb( oxDb::FETCH_MODE_ASSOC );
			$sQ = "select oxmindeltime from " . $this->getViewName( true ) . " where oxparentid = ".$oDb->quote( $sId )." and " .
				$this->getSqlActiveSnippet( true ) . " order by oxmindeltime";
			$oRs = $oDb->select( $sQ );
			if ( $oRs != false && $oRs->recordCount() > 0 ) {
				while (!$oRs->EOF) {
					$aSelect[] = reset( $oRs->fields );
					$oRs->moveNext();
				}
			}
		}
		return $aSelect;
	}

	/**
	 * holt die Maximale Lieferzeit
	 * @return array
	 */
	public function getmaxdeltime()
	{
		$aSelect = array();
		if ( ( $sId = $this->getId() ) ) {
			$oDb = oxDb::getDb( oxDb::FETCH_MODE_ASSOC );
			$sQ = "select oxmaxdeltime from " . $this->getViewName( true ) . " where oxparentid = ".$oDb->quote( $sId )." and " .
				$this->getSqlActiveSnippet( true ) . " order by oxmaxdeltime DESC";
			$oRs = $oDb->select( $sQ );
			if ( $oRs != false && $oRs->recordCount() > 0 ) {
				while (!$oRs->EOF) {
					$aSelect[] = reset( $oRs->fields );
					$oRs->moveNext();
				}
			}
		}
		return $aSelect;
	}

    /**
     * gibt die maximale Lieferdauer als String zurück
     * @return mixed
     */
    public function getmaxdeltimeS()
    {
        if($this->getmaxdeltime()){
            $a = $this->getmaxdeltime();
            $erg = $a[0];
            return $erg;

            #print_r($this->getmaxdeltime());
        }
    }

    /**
     * gibt die mindest Lieferdauer als String zurück
     * @return mixed
     */
    public function getmindeltimeS()
    {
        if($this->getmindeltime()){
            $a = $this->getmindeltime();
            $erg = $a[0];
            return $erg;

            #print_r($this->getmindeltime());
        }
    }

    /**
     * gibt den Wert von bnFlagStatus zurück
     * @return mixed
     */
    public function getBnFlag()
    {
        // 0 = grün
        // 1 = gelb
        // 2 = rot
        $flag = $this->oxarticles__bnflagbestand->value;
        return $flag;
    }

    /**
     * Ermittelt die StockFlag Farbe und gibt die Css Klasse zurück
     * @return null|string
     */
    public function showCssFlag()
    {
        // Farbe ermitteln und CSS Klasse zurück geben:
        $flag = $this->getBnFlag();
        $result = null;
        switch($flag){
            case 2:
                $result = "notOnStock";
                break;
            case 1:
                $result = "lowStock";
                break;
            default:
                $result = "onStock";
                break;
        }
        return $result;
    }

    /**
     * Gibt als String zurück ob das Produkt Varianten hat
     * @return null|string
     */
    public function getProductStatus(){
        $ergebnis = null;
        if($this->getVariantsCount() > 0){
            $ergebnis = "hat Varianten";
        }else{
            $ergebnis = "hat keine Varianten";
        }
        return $ergebnis;
    }

    /**
     * Returns formatted delivery date. If the date is not set ('0000-00-00') returns false.
     *
     * @return string | bool
     */
    public function getDeliveryDate()
    {
        if ( $this->oxarticles__oxdelivery->value != '0000-00-00') {
            return oxRegistry::get("oxUtilsDate")->formatDBDate( $this->oxarticles__oxdelivery->value);
        }
        #return $this->oxarticles__oxdelivery->value;

        return false;
    }

}
