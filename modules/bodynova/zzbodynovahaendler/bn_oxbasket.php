<?php

/**
 * @param $a
 * @param $b
 * @return bool
 */
function customBasketSorting($a,$b) {
	return $a->getTitle() > $b->getTitle();
}

/**
 * Class bn_oxbasket
 */
class bn_oxbasket
extends bn_oxbasket_parent
{
	/**
	 * Returns basket items array
	 *
	 * @return array
	 */
	public function getContents()
	{
		//
		if(!count($_POST)) {
			// store keys to articles
			$arrOxidsToKeys = array();
			foreach($this->_aBasketContents AS $strKey=>$objBasketItem) {
				$arrOxidsToKeys[$objBasketItem->getProductId()] = $strKey;
			}

			// Sort the fuck
			usort($this->_aBasketContents, 'customBasketSorting');


			// merge keys again into articles
			$arrRetNew = array();
			foreach($this->_aBasketContents AS $objBasketItem) {
				//
				$arrRetNew[$objBasketItem->getProductId()] = $objBasketItem;
				// ende
			}
			$this->_aBasketContents = $arrRetNew;
			// ende
		}
		//
		return $this->_aBasketContents;
		// ende
	}
}