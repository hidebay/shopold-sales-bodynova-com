<?php
/**
 * Created by PhpStorm.
 * User: manuelgeil
 * Date: 06.10.14
 * Time: 14:09
 */

class bn_oxorder extends bn_oxorder_parent
{
	/**
	 * Whitelabel Bestellungen.
	 * @var null
	 */
	protected $_isWhitelabel = null;

	/**
	 * @param $oBasket
	 */
	public function validateDelivery( $oBasket )
	{
		return;
	}

	/**
	 * @param $oBasket
	 */
	public function validatePayment( $oBasket )
	{
		return;
	}

	/**
	 * Updates order transaction status. Faster than saving whole object
	 *
	 * @param string $sStatus order transaction status
	 *
	 * @return null
	 */
	protected function _setOrderStatus( $sStatus )
	{
		// execute Parent Actions
		parent::_setOrderStatus( $sStatus );
		//

		if($sStatus == 'OK') {
			$oDb = oxDb::getDb();
			$sQ = 'update oxorder set iswhitelabel=' . $oDb->quote(isset($_POST['iswhitelabel']) ? '1' : '0') . ' where oxid=' . $oDb->quote($this->getId());
			$oDb->execute($sQ);
		}
		// ende
	}
	public function setWhitelabel($wert)
	{
		$this->_isWhitelabel = $wert;
	}


	public function lieferschein()
	{
		if(isset($_FILES['lieferschein']['tmp_name'])){

			move_uploaded_file(
				$_FILES['lieferschein']['tmp_name'],
				//dirname(__FILE__).'/lieferscheine/'.$oOrder->getId().substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'))
				dirname(__FILE__).'/lieferscheine/'.$this->oxorder__oxordernr->value.substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'))
			);


			// FTP Upload auf fritz.nas
			$ftp_server= '92.50.75.170';
			$ftp_user= 'haendlershop';
			$ftp_pass= 'peanuts30';
			$file = dirname(__FILE__).'/lieferscheine/'.$this->oxorder__oxordernr->value.substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'));
			//$destination_file = 'salespdf/'.$this->oxorder__oxordernr->value.substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'));
			$destination_file = 'JetFlash-Transcend16GB-01/FRITZ/salespdf/'.$this->oxorder__oxordernr->value.substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'));
			$cid=ftp_connect($ftp_server);
			ftp_login($cid, $ftp_user, $ftp_pass);
			ftp_pasv($cid, true);
			ftp_put($cid, $destination_file, $file, FTP_BINARY);
			ftp_close($cid);

		}
	}
}
