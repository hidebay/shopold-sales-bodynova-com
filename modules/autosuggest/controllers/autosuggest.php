<?php
if (isset ($_GET['q']) ) :
    if(strlen($_GET['q'])>1) :
        //
        require_once dirname(__FILE__).'/../../../bootstrap.php';

        //
        header("Content-Type: text/html; charset=utf-8");

        // searching ..
        $oSearchHandler = oxNew( 'oxsearch' );
        $strSearchString = $oSearchHandler->getSelectStatement($_GET['q'], 'IF(a.oxparentid!=\'\', a.oxparentid, a.oxid) AS oxid');

        // filter results for uniqe oxids
        $arrOXIDs = array();
        foreach(oxDb::getDb( oxDb::FETCH_MODE_ASSOC )->getAll( $strSearchString) AS $arrRecord)
        {
            $arrOXIDs[] = $arrRecord['oxid'];
        }
        //
        $arrOXIDs = array_unique($arrOXIDs);

        //
        $strSearchString = 'SELECT '.$oSearchHandler->getAutosuggestSelectColumns().' FROM oxarticles AS a WHERE oxid IN (\''.implode('\',\'', $arrOXIDs).'\')';
        $aData = oxDb::getDb( oxDb::FETCH_MODE_ASSOC )->getAll( $strSearchString, ' LIMIT 0, 10');//' LIMIT 0, 10'

        //
        if(count($aData)):
            echo '<table>';
            foreach($aData AS $row) :
                $picname = trim(utf8_encode($row['OXPIC1']));
                $seourl = 'javascript:jumpToArticle(\''.$row['OXID'].'\',\''.$row['OXID'].'\')';
                ?>
                <tr class="resall">
                    <td class="image"><a class="picture" href="<?php echo $seourl; ?>">
                        <?php if($picname): ?>
                            <img src="https://www.bodynova.de/out/pictures/generated/product/1/87_87_75/<?php echo $picname; ?>" width="50" height="50">
                        <?php else : ?>
                            <img src="https://www.bodynova.de/out/imagehandler.php?artnum=<?php echo $row['oxartnum'];?>&size=87_87_75" width="50" height="50" class="img-thumbnail">
                        <?php endif; ?>
                    </td>
                    <td class="title"><?php echo '<a href="' .$seourl .'">' . $row['OXTITLE'] . '</a>'; ?></td>
                </tr>
                <?php
            endforeach;
            echo '</table>';
        else:
            //
            $arrNewWords = $oSearchHandler->getFaultCorrectedSearch($_GET['q']);
            //
            $strWords = '';
            foreach($arrNewWords AS $arrWord)
            {
                $strWords.=$arrWord['word'].' ';
            }
            $strLink = '<a href="index.php?lang='.oxRegistry::getLang()->getBaseLanguage().'&cl=search&searchparam='.urlencode(trim($strWords)).'">';

            //
            $strWords = '';
            foreach($arrNewWords AS $arrWord)
            {
                $strWords.= ($arrWord['changed'] ? '<b><i>' : '').$arrWord['word'].($arrWord['changed'] ? '</i></b>' : '').' ';
            }
            $strLink = $strLink.$strWords.'</a>';
            echo oxRegistry::getLang()->translateString('AUTOSUGGEST_DIDYOUMEAN', oxRegistry::getLang()->getBaseLanguage()).$strLink;
        endif;
    endif;
endif;
?>
