<?php
/**
 * Created by PhpStorm.
 * User: manuelgeil
 * Date: 06.10.14
 * Time: 14:09
 */

class bn_oxsearch extends bn_oxsearch_parent
{


    /**
     *
     *
     * @param bool $sSearchParamForQuery
     * @param bool $sInitialSearchCat
     * @param bool $sInitialSearchVendor
     * @param bool $sInitialSearchManufacturer
     * @param bool $sSortBy
     * @return oxarticlelist
     */
    public function getSearchArticles( $sSearchParamForQuery = false, $sInitialSearchCat = false, $sInitialSearchVendor = false, $sInitialSearchManufacturer = false, $sSortBy = false )
    {
        // load only articles which we show on screen
        //setting default values to avoid possible errors showing article list
        $iNrofCatArticles = $this->getConfig()->getConfigParam( 'iNrofCatArticles' );
        $iNrofCatArticles = $iNrofCatArticles?$iNrofCatArticles:10;
        //
        $oArtList = oxNew( 'oxarticlelist' );
        $oArtList->setSqlLimit( $iNrofCatArticles * $this->iActPage, $iNrofCatArticles );
        //
        $strSearchString = $this->getSelectStatement(
            $sSearchParamForQuery,
            'DISTINCT(IF(a.OXPARENTID!=\'\', a.OXPARENTID, a.oxid)), '.$this->getAutosuggestSelectColumns()
        );
        //
        $oArtList->selectString( $strSearchString);
        //
        return $oArtList;
        // ende
    }


    /**
     * Gibt die Summe der Gefundenen ergebenisse Zurück
     *
     * @param bool $sSearchParamForQuery
     * @param bool $sInitialSearchCat
     * @param bool $sInitialSearchVendor
     * @param bool $sInitialSearchManufacturer
     * @return integer
     */
    public function getSearchArticleCount( $sSearchParamForQuery = false, $sInitialSearchCat = false, $sInitialSearchVendor = false, $sInitialSearchManufacturer = false )
    {
        // mit der ergebenissliste nur die
        $strSearchString = $this->getSelectStatement($sSearchParamForQuery, 'DISTINCT(IF(a.OXPARENTID!=\'\', a.OXPARENTID, a.oxid)) AS oxid');
        // filter results for uniqe oxids
        $arrOXIDs = array();
        foreach(oxDb::getDb( oxDb::FETCH_MODE_ASSOC )->getAll( $strSearchString) AS $arrRecord)
        {
            $arrOXIDs[] = $arrRecord['oxid'];
        }
        //
        $arrOXIDs = array_unique($arrOXIDs);
        //
        $strSearchString = 'SELECT count(*) FROM oxarticles AS a WHERE oxid IN (\''.implode('\',\'', $arrOXIDs).'\')';
        $intCount = oxDb::getDb( oxDb::FETCH_MODE_ASSOC )->getOne( $strSearchString);
        return $intCount;
        // ende
    }


    /**
     * Statement um Datenätze aus der Datenbank abzufragen
     *
     * @param $sSearchParamForQuery
     * @param $strFields
     * @return string
     */
    public function getSelectStatement($sSearchParamForQuery, $strFields)
    {
        //
        $strLanguageAppendix = '';
        if($this->_iLanguage>0) {
            $strLanguageAppendix = '_'.$this->_iLanguage;
        }
        //
        $sSearchParamForQuery = trim(preg_replace('/[^ÄÖÜäöüA-Za-z0-9\s]/', '', $sSearchParamForQuery));
        //
        $getRecord_sql = 'SELECT ';
        $getRecord_sql.= $strFields;
        $getRecord_sql.= ' , (SELECT count(oxarticlessub.oxid) AS count FROM oxarticles AS oxarticlessub WHERE oxarticlessub.OXPARENTID=a.OXID AND oxarticlessub.OXACTIVE=1) AS isparent ';
        $getRecord_sql.= ' , MATCH (a.oxartnum, a.oxtitle'.$strLanguageAppendix.', a.OXSEARCHKEYS'.$strLanguageAppendix.') AGAINST (\''.oxDb::getInstance()->escapeString(trim('+'.str_replace(' ', '* +', $sSearchParamForQuery.'*'))).'\' IN BOOLEAN MODE) as relevance ';
        $getRecord_sql.= 'FROM oxarticles a , oxartextends b WHERE oxactive = 1 AND a.OXID=b.OXID';
        $getRecord_sql.= ' AND (';
        $getRecord_sql.= 'MATCH (a.oxartnum, a.oxtitle'.$strLanguageAppendix.', a.OXSEARCHKEYS'.$strLanguageAppendix.') AGAINST (\''.oxDb::getInstance()->escapeString(trim('+'.str_replace(' ', '* +', $sSearchParamForQuery.'*'))).'\' IN BOOLEAN MODE) ';
        $getRecord_sql.= ' OR ';
        $getRecord_sql.= ' a.bestellcode=\''.oxDb::getInstance()->escapeString(trim($sSearchParamForQuery)).'\' ';
        $getRecord_sql.= ' ) ';
        //
        // prüfen ob es eine variante ist und vater aktiv
        //
        $getRecord_sql.= ' AND (';
        $getRecord_sql.= '          (SELECT count(*) FROM oxarticles AS sub WHERE sub.OXPARENTID=a.oxid AND sub.oxactive=1)>0';
        $getRecord_sql.= '      OR ';
        $getRecord_sql.= '          (SELECT count(*) FROM oxarticles AS sub WHERE sub.OXPARENTID=a.oxid)=0';
        $getRecord_sql.= ' )';
        //
        $getRecord_sql.= 'ORDER BY relevance DESC, a.oxtitle'.$strLanguageAppendix.' ASC';
        //
        return $getRecord_sql;
        // ende
    }


    /**
     * @return string
     */
    public function getAutosuggestSelectColumns()
    {
        //
        $strColumns = ' a.OXPARENTID, a.OXID, ';
        $strColumns.= ' a.oxartnum, a.OXSEARCHKEYS, OXARTNUM, OXPRICE, OXPRICEA, OXPRICEB, OXPRICEC, OXPRICED, OXPRICEE, OXPIC1, bnflagbestand, ';
        //
        $strColumns.= 'IF(a.OXPARENTID!=\'\', (SELECT oxsub.OXTITLE'.($this->_iLanguage>0 ? '_'.$this->_iLanguage : '').' FROM oxarticles AS oxsub WHERE oxsub.oxid=a.OXPARENTID), \'\') AS parenttitle, ';
        //
        $strColumns.= 'IF(a.OXPARENTID!=\'\', a.OXVARSELECT'.($this->_iLanguage>0 ? '_'.$this->_iLanguage : '').', a.OXTITLE'.($this->_iLanguage>0 ? '_'.$this->_iLanguage : '').') AS OXTITLE';
        //
        return $strColumns;
    }


    /**
     *
     */
    public function getFaultCorrectedSearch($strSearchedString)
    {
        $objDb = oxDb::getDb( oxDb::FETCH_MODE_ASSOC );
        //
        $arrWords = explode(' ', $strSearchedString);
        //
        $arrNewWords = array();
        //
        foreach ($arrWords AS $strWord)
        {
            $strWordSoundexed = soundex($strWord) . '<br>';
            //
            $arrRes = $objDb->getAll('SELECT wort FROM oxsearchablewords WHERE soundexchar=' . $objDb->quote(substr($strWordSoundexed, 0, 1)) . ' AND soundexval=' . $objDb->quote(substr($strWordSoundexed, 1)) . ' AND lang=\'' . oxRegistry::getLang()->getBaseLanguage() . '\' ORDER BY aufkommen DESC LIMIT 1');
            //
            if (isset($arrRes[0]))
            {
                $arrNewWords[] = array('changed' => (strtolower($strWord) != $arrRes[0]['wort']), 'word' => $arrRes[0]['wort']);
            }
            else
            {
                $arrNewWords[] = array('changed' => false, 'word' => $strWord);
            }
        }
        return $arrNewWords;
    }
}
