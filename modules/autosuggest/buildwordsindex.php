<?php
//
require_once dirname(__FILE__).'/../../bootstrap.php';

// get db instance
$oDb = oxDb::getDb( oxDb::FETCH_MODE_ASSOC );

// clear tables
$oDb->Execute('TRUNCATE TABLE oxsearchablewords');

foreach(
    array(
        'oxarticles'    =>  'OXSEARCHKEYS, OXARTNUM, OXTITLE, OXTITLE_1, OXTITLE_2',
    )
    AS $strTable=>$strFields)
{
    //
    $aData = $oDb->getAll( 'SELECT '.$strFields.' FROM '.$strTable );
    //
    foreach($aData AS $row)
    {
        foreach($row AS $strCell=>$strValue)
        {
            $iLang = is_numeric(substr($strCell, -1)) ? substr($strCell, -1) : '0';
            //
            if($strValue)
            {
                $arrValues = explode(' ', trim(strtolower($strValue)));
                //
                foreach($arrValues AS $strWord)
                {
                    $strWord = str_replace(array('"','\'','-','.',',','(',')',':'), array(), $strWord);
                    //
                    if($strWord && !is_numeric($strWord) && strlen($strWord)>2)
                    {
                        $res = oxDb::getDb()->getRow( 'SELECT * FROM oxsearchablewords WHERE wort='.$oDb->quote($strWord).' AND lang=\''.$iLang.'\'');
                        //
                        if(count($res))
                        {
                            $oDb->Execute('UPDATE oxsearchablewords SET aufkommen=aufkommen+1 WHERE wort='.$oDb->quote($strWord).' AND lang=\''.$iLang.'\'');
                        }
                        else
                        {
                            $oDb->Execute('INSERT INTO oxsearchablewords SET wort='.$oDb->quote($strWord).', soundexchar='.$oDb->quote(substr(soundex($strWord), 0, 1)).', soundexval='.$oDb->quote(substr(soundex($strWord), 1)).', lang=\''.$iLang.'\', aufkommen=1');
                        }
                    }
                }
            }
        }
    }
}