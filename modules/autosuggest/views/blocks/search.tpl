[{if $oView->showSearch()}]
    [{capture assign=searchScript}]
        function doSearch() {
        var q = jQuery.trim($('#searchparam').val());

        //
        //$('#header .searchBox').addClass('resultspresent');

        $('.navbar-form .searchBox').addClass('resultspresent');
        //

        if (q.length >= startSuggest) {

        // neu: $("#results").hide();

        $.ajax({
        url: '/modules/autosuggest/controllers/autosuggest.php?q=' + q,
        cache: false,
        dataType: "html",
        type: "get",
        success: function(data) {
        if(data) {
        $("#results").html(" ");
        $("#results").html(data);
        $("#results").show();
        }
        }
        });

        } else {
        $('#results').hide();
        }
        }

        var objQuickSearchTimoutHandler = null;
        function autoSuggest() {
        //
        if(objQuickSearchTimoutHandler)
        window.clearTimeout(objQuickSearchTimoutHandler);
        // load with a delay to avaoid duolciate querys on keyup
        objQuickSearchTimoutHandler = window.setTimeout(doSearch, 300);
        // end
        }
    [{/capture}]
    [{oxscript include=$oViewConf->getModuleUrl('autosuggest','src/initall.js') priority=10 }]
    [{oxscript add=$searchScript}]

    [{* oxstyle include=$oViewConf->getModuleUrl('autosuggest','src/autosuggest.css') *}]
    [{oxscript include="js/widgets/oxinnerlabel.js" priority=10 }]
    [{oxscript add="$( '#searchParam' ).oxInnerLabel();"}]



    <!-- Suche -->
    <li class="suche" role="presentation">
        <form class="navbar-form" role="search" action="[{$oViewConf->getSelfActionLink()}]" method="get" name="search" id="search">
            <div class="form-group form-group-suche" id="quicksearch">
                <div id="results"></div>
                [{$oViewConf->getHiddenSid()}]
                <input type="hidden" name="cl" value="search">
                [{block name="header_search_field"}]
                    <input type="text" class="form-control breite-100pc" id="searchparam" name="searchparam"
                           placeholder="[{oxmultilang ident="SEARCH"}]" autocomplete="off"
                           onkeyup="javascript:autoSuggest()" value="">
                [{/block}]


                <button type="submit" class="btn btn-default searchSubmit" onclick="$('#search').submit();">
                    <span class="glyphicon glyphicon-search"></span>
                </button>
            </div>
        </form>
    </li>
[{/if}]
[{*$oViewConf|var_dump*}]
