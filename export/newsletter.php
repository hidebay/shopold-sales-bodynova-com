<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Bodynova Newsletter</title>
</head>
<body style="font-family: Arial,Helvetica,sans-serif; font-size: 12px" vlink="#389CB4" bgcolor="#ffffff" alink="#18778E"
      link="#355222">

<!--deutsch--><!--Sprachwahl--><a name="deutsch" id="deutsch"></a>

<div style="width: 705px">
    <div style="padding: 0"><a href="http://sales.bodynova.com"><img
                src="http://sales.bodynova.com/out/bb2b/img/logo-small.png"
                alt="Bodynova Händler Portal" hspace="0" align="texttop" border="0" vspace="0"/></a></div>
    <div style="text-align: right; color: #532a1a; font-size: 14pt; padding-top: 15px; padding-right: 10px">
        Bodynova Händler Portal
    </div>
    <div style="text-align: right; font-size: 11pt; padding-right: 10px; padding-top: 5px"><span
            style="text-decoration: underline; font-size: 10px"><a href="#englisch">
                english</a></span>

        / <span style="text-decoration: underline; font-size: 10px"><a href="#french">
                français</a></span>
    </div>
    <!-- Anrede-->
    <div style="padding-left: 53px; padding-right: 53px">

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
            Liebe Wiederverkäufer ,</p><!-- Begrüßungstext-->

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
            nach längerer Entwicklungszeit ist unser neues Bestellportal für unsere Großhandelskunden endlich fertiggestellt. Die neue Webseite wurde entwickelt, um Ihnen die Übermittlung von Bestellungen zu vereinfachen.
            Darüber hinaus können wir viele zusätzliche Informationen zur Verfügung stellen. Gleichzeitig wird uns die Plattform helfen, schneller zu versenden, Rückfragen zu verringern und Fehler zu vermeiden.
        </p>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold">
            Ihre Vorteile:
        </p>

        <ul>
            <li>
                Die aktuelle Verfügbarkeit der Artikel ist sichtbar, d.h. Sie können mit hoher Wahrscheinlichkeit davon ausgehen, dass die gewünschte Ware auch kurzfristig geliefert werden kann.
            </li>
            <li>
                Sie können die Preise und Gewichte der gewünschten Artikel einsehen und zum Beispiel auch das Gesamtgewicht der Sendung optimieren.
            </li>
            <li>
                Der Versand Ihrer Bestellungen kann deutlich schneller erfolgen.
            </li>
            <li>
                Direktversand an Ihre Kunden, auf Wunsch neutral mit Ihrem Lieferschein: Gerne versenden wir direkt an Ihre Kunden.
            </li>
            <li>
                Geben Sie einfach die gewünschte Lieferanschrift ein und laden Sie dazu Ihren Lieferschein hoch. Der Versand geschieht dann neutral (ohne unseren Absender) und nur mit Ihrem Lieferschein.
            </li>
        </ul>


        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
            Wir möchten Sie über die Händlerplattform bestmöglich auf dem Laufenden halten und werden viele weitere Informationen bereitstellen, zum Beispiel über Neuheiten, Produktänderungen, Sonderangebote, Auslaufartikel.
        </p>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;">
            So geht’s:
        </p>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
            Melden Sie sich einfach unter <a href="http://www.sales.bodynova.com/index.php?cl=forgotpwd">http://www.sales.bodynova.com/index.php?cl=forgotpwd</a> mit der bei uns hinterlegten Email-Adresse:
        </p>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 16px; font-weight: bold; color:#0000ff; ">
            [{ $myuser->oxuser__oxusername->value }]
        </p>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
            an. Sie werden dann zur Vergabe eines neues Passwortes aufgefordert und können ab dann bestellen.
        </p>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
            Unser Angebot für Sie:<br>
            <strong>Bis zum 15.06.2015 versenden wir Ihre Bestellungen innerhalb Deutschlands versandkostenfrei *,<br>
            für alle Sendungen ins Ausland gewähren wir 25% Rabatt auf die Versandkosten *</strong><br>
            * ab einem Bestellwert von € 150,-
        </p>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
            Wir freuen uns über jedes Feedback!<br>
            Mit freundlichen Grüßen<br>
            Ihr Bodynova Team<br><br>
            Bodynova<br><br>
            <a href="http://sales.bodynova.com"><img
                    src="http://sales.bodynova.com/out/bb2b/img/logo-small.png"
                    alt="Bodynova Händler Portal" hspace="0" align="texttop" border="0" vspace="0"/></a><br>
            Bodynova GmbH<br>
            Scheidtweilerstr. 19<br>
            50933 Köln<br><br>
            Tel: +49 (0)221 35 66 35 – 0<br>
            Fax: +49 (0)221 35 66 35 – 20<br>
            email: service@bodynova.de<br><br>
            <a href="http://www.bodynova.de">www.bodynova.de</a><br>
            <a href="http://www.bodhi-meditation.com">www.bodhi-meditation.com</a><br><br><br>
            Geschäftsführer: Holger Ebenau<br>
            Handelsregister Köln<br>
            HRB Nr. 30131<br>
            Gerichtsstand Köln<br><br>
            Steuernummer: 223/5802/4800<br>
            Umsatzsteuer-ID-Nr.: DE194150608<br><br>
            Es gelten unsere allgemeinen Geschäftsbedingungen.<br>
        </p>
    </div>
</div>
<br/>
<br/><br/>
<br/><br/>
<br/><br/>
<br/><br/>
<br/>



<!--englisch--><a name="englisch" id="englisch"></a>

<div style="width: 705px">
    <div style="padding: 0"><a href="http://sales.bodynova.com?lang=1"><img
                src="http://sales.bodynova.com/out/bb2b/img/logo-small.png"
                alt="Bodynova Händler Portal" hspace="0" align="texttop" border="0" vspace="0"/></a></div>
    <div style="text-align: right; color: #532a1a; font-size: 14pt; padding-top: 15px; padding-right: 10px">
        Bodynova Wholesale Shop
    </div>
    <div style="text-align: right; font-size: 11pt; padding-right: 10px; padding-top: 5px"><span
            style="text-decoration: underline; font-size: 10px"><a href="#deutsch">deutsch</a></span> / <span
            style="text-decoration: underline; font-size: 10px"><a href="#french">français</a></span></div>
    <!-- Content -->
    <div style="padding-left: 53px; padding-right: 53px">

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
            Welcome!
        </p>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
            The new platform for our wholesale customers is finally ready. We wanted to make the ordering process easier and more efficient for you and with this in mind we have created a completely new site and layout for our resellers. With the new portal we will be able to process your orders more quickly, making it possible to ship earlier and at the same time eliminating errors.
        </p>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold">
            Advantages for you:
        </p>

        <ul>
            <li>
                You can see immediately if products are in stock or when they will be available again. When products are in stock we can ship immediately.
            </li>
            <li>
                All prices and the weight of each product are shown as well as the totals. This will help you to optimize your order accordingly.
            </li>
            <li>
                We can get your orders shipped more quickly and with fewer errors.
            </li>
            <li>
                Drop shipping directly to your customer is even easier now. After logging in, you simply enter the delivery address and on the same page you can enter the articles and amounts to be shipped. If you like you can upload your own packing list with your letterhead and we will add this to the shipment. Your order is then shipped neutrally, the customer doesn’t see who the sender is.
            </li>
        </ul>


        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
            We plan to use the platform to inform you on a regular basis of new developments, products or product changes, sales, discontinued products, etc.
        </p>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;">
            This is how it works:
        </p>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
            Please log in at <a href="http://www.sales.bodynova.com/index.php?cl=forgotpwd&lang=1">http://www.sales.bodynova.com/index.php?cl=forgotpwd</a> with your email address:
        </p>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 16px; font-weight: bold; color:#0000ff; ">
            [{ $myuser->oxuser__oxusername->value }]
        </p>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
            You will be asked to change the password. Then you can place your orders.
        </p>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
            Special introductory offer until 15 June, 2015:<br>
            <strong>Free shipping within in Germany, for orders outside of Germany, 25% off shipping charges.*</strong><br>
                *minimum order amount of 150€
        </p>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
            We would be very happy to receive any feedback that will help us to improve the site. We look forward to hearing from you!
            <br>
            Best regards<br>
            The Bodynova Team<br><br>
            Bodynova<br><br>
            <a href="http://sales.bodynova.com?lang=2"><img
                    src="http://sales.bodynova.com/out/bb2b/img/logo-small.png"
                    alt="Bodynova Händler Portal" hspace="0" align="texttop" border="0" vspace="0"/></a><br>
            Bodynova GmbH<br>
            Scheidtweilerstr. 19<br>
            50933 Köln(Cologne)<br>
            Germany<br><br>
            Tel: +49 (0)221 35 66 35 – 0<br>
            Fax: +49 (0)221 35 66 35 – 20<br>
            email: service@bodynova.de<br><br>
            <a href="http://www.bodynova.de">www.bodynova.de</a><br>
            <a href="http://www.bodhi-meditation.com">www.bodhi-meditation.com</a><br><br><br>
            General Manager: Holger Ebenau<br>
            Registration: Trade Register Cologne<br>
            HRB Nr. 30131<br>
            Place of Jurisdiction: Cologne<br><br>
            Tax No: 223/5802/4800<br>
            VAT No.: DE194150608<br><br>
            Es gelten unsere allgemeinen Geschäftsbedingungen.<br>
        </p>

    </div>
</div>
<br/>
<br/><br/>
<br/><br/>
<br/><br/>
<br/><br/>
<br/><br/>
<br/><br/>





<!--französisch--><a name="french" id="french"></a>

<div style="width: 705px">
    <div style="padding: 0"><a href="http://sales.bodynova.com?lang=2"><img
                src="http://sales.bodynova.com/out/bb2b/img/logo-small.png"
                alt="Bodynova Händler Portal" hspace="0" align="texttop" border="0" vspace="0"/></a></div>
    <div style="text-align: right; color: #532a1a; font-size: 14pt; padding-top: 15px; padding-right: 10px">
        Notre nouveau shop pro
    </div>
    <div style="text-align: right; font-size: 11pt; padding-right: 10px; padding-top: 5px"><span
            style="text-decoration: underline; font-size: 10px"><a href="#deutsch">
                deutsch</a></span>
        / <span style="text-decoration: underline; font-size: 10px"><a href="#englisch">
                englisch</a></span></div>
    <!-- Content -->
    <div style="padding-left: 53px; padding-right: 53px">
        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
            Chers revendeurs,
        </p>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
            Après une longue période de développement, notre nouveau site dédié exclusivement aux revendeurs est enfin prêt. Ces nouvelles pages web ont été conçues pour faciliter la transmission de vos commandes.
            Nous pourrons ainsi vous mettre de nombreuses autres informations à disposition. Vos commandes seront traitées et la marchandise envoyée plus rapidement, tout en réduisant les questions et évitant les erreurs.
        </p>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold">
            Vos avantages :
        </p>

        <ul>
            <li>
                La disponibilité des articles est visible directement et en général vous pouvez partir du principe que la marchandise souhaitée peut être livrée rapidement.
            </li>
            <li>
                Vous pouvez voir les prix et poids des articles souhaités et optimiser ainsi le poids total de votre envoi.
            </li>
            <li>
                L’expédition de vos commandes peut être effectuée plus rapidement.
            </li>
            <li>
                Nous vous offrons la possibilité de laisser livrer vos commandes avec un bon de livraison neutre directement à vos clients. Vous avez la possibilité de préciser l’adresse de livraison du client ou de nous mettre à disposition un propre bon de livraison par téléchargement de fichier.
            </li>
            <li>
                Nous souhaitons utiliser ce site pro pour vous informer au mieux et nous allons mettre de nombreuses autres informations à votre disposition, p.ex. concernant les nouveautés, modifications de produits, offres spéciales et fins de série.
            </li>
        </ul>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;">
            Comment ça fonctionne :
        </p>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
            Merci de vous inscrire sous le lien <a href="http://www.sales.bodynova.com/index.php?cl=forgotpwd&lang=2">http://www.sales.bodynova.com/index.php?cl=forgotpwd</a> avec votre adresse email
        </p>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 16px; font-weight: bold; color:#0000ff; ">
            [{ $myuser->oxuser__oxusername->value }]
        </p>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
            Vous serez invités à choisir un nouveau mot de passe et pourrez alors passer commande.
        </p>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
            Offre de lancement :<br>
            <strong>Pour toute commande passée avant le 15.06.2015, vous bénéficiez en Allemagne d’un envoi sans frais de port*; pour toute commande vers l’étranger, nous vous accordons 25% sur les frais de transport*.</strong><br>
            * à partir de 150,00 € d'achat
        </p>

        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
            Nous nous réjouissons de vos commentaires et feedback constructifs !
            <br>
            Cordialement,<br>
            Votre équipe Bodynova<br><br>
            Bodynova<br><br>
            <a href="http://sales.bodynova.com?lang=2"><img
                    src="http://sales.bodynova.com/out/bb2b/img/logo-small.png"
                    alt="Bodynova Händler Portal" hspace="0" align="texttop" border="0" vspace="0"/></a><br>
            Bodynova GmbH<br>
            Scheidtweilerstr. 19<br>
            50933 Köln(Cologne)<br>
            Germany<br><br>
            Tel: +49 (0)221 35 66 35 – 0<br>
            Fax: +49 (0)221 35 66 35 – 20<br>
            email: service@bodynova.de<br><br>
            <a href="http://www.bodynova.de">www.bodynova.de</a><br>
            <a href="http://www.bodhi-meditation.com">www.bodhi-meditation.com</a><br><br><br>
            General Manager: Holger Ebenau<br>
            Registration: Trade Register Cologne<br>
            HRB Nr. 30131<br>
            Place of Jurisdiction: Cologne<br><br>
            Tax No: 223/5802/4800<br>
            VAT No.: DE194150608<br><br>
            Es gelten unsere allgemeinen Geschäftsbedingungen.<br>
        </p>
    </div>
</div>
</body>
</html>
